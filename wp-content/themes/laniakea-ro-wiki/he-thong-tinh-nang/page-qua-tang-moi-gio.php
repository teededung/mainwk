<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
	<div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

		<h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="mb-4">
            <h3 class="color-600">Giới thiệu</h3>

            <p class="lead">
                Hệ thống quà tặng mỗi giờ đã được tích hợp vào Laniakea RO vào bản cập nhật tháng 5/2018,
                nhằm mang đến nhiều phần quà hơn cho các bạn đã tích cực online chơi game. <br>

                Hệ thống sẽ tặng bạn các vật phẩm có ích cữ mỗi 1 giờ.
                Ngoài ra nếu bạn online được 3 tiếng bạn sẽ nhận được phần thưởng đặc biệt khác.
            </p>
        </div>

        <div class="mb-4">
            <h3 class="color-600">Mùa hè đã đến</h3>

            <p class="lead">
                Đến với Ragnarok Online, điều thú vị là bạn sẽ cảm nhận được 4 mùa thông qua các trang phục, kiểu trang trí
                của thành phố cùng với các hiệu ứng của mùa. Laniakea RO sẽ cố gắng cập nhật các vật phẩm thời trang,
                các trang trí thành phố và các sự kiện nổi bật để giúp các bạn đạt được trải nghiệm Ragnarok Online tốt nhất.<br>
                Hiện tại ở Laniakea RO đang diễn ra sự kiện mùa hè, cứ mỗi tiếng Online bạn sẽ nhận được <strong>x25 Summer Coin</strong>.
            </p>
        </div>

        <div class="mb-4">
            <h3 class="mb-3 color-600">Làm sao để biết khi nào nhận thưởng?</h3>

            <div class="row">
                <div class="col-lg-9 order-md-2">
                    <p class="lead">
                        Mỗi lần đăng nhập vào game, bạn sẽ thấy 1 icon nhỏ phía bên tay phải màn hình của các bạn. Icon này giúp bạn
                        nhận biết được thời gian còn lại để nhận thưởng.<br>
                    </p>

                    <p class="lead">
                        Nếu bạn duy trì Online được 3 tiếng, đầu nhân vật của bạn sẽ có một chiếc mũ AFK lạ lẫm :3. Lúc này bạn sẽ không nhận được quà tặng mỗi giờ nữa.
                        Để kích hoạt nhận quà mỗi giờ lại, bạn phải relog hay còn gọi là đăng nhập lại. Sau khi đăng nhập lại bạn sẽ nhận được quà tặng đặc biệt ngẫu nhiên trong 10 món.
                        <br>
                        Phần thưởng thì để hấp dẫn, mình không tiết lộ sớm được :3.
                    </p>

                    <div class="alert alert-danger">
                        <strong>Chú ý:</strong> Phần thưởng online 3 tiếng sẽ tính lại từ đầu nếu bạn relog khi chưa đủ 3 tiếng.
                    </div>
                </div>
                <div class="col-lg-3 order-md-1">
                    <img class="img-fluid img-thumbnail mb-3 mb-lg-0" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/qua-tang-moi-gio-1.jpg'; ?>" alt="qua-tang-moi-gio-ragnarok">
                </div>
            </div>
        </div>

        <div class="mb-4">
            <h3 class="mb-3 color-600">Xem thời gian đã Online được bao lâu?</h3>

            <p class="lear">
                Bạn có thể kiểm tra thời gian Online của bạn bằng cách dùng lệnh <code class="clipboard" data-clipboard-text="@onlinetime" >@onlinetime</code>.<br>
                Một đoạn thông báo thời gian Online sẽ xuất hiện trên đầu nhân vật của bạn.
            </p>

            <img class="img-fluid img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/qua-tang-moi-gio-2.jpg'; ?>" alt="qua-tang-moi-gio-ragnarok-2">
        </div>

        <div class="mb-4">
            <h3 class="mb-3 color-600">Treo máy (AFK) có được nhận quà mỗi giờ không?</h3>

            <h5>Các trường hợp sau bạn sẽ không nhận được quà mỗi giờ:</h5>

            <ul>
                <li class="lead">
                    Nhân vật của bạn dưới cấp độ 120.
                </li>
                <li class="lead">
                    Treo bán hàng, treo mua hàng.
                </li>
            </ul>
        </div>
	</div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

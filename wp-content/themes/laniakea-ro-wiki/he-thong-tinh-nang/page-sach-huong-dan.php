<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content-sm">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="bg-light p-3 border rounded mb-5">
            <h3 class="color-600">Giới thiệu</h3>

            <p class="lead">
                Một quyển sách dày và cũ kĩ, được tất cả các nhà thám hiểm sử dụng như một Bách khoa toàn thư.
                Sách hướng dẫn là một vật phẩm riêng biệt của Laniakea RO, sách giúp bạn đỡ đau đầu hơn với những điều phức tạp ở thế giới Ragnarok.
            </p>

            <p class="lead">
                Khi vừa tạo nhân vật xong, trong hành trang của bạn sẽ có sẵn Sách hướng dẫn. Hãy nhấp đôi vào nó để đọc nhé.
            </p>

            <img class="img-thumbnail mb-5" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/sach-huong-dan/sach-huong-dan-ragnarok.jpg' ?>" alt="sach-huong-dan-ragnarok">

            <h3 class="color-600">Chức năng</h3>

            <ul class="list-group mb-3">
                <li class="list-group-item">Nhận quà tân thủ, nhập mã quà tặng từ GM.</li>
                <li class="list-group-item">Hướng dẫn bạn nơi luyện cấp.</li>
                <li class="list-group-item">Giúp bạn dịch chuyển tới Sự phụ để chuyển nghề.</li>
                <li class="list-group-item">Giúp bạn dịch chuyển đến NPC thuê Falcon, Peco Peco, Dragon, Gryphon.</li>
                <li class="list-group-item">Giúp bạn dịch chuyển đến NPC thay đổi tóc và màu quần áo.</li>
                <li class="list-group-item">Giúp bạn dịch chuyển đến NPC thay đổi trang phục nghề 3.</li>
            </ul>

            <div class="alert alert-info">
                Các chức năng dịch chuyển trong Sách hướng dẫn sẽ yêu cầu bạn phải có 1 <?php echo do_shortcode('[item item_id="602"]Butterfly Wing[/item]') ?> hoặc 1 <?php echo do_shortcode('[item item_id="12324"]Novice Butterfly Wing[/item]') ?>.
            </div>

            <div class="alert alert-info">
                <strong>Mẹo:</strong> Nếu bạn mới tham gia Laniakea RO, hãy nhận quà tân thủ từ Sách hướng dẫn. Bạn sẽ nhận một số vật phẩm khá tốt để bắt đầu cuộc hành trình.
            </div>
        </div>
    </div>
</div>

<?php endwhile ?>
<?php get_footer(); ?>
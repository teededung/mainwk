<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
	<div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-5">Quản lý tự động nhặt đồ</h1>

        <div class="bg-light p-3 border rounded mb-5">
            <div class="mb-4">
                <h3 class="color-600">Giới thiệu</h3>
                <p class="mb-3">
                    Quản lý tự động nhặt đồ là một tính năng riêng biệt của Laniakea RO.
                    Tính năng này giúp bạn lưu lại danh sách tự động nhặt đồ và bạn có thể dùng lại ở các lần sau mà không cần
                    phải gõ lại các lệnh <code>@autoloot</code>, <code>@alootid</code> hay <code>@autoloottype</code>.
                </p>

                <h3 class="color-600">Tính năng</h3>
                <ul>
                    <li>Tạo được nhiều danh sách (Hiện tại là 4).</li>
                    <li>Bạn có thể đặt tên cho dánh sách tùy theo mục đích như: Rau củ quả, Hazy Forest, Farm Zeny,...</li>
                    <li>Lưu lại danh sách theo tài khoản. Các nhân vật trong cùng tài khoản đó đều sử dụng chung được các danh sách.</li>
                    <li>Chuyển đổi qua lại các danh sách bằng lệnh <code>@nhatdo số_thứ_tự</code>. Ví dụ <code>@nhatdo 1</code> hoặc <code>@nhatdo 2</code>.</li>
                    <li>Hủy tự động nhặt đồ nhanh chóng bằng lệnh <code>@nhatdo 0</code>.</li>
                    <li>Bạn có thể chia sẻ danh sách nhặt đồ cho người chơi khác.</li>
                    <li>Có tính năng áp dụng tự động nhặt đồ khi đăng nhập.</li>
                </ul>
            </div>

            <div class="alert alert-info">
                Chỉ cần bỏ ra 1 chút thời gian cài đặt, bạn sẽ cảm thấy dễ chịu hơn ở các lần sử dụng tới.
            </div>

            <div class="mb-4">
                <h3 class="color-600">Hướng dẫn sử dụng</h3>

                <div class="mb-3">
                    <p class="mb-3">
                        <strong>Bước 1:</strong><br>
                        Chỉ cần sử dụng lệnh <code class="clipboard" data-clipboard-text="@nhatdo">@nhatdo</code> và bảng hội thoại sẽ xuất hiện lên như sau:
                    </p>

                    <div class="row ">
                        <div class="col-md-4">
                            <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-1.jpg'; ?>" alt="quan-ly-nhat-do-giao-dien">
                        </div>
                        <div class="col-md-8">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    * Danh sách -> Liệt kê các danh sách tự động nhặt của bạn.
                                </li>
                                <li class="list-group-item">
                                    * Hủy tự động nhặt đồ -> Reset <code>@autoloot</code>, <code>@alootid</code> và <code>@autoloottype</code>.
                                </li>
                                <li class="list-group-item">
                                    * Hủy tự động nhặt đồ khi đăng nhập
                                </li>
                                <li class="list-group-item">
                                    * Đóng -> Đóng cửa sổ quản lý tự động nhặt đồ.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 2:</strong><br>
                        Chúng ta sẽ tiến hành tạo 1 danh sách mới.<br>
                        Ban đầu 4 danh sách đều trống. Bạn chọn 1 danh sách bất kì và đặt tên cho danh sách đó.<br>
                    </p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-2.jpg'; ?>" alt="them-danh-sach-tu-dong-nhat-do">

                    <p>Ở đây mình sẽ đặt tên danh sách là "Hay nhặt" làm ví dụ.</p>

                    <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-3.jpg'; ?>" alt="dat-ten-danh-dach-tu-dong-nhat-do">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 3:</strong><br>
                        Chúng ta đã tạo xong danh sách <code>Hay nhặt</code>. <br>
                        Tiếp theo chúng ta sẽ thêm vật phẩm vào danh sách này.
                    </p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-4.jpg'; ?>" alt="them-vat-pham-vao-danh-sach-1">

                    <p>Mình sẽ thêm vào "Blue Potion".</p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-5.jpg'; ?>" alt="them-vat-pham-vao-danh-sach-2">

                    <p>Sau khi thêm xong bạn sẽ được như hình dưới.</p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-6.jpg'; ?>" alt="them-vat-pham-vao-danh-sach-3">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 4:</strong><br>
                        Mình thêm vào vài vật phẩm khác nữa... Kết quả là...
                    </p>

                    <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-7.jpg'; ?>" alt="them-vat-pham-vao-danh-sach-4">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 5:</strong><br>
                        Ngoài ra mình còn muốn nhặt thêm các vật phẩm rơi từ quái vật có tỉ lệ từ 1% trở xuống.<br>
                        Để làm được điều này bạn chọn menu <code>Thêm @autoloot &#60;tỉ lệ %&#62;</code>
                    </p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-8.jpg'; ?>" alt="them-ti-le-phan-tram-autoloot">

                    <p>Điền vào số 1 như hình</p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-9.jpg'; ?>" alt="them-ti-le-phan-tram">

                    <p>Sau khi bạn điền xong, ở khung hộp thoại bạn lăn xuống để kiểm tra...</p>

                    <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-10.jpg'; ?>" alt="kiem-tra-autoloot">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 6:</strong><br>
                        Xong rồi, giờ mình sẽ tự động nhặt các vật phẩm trong danh sách này bằng cách chọn <code>Tự động nhặt các vật phẩm trên</code>.
                    </p>
                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-11.jpg'; ?>" alt="tu-dong-nhat-cac-vat-pham-tren">

                    <p>Bạn sẽ thấy trên đầu nhân vật hiện đoạn thông báo như thế này.</p>

                    <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-12.jpg'; ?>" alt="ap-dung-tu-dong-nhat-do">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        <strong>Bước 7:</strong><br>
                        Ngoài ra mình cũng muốn khi đăng nhập vào game thì hệ thống tự động áp dụng nhặt đồ ở danh sách <code>Hay nhặt</code>.<br>

                        Để làm được điều này thì mình sẽ vào lại danh sách và chọn <code>Sử dụng danh sách này khi đăng nhập</code>.
                    </p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-13.jpg'; ?>" alt="tu-dong-ap-dung-khi-dang-nhap-1">

                    <p>Từ lúc này trở đi, khi bạn đăng nhập vào game bạn sẽ thấy trên đầu nhân vật có đoạn thông báo sau.</p>

                    <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/autoloot/quan-ly-nhat-do-12.jpg'; ?>" alt="tu-dong-ap-dung-khi-dang-nhap-2">
                </div>

                <hr>

                <div class="mb-3">
                    <p>
                        Còn nhiều tính năng khác của hệ thống quản lý nhặt đồ này đang chờ các bạn khám phá. <br>
                        Nếu bạn thấy hệ thống này có ích, hãy review cho mình nhé :D
                    </p>
                </div>
            </div>
        </div>
	</div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

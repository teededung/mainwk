<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 border rounded mb-5">
                <div class="mb-5">
                    <h3 class="color-600">Giới thiệu</h3>

                    <p>
                        1 ngày đẹp trời nhân vật của bạn vừa lên cấp và bạn tự hỏi rằng... Hmm... lỡ mà có tăng sai thì sao ta...<br>
                        Bạn đành hỏi 1 bạn trong game và bạn đó chỉ rằng "Reset đi!"<br>
                        Bạn cũng không biết "Reset đi!" là cái quái gì...<br>
                        Bạn cũng định hỏi Reset ở đâu thì người ta đi mất tiêu luôn!
                    </p>

                    <p>Okay, bài viết này sẽ giúp bạn giải tỏa nỗi đau đó...</p>

                    <p>
                        Với phương châm là không giới hạn, thỏa sức sáng tạo xây dựng nhân vật.<br>
                        GM đã cử NPC <strong>Bé Tiên</strong> để phục vụ các bạn.
                    </p>

                    <p>
                        Các thuật ngữ sau có thể bạn sẽ bắt gặp trong game:
                    </p>

                    <ul>
                        <li><strong>"Đường build", "kiểu build":</strong> tức là cách xây dựng nhân vật, bao gồm chỉ số nhân vật (Stat) và điểm kỹ năng (Skill) và trang bị của bạn.</li>
                        <li><strong>"Reset đi!":</strong> tức là tẩy điểm đi.</li>
                        <li><strong>"Stat":</strong> tức là chỉ số nhân vật như STR, AGI, VIT, INT, DEX, LUK.</li>
                    </ul>
                </div>

                <div class="mb-5">
                    <h3 class="color-600">Tẩy điểm ở đâu?</h3>

                    <p><strong>Bé Tiên</strong> ở trung tâm thành phố Prontera <code class="clipboard" data-clipboard-text="/navi prontera 148/193">prontera 148/193</code>.</p>

                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/be-tien-reset-npc.jpg' ?>" alt="be-tien-reset-npc">
                </div>

                <div class="mb-5">
                    <h3 class="color-600">Chi phí tẩy điểm</h3>

                    <p>
                        Đối với nhân vật dưới cấp độ 150, <strong>Bé Tiên</strong> sẽ tẩy điểm miễn phí cho bạn.
                    </p>

                    <p>
                        Từ cấp độ trên 150. Mỗi lần tẩy điểm <strong>Bé Tiên</strong> sẽ lấy của bạn <code>100.000</code> Zeny.
                    </p>
                </div>

                <div class="mb-3">
                    <h3 class="color-600">Không chỉ tẩy điểm mà còn có lưu lại kiểu build.</h3>

                    <p>
                        Để thoải mái nghiên cứu kiểu build, <strong>Bé Tiên</strong> còn ra dịch vụ lưu lại điểm chỉ số của nhân vật.<br>
                        Mặc định <strong>Bé Tiên</strong> cho bạn 2 ô lưu kiểu build stat.<br>
                        Bạn có thể mua thêm kiểu build chỉ với 1 <?php echo do_shortcode('[item id="6320"]Premium Reset Stone[/item]') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php endwhile ?>
<?php get_footer(); ?>

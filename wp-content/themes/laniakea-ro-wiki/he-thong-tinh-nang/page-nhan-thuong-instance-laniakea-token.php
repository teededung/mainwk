<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 border rounded">

                <h3 class="color-600">Giới thiệu</h3>

                <div class="mb-3">
                    <p class="lead">
                        Chắc hẳn bạn đã từng nhận được những đồng <?php echo do_shortcode('[item id="52009"]Laniakea Token[/item]'); ?> và tự hỏi cái đồng xu này để làm gì.<br>
                        Nó là một tiền tệ khá phổ biến ở <strong>Laniakea RO</strong>, tại sao nó lại phổ biến nhỉ? Chúng ta cùng nhau tìm hiểu về nguồn gốc của nó nhé.<br>
                    </p>

                    <p class="lead">
                        Thuở xưa <strong>Lee</strong> từng đi rất nhiều Instance và chăm chỉ làm nhiệm vụ từ Ông Ngoại. <br>
                        Từ những nỗ lực của Lee, Ngoại đã gửi yêu cầu Hội Eden đề xuất cho ra những phần thưởng dành cho những người chăm chỉ.<br>
                        Một loại đồng xu tượng trưng cho sự kiên trì, chăm chỉ và sức mạnh ý chí, nó có tên là <?php echo do_shortcode('[item id="52009"]Laniakea Token[/item]'); ?>.<br>
                    </p>

                    <p class="lead">
                        Sau nhiều năm rèn luyện chăm chỉ, Lee đã được những người dẫn đầu Hội Eden tin tưởng và giao nhiệm vụ trao thưởng cho những nhà thám hiểm khác.
                    </p>
                </div>

                <div class="mb-3">
                    <h3 class="color-600">Laniakea Token dùng để làm gì?</h3>
                    <p>
                        Bạn có thể dùng <?php echo do_shortcode('[item id="52009"]Laniakea Token[/item]'); ?> để mua vật phẩm có ích như đá nâng cấp đồ, food stat, các cuộn giấy phép thuật và lính đánh thuê ở
                        <strong>Laniakea Shop</strong> ở <code class="clipboard" data-clipboard-text="/navi prontera 182/212">prontera 182/212</code>. Chủ shop là <strong>Tara</strong>.
                    </p>
                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/tara-laniakea-shop.jpg' ?>" alt="laniakea-shop">
                </div>

                <div class="mb-3">
                    <h3 class="color-600">Làm thế nào để nhận Laniakea Token?</h3>
                    <p>
                        Mỗi khi bạn <strong>hoàn thành Instance</strong> hoặc <strong>hoàn thành nhiệm vụ săn quái vật của Ông ngoại</strong> ở Hội Eden, bạn sẽ nhận được Laniakea Token.
                    </p>
                </div>

                <div class="mb-3">
                    <h3 class="color-600">Vị trí của NPC Lee?</h3>
                    <p >
                        Lee đứng ở 1 góc của Prontera. Tọa độ <code class="clipboard" data-clipboard-text="/navi prontera 171/181">prontera 171/181</code>.<br>
                        Lee sẽ giúp bạn thống kê Laniakea Token và số lượng Instance mà bạn đã đi trong ngày.
                    </p>

                    <div class="mb-3">
                        <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/lee-quan-ly-instance.jpg'; ?>" alt="lee-quan-ly-instance">
                    </div>
                </div>

                <h3 class="color-600">Thông tin nhận thưởng Laniakea Token:</h3>

                <table class="table table-bordered table-striped table-hover mb-3">
                    <thead>
                    <tr>
                        <th>Tên Instance/Nhiệm vụ</th>
                        <th>Laniakea Token</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Jitterbug</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Isle of Bios</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Morse's Cave</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Temple of Demon God</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td>Devil's Tower (100%)</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td>Devil's Tower (75%)</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Devil's Tower (50%)</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/airship-assault-instance/') ?>">Airship Assault</a></td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>Faceworm</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/ghost-palace-instance/') ?>">Ghost Palace</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/nhiem-vu-wandering-guardian-hazy-forest/') ?>">Hazy Forest</a></td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Horror Toy Factory</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/saras-memory-instance-ky-uc-cua-sara/'); ?>">Sara Memory</a></td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>Bakonawa Lake</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Buwaya Cave</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Bangungo Hospital</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Last Room</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Wolfchev Laboratory</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Endless Tower</td>
                        <td>40</td>
                    </tr>
                    <tr>
                        <td>Endless Tower (Mini)</td>
                        <td>40</td>
                    </tr>
                    <tr>
                        <td>Endless Tower (Hard - Ngoài cổng vào Pori)</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td>Endless Tower (Hard - Tiêu diệt Pori)</td>
                        <td>100</td>
                    </tr>
                    <tr>
                        <td>Sara and Fenrir</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/geffen-magic-tournament/') ?>">Geffen Magic Tournament</a></td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Old Glast Heim</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Old Glast Heim (Hard)</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td>Nidhoggur's Nest</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>Octopus Cave</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/charleston-crisis-instance/') ?>">Charleston Crisis</a></td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Room of Consciousness</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/sky-fortress-instance/') ?>">Sky Fortress</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td>Malangdo Culvert</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/central-laboratory/') ?>">Central Laboratory</a></td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/ac-mong-cua-alice/') ?>">Ác mộng của Alice</a></td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>Gramps (Ông ngoại)</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/tim-lai-meo-con-cua-jakk/') ?>">Tìm mèo con của Jakk</a></td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td><a>Poring Village</a></td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td><a>Poring Village (Hard)</a></td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td><a>Infinite Space</a></td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/instance-heart-hunter-war-base/') ?>">Heart Hunter War Base</a></td>
                        <td>25</td>
                    </tr>
                    <tr>
                        <td><a target="_blank" href="<?php echo home_url('/instance/instance-werner-laboratory-central-room/') ?>">Werner Laboratory Central Room</a></td>
                        <td>
                            30 với MvP Cutie<br>
                            40 với MvP Seyren
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="alert alert-info mb-3">
                    <p><strong>Mẹo:</strong></p>

                    <ul>
                        <li><span class="badge badge-danger">NEW!</span> Hoàn thành Instance lần đầu trong ngày cũng nhận thêm phần thưởng đặc biệt.</li>
                        <li>Nếu bạn chưa đủ sức để đi các Instance, hãy cùng nhau lập nhóm cùng đi Instance khó.</li>
                        <li>Nếu tài khoản của bạn là VIP, bạn sẽ được x2 Laniakea Token sau khi hoàn thành Instance.</li>
                        <li>Trong nhóm càng nhiều người đi thì bạn càng nhận nhiều Laniakea Token. Khi nhóm trên 1 người thì cứ mỗi 1 người phần thưởng Laniakea Token được tăng thêm 10%.</li>
                        <li>Thường xuyên theo dõi thông tin sự kiện trên Fanpage hoặc Discord. "Canh me" thời gian diễn ra Happy Instance, khi có Happy Instance bạn sẽ nhận được x2 Laniakea Token sau khi hoàn thành Instance.</li>
                    </ul>
                </div>

                <div class="alert alert-warning">
                    <p><strong>Một số lưu ý:</strong></p>
                    <ul>
                        <li>Những Instance nào chưa có trong danh sách trên tức là chưa có phần thưởng Laniakea Token.</li>
                        <li>Bạn có thể dùng lệnh <code class="clipboard" data-clipboarrd-text="@checkcd">@checkcd</code> để kiểm tra cooldown của các Instance.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php endwhile ?>
<?php get_footer(); ?>
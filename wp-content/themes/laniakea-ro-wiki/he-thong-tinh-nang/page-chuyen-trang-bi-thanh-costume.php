<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="bg-light p-3 border rounded">
            <div class="mb-3">
                <h3 class="color-600">Giới thiệu</h3>

                <p class="lead">
                    Một chú mèo tên là Tú, thuở nhỏ cậu ta rất thích chơi với chỉ và vải. <br>
                    Lớn lên cậu ta mở 1 sạp đồ chuyên may vá, cậu ta có khả năng biến bất kì mũ thông thường nào thành mũ thời trang.<br>
                    Nếu bạn tò mò về khả năng của anh ta thì hãy ghé thăm sạp đồ của cậu ấy nhé.
                </p>

                <h3 class="color-600">Chuyển mũ thường thành mũ Costume?</h3>

                <p class="lead">
                    Đúng vậy, cậu ta có thể may được mũ (Headgear), áo choàng (Manteau) thành vật phẩm thời trang hay còn gọi là Costume.
                </p>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Hướng dẫn</h3>

                <div class="row mb-3">
                    <div class="col-md-4 mb-3 mb-md-0">
                        <div class="card">
                            <img class="card-img-top" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-to-costume/tu-tho-may.jpg' ?>" alt="tu-tho-may-ragnarok-online">
                            <div class="card-body">
                                <h5 class="card-title">Tú thợ may</h5>
                                <p class="card-text">
                                    Vị trí: <code class="clipboard" data-clipboard-text="/navi payon 186/179">payon 186/179</code>.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="alert alert-info">
                            <p>Có <strong>2 cách</strong> để bạn nhờ Tú thợ may chuyển đồ thường thành đồ thời trang: </p>

                            <ul class="mb-3">
                                <li><strong>Cách 1: </strong>từ 2 đồ thường chuyển thành đồ thời trang. Nếu bạn chọn cách này thì bạn phải trang bị 1 món vào người và 1 món để trong hành trang.</li>
                                <li><strong>Cách 2:</strong> từ 1 món đồ thường và 3 đồng Laniakea Coin D (mỗi đồng trị giá 100 CP).</li>
                            </ul>
                        </div>

                        <div class="alert alert-danger">
                            <p>
                                Đồ thời trang được làm từ <strong>Tú thợ may</strong> có những hạn chế sau:
                            <p>

                            <ul>
                                <li>Vẫn chịu ảnh hưởng bởi nghề nghiệp được ghi trong đoạn giới thiệu của món đồ đó.</li>
                                <li>Hiệu ứng, thuộc tính, lá bài, cường hóa của vật phẩm sẽ không còn tác dụng.</li>
                                <li>Không thể phục lại thành đồ thường.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endwhile ?>
<?php get_footer(); ?>

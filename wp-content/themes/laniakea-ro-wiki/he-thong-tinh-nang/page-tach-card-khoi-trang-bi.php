<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="bg-light p-3 border rounded mb-5">
            <div class="mb-3">
                <h3 class="color-600">Giới thiệu</h3>

                <p>
                    Ở thế giới Ragnarok, bạn có thể gắn card vào trang bị một cách dễ dàng nhưng tháo card ra là một vấn đề lớn đó. <br>
                    Ragnarok đã cập nhật 2 NPC tách card bên Malangdo (đảo mèo) đó là <strong>Richard</strong> và <strong>Jeremy</strong>.<br>
                    Mặc dù vậy, điều kiện để tách được card cũng khá là chua khiến cho <strong>Richard</strong> và <strong>Jeremy</strong> không được ủng hộ cho lắm.
                </p>

                <p>
                    Một ngày nọ khi <strong>Richard</strong> và <strong>Jeremy</strong> tham gia Laniakea RO, GM đã cho qua "bển" du học.<br>
                    Sau khi đi du học về <strong>Richard</strong> và <strong>Jeremy</strong> đã biết tách card nhiều trang bị hơn.<br>
                    Ngoài ra <strong>Richard</strong> và <strong>Jeremy</strong> hiểu rằng việc săn card khá là đau đớn cho tinh thần và sức khỏe nên đã quyết định thay đổi cách thức tách card.
                </p>

                <p>
                    Okay, chúng ta cùng nhau tìm hiểu <strong>Richard</strong> và <strong>Jeremy</strong> sau 1 cuộc phỏng vấn từ GM.
                </p>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600"><strong>Richard</strong> và <strong>Jeremy</strong> ở đâu?</h3>
                <p>
                    <strong>Richard</strong> và <strong>Jeremy</strong> ở vị trí <code class="clipboard" data-clipboard-text="/navi malangdo 211/163">malangdo 211/163</code>.
                </p>

                <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/tach-card-khoi-trang-bi-vi-tri-npc.jpg'; ?>" alt="vi-tri-npc-tach-card-khoi-trang-bi">

                <p>
                    Để qua được Malangdo. Có 2 cách sau:
                </p>
                <ul>
                    <li class="mb-3">
                        Cách truyền thống: Gặp NPC <strong>Odgnalam</strong> ở Izlude tọa độ <code class="clipboard" data-clipboard-text="/navi izlude 180/218">izlude 180/218</code>.<br>

                        <img class="img-thumbnail mt-1" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/cach-qua-malangdo-1.jpg'; ?>" alt="cach-qua-malangdo-1">
                    </li>
                    <li class="mb-3">
                        Cách hiện đại (ở Laniakea RO): Gặp <strong>NPC Warper đặc biệt</strong> và chọn đi đến <strong>Malangdo</strong> ở mục Thành phố.
                    </li>
                </ul>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600">Tách card thế nào?</h3>

                <p>
                    <strong>Richard</strong> mèo trắng sẽ giúp bạn tách card ra khỏi vũ khí.<br>
                    <strong>Jeremy</strong> mèo đen sẽ giúp bạn tách card ra khỏi áo giáp, trang sức.
                </p>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600">Chi phí tách card?</h3>

                <p>
                    Có 3 cách tính phí, bạn chọn 1 trong 3 cách:
                </p>

                <ul>
                    <li><strong>Cách 1:</strong> <code>100,000</code> zeny. Cách này rẻ nhất nhưng có rủi ro là 20% sẽ mất trang bị hoặc mất card. Có thể mất cả 2 luôn.</li>
                    <li><strong>Cách 2:</strong> <code>1,000,000</code> zeny. Cách này 100% thành công.</li>
                </ul>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600">Tách được card MVP không?</h3>

                <p>
                    Card MVP thì đặc biệt hơn, nhưng nó không gây khó khăn với <strong>Richard</strong> và <strong>Jeremy</strong>.
                </p>
                <p>
                    Để tách Card MVP bạn cần x1 <?php echo do_shortcode('[item id="6320"]Premium Reset Stone[/item]') ?>.
                </p>
            </div>
        </div>
    </div>
</div>

<?php endwhile ?>
<?php get_footer(); ?>

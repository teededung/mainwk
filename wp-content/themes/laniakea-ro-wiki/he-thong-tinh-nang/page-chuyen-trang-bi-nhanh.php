<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 border rounded">
                <div class="mb-3">
                    <h3 class="color-600">Giới thiệu</h3>

                    <p class="lead">
                        Sau những giờ căng thẳng, miệt mài nghiên cứu... Laniakea RO đã tích hợp thành công tính năng chuyển trang bị nhanh. <br>
                        Tính năng khá hữu ích cho các bạn có nhiều trang bị, costume. Chỉ cần 1 nhấp chuột, trang bị của bạn sẽ được thay thế một cách nhanh chóng.
                    </p>
                </div>

                <hr>

                <div class="mb-3">
                    <h3 class="color-600 mb-3">Hướng dẫn</h3>

                    <div class="mb-3">
                        <strong>Bước 1:</strong>
                        <p>
                            Hãy mở cửa sổ trang bị của bạn bằng phím tắt <kbd>ALT + Q</kbd>.
                        </p>
                        <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-switch/eq-switch-feature.jpg' ?>" alt="chuyen-doi-trang-bi-nhanh">
                    </div>

                    <hr>

                    <div class="mb-3">
                        <strong>Bước 2:</strong>
                        <p>
                            Nhấp vào nút <strong>Set item setting</strong> và bạn sẽ thấy được 1 cửa sổ trang bị khác như hình bên dưới.
                        </p>
                        <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-switch/eq-switch-feature-2.jpg' ?>" alt="cua-so-chuyen-doi-trang-bi-nhanh">
                    </div>

                    <hr>

                    <div class="mb-3">
                        <strong>Bước 3:</strong>
                        <p>
                            Hãy kéo những trang bị mà bạn muốn thay đổi nhanh vào cửa sổ trang bị bên dưới.
                        </p>
                        <img class="img-thumbnail img-fluid" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-switch/eq-switch-feature-3.jpg' ?>" alt="chuan-bi-chuyen-doi-trang-bi-nhanh">
                    </div>

                    <hr>

                    <div class="mb-3">
                        <strong>Bước 4:</strong>
                        <p>
                            Khi nào bạn cần chuyển trang bị nhanh hãy mở cửa sổ trang bị <kbd>ALT + Q</kbd>, sau đó nhấp vào nút <strong>Chuyển</strong>.
                        </p>
                        <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-switch/eq-switch-feature-4.jpg' ?>" alt="chuyen-doi-trang-bi-xong">
                        <p>
                            Boom! Mọi trang bị sẽ được thay đổi một cách nhanh chóng. Bạn sẽ thấy nút <strong>Chuyển</strong> sẽ bị đen đi và không cho bấm được nữa, đừng lo vì 10 giây sau bạn sẽ sử dụng được tiếp.
                        </p>
                    </div>

                    <hr>

                    <div class="mb-3">
                        <div class="alert alert-info">
                            <strong>Mẹo 1:</strong><br>
                            <p>Bạn có thể sử dụng tính năng chuyển trang bị nhanh đối với đồ thời trang (Costume).</p>
                        </div>

                        <div class="alert alert-info">
                            <strong>Mẹo 2:</strong><br>
                            <p>Bạn có thể sử dụng cách thay trang bị truyền thống kết hợp với kiểu thay đổi trang bị mới này để thao tác nhanh hơn nữa.</p>
                        </div>

                        <div class="alert alert-info">
                            <strong>Mẹo 3:</strong><br>
                            <p>Bạn sẽ có các trang bị trong hành trang sẽ có hình nhỏ màu đỏ như thế này. Đó là những trang bị mà bạn đã trang bị vào cửa sổ thay đổi trang bị nhanh.</p>
                            <img class="img-thumbnail img-fluid mb-3" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/eq-switch/eq-switch-feature-5.jpg' ?>" alt="icon-chuyen-doi-trang-bi-nhanh">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endwhile ?>
<?php get_footer(); ?>

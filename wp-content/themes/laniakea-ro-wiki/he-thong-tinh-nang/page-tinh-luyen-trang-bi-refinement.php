<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="bg-light p-3 border rounded mb-5">
            <div class="mb-3">
                <h3 class="color-600">Giới thiệu</h3>

                <p class="lead">
                    Ở Ragnarok bạn có thể tinh luyện trang bị, vũ khí để chúng mạnh hơn. Mỗi lần tinh luyện thành công, trang bị của bạn sẽ +1 độ tinh luyện và được thể hiện trước tên của trang bị, tối đa là +20.
                    Bạn có thể tinh luyện mũ (upper, middle, lower), áo giáp (armor), giày (boot, shoes), áo choàng (garment) nhưng không thể tinh luyện trang sức (accessory).
                </p>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Vật phẩm cần thiết để tinh luyện</h3>
                <p>Đối với vũ khí, vật phẩm để tinh luyện sẽ thay đổi tùy vào độ tinh luyện hiện tại của vũ khí.</p>

                <div class="mb-3">
                    <h5>Trang bị có độ tinh luyện dưới 10:</h5>
                    <ul>
                        <li>Vũ khí cấp 1 - <?php echo_item(1010, "Phracon"); ?></li>
                        <li>Vũ khí cấp 2 - <?php echo_item(1011, "Emveretarcon"); ?></li>
                        <li>
                            Vũ khí cấp 3 và 4 (1 trong 5 loại dưới đây)
                            <ul>
                                <li class="mb-1"><?php echo_item(984, "Oridecon"); ?></li>
                                <li class="mb-1"><?php echo_item(7620, "Enriched Oridecon"); ?></li>
                                <li class="mb-1"><?php echo_item(6240, "HD Oridecon"); ?></li>
                                <li class="mb-1"><?php echo_item(6438, "Blessed Oridecon"); ?></li>
                            </ul>
                        </li>
                        <li>
                            Áo giáp (1 trong 5 loại dưới đây)
                            <ul>
                                <li class="mb-1"><?php echo do_shortcode('[item id="985"]Elunium[/item]') ?></li>
                                <li class="mb-1"><?php echo do_shortcode('[item id="7619"]Enriched Elunium[/item]') ?></li>
                                <li class="mb-1"><?php echo do_shortcode('[item id="6241"]HD Elunium[/item]') ?></li>
                                <li class="mb-1"><?php echo do_shortcode('[item id="6439"]Blessed Elunium[/item]') ?></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="mb-3">
                    <h5>Trang bị có độ tinh luyện trên 10:</h5>
                    <ul>
                        <li>Vũ khí - <?php echo do_shortcode('[item id="6224"]Bradium[/item]') ?> hoặc <?php echo do_shortcode('[item id="6226"]HD Bradium[/item]') ?></li>
                        <li>Áo giáp - <?php echo do_shortcode('[item id="6223"]Carnium[/item]') ?> hoặc <?php echo do_shortcode('[item id="6225"]HD Carnium[/item]') ?></li>
                    </ul>
                </div>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Cách tìm nguyên liệu để tinh luyện</h3>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Vật phẩm</th>
                        <th>Cách tìm</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo do_shortcode('[item id="1010"]Phracon[/item]') ?></td>
                        <td>Giá 200 Zeny từ NPC hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="1011"]Emveretarcon[/item]') ?></td>
                        <td>Giá 1.000 Zeny từ NPC hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="984"]Oridecon[/item]') ?></td>
                        <td>Tạo từ 5 viên <?php echo do_shortcode('[item id="756"]Rough Oridecon[/item]') ?> hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="985"]Elunium[/item]') ?></td>
                        <td>Tạo từ 5 viên <?php echo do_shortcode('[item id="757"]Rough Elunium[/item]') ?> hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6240"]HD Oridecon[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6241"]HD Elunium[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="7620"]Enriched Oridecon[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="7619"]Enriched Elunium[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6438"]Blessed Oridecon[/item]') ?></td>
                        <td>Mua từ Cửa hàng Poring ở tọa độ <code class="clipboard" data-clipboard-text="/navi prontera 187/210">prontera 187/210</code>.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6439"]Blessed Elunium[/item]') ?></td>
                        <td>Mua từ Cửa hàng Poring. ở tọa độ <code class="clipboard" data-clipboard-text="/navi prontera 187/210">prontera 187/210</code>.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6224"]Bradium[/item]') ?></td>
                        <td>Trao đổi từ NPC với 3 <?php echo do_shortcode('[item id="984"]Oridecon[/item]') ?> và 50.000 Zeny hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6223"]Carnium[/item]') ?></td>
                        <td>Trao đổi từ NPC với 3 <?php echo do_shortcode('[item id="985"]Elunium[/item]') ?> và 50.000 Zeny hoặc săn từ quái vật.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6226"]HD Bradium[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6225"]HD Carnium[/item]') ?></td>
                        <td>Mua từ Cash Shop.</td>
                    </tr>
                    <tr>
                        <td><?php echo do_shortcode('[item id="6635"]Blacksmith Blessing[/item]') ?></td>
                        <td>Nhận từ mini game Cuộc đua Poring (Poring Race).</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Các cách để tinh luyện</h3>

                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Cách 1:</strong> Gặp các thợ rèn ở các thành phố, để dễ tìm bạn hãy gặp <strong>Hollengrhen</strong> ở Prontera (<code class="clipboard" data-clipboard-text="/navi prt_in 63/60">prt_in 63/60</code>).
                    </li>
                    <li class="list-group-item">
                        <strong>Cách 2:</strong> Kỹ năng <strong>Weapon Refine</strong> của nghề Blacksmith để tinh luyện vũ khí. Mỗi cấp độ nghề sau 50 sẽ +0.5% tỉ lệ thành công và bạn sẽ đạt được 10% khi ở cấp độ nghề 70.
                    </li>
                </ul>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">tinh luyện trang bị được lợi ích gì?</h3>

                <div class="mb-3">
                    <h5>Đối với vũ khí:</h5>
                    <ul>
                        <li>Vũ khí cấp 1 - <span class="color-500">2 ATK</span> mỗi độ tinh luyện, <span class="color-500">0~3 ATK</span> mỗi độ tinh luyện trên +7, <span class="color-500">3 ATK</span> mỗi độ tinh luyện trên +15</li>
                        <li>Vũ khí cấp 2 - <span class="color-500">3 ATK</span> mỗi độ tinh luyện, <span class="color-500">0~5 ATK</span> mỗi độ tinh luyện trên +6, <span class="color-500">6 ATK</span> mỗi độ tinh luyện trên +15</li>
                        <li>Vũ khí cấp 3 - <span class="color-500">5 ATK</span> mỗi độ tinh luyện, <span class="color-500">0~8 ATK</span> mỗi độ tinh luyện trên +5, <span class="color-500">9 ATK</span> mỗi độ tinh luyện trên +15</li>
                        <li>Vũ khí cấp 4 - <span class="color-500">7 ATK</span> mỗi độ tinh luyện, <span class="color-500">0~14 ATK</span> mỗi độ tinh luyện trên +4, <span class="color-500">12 ATK</span> mỗi độ tinh luyện trên +15</li>
                    </ul>

                    <small class="mb-3 d-block"><em>Nhận MATK thay vì ATK nếu vũ khí của bạn thuộc loại sát thương phép thuật.</em></small>

                    <div class="alert alert-info">
                        <strong>Ví dụ:</strong> Một vũ khí cấp 4 được tinh luyện lên +20 sẽ nhận thêm 201~424 ATK hoặc MATK.
                    </div>
                </div>
                <div class="mb-3">
                    <h5>Đối với áo giáp:</h5>
                    <ul>
                        <li>Mỗi tinh luyện sẽ tăng <strong>Hard DEF</strong> bằng công thức <code>Floor[(3 + cấp độ tinh luyện hiện tại) / 4]</code> (Floor là làm tròn).</li>
                        <li>Mỗi +1 tinh luyện từ +1 đến +4 tăng 1 Hard DEF.</li>
                        <li>Mỗi +1 tinh luyện từ +5 đến +8 tăng 2 Hard DEF.</li>
                        <li>Mỗi +1 tinh luyện từ +9 đến +12 tăng 3 Hard DEF.</li>
                        <li>Mỗi +1 tinh luyện từ +13 đến +16 tăng 4 Hard DEF.</li>
                        <li>Mỗi +1 tinh luyện từ +17 đến +20 tăng 5 Hard DEF.</li>
                    </ul>

                    <small class="mb-3 d-block"><em>Hard DEF (Equipment DEF) là DEF nhận từ áo giáp. Soft DEF là DEF nhận từ điểm Stat.</em></small>

                    <div class="alert alert-info">
                        <strong>Ví dụ:</strong> Khiên +10 Buckler sẽ cho bạn 90 + (1+1+1+1) + (2+2+2+2) + (3+3) = 108 DEF.
                    </div>
                </div>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Độ an toàn để tinh luyện</h3>
                <p>
                    Nếu bạn tinh luyện trang bị trong độ an toàn này thì <span class="text-success">tỉ lệ thành công khi tinh luyện luôn là 100%</span>.
                    Khi trang bị của bạn càng tinh luyện lên cao thì tỉ lệ thành công khi tinh luyện càng giảm.
                    Trang bị sẽ <strong>biến mất vĩnh viễn</strong> nếu bạn <span class="text-danger">tinh luyện thất bại</span>.
                </p>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Loại</th>
                        <td>Độ an toàn</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Vũ khí cấp 1</td>
                        <td>+7</td>
                    </tr>
                    <tr>
                        <td>Vũ khí cấp 2</td>
                        <td>+6</td>
                    </tr>
                    <tr>
                        <td>Vũ khí cấp 3</td>
                        <td>+5</td>
                    </tr>
                    <tr>
                        <td>Vũ khí cấp 4</td>
                        <td>+4</td>
                    </tr>
                    <tr>
                        <td>Áo giáp</td>
                        <td>+4</td>
                    </tr>
                    <tr>
                        <td>Shadow Gear</td>
                        <td>+4</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Bảo vệ trang bị khi tinh luyện vượt qua mức tinh luyện an toàn?</h3>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Vật phẩm</th>
                        <th>Công dụng</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6240"]HD Oridecon[/item]') ?></td>
                        <td>
                            Tỉ lệ tinh luyện thành công bằng với Oridecon. <br>
                            Nếu tinh luyện thất bại, vũ khí của bạn không biến mất nhưng có tỉ lệ 10% bị giảm đi 1 độ tinh luyện.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6241"]HD Elunium[/item]') ?></td>
                        <td>
                            Tỉ lệ tinh luyện thành công bằng với Elunium. <br>
                            Nếu tinh luyện thất bại, giáp của bạn không biến mất nhưng có tỉ lệ 10% bị giảm đi 1 độ tinh luyện.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6226"]HD Bradium[/item]') ?></td>
                        <td>
                            Chỉ được dùng khi độ tinh luyện của vũ khí đạt +10 và trở lên.<br>
                            Nếu tinh luyện thất bại, vũ khí của bạn không biến mất nhưng có tỉ lệ 10% vũ khí sẽ bị giảm đi 1 cấp độ tinh luyện.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6225"]HD Carnium[/item]') ?></td>
                        <td>
                            Chỉ được dùng khi độ tinh luyện của áo giáp đạt +10 và trở lên.<br>
                            Nếu tinh luyện thất bại, áo giáp của bạn không biến mất nhưng có tỉ lệ 10% áo giáp sẽ bị giảm đi 1 cấp độ tinh luyện.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6438"]Blessed Oridecon[/item]') ?></td>
                        <td>
                            Chỉ được dùng khi độ tinh luyện của vũ khí đạt +1 đến +11.<br>
                            Tỉ lệ tinh luyện thành công ngang với Oridecon. Nếu tinh luyện thất bại, vũ khí của bạn sẽ không biến mất nhưng có tỉ lệ 25% độ tinh luyện của vũ khí sẽ giảm đi 1.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6439"]Blessed Elunium[/item]') ?></td>
                        <td>
                            Chỉ được dùng khi độ tinh luyện của áo giáp đạt +1 đến +11.<br>
                            Tỉ lệ tinh luyện thành công ngang với Elunium. Nếu tinh luyện thất bại, áo giáp của bạn sẽ không biến mất nhưng có tỉ lệ 25% độ tinh luyện của áo giáp sẽ giảm đi 1.
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><?php echo do_shortcode('[item id="6635"]Blacksmith Blessing[/item]') ?></td>
                        <td>
                            Sử dụng kèm với vật phẩm tinh luyện. Chỉ được sử dụng khi trang bị đạt độ tinh luyện +7 đến +11.<br>
                            Số lượng sử dụng thay đổi theo độ tinh luyện hiện tại của trang bị.<br>
                            +7 -> +8 cần 1 lọ<br>
                            +8 -> +9 cần 2 lọ<br>
                            +9 -> +10 cần 4 lọ<br>
                            +10 -> +11 cần 7 lọ<br>
                            +11 -> +12 cần 11 lọ
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Tỉ lệ tinh luyện</h3>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <caption>Tỉ lệ thành công khi tinh luyện bằng các vật phẩm tinh luyện thông thường.</caption>
                        <thead>
                        <tr>
                            <th>Độ tinh luyện</th>
                            <th>Lv. 1</th>
                            <th>Lv. 2</th>
                            <th>Lv. 3</th>
                            <th>Lv. 4</th>
                            <th>Áo giáp</th>
                            <th>Trang bị Shadow</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><strong>+4 <i class="icon ion-ios-arrow-round-forward"></i> +5</strong></td>
                            <td>100%</td>
                            <td>100%</td>
                            <td>100%</td>
                            <td>60%</td>
                            <td>60%</td>
                            <td>60%</td>
                        </tr>
                        <tr>
                            <td><strong>+5 <i class="icon ion-ios-arrow-round-forward"></i> +6</strong></td>
                            <td>100%</td>
                            <td>100%</td>
                            <td>60%</td>
                            <td>40%</td>
                            <td>40%</td>
                            <td>40%</td>
                        </tr>
                        <tr>
                            <td><strong>+6 <i class="icon ion-ios-arrow-round-forward"></i> +7</strong></td>
                            <td>100%</td>
                            <td>60%</td>
                            <td>50%</td>
                            <td>40%</td>
                            <td>40%</td>
                            <td>40%</td>
                        </tr>
                        <tr>
                            <td><strong>+7 <i class="icon ion-ios-arrow-round-forward"></i> +8</strong></td>
                            <td>60%</td>
                            <td>40%</td>
                            <td>20%</td>
                            <td>20%</td>
                            <td>20%</td>
                            <td>20%</td>
                        </tr>
                        <tr>
                            <td><strong>+8 <i class="icon ion-ios-arrow-round-forward"></i> +9</strong></td>
                            <td>40%</td>
                            <td>20%</td>
                            <td>20%</td>
                            <td>20%</td>
                            <td>20%</td>
                            <td>20%</td>
                        </tr>
                        <tr>
                            <td><strong>+9 <i class="icon ion-ios-arrow-round-forward"></i> +10</strong></td>
                            <td>19%</td>
                            <td>19%</td>
                            <td>19%</td>
                            <td>9%</td>
                            <td>9%</td>
                            <td>9%</td>
                        </tr>
                        <tr>
                            <td><strong>+11 ~ +14</strong></td>
                            <td>18%</td>
                            <td>18%</td>
                            <td>18%</td>
                            <td>8%</td>
                            <td>8%</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td><strong>+15 ~ +18</strong></td>
                            <td>17%</td>
                            <td>17%</td>
                            <td>17%</td>
                            <td>7%</td>
                            <td>7%</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td><strong>+19 ~ +20</strong></td>
                            <td>15%</td>
                            <td>15%</td>
                            <td>15%</td>
                            <td>5%</td>
                            <td>5%</td>
                            <td>N/A</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-0">
                    <caption>Tỉ lệ thành công khi tinh luyện bằng Enriched Oridecon, Enriched Elunium.</caption>
                    <thead>
                    <tr>
                        <th>Độ tinh luyện</th>
                        <th>Lv. 1</th>
                        <th>Lv. 2</th>
                        <th>Lv. 3</th>
                        <th>Lv. 4</th>
                        <th>Áo giáp</th>
                        <th>Trang bị Shadow</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>+4 <i class="icon ion-ios-arrow-round-forward"></i> +5</strong></td>
                        <td><span class="text-danger">100%</span></td>
                        <td><span class="text-danger">100%</span></td>
                        <td><span class="text-danger">100%</span></td>
                        <td>90%</td>
                        <td>90%</td>
                        <td>90%</td>
                    </tr>
                    <tr>
                        <td><strong>+5 <i class="icon ion-ios-arrow-round-forward"></i> +6</strong></td>
                        <td><span class="text-danger">100%</span></td>
                        <td><span class="text-danger">100%</span></td>
                        <td>90%</td>
                        <td>70%</td>
                        <td>70%</td>
                        <td>70%</td>
                    </tr>
                    <tr>
                        <td><strong>+6 <i class="icon ion-ios-arrow-round-forward"></i> +7</strong></td>
                        <td><span class="text-danger">100%</span></td>
                        <td>90%</td>
                        <td>80%</td>
                        <td>70%</td>
                        <td>70%</td>
                        <td>70%</td>
                    </tr>
                    <tr>
                        <td><strong>+7 <i class="icon ion-ios-arrow-round-forward"></i> +8</strong></td>
                        <td>90%</td>
                        <td>70%</td>
                        <td>40%</td>
                        <td>40%</td>
                        <td>40%</td>
                        <td>40%</td>
                    </tr>
                    <tr>
                        <td><strong>+8 <i class="icon ion-ios-arrow-round-forward"></i> +9</strong></td>
                        <td>70%</td>
                        <td>40%</td>
                        <td>40%</td>
                        <td>40%</td>
                        <td>40%</td>
                        <td>40%</td>
                    </tr>
                    <tr>
                        <td><strong>+9 <i class="icon ion-ios-arrow-round-forward"></i> +10</strong></td>
                        <td>30%</td>
                        <td>30%</td>
                        <td>30%</td>
                        <td>20%</td>
                        <td>20%</td>
                        <td>20%</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <hr>

            <div class="mb-3">
                <h3 class="color-600 mb-3">Mẹo khi tinh luyện trang bị</h3>
                <ul>
                    <li>
                        Để đạt tinh luyện +13, hãy cố gắng đi nhiều Instance để có được Poring Badge. Sau khi có Laniakea Token bạn sẽ đổi được <?php echo do_shortcode('[item id="6438"]Blessed Oridecon[/item]') ?> hoặc <?php echo do_shortcode('[item id="6439"]Blessed Elunium[/item]') ?>.
                        Vật phẩm này cho phép bạn tinh luyện từ +1 đến +11 mà không có rủi ro mất trang bị.
                    </li>
                    <li>Nếu bạn tinh luyện thất bại khá nhiều trong ngày (kể cả cường hóa), hãy dừng lại và thư giãn. Bạn nên dời việc tinh luyện vào một ngày khác.</li>
                    <li>Nếu bạn lại tiếp tục tinh luyện thất bại trong nhiều ngày, thậm chí nản chí, muốn chửi bới,... Đừng lo lắng, hãy nhờ bạn bè trong game giúp đỡ, nhiều khi còn được cho trang bị nữa.</li>
                    <li>Đừng chơi game một mình, vì đầy là game cộng đồng. Hãy chia sẽ cho nhiều bạn khác khi bạn đạt được trang bị "xịn" nhé.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 border rounded mb-5">
                <h3 class="color-600">Cách nhận quà tặng hàng ngày</h3>

                <p>
                Mỗi ngày khi bạn đăng nhập vào game bạn sẽ thấy một hộp thoại như sau:<br><br>

                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/hop-thoai-qua-tang-hang-ngay.jpg'; ?>" alt="hop-thoai-qua-tang-hang-ngay">
                    <br><br>

                    Điều đó có nghĩa là bạn đã có thể nhận quà hằng ngày. <br>
                    Hãy đến thành phố Prontera và vào trụ sở chính (<code class="clipboard" data-clipboard-text="/navi prt_in 43/114">prt_in 43/114</code>) nói chuyện với NPC <strong>Daily Reward</strong> để nhận quà.<br>
                </p>

                <p>
                    Vị trí NPC:<br><br>
                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/daily-reward-office.jpg' ?>" alt="daily-reward-office">
                </p>

                <div class="alert alert-warning">
                    <p>Bạn chỉ nhận được quà tặng hằng ngày với 1 nhân vật duy nhất. Hãy chọn nhân vật chính của bạn để nhận quà nhé.</p>
                </div>

                <div class="alert alert-info">
                    <p>
                        Hệ thống nhận quà sẽ tự động reset sau 0h sáng mỗi ngày. Sau thời gian này thì bạn đăng nhập bất kì khung giờ nào cũng nhận được quà.<br>
                        Nếu bạn có không đăng nhập ngày nào đó thì phần quà ngày kế tiếp vẫn được giữ.
                    </p>
                </div>
            </div>

            <div class="bg-light p-3 border rounded mb-5">
                <h3 class="color-600">Lịch nhận quà</h3>
                <p class="lead">Dưới đây là lịch nhận quà tặng hàng ngày:</p>

                <div class="row no-gutters row-daily text-center mb-3 mb-md-0 justify-content-center">
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 1</span>
                        <?php echo do_shortcode('[item id="12208"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">50% Battle Manual</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 2</span>
                        <?php echo do_shortcode('[item id="12211"][/item]') ?>
                        <span class="d-block">x5</span>
                        <span class="d-block">Kafra Card</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 3</span>
                        <?php echo do_shortcode('[item id="51001"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Free Cash Coin D</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 4</span>
                        <?php echo do_shortcode('[item id="12215"][/item]') ?>
                        <span class="d-block">x5</span>
                        <span class="d-block">Blessing Scroll</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 5</span>
                        <?php echo do_shortcode('[item id="12210"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Bubble Gum</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 6</span>
                        <?php echo do_shortcode('[item id="7621"][/item]') ?>
                        <span class="d-block">x2</span>
                        <span class="d-block">Token of Siegfried</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 7</span>
                        <?php echo do_shortcode('[item id="14592"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Job Manual</span>
                    </div>
                </div>

                <div class="row no-gutters row-daily text-center mb-3 mb-md-0 justify-content-center">
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 8</span>
                        <?php echo do_shortcode('[item id="12103"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Bloody Branch</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 9</span>
                        <?php echo do_shortcode('[item id="12214"][/item]') ?>
                        <span class="d-block">x2</span>
                        <span class="d-block">Convex Mirror</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 10</span>
                        <?php echo do_shortcode('[item id="51001"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">Free Cash Coin D</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 11</span>
                        <?php echo do_shortcode('[item id="25464"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">World Moving Rights</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 12</span>
                        <?php echo do_shortcode('[item id="12216"][/item]') ?>
                        <span class="d-block">x10</span>
                        <span class="d-block">Increase AGI Scroll</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 13</span>
                        <?php echo do_shortcode('[item id="6635"][/item]') ?>
                        <span class="d-block">x5</span>
                        <span class="d-block">Blacksmith_Blessing</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 14</span>
                        <?php echo do_shortcode('[item id="12214"][/item]') ?>
                        <span class="d-block">x2</span>
                        <span class="d-block">Convex Mirror</span>
                    </div>
                </div>

                <div class="row no-gutters row-daily text-center mb-3 mb-md-0 justify-content-center">
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 15</span>
                        <?php echo do_shortcode('[item id="616"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Old Card Album</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 16</span>
                        <?php echo do_shortcode('[item id="14534"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">Small Life Potion</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 17</span>
                        <?php echo do_shortcode('[item id="12211"][/item]') ?>
                        <span class="d-block">x10</span>
                        <span class="d-block">Kafra Card</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 18</span>
                        <?php echo do_shortcode('[item id="12103"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">Bloody Branch</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 19</span>
                        <?php echo do_shortcode('[item id="51001"][/item]') ?>
                        <span class="d-block">x5</span>
                        <span class="d-block">Free Cash Coin D</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 20</span>
                        <?php echo do_shortcode('[item id="25464"][/item]') ?>
                        <span class="d-block">x6</span>
                        <span class="d-block">World Moving Rights</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 21</span>
                        <?php echo do_shortcode('[item id="12214"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">Convex Mirror</span>
                    </div>
                </div>

                <div class="row no-gutters row-daily text-center mb-3 mb-md-0 justify-content-center">
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 22</span>
                        <?php echo do_shortcode('[item id="12210"][/item]') ?>
                        <span class="d-block">x2</span>
                        <span class="d-block">Bubble Gum</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 23</span>
                        <?php echo do_shortcode('[item id="7621"][/item]') ?>
                        <span class="d-block">x3</span>
                        <span class="d-block">Token of Siegfried</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 24</span>
                        <?php echo do_shortcode('[item id="12103"][/item]') ?>
                        <span class="d-block">x5</span>
                        <span class="d-block">Bloody Branch</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 25</span>
                        <?php echo do_shortcode('[item id="25464"][/item]') ?>
                        <span class="d-block">x9</span>
                        <span class="d-block">World Moving Rights</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 26</span>
                        <?php echo do_shortcode('[item id="51001"][/item]') ?>
                        <span class="d-block">x7</span>
                        <span class="d-block">Free Cash Coin D</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 27</span>
                        <?php echo do_shortcode('[item id="12246"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Mystical Card Album</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1">Ngày 28</span>
                        <?php echo do_shortcode('[item id="12412"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">HE Bubble Gum</span>
                    </div>
                </div>

                <div class="row no-gutters row-daily justify-content-center text-center">
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1 text-danger">Ngày 29</span>
                        <?php echo do_shortcode('[item id="50007"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Vé VIP 3 Ngày</span>
                    </div>
                    <div class="col-6 col-md-4 col-lg border">
                        <span class="d-block border-bottom mb-2 pb-1 pt-1 text-danger">Ngày 30</span>
                        <?php echo do_shortcode('[item id="50050"][/item]') ?>
                        <span class="d-block">x1</span>
                        <span class="d-block">Random Hat Box</span>
                    </div>
                </div>
            </div>


            <div class="bg-light p-3 border rounded mb-5">
                <h3 class="color-600">Kiểm tra</h3>
                <p class="lead">Bạn có thể sử dụng lệnh <code>@daily</code> để kiểm tra bạn đã nhận quà ở ngày mấy trong lịch nhận quà.</p>

                <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/kiem-tra-qua-hang-ngay.jpg' ?>" alt="kiem-tra-qua-hang-ngay">
            </div>
        </div>
    </div>

<?php endwhile ?>
<?php get_footer(); ?>

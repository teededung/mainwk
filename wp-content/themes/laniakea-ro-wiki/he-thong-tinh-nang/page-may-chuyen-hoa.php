<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
		<div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Hệ thống & Tính năng</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

			<h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 border rounded">
                <div class="mb-3">
                    <h3 class="color-600">Giới thiệu</h3>

                    <p class="lead">
                        Máy chuyển hóa giúp bạn chuyển hóa một số vật phẩm và tích thành năng lượng tí hon. Những năng lượng tí hon này là nguyên liệu để chế tạo ra những costume aura đẹp lồng lộng.
                    </p>
                </div>

                <hr>

                <div class="mb-3">
                    <h3 class="color-600">Hướng dẫn sử dụng máy chuyển hóa</h3>

                    <p>
                        Máy được đạt ở <code class="clipboard" data-clipboard-text="/navi prontera 127/209">prontera 127/209</code>.
                    </p>

                    <p>
                        <a href="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/may-chuyen-hoa-laniakea-ro.jpg'; ?>" data-lity>
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/may-chuyen-hoa-laniakea-ro.jpg'; ?>" alt="may-chuyen-hoa-laniakea-ro">
                        </a>
                    </p>
                </div>

                <hr>

                <div class="mb-3">
                    <h4 class="color-600 mb-3">
                        1. Các vật phẩm mà máy có thể chuyển hóa thành năng lượng tí hon:
                    </h4>

                    <ul class="mb-3">
                        <li><?php echo _item(1415, 'Brocca') ?></li>
                        <li><?php echo _item(2004, 'Kronos') ?></li>
                        <li><?php echo _item(2022, 'Staff of Geffen [1]') ?></li>
                        <li><?php echo _item(2115, 'Valkyrie Shield [1]') ?></li>
                        <li><?php echo _item(2327, 'Holy Robe') ?></li>
                        <li><?php echo _item(2375, 'Diabolus Armor [1]') ?></li>
                        <li><?php echo _item(2423, 'Variant Shoes') ?></li>
                        <li><?php echo _item(2486, 'Shadow Walk [1]') ?></li>
                        <li><?php echo _item(2357, 'Valkyrie Armor [1]') ?></li>
                        <li><?php echo _item(15032, 'Tidung [1]') ?></li>
                        <li><?php echo _item(5019, 'Corsair') ?></li>
                        <li><?php echo _item(2650, 'Morrigane\'s Belt') ?></li>
                        <li><?php echo _item(2651, 'Morrigane\'s Pendant') ?></li>
                        <li><?php echo _item(2519, 'Morrigane\'s Manteau') ?></li>
                        <li><?php echo _item(2554, 'Nydhorgg\'s Shadow Garb [1]') ?></li>
                        <li><?php echo _item(16027, 'Evil Slayer Destroyer Hammer [1]') ?></li>
                        <li><?php echo _item(18120, 'Evil Slayer Piercer Bow [1]') ?></li>
                        <li><?php echo _item(28001, 'Evil Slayer Ripper Katar [1]') ?></li>
                        <li><?php echo _item(21010, 'Evil Slayer Slasher Sword [1]') ?></li>
                        <li><?php echo _item(13094, 'Evil Slayer Stabber Dagger [1]') ?></li>
                        <li>----</li>
                        <li><?php echo _item(4909, 'Darklord Essence Force 2') ?></li>
                        <li><?php echo _item(4912, 'Darklord Essence Intelligence 2') ?></li>
                        <li><?php echo _item(4915, 'Darklord Essence Speed 2') ?></li>
                        <li><?php echo _item(4918, 'Darklord Essence Vitality 2') ?></li>
                        <li><?php echo _item(4921, 'Darklord Essence Concentration 2') ?></li>
                        <li><?php echo _item(4924, 'Darklord Essence Luk 2') ?></li>
                        <li><?php echo _item(20717, 'Gigant Snake Skin') ?></li>
                        <li><?php echo _item(20718, 'Gigant Snake Skin [1]') ?></li>
                        <li><?php echo _item(22035, 'Brave Nepenthes Shoes [1]') ?></li>
                        <li><?php echo _item(22037, 'Brave Ungoliant Boots [1]') ?></li>
                        <li><?php echo _item(751, 'Osiris Doll') ?></li>
                        <li><?php echo _item(1029, 'Tiger Skin') ?></li>
                        <li><?php echo _item(6326, 'Piece of Queen\'s Wing') ?></li>
                        <li><?php echo _item(7036, 'Fang of Hatii') ?></li>
                        <li><?php echo _item(7108, 'Piece of Shield') ?></li>
                        <li><?php echo _item(7109, 'Shining Spear Blade') ?></li>
                        <li><?php echo _item(7113, 'Broken Pharaoh Emblem') ?></li>
                        <li><?php echo _item(7114, 'Masque of Tutankhamen') ?></li>
                        <li><?php echo _item(7169, 'Ba Gua') ?></li>
                        <li><?php echo _item(7211, 'Fragment of Rossata Stone') ?></li>
                        <li><?php echo _item(7300, 'Gemstone') ?></li>
                        <li><?php echo _item(7451, 'Fire Dragon Scale') ?></li>
                        <li><?php echo _item(7566, 'Will of Red Darkness') ?></li>
                        <li><?php echo _item(7562, 'Ice Scale') ?></li>
                        <li><?php echo _item(7022, 'Old Hilt') ?></li>
                        <li><?php echo _item(7024, 'Bloody Edge') ?></li>
                        <li><?php echo _item(6649, 'Broken Horn') ?></li>
                        <li><?php echo _item(7513, 'Pocket Watch') ?></li>
                        <li><?php echo _item(7450, 'Skeletal Armor Piece') ?></li>
                        <li><?php echo _item(6091, 'Darkred Scale Piece') ?></li>
                    </ul>

                    <div class="alert alert-info">
                        Dù vậy, máy không phải lúc nào cũng nhận toàn bộ vật phẩm trên,
                        mỗi ngày cứ đến <strong>05h15</strong>, <strong>9h15</strong>, <strong>13h15</strong>, <strong>17h15</strong>, <strong>21h15</strong>, <strong>01h15</strong> máy sẽ đổi vật phẩm cần thiết để tạo thành năng lượng tí hon.
                    </div>

                    <p>Tỉ lệ thành công khi chuyển hóa thành năng lượng là 50%. Mỗi lần thành công bạn sẽ nhận được 1 đến 3 Năng lượng tí hon.</p>
                </div>

                <hr>

                <div class="mb-3">
                    <h4 class="color-600 mb-3">
                        2. Của hàng năng lượng tí hon:
                    </h4>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Tên vật phẩm</th>
                            <th>Năng lượng tí hon</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Costume Bloody Aura [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Bloody Aura [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume saLUsalo Hat</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Invoker's Aura [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Abbadon Aura [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Flower Aura [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Crystal Ring [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Auring Of Nature [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Bubble Ring [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Cookie Ring [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Fire Spirit [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Haunting Spirits [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Jazz Ring [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Butterfly Blessing [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Falling Rose Petals [L] hoặc [M]</td>
                            <td>1500</td>
                        </tr>
                        <tr>
                            <td>Costume Electric Aura [L] hoặc [M]</td>
                            <td>
                                1500<br>
                                ---<br>
                                Ngoài ra máy còn có chức năng chuyển màu của Costume này với 5 năng lượng tí hon.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <hr>

                <div class="mb-3">
                    <h4 class="color-600 mb-3">
                        3. 750 năng lượng tí hon cũng có thể đổi vật phẩm!
                    </h4>

                    <p>
                        Nếu bạn thấy 1500 Năng lượng là quá nhiều, bạn có thử vận may bằng cách chỉ tiêu dùng 750 năng lượng tí hon để đổi lấy vật phẩm. Bạn sẽ nhận được ngẫu nhiên 1 trong các vật phẩm trong cửa hàng của Máy chuyển hóa.
                    </p>
                </div>

                <hr>

                <div class="mb-3">
                    <h4 class="color-600 mb-3">
                        3. Các Dark MvP cũng rơi ra năng lượng tí hon!
                    </h4>

                    <p>
                        Các Dark MvP khá mạnh, trâu bò, khi chúng chết cũng rơi ra năng lượng tí hon. Dark MvP trú ngự ở Endless Tower (Hard) tầng cuối cùng.
                    </p>
                </div>
            </div>
		</div>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
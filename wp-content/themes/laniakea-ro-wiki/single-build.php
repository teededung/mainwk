<?php
while (have_posts()): the_post();
    $args = array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key'	 	=> 'builds',
                'value'	  	=> '"' . get_the_ID() . '"',
                'compare' 	=> 'LIKE',
            )
        )
    );

    $job_object = new WP_Query($args);
    if ($job_object->have_posts()):
        while ($job_object->have_posts()): $job_object->the_post();
            $link = get_the_permalink();
            wp_redirect($link, 301);
            exit;
        endwhile;
    endif;
endwhile;

wp_redirect(home_url('/builds/'), 301);
exit;
<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="card mb-3">
            <div class="card-body">
                <h5 class="card-title">
                    Mục lục
                </h5>
                <div class="each-item mb-2">
                    <span>1.</span> <?php echo do_shortcode("[scroll]Các điều kiện để chuyển nghề[/scroll]"); ?>
                </div>
                <div class="each-item mb-2">
                    <span>2.</span> <?php echo do_shortcode("[scroll]Các cách chuyển nghề[/scroll]"); ?>
                </div>
                <div class="each-item mb-2">
                    <span>3.</span> <?php echo do_shortcode("[scroll]Hướng dẫn chuyển nghề[/scroll]"); ?>
                    <ul class="mb-0 pl-3 list-unstyled">
                        <li>
                            <span>3.1.</span> <?php echo do_shortcode("[scroll]Nghề Tập sự (Novice) chuyển lên nghề 1[/scroll]"); ?>
                        </li>
                        <li>
                            <span>3.2.</span> <?php echo do_shortcode("[scroll]Chuyển nghề 1, 2, 3 hoặc Tái sinh từ Sư phụ[/scroll]"); ?>
                        </li>
                    </ul>
                </div>
                <div class="each-item mb-2">
                    <span>4.</span> <?php echo do_shortcode("[scroll]Nên chọn Sư phụ hay làm nhiệm vụ?[/scroll]"); ?>
                </div>
                <div class="each-item mb-2">
                    <span>5.</span> <?php echo do_shortcode("[scroll]Các nhánh nghề[/scroll]"); ?>
                </div>
            </div>
        </div>

        <div class="bg-light p-3 border rounded">
            <div class="mb-3">
                <p class="lead">
                    Khi cấp độ nhân vật đã đạt tới giới hạn, bạn cần phải chuyển nghề để nhân vật mạnh mẽ hơn và có nhiều kỹ năng hơn.
                </p>

                <hr>

                <h2 class="color-600 mb-3" id="cac-dieu-kien-de-chuyen-nghe">Các điều kiện để chuyển nghề</h2>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ Tập sự (Novice) chuyển lên nghề 1</strong></p>
                    <ul>
                        <li>Đạt cấp độ nghề (Job) 10.</li>
                        <li>Tăng hết điểm kỹ năng.</li>
                    </ul>
                </div>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ nghề 1 chuyển lên nghề 2</strong></p>
                    <ul>
                        <li>Đạt cấp độ nghề 40 trở lên (tốt nhất là 50).</li>
                        <li>Tăng hết điểm kỹ năng.</li>
                    </ul>
                </div>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ nghề 2 chuyển lên nghề 3</strong></p>
                    <ul>
                        <li>Đạt cấp độ 99.</li>
                        <li>Đạt cấp độ nghề 50.</li>
                        <li>Tăng hết điểm kỹ năng.</li>
                    </ul>
                </div>

                <hr>

                <h2 class="color-600 mb-3" id="cac-cach-chuyen-nghe">Các cách chuyển nghề</h2>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ Tập sự (Novice) chuyển lên nghề 1</strong></p>
                    <ul>
                        <li>Cách 1: <?php echo do_shortcode('[scroll]Từ các NPC trong Học viện Criatura[/scroll]') ?>.</li>
                        <li>Cách 2: Từ nhiệm vụ chuyển nghề.</li>
                        <li>Cách 3: Từ NPC <?php echo do_shortcode('[npc]Sư phụ[/npc]') ?>.</li>
                    </ul>
                </div>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ nghề 1 chuyển lên nghề 2</strong></p>
                    <ul>
                        <li>Cách 1: Làm nhiệm vụ chuyển nghề.</li>
                        <li>Cách 2: Từ NPC <?php echo do_shortcode('[npc]Sư phụ[/npc]') ?>.</li>
                    </ul>
                </div>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Tái sinh (Rebirth)</strong></p>
                    <ul>
                        <li>Cách 1: Gặp Valkyrie ở Valhalla.</li>
                        <li>Cách 2: Từ NPC <?php echo do_shortcode('[npc]Sư phụ[/npc]') ?>.</li>
                    </ul>
                </div>

                <div class="mb-3">
                    <p class="mb-1 color-600"><strong>Từ nghề 2 chuyển lên nghề 3</strong></p>
                    <ul>
                        <li>Cách 1: Làm nhiệm vụ chuyển nghề.</li>
                        <li>Cách 2: Từ NPC <?php echo do_shortcode('[npc]Sư phụ[/npc]') ?>.</li>
                    </ul>
                </div>

                <div class="alert alert-info">
                    <p>Mình khuyên các bạn nên chuyển nghề bằng NPC <?php echo do_shortcode('[npc]Sư phụ[/npc]') ?> sẽ nhanh hơn. Còn bạn nào muốn khám phá nhiệm vụ có thể chuyển nghề bằng cách làm nhiệm vụ, lưu ý rằng Tái sinh từ Valkyrie và các nhiệm vụ chuyển nghề 1, 2 và 3 chưa được Việt hóa.</p>
                </div>

                <hr>

                <h2 class="color-600 mb-3" id="huong-dan-chuyen-nghe">Hướng dẫn chuyển nghề</h2>

                <div class="mb-3">
                    <h5 class="mb-2 color-600" id="nghe-tap-su-novice-chuyen-len-nghe-1"><i class="ion-md-arrow-forward"></i> Nghề Tập sự (Novice) chuyển lên nghề 1</h5>
                    <p class="mb-1">Chuyển nghề <span id="tu-cac-npc-trong-hoc-vien-criatura">từ các NPC trong Học viện Criatura</span> (Click vào tên NPC dưới đây để biết vị trí của NPC). <br>Bạn có thể xem đặc điểm của nghề và dùng thử kỹ năng trước khi quyết định chuyển nghề.</p>
                    <ul>
                        <li>NPC <?php echo do_shortcode('[npc]Swordman Trainer[/npc]') ?> giúp bạn chuyển nghề lên <strong>Kiếm sĩ (Swordman)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Archer Teacher[/npc]') ?> giúp bạn chuyển nghề lên <strong>Cung thủ (Archer)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Mage Chuck[/npc]') ?> giúp bạn chuyển nghề lên <strong>Pháp sư (Mage)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Salim Hamid[/npc]') ?> giúp bạn chuyển nghề lên <strong>Thương gia (Merchant)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Guest Lecturer Mayssel[/npc]') ?> giúp bạn chuyển nghề lên <strong>Đạo chích (Thief)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Acolyte Leader Alice[/npc]') ?> giúp bạn chuyển nghề lên <strong>Tu sĩ tập sự (Acolyte)</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Ninja Guide[/npc]') ?> giúp bạn chuyển nghề lên <strong>Ninja</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Lusa[/npc]') ?> giúp bạn chuyển nghề lên <strong>Gunslinger</strong>.</li>
                        <li>NPC <?php echo do_shortcode('[npc]Arang[/npc]') ?> giúp bạn chuyển nghề lên <strong>Taekwon</strong>.</li>
                    </ul>

                    Hoặc bạn có thể chuyển nghề bằng Sư phụ.
                </div>

                <div class="mb-3">
                    <h5 class="mb-2 color-600" id="chuyen-nghe-1-2-3-hoac-tai-sinh-tu-su-phu"><i class="ion-md-arrow-forward"></i> Chuyển nghề 1, 2, 3 hoặc Tái sinh từ Sư phụ</h5>

                    <p class="mb-1">NPC Sư phụ đứng ở 2 vị trí.</p>
                    <ul>
                        <li>
                            <span class="d-block mb-2"><?php echo do_shortcode('[npc]Sư phụ[/npc]') ?> ở Izlude tọa độ <code class="clipboard" data-clipboard-text="/navi izlude 117/255">izlude 117/255</code>.</span>
                            <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/chuyen-nghe-tu-su-phu-o-izlude.jpg'; ?>" alt="Chuyển nghề từ Sư phụ ở Izlude">
                        </li>
                        <li>
                            <span class="d-block mb-2"><?php echo do_shortcode('[npc index="2"]Sư phụ[/npc]') ?> ở Payon tọa độ <code class="clipboard" data-clipboard-text="/navi payon 156/124">payon 156/124</code>.</span>
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/chuyen-nghe-tu-su-phu-o-payon.jpg'; ?>" alt="Chuyển nghề từ Sư phụ ở Payon">
                        </li>
                    </ul>
                </div>

                <hr>

                <h2 class="color-600 mb-3" id="nen-chon-su-phu-hay-lam-nhiem-vu">Nên chọn Sư phụ hay làm nhiệm vụ?</h2>
                <p>Những khác biệt khi chuyển nghề từ Sư phụ và làm nhiệm vụ:</p>
                <ul>
                    <li>Chuyển nghề từ Sư phụ nhanh hơn, tiết kiệm thời gian hơn.</li>
                    <li>Chuyển nghề từ nhiệm vụ dành cho các bạn muốn tìm hiểu cốt truyện (tiếng Anh).</li>
                    <li>Phần thưởng khi chuyển nghề từ Sư phụ hoặc từ nhiệm vụ <strong>giống nhau</strong>.</li>
                </ul>

                <hr>

                <h2 class="color-600 mb-3" id="cac-nhanh-nghe">Các nhánh nghề</h2>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Kiếm sĩ</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Swordsman</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Swordman.png' ?>" alt="Swordsman">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Knight</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Knight.png' ?>" alt="Knight">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Lord Knight</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Lord_Knight.png' ?>" alt="Lord Knight">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Rune Knight</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rune_Knight.png' ?>" alt="Rune Knight">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Crusader</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Crusader.png' ?>" alt="Crusader">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Paladin</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Paladin.png' ?>" alt="Paladin">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Royal Guard</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Royal_Guard.png' ?>" alt="Royal Guard">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Cung thủ</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Archer</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Archer.png' ?>" alt="Archer">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Hunter</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Hunter.png' ?>" alt="Hunter">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Sniper</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sniper.png' ?>" alt="Sniper">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Ranger</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Ranger.png' ?>" alt="Ranger">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Bard</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Bard.png' ?>" alt="Bard">
                                        <div class="job-name">Dancer</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Dancer.png' ?>" alt="Dancer">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Minstrel</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Minstrel.png' ?>" alt="Minstrel">
                                        <div class="job-name">Gypsy</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Gypsy.png' ?>" alt="Gypsy">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Maestro</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Maestro.png' ?>" alt="Maestro">
                                        <div class="job-name">Wanderer</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Wanderer.png' ?>" alt="Wanderer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Pháp sư</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Magician</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Mage.png' ?>" alt="Mage">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Wizard</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Wizard.png' ?>" alt="Wizard">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">High Wizard</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/High_Wizard.png' ?>" alt="High Wizard">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Warlock</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Warlock.png' ?>" alt="Warlock">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Sage</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sage.png' ?>" alt="Sage">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Professor</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Scholar.png' ?>" alt="Scholar">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Sorcerer</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sorcerer.png' ?>" alt="Sorcerer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Thương gia</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Merchant</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Merchant.png' ?>" alt="Merchant">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Blacksmith</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Blacksmith.png' ?>" alt="Blacksmith">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Whitesmith</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Mastersmith.png' ?>" alt="Whitesmith">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Mechanic</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Mechanic.png' ?>" alt="Mechanic">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Alchemist</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Alchemist.png' ?>" alt="Alchemist">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Biochemist</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Biochemist.png' ?>" alt="Biochemist">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Geneticist</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Geneticist.png' ?>" alt="Geneticist">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Đạo chích</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Thief</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Thief.png' ?>" alt="Thief">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Assassin</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Assassin.png' ?>" alt="Assassin">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Assassin Cross</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Assassin_Cross.png' ?>" alt="Assassin Cross">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Guillotine Cross</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Guillotine_Cross.png' ?>" alt="Guillotine Cross">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Rogue</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rogue.png' ?>" alt="Rogue">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Stalker</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Stalker.png' ?>" alt="Stalker">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Shadow Chaser</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Shadow_Chaser.png' ?>" alt="Shadow Chaser">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Tu sĩ</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Acolyte</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Acolyte.png' ?>" alt="Acolyte">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Priest</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Priest.png' ?>" alt="Priest">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">High Priest</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/High_Priest.png' ?>" alt="High Priest">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Arch Bishop</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Arch_Bishop.png' ?>" alt="Arch Bishop">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Monk</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Monk.png' ?>" alt="Monk">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Tái sinh</div>
                                        <div class="job-name">Champion</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Champion.png' ?>" alt="Champion">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Sura</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sura.png' ?>" alt="Sura">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Ninja</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Ninja</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Ninja.png' ?>" alt="Ninja">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Kagerou</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Kagerou.png' ?>" alt="Kagerou">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Oboro</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Oboro.png' ?>" alt="Oboro">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Xạ thủ</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Gunslinger</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Gunslinger.png' ?>" alt="Gunslinger">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center">
                                <div class="col">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Rebellion</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rebel.png' ?>" alt="Rebellion">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề TaeKwon</h5>

                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="block-job text-center mb-3 mb-lg-0">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">TaeKwon Boy</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/TaeKwon_Kid.png' ?>" alt="TaeKwon Boy">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row row-job-options justify-content-center mb-3">
                                <div class="col-6">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Star Gladiator</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/TaeKwon_Master.png' ?>" alt="TaeKwon Master">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Star Emperor</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Star_Emperor.png' ?>" alt="Star Emperor">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-job-options justify-content-center">
                                <div class="col-6">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 2</div>
                                        <div class="job-name">Soul Linker</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Soul_Linker.png' ?>" alt="Soul Linker">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="block-job text-center">
                                        <div class="label">Nghề 3</div>
                                        <div class="job-name">Soul Reaper</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Soul_Reaper.png' ?>" alt="Soul Reaper">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 p-3 border block-jobs">
                    <h5 class="color-600 mb-3">Nhánh nghề Mèo</h5>

                    <div class="row align-items-center">
                        <div class="col">
                            <div class="block-job text-center">
                                <div class="label">Nghề 1</div>
                                <div class="job-name">Summoner</div>
                                <img src="<?php echo get_template_directory_uri() . '/images/jobs/Summoner.png' ?>" alt="Summoner">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

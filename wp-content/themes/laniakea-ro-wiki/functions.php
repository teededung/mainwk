<?php
require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/ajax.php');
require_once locate_template('/functions/cpt.php');

include_once locate_template('/functions/tee-functions.php');
include_once locate_template('/functions/parser.php');
include_once locate_template('/functions/shortcode.php');
include_once locate_template('/functions/filters.php');

/*========== Your custom functions below here ==========*/
/**
 * @param $tag_name
 *
 * @return string
 */
function get_badge_type($tag_name) {
    switch (strtolower($tag_name)):
        case 'custom':
            return 'badge-primary';
        case 'official':
            return 'badge-warning';
        default:
            return 'badge-secondary';
    endswitch;
}

/**
 * @param $image_url
 * @param $post_id
 * @param $name
 * @return bool|int|WP_Error
 */
function add_skill_icon($image_url, $post_id, $name) {
    $upload_dir = wp_upload_dir();

    $arrContextOptions = array(
        "ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
        ),
    );

    $image_data = file_get_contents($image_url, false, stream_context_create($arrContextOptions));

    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))
        $file = $upload_dir['path'] . '/' . $filename;
    else
        $file = $upload_dir['basedir'] . '/' . $filename;

    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => $name,
        'post_excerpt' => 'Skill Icon',
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    if ($attach_id) {
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1 = wp_update_attachment_metadata( $attach_id, $attach_data );
        $res2 = set_post_thumbnail( $post_id, $attach_id );
        return $attach_id;
    }
    return false;
}

function get_base_class($jobName) {
    switch($jobName):
        case 'Novice':
        case 'Super Novice':
        case 'Super Novice 2':
            return 'Novice';
        case 'Swordman':
        case 'Knight': case 'Crusader':
        case 'Lord Knight': case 'Paladin':
        case 'Rune Knight': case 'Royal Guard':
            return 'Nhánh nghề Swordman';
        case 'Archer':
        case 'Hunter':
        case 'Sniper':
        case 'Ranger':
            return 'Nhánh nghề Archer';
        case 'Dancer':
        case 'Gypsy':
        case 'Wanderer':
            return 'Nhánh nghề Archer (Nữ)';
        case 'Clown':
        case 'Bard':
        case 'Minstrel':
            return 'Nhánh nghề Archer (Nam)';
        case 'Thief':
        case 'Assassin': case 'Rogue':
        case 'Assassin Cross': case 'Stalker':
        case 'Guillotine Cross': case 'Shadow Chaser':
            return 'Nhánh nghề Thief';
        case 'Acolyte':
        case 'Priest': case 'Monk':
        case 'High Priest': case 'Champion':
        case 'Arch Bishop': case 'Sura':
            return 'Nhánh nghề Acolyte';
        case 'Mage':
        case 'Magician':
        case 'Wizard': case 'Sage':
        case 'High Wizard': case 'Professor':
        case 'Warlock': case 'Sorcerer':
            return 'Nhánh nghề Mage';
        case 'Merchant':
        case 'Blacksmith': case 'Alchemist':
        case 'Whitesmith': case 'Creator':
        case 'Mechanic': case 'Genetic': case 'Geneticist':
            return 'Nhánh nghề Merchant';
        case 'TaeKwon Boy': case 'TaeKwon Kid':
        case 'Soul Linker': case 'Star Gladiator':
        case 'Soul Reaper': case 'Star Emperor':
            return 'Nhánh nghề TaeKwon';
        case 'Ninja':
            return 'Nhánh nghề Ninja';
        case 'Kagerou':
            return 'Nhánh nghề Ninja (Nam)';
        case 'Oboro':
            return 'Nhánh nghề Ninja (Nữ)';
        case 'Doram': case 'Summoner':
            return 'Doram';
    endswitch;
}

function convert_random_option($name) {
    switch ($name):
        default:
            return $name;
        case 'VAR_MAXHPAMOUNT':
            return 'MaxHP';
        case 'VAR_MAXSPAMOUNT':
            return 'MaxSP';
        case 'VAR_STRAMOUNT':
            return 'STR';
        case 'VAR_AGIAMOUNT':
            return 'AGI';
        case 'VAR_VITAMOUNT':
            return 'VIT';
        case 'VAR_INTAMOUNT':
            return 'INT';
        case 'VAR_DEXAMOUNT':
            return 'DEX';
        case 'VAR_LUKAMOUNT':
            return 'LUK';
        case 'VAR_MAXHPPERCENT':
            return 'MaxHP %';
        case 'VAR_MAXSPPERCENT':
            return 'MaxSP %';
        case 'VAR_HPACCELERATION':
            return 'Tốc độ hồi phục HP %';
        case 'VAR_SPACCELERATION':
            return 'Tốc độ hồi phục SP %';
        case 'VAR_ATKPERCENT':
            return 'ATK %';
        case 'VAR_MAGICATKPERCENT':
            return 'MATK %';
        case 'VAR_PLUSASPD':
            return 'ASPD';
        case 'VAR_PLUSASPDPERCENT':
            return 'ASPD %';
        case 'VAR_ATTPOWER':
            return 'ATK';
        case 'VAR_HITSUCCESSVALUE':
            return 'HIT';
        case 'VAR_ATTMPOWER':
            return 'MATK';
        case 'VAR_ITEMDEFPOWER':
            return 'DEF';
        case 'VAR_MDEFPOWER':
            return 'MDEF';
        case 'VAR_AVOIDSUCCESSVALUE':
            return 'FLEE';
        case 'VAR_PLUSAVOIDSUCCESSVALUE':
            return 'Perfect Dodge';
        case 'VAR_CRITICALSUCCESSVALUE':
            return 'CRIT';
        case 'ATTR_TOLERACE_NOTHING':
            return 'Kháng % thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'ATTR_TOLERACE_WATER':
            return 'Kháng % thuộc tính <span style="color: #0000BB;">Nước</span>';
        case 'ATTR_TOLERACE_GROUND':
            return 'Kháng % thuộc tính <span style="color: #CC5500;">Đất</span>';
        case 'ATTR_TOLERACE_FIRE':
            return 'Kháng % thuộc tính <span style="color: #FF0000;">Lửa</span>';
        case 'ATTR_TOLERACE_WIND':
            return 'Kháng % thuộc tính <span style="color: #33CC00;">Gió</span>';
        case 'ATTR_TOLERACE_POISON':
            return 'Kháng % thuộc tính <span style="color: #663399;">Độc</span>';
        case 'ATTR_TOLERACE_SAINT':
            return 'Kháng % thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'ATTR_TOLERACE_DARKNESS':
            return 'Kháng % thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'ATTR_TOLERACE_TELEKINESIS':
            return 'Kháng % thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'ATTR_TOLERACE_UNDEAD':
            return 'Kháng % thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'ATTR_TOLERACE_ALLBUTNOTHING':
            return 'Kháng % tất cả thuộc tính, trừ <span style="color: #777777;">Trung tính</span>';
        case 'DAMAGE_PROPERTY_NOTHING_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'DAMAGE_PROPERTY_NOTHING_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'DAMAGE_PROPERTY_WATER_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #0000BB;">Nước</span>';
        case 'DAMAGE_PROPERTY_WATER_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>';
        case 'DAMAGE_PROPERTY_GROUND_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #CC5500;">Đất</span>';
        case 'DAMAGE_PROPERTY_GROUND_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>';
        case 'DAMAGE_PROPERTY_FIRE_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>';
        case 'DAMAGE_PROPERTY_FIRE_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>';
        case 'DAMAGE_PROPERTY_WIND_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #33CC00;">Gió</span>';
        case 'DAMAGE_PROPERTY_WIND_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>';
        case 'DAMAGE_PROPERTY_POISON_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #663399;">Độc</span>';
        case 'DAMAGE_PROPERTY_POISON_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #663399;">Độc</span>';
        case 'DAMAGE_PROPERTY_SAINT_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'DAMAGE_PROPERTY_SAINT_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'DAMAGE_PROPERTY_DARKNESS_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'DAMAGE_PROPERTY_DARKNESS_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'DAMAGE_PROPERTY_TELEKINESIS_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'DAMAGE_PROPERTY_TELEKINESIS_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'DAMAGE_PROPERTY_UNDEAD_USER':
            return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'DAMAGE_PROPERTY_UNDEAD_TARGET':
            return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'MDAMAGE_PROPERTY_NOTHING_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'MDAMAGE_PROPERTY_NOTHING_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'MDAMAGE_PROPERTY_WATER_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #0000BB">Nước</span>';
        case 'MDAMAGE_PROPERTY_WATER_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #0000BB">Nước</span>';
        case 'MDAMAGE_PROPERTY_GROUND_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #CC5500">Đất</span>';
        case 'MDAMAGE_PROPERTY_GROUND_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #CC5500">Đất</span>';
        case 'MDAMAGE_PROPERTY_FIRE_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #FF0000">Lửa</span>';
        case 'MDAMAGE_PROPERTY_FIRE_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #FF0000">Lửa</span>';
        case 'MDAMAGE_PROPERTY_WIND_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #33CC00">Gió</span>';
        case 'MDAMAGE_PROPERTY_WIND_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #33CC00">Gió</span>';
        case 'MDAMAGE_PROPERTY_POISON_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #663399">Độc</span>';
        case 'MDAMAGE_PROPERTY_POISON_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #663399">Độc</span>';
        case 'MDAMAGE_PROPERTY_SAINT_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'MDAMAGE_PROPERTY_SAINT_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'MDAMAGE_PROPERTY_DARKNESS_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'MDAMAGE_PROPERTY_DARKNESS_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'MDAMAGE_PROPERTY_TELEKINESIS_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'MDAMAGE_PROPERTY_TELEKINESIS_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'MDAMAGE_PROPERTY_UNDEAD_USER':
            return 'Giảm % MATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'MDAMAGE_PROPERTY_UNDEAD_TARGET':
            return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'BODY_ATTR_NOTHING':
            return 'Thuộc tính áo giáp: <span style="color: #777777;">Trung tính</span>';
        case 'BODY_ATTR_WATER':
            return 'Thuộc tính áo giáp: <span style="color: #0000BB">Nước</span>';
        case 'BODY_ATTR_GROUND':
            return 'Thuộc tính áo giáp: <span style="color: #CC5500">Đất</span>';
        case 'BODY_ATTR_FIRE':
            return 'Thuộc tính áo giáp: <span style="color: #FF0000">Lửa</span>';
        case 'BODY_ATTR_WIND':
            return 'Thuộc tính áo giáp: <span style="color: #33CC00">Gió</span>';
        case 'BODY_ATTR_POISON':
            return 'Thuộc tính áo giáp: <span style="color: #663399">Độc</span>';
        case 'BODY_ATTR_SAINT':
            return 'Thuộc tính áo giáp: <span style="color: #777777;">Thánh</span>';
        case 'BODY_ATTR_DARKNESS':
            return 'Thuộc tính áo giáp: <span style="color: #777777;">Bóng tối</span>';
        case 'BODY_ATTR_TELEKINESIS':
            return 'Thuộc tính áo giáp: <span style="color: #777777;">Hồn ma</span>';
        case 'BODY_ATTR_UNDEAD':
            return 'Thuộc tính áo giáp: <span style="color: #777777;">Thây ma</span>';
        case 'RACE_TOLERACE_NOTHING':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_TOLERACE_UNDEAD':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_TOLERACE_ANIMAL':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_TOLERACE_PLANT':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_TOLERACE_INSECT':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_TOLERACE_FISHS':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_TOLERACE_DEVIL':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_TOLERACE_HUMAN':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
        case 'RACE_TOLERACE_ANGEL':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_TOLERACE_DRAGON':
            return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Rồng</span>';
        case 'RACE_DAMAGE_NOTHING':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_DAMAGE_UNDEAD':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_DAMAGE_ANIMAL':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_DAMAGE_PLANT':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_DAMAGE_INSECT':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_DAMAGE_FISHS':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_DAMAGE_DEVIL':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_DAMAGE_HUMAN':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
        case 'RACE_DAMAGE_ANGEL':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_DAMAGE_DRAGON':
            return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Rồng</span>';
        case 'RACE_MDAMAGE_NOTHING':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_MDAMAGE_UNDEAD':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_MDAMAGE_ANIMAL':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_MDAMAGE_PLANT':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_MDAMAGE_INSECT':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_MDAMAGE_FISHS':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_MDAMAGE_DEVIL':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_MDAMAGE_HUMAN':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
        case 'RACE_MDAMAGE_ANGEL':
            return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_MDAMAGE_DRAGON':
            return 'Tăng % MATK lên chủng  <span style="color: #FF0000;">Rồng</span>';
        case 'RACE_CRI_PERCENT_NOTHING':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_CRI_PERCENT_UNDEAD':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_CRI_PERCENT_ANIMAL':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_CRI_PERCENT_PLANT':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_CRI_PERCENT_INSECT':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_CRI_PERCENT_FISHS':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_CRI_PERCENT_DEVIL':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_CRI_PERCENT_HUMAN':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Á thần</span>';
        case 'RACE_CRI_PERCENT_ANGEL':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_CRI_PERCENT_DRAGON':
            return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Rồng</span>';
        case 'RACE_IGNORE_DEF_PERCENT_NOTHING':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_IGNORE_DEF_PERCENT_UNDEAD':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_IGNORE_DEF_PERCENT_ANIMAL':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_IGNORE_DEF_PERCENT_PLANT':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_IGNORE_DEF_PERCENT_INSECT':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_IGNORE_DEF_PERCENT_FISHS':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_IGNORE_DEF_PERCENT_DEVIL':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_IGNORE_DEF_PERCENT_HUMAN':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
        case 'RACE_IGNORE_DEF_PERCENT_ANGEL':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_IGNORE_DEF_PERCENT_DRAGON':
            return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Rồng</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_NOTHING':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Vô dạng</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_UNDEAD':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thây ma</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_ANIMAL':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Quái thú</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_PLANT':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thực vật</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_INSECT':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Côn trùng</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_FISHS':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Cá</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_DEVIL':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Quỷ</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_HUMAN':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_ANGEL':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thiên thần</span>';
        case 'RACE_IGNORE_MDEF_PERCENT_DRAGON':
            return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Rồng</span>';
        case 'CLASS_DAMAGE_NORMAL_TARGET':
            return 'Tăng % ATK lên quái vật Thường';
        case 'CLASS_DAMAGE_BOSS_TARGET':
            return 'Tăng % ATK lên quái vật Trùm';
        case 'CLASS_DAMAGE_NORMAL_USER':
            return 'Giảm %d sát thương nhận từ quái vật Thường';
        case 'CLASS_DAMAGE_BOSS_USER':
            return 'Giảm % sát thương nhận từ quái vật Trùm';
        case 'CLASS_MDAMAGE_NORMAL':
            return 'Tăng % MATK lên quái vật Thường';
        case 'CLASS_MDAMAGE_BOSS':
            return 'Tăng % MATK lên quái vật Trùm';
        case 'CLASS_IGNORE_DEF_PERCENT_NORMAL':
            return 'Xuyên % DEF quái vật Thường';
        case 'CLASS_IGNORE_DEF_PERCENT_BOSS':
            return 'Xuyên % DEF quái vật Trùm';
        case 'CLASS_IGNORE_MDEF_PERCENT_NORMAL':
            return 'Xuyên % MDEF quái vật Thường';
        case 'CLASS_IGNORE_MDEF_PERCENT_BOSS':
            return 'Xuyên % MDEF quái vật Trùm';
        case 'DAMAGE_SIZE_SMALL_TARGET':
            return 'Tăng % ATK lên quái vật cỡ nhỏ';
        case 'DAMAGE_SIZE_MIDIUM_TARGET':
            return 'Tăng % ATK lên quái vật cỡ vừa';
        case 'DAMAGE_SIZE_LARGE_TARGET':
            return 'Tăng % ATK lên quái vật cỡ lớn';
        case 'DAMAGE_SIZE_SMALL_USER':
            return 'Giảm % ATK nhận từ quái vật cỡ nhỏ';
        case 'DAMAGE_SIZE_MIDIUM_USER':
            return 'Giảm % ATK nhận từ quái vật cỡ vừa';
        case 'DAMAGE_SIZE_LARGE_USER':
            return 'Giảm % ATK nhận từ quái vật cỡ lớn';
        case 'DAMAGE_SIZE_PERFECT':
            return 'Kích cỡ quái vật không giảm sát thương từ vũ khí';
        case 'DAMAGE_CRI_TARGET':
            return 'Tăng % sát thương chí mạng';
        case 'DAMAGE_CRI_USER':
            return 'Giảm % sát thương chí mạng nhận từ đối phương';
        case 'RANGE_ATTACK_DAMAGE_TARGET':
            return 'Tăng % sát thương vật lý tầm xa';
        case 'RANGE_ATTACK_DAMAGE_USER':
            return 'Giảm % sát thương vật lý tầm xa nhận từ đối phương';
        case 'HEAL_VALUE':
            return 'Tăng % lượng hồi phục các kỹ năng HEAL';
        case 'HEAL_MODIFY_PERCENT':
            return 'Tăng % lượng hồi phục kỹ năng HEAL nhân từ người chơi khác';
        case 'DEC_SPELL_CAST_TIME':
            return 'Giảm % thời gian thi triển kỹ năng';
        case 'DEC_SPELL_DELAY_TIME':
            return 'Giảm % thời gian delay kỹ năng';
        case 'DEC_SP_CONSUMPTION':
            return 'Giảm % độ tiêu hao SP của các kỹ năng';
        case 'WEAPON_ATTR_NOTHING':
            return 'Thuộc tính vũ khí: <span style="color: #777777;">Trung tính</span>';
        case 'WEAPON_ATTR_WATER':
            return 'Thuộc tính vũ khí: <span style="color: #0000BB;">Nước</span>';
        case 'WEAPON_ATTR_GROUND':
            return 'Thuộc tính vũ khí: <span style="color: #CC5500;">Đất</span>';
        case 'WEAPON_ATTR_FIRE':
            return 'Thuộc tính vũ khí: <span style="color: #FF0000;">Lửa</span>';
        case 'WEAPON_ATTR_WIND':
            return 'Thuộc tính vũ khí: <span style="color: #33CC00;">Gió</span>';
        case 'WEAPON_ATTR_POISON':
            return 'Thuộc tính vũ khí: <span style="color: #663399;">Độc</span>';
        case 'WEAPON_ATTR_SAINT':
            return 'Thuộc tính vũ khí: <span style="color: #777777;">Thánh</span>';
        case 'WEAPON_ATTR_DARKNESS':
            return 'Thuộc tính vũ khí: <span style="color: #777777;">Bóng tối</span>';
        case 'WEAPON_ATTR_TELEKINESIS':
            return 'Thuộc tính vũ khí: <span style="color: #777777;">Hồn ma</span>';
        case 'WEAPON_ATTR_UNDEAD':
            return 'Thuộc tính vũ khí: <span style="color: #777777;">Thây ma</span>';
        case 'WEAPON_INDESTRUCTIBLE':
            return 'Vũ khí không bị hư trong giao tranh';
        case 'BODY_INDESTRUCTIBLE':
            return 'Giáp không bị hư trong giao tranh';
        case 'MDAMAGE_SIZE_SMALL_TARGET':
            return 'Tăng % MATK lên quái vật cỡ nhỏ';
        case 'MDAMAGE_SIZE_MIDIUM_TARGET':
            return 'Tăng % MATK lên quái vật cỡ vừa';
        case 'MDAMAGE_SIZE_LARGE_TARGET':
            return 'Tăng % MATK lên quái vật cỡ lớn';
        case 'MDAMAGE_SIZE_SMALL_USER':
            return 'Giảm % MATK nhận từ quái vật cỡ nhỏ';
        case 'MDAMAGE_SIZE_MIDIUM_USER':
            return 'Giảm % MATK nhận từ quái vật cỡ vừa';
        case 'MDAMAGE_SIZE_LARGE_USER':
            return 'Giảm % MATK nhận từ quái vật cỡ lớn';
        case 'MELEE_ATTACK_DAMAGE_TARGET':
            return 'Tăng % sát thương vật lý cận chiến';
        case 'MELEE_ATTACK_DAMAGE_USER':
            return 'Kháng % sát thương vật lý cận chiến';
        case 'ADDSKILLMDAMAGE_NOTHING':
            return 'Tăng % MATK thuộc tính <span style="color: #777777;">Trung tính</span>';
        case 'ADDSKILLMDAMAGE_WATER':
            return 'Tăng % MATK thuộc tính <span style="color: #0000BB;">Nước</span>';
        case 'ADDSKILLMDAMAGE_GROUND':
            return 'Tăng % MATK thuộc tính <span style="color: #CC5500;">Đất</span>';
        case 'ADDSKILLMDAMAGE_FIRE':
            return 'Tăng % MATK thuộc tính <span style="color: #FF0000;">Lửa</span>';
        case 'ADDSKILLMDAMAGE_WIND':
            return 'Tăng % MATK thuộc tính <span style="color: #33CC00;">Gió</span>';
        case 'ADDSKILLMDAMAGE_POISON':
            return 'Tăng % MATK thuộc tính <span style="color: #663399;">Độc</span>';
        case 'ADDSKILLMDAMAGE_SAINT':
            return 'Tăng % MATK thuộc tính <span style="color: #777777;">Thánh</span>';
        case 'ADDSKILLMDAMAGE_DARKNESS':
            return 'Tăng % MATK thuộc tính <span style="color: #777777;">Bóng tối</span>';
        case 'ADDSKILLMDAMAGE_TELEKINESIS':
            return 'Tăng % MATK thuộc tính <span style="color: #777777;">Hồn ma</span>';
        case 'ADDSKILLMDAMAGE_UNDEAD':
            return 'Tăng % MATK thuộc tính <span style="color: #777777;">Thây ma</span>';
        case 'ATTR_TOLERACE_ALL':
            return 'Kháng % tất cả thuộc tính';
    endswitch;
}

/**
 * @param $post_id
 * @return mixed|void|null
 */
function convert_post_id_to_skill_id($post_id) {
    return get_field('id', $post_id);
}

function convert_item_id_2019_client($item_id) {
    switch ($item_id):
        case 47000:
            return 300131;
        case 47001:
            return 300132;
        case 47002:
            return 300133;
        case 47003:
            return 300134;
        case 47004:
            return 300135;
        case 47005:
            return 300136;
        case 47008:
            return 300163;
        case 47009:
            return 300164;
        case 47010:
            return 300165;
        case 47011:
            return 300166;
        case 47012:
            return 300167;
        case 47013:
            return 300168;
        case 47021:
            return 310076;
        case 47022:
            return 310077;
        case 47023:
            return 310078;
        case 47024:
            return 310079;
        case 47025:
            return 310080;
        case 47026:
            return 310081;
        case 47027:
            return 300001;
        case 47028:
            return 300002;
        case 47029:
            return 300003;
        case 47030:
            return 300004;
        case 47031:
            return 300005;
        case 47032:
            return 300006;
        case 47033:
            return 300007;
        case 47034:
            return 300008;
        case 47035:
            return 300009;
        case 47036:
            return 300010;
        case 47037:
            return 300011;
        case 47038:
            return 300012;
        case 47039:
            return 300013;
        case 47040:
            return 300014;
        case 47041:
            return 300015;
        case 47042:
            return 300016;
        case 47043:
            return 300017;
        case 47044:
            return 300018;
        case 47045:
            return 300019;
        case 47046:
            return 300020;
        case 47047:
            return 300021;

        case 58053:
            return 100144;
        case 58054:
            return 100145;
        case 58055:
            return 100142;

        // Noblesse Set
        case 56000:
            return 450018;
        case 56001:
            return 450019;
        case 56002:
            return 450020;
        case 56003:
            return 450021;
        case 56004:
            return 450022;
        case 56005:
            return 450023;
        case 56006:
            return 450024;
        case 56007:
            return 450025;
        case 56008:
            return 450026;
        case 56009:
            return 450027;
        case 56010:
            return 450050;
        case 56011:
            return 450051;
        case 56012:
            return 450028;
        case 56013:
            return 450029;
        case 56014:
            return 450030;
        case 56015:
            return 450031;
        case 56016:
            return 450032;
        case 56017:
            return 450033;
        case 56018:
            return 450034;
        case 56019:
            return 450035;
        case 56020:
            return 450036;
        case 56021:
            return 450037;
        case 56022:
            return 450038;
        case 56023:
            return 450039;
        case 56024:
            return 450040;
        case 56025:
            return 450041;
        case 56026:
            return 450042;
        case 56027:
            return 450043;
        case 56028:
            return 450044;
        case 56029:
            return 450045;
        case 56030:
            return 450046;
        case 56031:
            return 450047;
        case 56032:
            return 450048;
        case 56033:
            return 450049;
        case 56034:
            return 450121;
        case 56035:
            return 450122;
        case 56036:
            return 470016;
        case 56037:
            return 470017;
        case 56038:
            return 480012;
        case 56039:
            return 480014;
        case 56040:
            return 490014;
        case 56041:
            return 490015;
        // Imperial Set
        case 56042:
            return 450052;
        case 56043:
            return 450053;
        case 56044:
            return 450054;
        case 56045:
            return 450055;
        case 56046:
            return 450056;
        case 56047:
            return 450057;
        case 56048:
            return 450058;
        case 56049:
            return 450059;
        case 56050:
            return 450060;
        case 56051:
            return 450061;
        case 56052:
            return 450062;
        case 56053:
            return 450063;
        case 56054:
            return 450064;
        case 56055:
            return 450065;
        case 56056:
            return 450066;
        case 56057:
            return 450067;
        case 56058:
            return 450068;
        case 56059:
            return 450069;
        case 56060:
            return 450070;
        case 56061:
            return 450071;
        case 56062:
            return 450072;
        case 56063:
            return 450073;
        case 56064:
            return 450074;
        case 56065:
            return 450075;
        case 56066:
            return 450076;
        case 56067:
            return 450077;
        case 56068:
            return 450078;
        case 56069:
            return 450079;
        case 56070:
            return 450080;
        case 56071:
            return 450081;
        case 56072:
            return 450082;
        case 56073:
            return 450083;
        case 56074:
            return 450084;
        case 56075:
            return 450085;
        case 56076:
            return 450123;
        case 56077:
            return 450124;
        case 56078:
            return 470018;
        case 56079:
            return 470019;
        case 56080:
            return 480016;
        case 56081:
            return 480017;
        case 56082:
            return 490017;
        case 56083:
            return 490018;
        case 63004:
            return 400023;
        case 63014:
            return 470027;
        case 63015:
            return 470036;
        case 63016:
            return 460003;
        case 63017:
            return 470025;
        case 63018:
            return 470060;
        case 63019:
            return 470061;
        case 63020:
            return 470056;
        case 63022:
            return 470058;
        case 63030:
            return 470008;
        case 63032:
            return 490029;
        case 63034:
            return 410017;
        case 63035:
            return 490083;
        case 63036:
            return 490079;
        case 63037:
            return 490038;

        case 57000:
            return 100128;
        case 57001:
            return 100129;
        case 57003:
            return 100135;
        case 57004:
            return 100136;
        case 57005:
            return 100131;
        case 57006:
            return 100132;
        case 58047:
            return 100205;
        case 58048:
            return 100206;
        case 58049:
            return 100207;
        case 58050:
            return 100208;
        case 58051:
            return 100003;
        case 58052:
            return 100004;
        case 55500:
            return 5900;
        case 63028:
            return 470006;
        case 63024:
            return 480075;
        case 63041:
            return 480077;
        case 63044:
            return 490096;
        case 63047:
            return 420006;
        case 63048:
            return 490098;
        case 63049:
            return 400060;
        case 63050:
            return 400077;
        case 63065:
            return 470084;
        case 63066:
            return 470085;
        case 63067:
            return 400048;
        case 63068:
            return 470004;
        case 63069:
            return 470005;
        case 63070:
            return 410004;
        case 63071:
            return 470049;

        // Patent
        case 56139:
            return 550014;
        case 56140:
            return 640011;
        case 56141:
            return 640012;
        case 56142:
            return 550013;
        case 56143:
            return 550012;
        case 56144:
            return 530006;
        case 56145:
            return 500017;
        case 56146:
            return 510020;
        case 56147:
            return 510019;
        case 56148:
            return 620004;
        case 56149:
            return 590011;
        case 56150:
            return 580009;
        case 56151:
            return 610009;
        case 56152:
            return 610008;
        case 56153:
            return 540011;
        case 56154:
            return 590012;
        case 56155:
            return 510022;
        case 56156:
            return 650004;
        case 56157:
            return 600009;
        case 56158:
            return 560008;
        case 56159:
            return 560009;
        case 56160:
            return 700013;
        case 56161:
            return 700018;
        case 56162:
            return 700019;
        case 56163:
            return 500013;
        case 56164:
            return 500014;
        case 56165:
            return 570009;
        case 56166:
            return 840001;
        case 56167:
            return 830002;
        case 56168:
            return 820001;
        case 56169:
            return 810001;
        case 56170:
            return 800002;
        case 56173:
            return 550006;
        case 58000:
            return 100009;
        case 58001:
            return 100481;
        case 58045:
            return 100412;
        case 58046:
            return 100413;

        default:
            return $item_id;
    endswitch;
}
<?php get_header(); ?>

<div class="bg-colored">
    <div class="container content">
        <?php while(have_posts()): the_post(); ?>
                <?php the_content(); ?>
        <?php endwhile; ?>
    </div><!-- /.container -->
</div>

<?php get_footer(); ?>

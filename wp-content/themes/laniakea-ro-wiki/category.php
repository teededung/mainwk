<?php get_header(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/huong-dan/') ?>">Hướng dẫn</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo single_cat_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php echo single_cat_title(); ?></h1>

            <div class="row mb-2">
                <?php if (have_posts()): ?>
                    <?php while (have_posts()): the_post(); ?>
                    <div class="col-lg-6">
                        <?php get_template_part('/parts/loop/loop-post-excerpt'); ?>
                    </div>
                    <?php endwhile; ?>
                <?php else: ?>
                    <div class="col">
                        <div class="alert alert-info">
                            Chưa có bài viết ở danh mục <?php echo single_cat_title(); ?>. Vui lòng quay lại sau.
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="mt-3">
                <?php echo wp_pagenavi(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

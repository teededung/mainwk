<?php get_header(); ?>

    <div class="container content">
        <h1 class="page-title">
            <?php echo 'Kết quả tìm kiếm cho từ khóa: <span>' ?> <?php echo get_search_query() ?></span>
        </h1>

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'parts/loop/loop-post-excerpt' ); ?>
            <?php endwhile; ?>
        <?php endif ?>
    </div>

<?php get_footer(); ?>

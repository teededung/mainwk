<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700&amp;subset=vietnamese" rel="stylesheet">
    
    <?php wp_head(); ?>
	
    <?php $scripts_in_header = get_field('scripts_in_header', 'option'); ?>
    <?php if ($scripts_in_header): ?>
        <script>
	        <?php echo $scripts_in_header; ?>
        </script>
    <?php endif; ?>
	
</head>

<body <?php body_class(); ?>>

    <!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="wrapper">

        <header class="header-page">
            <nav class="navbar fixed-top narbar-fluid navbar-light" role="navigation">
                <div class="container-fluid">
                    <a class="navbar-brand" href="<?php echo home_url('/'); ?>">LANIAKEA RO WIKI</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav" aria-controls="header-nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
			        <?php
			        wp_nav_menu( array(
                        'theme_location'    => 'header-nav',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'header-nav',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker())
			        );
			        ?>
                </div>
            </nav>
        </header>



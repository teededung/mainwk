<?php get_header(); ?>
<?php while(have_posts()): the_post() ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="<?php echo home_url('/npc/') ?>">NPC</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <div class="bg-light border rounded p-3">
            <div class="row row-parent">
                <div class="col-md-8">
                    <div class="single-npc-name mb-2">
                        <h1><?php the_title(); ?></h1>
                        <div class="single-npc-sprite mb-3">
                            <img class="img-fluid" onerror="this.src='<?php echo get_template_directory_uri() . '/images/NPC/npc-no-img.png' ?>'" src="<?php echo get_template_directory_uri() . '/images/NPC/' . get_field('sprite_constant') . '.gif'; ?>" alt="<?php the_title(); ?>">
                        </div>

                        <?php $sell_item = get_field('sell_item'); ?>
                        <?php if ($sell_item): ?>
                            <?php $shops = get_field('shops') ?>
                            <?php foreach ($shops as $shop): ?>
                                <?php
                                $currency_id = strtolower($shop['currency_item_id']);
                                switch ($currency_id):
                                    case 'zeny':
                                        $currency = 'Zeny';
                                        break;
                                    case 'cp':
                                        $currency = 'Cash Point';
                                        break;
                                    case 'point':
                                        $currency = '<code>Điểm ' . $shop['currency_item_name'] . '</code>';
                                        break;
                                    default:
                                        $currency = _item($shop['currency_item_id'], $shop['currency_item_name']);
                                        break;
                                endswitch;
                                ?>
                                <div class="mb-3">
                                    <?php $option_name = $shop['option_name'] ?>
                                    <div class="shop-title-items">
                                        <?php if ($option_name == ""): ?>
                                            <h6 class="mb-0">Các vật phẩm được bán bằng <?php echo $currency ?></h6>
                                        <?php else: ?>
                                            <h5><span class="badge badge-dark d-inline-block"><?php echo $option_name ?></span></h5>
                                            <h6 class="mb-0">Các vật phẩm ở mục này được bán bằng <?php echo $currency ?></h6>
                                        <?php endif; ?>
                                    </div>

                                    <table class="table table-border">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Vật phẩm</th>
                                            <th>Giá</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($shop['items'] as $index => $item): ?>
                                            <tr>
                                                <td><?php echo ($index+1) ?></td>
                                                <td><?php echo_item($item['item_id'], $item['item_name']); ?></td>
                                                <td><?php echo number_format($item['price'] , 0, '.', ','); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>

                            <?php endforeach; ?>

                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sticky-col">
                        <?php if (has_post_thumbnail()): ?>
                        <div class="npc-in-game mb-3">
                            <strong>Hình trong game:</strong>
                            <?php the_post_thumbnail('full', array('class' => 'img-fluid img-thumbnail')); ?>
                        </div>
                        <?php endif; ?>

                        <div class="npc-location">
                            <strong>Vị trí:</strong>
                            <?php $map = get_field('map'); ?>
                            <?php if ($map): ?>
                                <?php $location = get_field('map') . ' ' . get_field('x') . '/' . get_field('y') ?>
                                <div>
                                    <code class="clipboard" data-clipboard-text="<?php echo '/navi '. $location ?>"><?php echo $location; ?></code>
                                </div>
                            <?php else: ?>
                                Chưa có vị trí.
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

<?php
/**
 * Template Name: Trang bị & Vật phẩm
 */

$post_slug = $post->post_name;

if (file_exists ( locate_template('/trang-bi-vat-pham/page-' . $post_slug. '.php' ))){
    include locate_template('/trang-bi-vat-pham/page-' . $post_slug. '.php');
}
else {
    include locate_template('/page.php');
}
?>
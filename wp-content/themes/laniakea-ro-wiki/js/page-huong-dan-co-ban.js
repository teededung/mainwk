(function($) {

  $(window).resize(
    _.debounce(function() {
      applyScrollSpy();
    }, 500)
  );

  $('.nav-pills-custom .nav-link').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var hash = $(this).attr('href');
    if(history.pushState) {
      history.pushState(null, null, hash);
    } else {
      location.hash = hash;
    }
    TweenLite.to('.div-scroll', 0.2, { scrollTo: { y: hash, offsetY: 0 } });
  });

  function applyScrollSpy() {
    var $elementScrollSpy = $('.div-scroll');
    var $colNav = $('.col-nav');

    if (window.matchMedia("(min-width: 768px)")["matches"]) {

      const wh = $(window).height() - $('.navbar').height() - 43;
      $elementScrollSpy.height(wh);
      $colNav.height(wh + 25);
      $elementScrollSpy.scrollspy({ target: '#myScrollspy', offset: 50 });
    }
  }

  if (window.location.hash) {
    var hash = window.location.hash;
    if ($(hash).length > 0) {
      TweenLite.to('.div-scroll', 1, { scrollTo: $(hash).offset().top - 80 });
    }
  }

  applyScrollSpy();

})(jQuery);
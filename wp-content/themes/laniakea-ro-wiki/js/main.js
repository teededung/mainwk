(function($) {

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  let clipboard = new ClipboardJS('.clipboard');

  clipboard.on('success', function(e) {
    toastr["success"]("Đã copy.");
  });

  $('.clipboard').click(function(e) {
    e.preventDefault();
  });

  $('.updating-flag a').click(function(e) {
    e.preventDefault();
    toastr["warning"]("Mục này đang được cập nhật. Xin lỗi bạn vì sự bất tiện này.");
  });
  
  /**
   * Scroll to top
   */
  $(document).on( 'scroll', function(){
    if ($(window).scrollTop() > 100) {
      $('.scroll-top-wrapper').addClass('show');
    } else {
      $('.scroll-top-wrapper').removeClass('show');
    }
  });
  
  $('.scroll-top-wrapper').on('click', scrollToTop);
  
  function scrollToTop() {
    $(window).scrollTo(0, 750, { axis: 'y', ease: 'easeOut' });
  }
  
})(jQuery);








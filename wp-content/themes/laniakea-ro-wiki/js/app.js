(function($) {
  let mobileScreen = window.matchMedia('(max-width: 767px)')['matches'];

  function imageExists(image_url){
    let http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status !== 404;

  }

  if ($('#app').length > 0) {
    let vm = new Vue({
      name: 'App',
      el: '#app',
      data: {
        ajaxurl: window.location.origin + '/' + (window.location.hostname === 'localhost' ? 'laniakea-ro-wiki' : '') + '/wp-admin/admin-ajax.php',
        apiUrl: window.location.hostname === 'localhost' ? 'http://localhost:3000' : 'https://api.hahiu.com:3000',
        keyword: '',
        results: [],
        notFound: false,
        searching: false,
        searchSelectIndex: 0,

        npcSource: $('#npcSource').val(),
        showSidebar: false,

        tabActive: 'npc',

        searchContent: '',
        searchPet: '',

        loadingSidebar: false,
        loadedNPC: false,
        loadNPCFailed: false,
        loadingSkillInfo: false,

        npcList: [],
        items: [],
        mobs: [],
        pets: [],
        skills: [],

        skillTree: [],
        skillTreeURL1: '',
        skillTreeURL2: '',
        skillTreeText: '',

        howToGetContent: '',

        modalMob: {
          id: 0,
          level: 0,
          hp: 0,
          name: '',
          class: 0,
          // type: '',
          baseExp: 0,
          jobExp: 0,
          mvpExp: 0,
          def: 0,
          mdef: 0,
          // minAtk: 0,
          // maxAtk: 0,
          // minMatk: 0,
          // maxMatk: 0,
          atk1: 0,
          atk2: 0,
          mStr: 0,
          mAgi: 0,
          mVit: 0,
          mInt: 0,
          mDex: 0,
          mLuk: 0,
          size: '',
          race: '',
          element: '',
          elementLv: 1,
          speed: 0,
          drops: [],
          mvpDrops: [],
        },
        modalPet: {},
        loadingItemInfo: false,
        loadingMobInfo: false,
        noItemLink: false,
        collapseItems: [],
        collapseSKills: [],
        showGallery: false,
        shop_NPCID: 0,
        shop_currency: '',
        shop_ItemID_Price: '',
        shop_optionName: '',
        activeBuildName: '',
        buildEquips: {},
        buildEquipExample_index: 0,
        buildEquipExamples: {},
        buildDetail_itemId: 0,
        buildDetail_position: '',
        buildDetail_itemName: '',
        buildDetail_refine: 0,
        buildDetail_cards: [],
        buildDetail_options: []
      },
      methods: {
        search: _.debounce(e => {
          if (vm.keyword.length > 3) {
            vm.searchSelectIndex = 0;

            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function',
                method: 'search_posts',
                keyword: vm.keyword,
                n: TEE.n
              },
              beforeSend() {
                vm.searching = true;
                NProgress.start();
              },
              success(response) {
                vm.searching = false;
                NProgress.done();
                const {status, results} = response;
                if (typeof (status) === 'undefined') {
                  return toastr["warning"]("Lỗi tìm kiếm! Vui lòng báo lỗi cho GM.");
                }
                if (status === 'OK') {
                  vm.notFound = false;
                  vm.results = [];
                  vm.results = [...vm.results, results][0];
                } else if (status === 'Not Found') {
                  vm.notFound = true;
                  vm.results = [];
                }
              },
              error(e) {
                vm.searching = false;
                NProgress.stop();
                return toastr["warning"]("Lỗi tìm kiếm! Vui lòng báo lỗi cho GM.");
              }
            })
          } else {
            vm.notFound = false;
            vm.results = [];
          }
        },350),
        filterByType(type) {
          return _.filter(this.results, r => {
            return r.type === type;
          });
        },
        hideResults() {
          this.notFound = 0;
          this.results = [];
        },
        searchKeydown(event) {
          const length = this.results.length;
          if (length === 0) return;

          const key = event.key;

          if (key === 'Escape') {
            this.hideResults();
          }

          if (key === 'ArrowDown' || key === 'ArrowUp') {
            event.preventDefault();
          }

          if (key === 'ArrowDown') {
            this.searchSelectIndex++;
          } else if (key === 'ArrowUp') {
            this.searchSelectIndex--;
          } else if (key === 'Enter') {
            return location.href = this.results[this.searchSelectIndex-1]['permalink'];
          }

          if (this.searchSelectIndex <= 0) {
            this.searchSelectIndex = length;
          } else if (this.searchSelectIndex > length) {
            this.searchSelectIndex = 1;
          }

          _.find(this.results, (result, index) => {
            this.results[index]['select'] = ((index+1) === this.searchSelectIndex);
          });
        },
        isEmpty(obj) {
          for(let key in obj) {
            if(obj.hasOwnProperty(key))
              return false;
          }
          return true;
        },
        convertHTML(text) {
          if (!text) return text;
          let newText = "";

          // Breakline
          newText = text.split("\n").join("<br>");
          newText = newText.replaceAll("^000000", "</span>");

          // Color
          const regex = /\^[a-zA-Z-0-9]{6}/gm;
          let colors = _.uniqBy(newText.match(regex));
          _.forEach(colors, (color) => {
            const hexColor = color.replace("^", "#");
            newText = newText.replaceAll(color, `<span style='color: ${hexColor}'>`);
          });

          // Navi
          const regexNavi = /<INFO>.*?<\/INFO>/gm;
          newText = newText.replace(regexNavi, "");
          return newText;
        },
        whereNPC(id, index) {
          const _this = this;
          this.showSidebar = true;
          this.tabActive = 'npc';

          if (_this.loadedNPC) {
            // Mark NPC first
            _.forEach(_this.npcList, (npc, i) => {
                _this.npcList[i]['mark'] = (npc.id === id);
                _this.npcList[i]['index'] = index;
            });

            // Then scroll
            setTimeout(() => {
              $('.npc-list').scrollTo("#npc-" + id, 500);
            },50);

            return;
          }

          // Load all NPC if exists
          if (!_this.loadedNPC) {
            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function',
                method: 'load_npc',
                post_id: $('#post_id').val()
              },
              beforeSend() {
                _this.loadingSidebar = true;
              },
              success(response) {
                const { status, npc } = response;

                if (typeof status !== "undefined") {
                  _this.npcList = npc;
                  _this.loadedNPC = true;
                  _this.loadNPCFailed = false;
                  _this.whereNPC(id, index);
                } else {
                  _this.loadNPCFailed = true;
                }

                _this.loadingSidebar = false;
              },
              error(e) {
                _this.loadingSidebar = false;
                _this.loadNPCFailed = true;
              }
            });
          }
        },
        getItemInfo(itemId, nameFix = '') {
          const _this = this;
          _this.showSidebar = true;

          // Check is loading
          if (_this.loadingItemInfo)
            return toastr["warning"]("Đang tải thông tin vật phẩm...");

          // Check loaded
          const index = _.findIndex(_this.items, item => {
            return Number(item.itemInfo.itemId) === Number(itemId);
          });

          // If loaded
          // Show item in sidebar
          if (index !== -1) {
            _this.tabActive = 'items';

            // Mark
            _.each(_this.items, item => {
              item['mark'] = false;
            });
            _this.items[index]['mark'] = true;

            setTimeout(() => {
              if (mobileScreen) {
                _this.$refs['modalMob'].hide();
              }
              $('.npc-list').scrollTo("#item-" + itemId, 250, { axis: 'y', onAfter: () => {
                  if (_this.collapseItems.indexOf(itemId) === -1) {
                    _this.collapseItems = [..._this.collapseItems, itemId];
                    _this.$root.$emit('bv::toggle::collapse', 'collapse-item-' + itemId);
                  }
              }});
            }, 1);

            // Get How to get
            if (!_this.items[index]['loadedHowToGet']) {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function',
                  method: 'get_how_to_get',
                  itemId,
                  n: TEE.n
                },
                success(response) {
                  _this.items[index]['loadedHowToGet'] = true;

                  const {status, howToGet} = response;
                  if (typeof (status) === 'undefined') {
                    return toastr["error"]("Lỗi tìm kiếm! Vui lòng báo lỗi cho GM.");
                  }
                  if (status === 'OK') {
                    _this.items[index]['itemInfo']['howToGet'] = howToGet;
                  }

                  setTimeout(() => {
                    _this.$root.$emit('bv::toggle::collapse', 'collapse-howToGet-' + itemId);
                  }, 550);
                },
                error(e) {
                  _this.items[index]['loadedHowToGet'] = true;
                }
              });
            }

            return;
          }

          // If not loaded item
          $.ajax({
            url: _this.apiUrl + '/items/id/' + itemId,
            type: 'get',
            dataType: 'json',
            beforeSend() {
              _this.loadingSidebar = true;
              _this.loadingItemInfo = true;
              NProgress.start();
            },
            success(data) {
              _this.loadingSidebar = false;
              _this.loadingItemInfo = false;
              NProgress.done();

              let itemInfo = {...data};
              //let itemDrops = data.itemDrops;

              // Custom variable for this item
              if (nameFix === '' && itemInfo.identifiedDisplayName !== '') {
                nameFix = itemInfo.identifiedDisplayName;

                if (itemInfo.slots > 0) {
                  nameFix += ` [${itemInfo.slots}]`;
                }
              }

              itemInfo['identifiedDisplayName'] = nameFix;
              itemInfo['identifiedDescriptionName'] = JSON.parse(itemInfo['identifiedDescriptionName']);
              itemInfo['expandedDescription'] = itemInfo['identifiedDescriptionName'].length < 250;
              
              itemInfo['howToGet'] = '';
              itemInfo['updateHowToGet'] = '';

              if (typeof itemInfo === 'undefined') {
                _this.loadingSidebar = true;
                _this.loadingItemInfo = true;
                return toastr["warning"]("Không tìm thấy thông tin vật phẩm.");
              }

              const item = {
                itemInfo,
                //itemDrops,
                mark: false,
                loadedHowToGet: false
              };

              _this.items = [item, ..._this.items];

              // Call getItemInfo again
              _this.getItemInfo(itemId, nameFix);
            },
            error (e) {
              //console.log(e);
              _this.loadingItemInfo = false;
              NProgress.done();
              toastr["error"]("Lỗi! Không thể tải thông tin vật phẩm này.");
            }
          });
        },
        getMobInfo(mobId, nameFix) {
          const _this = this;

          // Check is loading
          if (_this.loadingMobInfo) {
            return toastr["warning"]("Đang tải thông tin quái vật...");
          }

          _this.loadingMobInfo = true;

          // Clear old info
          _this.modalMob = {};

          // Check loaded
          const index = _.findIndex(_this.mobs, m => {
            return Number(m.monsterId) === mobId;
          });

          // If loaded
          // Show modal mob
          if (index !== -1) {
            _this.modalMob = _this.mobs[index];

            // Increase sidebar z-index
            if (mobileScreen)
              _this.showSidebar = false;

            _this.$refs['modalMob'].show();
            return _this.loadingMobInfo = false;
          }

          // If not loaded item
          $.ajax({
            url: `${_this.apiUrl}/monsters/id/${mobId}`,
            type: 'get',
            dataType: 'json',
            beforeSend() {
              _this.loadingMobInfo = true;
              NProgress.start();
            },
            success(mobInfo) {
              _this.loadingMobInfo = false;
              NProgress.done();

              if (typeof mobInfo === 'undefined') {
                _this.loadingMobInfo = false;
                return toastr["warning"]("Không tìm thấy thông tin quái vật.");
              }

              const mobModified = {
                ...mobInfo,
                race: monsterRaceNumToText(mobInfo.race),
                element: monsterElementToText(mobInfo.element, mobInfo.elementLv),
                size: monsterSizeToText(mobInfo.size)
              }

              _this.mobs = [..._this.mobs, mobModified];
              _this.getMobInfo(mobId, nameFix);
            },
            error (e) {
              console.log(e);
              _this.loadingMobInfo = false;
              NProgress.done();
              toastr["error"]("Lỗi! Không thể tải thông tin quái vật này.");
            }
          });
        },
        getPostByCategory(category) {
          console.log(category);
        },
        filterPets: _.debounce(() => {
          const keyword = vm.searchPet.toLowerCase();
          let searchByName = vm.pets.filter(pet => {
              return pet.name.toLowerCase().includes(keyword);
          });
          let searchByFood = vm.pets.filter(pet => {
              return pet['food'][1].toLowerCase().includes(keyword);
          });
          let searchByTaming = vm.pets.filter(pet => {
            const keywordNoUnicode = change_alias(keyword);
            if (Array.isArray(pet['taming'])) {
              return change_alias(pet['taming'][1]).toLowerCase().includes(keywordNoUnicode);
            } else {
              return change_alias(pet['taming']).toLowerCase().includes(keywordNoUnicode);
            }
          });

          vm.pets.forEach(p => {
            p.show = false;
          });

          let pets = [...searchByName, ...searchByFood, ...searchByTaming];
          pets.forEach(p => {
            p.show = true;
          });
        }, 300),
        showModalPet(pet) {
          this.modalPet = pet;

          _.forEach(this.pets, p => {
            p.expand = pet.name === p.name;
          });

          setTimeout(() => {
            $(window).scrollTo("#pet-" + pet.mobID, 750, { axis: 'y' });
          }, 1);

        },
        getPetDetail(mobID, type) {
          const pet = _.find(this.pets, p => {
            return p['mobID'] === mobID;
          });

          switch (type) {
            case 'name':
              return pet['name'];
            case 'img':
              return pet['img'];
          }
        },
        skillChangeValue(jobName, skill, value) {
          const _this = this;

          // No change level for quest skill or spirit skill
          if (skill['isQuestSkill'] || skill['isSpiritSkill'])
            return;

          // Change value for required skills
          if (value > 0 && !_this.isEmpty(skill['requiredSkills'])) {
            _.each(_this.skillTree, (jobSkills, jobName) => {
              _.each(jobSkills['skills'], (sk, i) => {
                if (sk['skillID'] !== undefined) {
                  _.each(skill['requiredSkills'], skillRequired => {
                    if (skillRequired['skillID'] === sk['skillID'] && sk['currentLv'] < skillRequired['level']) {
                      setTimeout(() => {
                        const v = skillRequired['level'] - sk['currentLv'];
                        if (v > 0)
                          _this.skillChangeValue(jobName, sk, v);
                      }, 10);
                    }
                  });
                }
              });
            });
          }

          // Change level for this skill
          _.each(_this.skillTree[jobName]['skills'], (sk, i) => {
            if (sk['skillID'] === skill['skillID']) {
              // Limit to MaxLv
              if (value > _this.skillTree[jobName]['skills'][i]['max'])
                value = _this.skillTree[jobName]['skills'][i]['max'];

              if (value > 0) {
                if (_this.skillTree[jobName]['skills'][i]['currentLv'] < _this.skillTree[jobName]['skills'][i]['max']) {
                  _this.skillTree[jobName]['skills'][i]['currentLv'] += value;
                  _this.skillTree[jobName]['skillPointsUsed'] += value;
                }
              } else {
                if (_this.skillTree[jobName]['skills'][i]['currentLv'] > 0) {
                  _this.skillTree[jobName]['skills'][i]['currentLv'] += value;
                  _this.skillTree[jobName]['skillPointsUsed'] += value;
                }
              }
            }
          });

          // Change value for skill has required skills
          if (value < 0) {
            _.each(_this.skillTree, (skillData, jobName) => {
              _.each(skillData['skills'], (sk, i) => {
                // Make sure this skill has required skills
                if (sk['skillID'] !== undefined && sk['requiredSkills'] !== undefined) {
                  _.each(sk['requiredSkills'], requiredSkill => {
                    if (requiredSkill['skillID'] === skill['skillID']) {
                      if (skill['currentLv'] < requiredSkill['level']) {
                        const v = -(_this.skillTree[jobName]['skills'][i]['currentLv']);
                        _this.skillChangeValue(jobName, sk, v);
                      }
                    }
                  });
                }
              });
            });
          }

          _this.skillUpdateURL();
          _this.skillGetSummary();
        },
        resetSkill(jobName) {
          const _this = this;
          _.each(_this.skillTree[jobName]['skills'], (sk, i) => {
            if (sk['skillID'] !== undefined) {
              const v = - sk['currentLv'];
              _this.skillChangeValue(jobName, sk, v);
            }
          });
        },
        skillMouseOver(jobName, thisSkill) {
          const _this = this;

          // Find required skills
          if (!_this.isEmpty(thisSkill['requiredSkills'])) {
            _.each(_this.skillTree, (jobSkills, jobName) => {
              _.each(jobSkills['skills'], (sk, i) => {
                if (sk['skillID'] !== undefined) {
                  _.each(thisSkill['requiredSkills'], skillRequired => {
                    if (skillRequired['skillID'] === sk['skillID']) {

                      if (_this.skillTree[jobName]['skills'][i]['requiredLv'] < skillRequired['level']) {
                        _this.skillTree[jobName]['skills'][i]['required'] = true;
                        _this.skillTree[jobName]['skills'][i]['requiredLv'] = skillRequired['level'];
                      }

                      // Find more required skills
                      if (sk['requiredSkills'] !== undefined) {
                        _this.skillMouseOver(jobName, sk);
                      }
                    }
                  });
                }
              });
            });
          }
        },
        skillMouseLeave() {
          const _this = this;
          _.each(_this.skillTree, (skillData, jobName) => {
            _.each(skillData['skills'], (skill, i) => {
              if (skill['skillID'] !== undefined) {
                _this.skillTree[jobName]['skills'][i]['required'] = false;
                _this.skillTree[jobName]['skills'][i]['requiredLv'] = 0;
              }
            });
          });
        },
        skillGetSummary() {
          const _this = this;
          const length = Object.keys(_this.skillTree).length;
          let j = 0;
          _this.skillTreeText = '';

          _.each(_this.skillTree, (skillData, jobName) => {
            _this.skillTreeText += "* " + jobName + "\n";

            _.each(skillData['skills'], (skill, i) => {
              if (skill['skillID'] !== undefined && !skill['isQuestSkill'] && skill['currentLv'] > 0) {
                _this.skillTreeText += skill['skillName'] + ": " + skill['currentLv'] + "\n";
              }
            });

            if (j < (length - 1))
              _this.skillTreeText += "\n";

            j++;
          });
        },
        skillUpdateURL() {
          const _this = this;
          let newURL = '';
          let newURL2 = '';
          let array = [];

          _.each(_this.skillTree, (skillData, jobName) => {
            _.each(skillData['skills'], (skill, i) => {
              if (skill['skillID'] !== undefined) {
                if (skill['currentLv'] > 0) {
                  array.push([skill['skillID'], skill['currentLv']]);
                }
              }
            });
          });

          // Remove Basic Skill
          array = array.filter(item => {
            return item[0] !== 1;
          });

          if (array.length > 0) {
            newURL = _this.updateURLParameter(window.location.href, 'data', encodeURIComponent(JSON.stringify(array)));
            newURL2 = _this.updateURLParameter(window.location.href, 'data', JSON.stringify(array));
          } else {
            newURL = location.protocol + '//' + location.host + location.pathname;
            newURL2 = location.protocol + '//' + location.host + location.pathname;
          }
          window.history.replaceState('', '', newURL);
          _this.skillTreeURL1 = newURL;
          _this.skillTreeURL2 = newURL2;
        },
        getSkillInfo(skillID) {
          const _this = this;
          _this.showSidebar = true;

          // Check is loading
          if (_this.loadingSidebar) {
            return toastr["warning"]("Đang tải thông tin kỹ năng...");
          }

          // Check loaded skill info
          const index = _.findIndex(_this.skills, sk => {
            return Number(sk['skillID']) === skillID;
          });

          // If loaded
          // Show modal mob
          if (index !== -1) {
            _this.tabActive = 'skills';

            // Mark
            _.each(_this.skills, skill => {
              skill['mark'] = false;
            });
            _this.skills[index]['mark'] = true;

            // Show skill info in sidebar
            return setTimeout(() => {
              $('.npc-list').scrollTo("#skill-" + skillID, 250, { axis: 'y', onAfter: () => {
                  if (_this.collapseSKills.indexOf(skillID) === -1) {
                    _this.collapseSKills = [..._this.collapseSKills, skillID];
                    _this.$root.$emit('bv::toggle::collapse', 'collapse-skill-' + skillID);
                  }
              }});
            }, 1);
          }

          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function',
              method: 'get_skill_info',
              skill_id: skillID,
              get_required_skills: true,
              n: TEE.n
            },
            beforeSend() {
              _this.loadingSidebar = true;
              NProgress.start();
            },
            success(data) {
              _this.loadingSidebar = false;
              NProgress.done();

              if (typeof data['skillInfo'] === 'undefined') {
                return toastr["warning"]("Không tìm thấy thông tin kỹ năng này.");
              }

              _this.skills = [data['skillInfo'], ..._this.skills];
              _this.getSkillInfo(skillID);
            },
            error (e) {
              console.log(e);
              _this.loadingMobInfo = false;
              NProgress.done();
              toastr["error"]("Lỗi! Không tìm thấy thông tin kỹ năng này.");
            }
          });
        },
        updateURLParameter(url, param, paramVal) {
          let newAdditionalURL = "";
          let tempArray = url.split("?");
          let baseURL = tempArray[0];
          let additionalURL = tempArray[1];
          let temp = "";
          if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (let i=0; i<tempArray.length; i++){
              if(tempArray[i].split('=')[0] !== param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
              }
            }
          }
          let rowsTxt = temp + "" + param + "=" + paramVal;
          return baseURL + "?" + newAdditionalURL + rowsTxt;
        },
        copyToClipboard($event) {
          $event.target.select();
          //document.execCommand("copy");
          //toastr["success"]("Đã copy.");
        },
        scrollTo(id, isMobile = false) {
          if (isMobile && !mobileScreen)
            return;

          $(window).scrollTo(`#${id}`, 500);
        },
        notRelease() {
          return toastr["info"]("Trang này đang được xây dựng...");
        },
        errorNPCImage(npc, index) {
          const _this = this;
          _this.npcList[index]['img'] = 'npc-no-img.png';
        },
        numberWithCommas(x) {
          if (x !== undefined)
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        onSubmitHowToGet(itemId) {
          const _this = this;
          const index = _.findIndex(_this.items, item => {
            return Number(item.itemInfo.item_id) === Number(itemId);
          });

          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function',
              method: 'insert_how_to_get',
              itemId,
              howToGet: _this.items[index]['itemInfo']['updateHowToGet'],
              n: TEE.n
            },
            beforeSend() {
               $('#collapse-howToGet-' + itemId).LoadingOverlay('show');
            },
            success(response) {
              $('#collapse-howToGet-' + itemId).LoadingOverlay('hide');
              const {status, howToGet} = response;
              if (typeof (status) === 'undefined') {
                return toastr["error"]("Lỗi tìm kiếm! Vui lòng báo lỗi cho GM.");
              }
              if (status === 'OK') {
                const name = _this.items[index]['itemInfo']['name'];
                // Remove old item in sidebar
                _.remove(_this.items, item => {
                  return Number(item.itemInfo.item_id) === Number(itemId);
                });
                // Remove old item in collapse
                _.remove(_this.collapseItems, item_id => {
                  return Number(item_id) === Number(itemId);
                });

                // Load item again
                _this.$forceUpdate();
                _this.getItemInfo(itemId, name);
              } else if (status === 'Failed') {
                return toastr["error"]("Nhập thất bại!");
              }
            },
            error(e) {
              $('#collapse-howToGet-' + itemId).LoadingOverlay('hide');
              return toastr["error"]("Lỗi server!");
            }
          });
        },
        getGallery() {
          initCube();
          this.showGallery = true;
        },
        readMore(lapineID) {
          const $parent = $('#lapine-' + lapineID);
          $parent.addClass('read-more-expanded');
        },
        updateShop() {
          const _this = this;

          if ((Number(_this.shop_NPCID) === 0)) {
            return toastr['error']('NPC ID không được bỏ trống!');
          }

          if (_this.shop_ItemID_Price.length === 0) {
            return toastr['error']('Item ID: Price không được bỏ trống!');
          }

          $.ajax({
            url: TEE.ajaxurl,
            type: 'POST',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function',
              method: 'update_shop',
              shop_NPCID: _this.shop_NPCID,
              shop_currency: _this.shop_currency,
              shop_ItemID_Price: _this.shop_ItemID_Price,
              shop_optionName: _this.shop_optionName,
              n: TEE.n
            },
            beforeSend() {
              $.LoadingOverlay('show');
            },
            success(success) {
              $.LoadingOverlay('hide');

              if (success) {
                toastr['success']('Nhập vật phẩm vào shop thành công!');

              } else {
                toastr['error']('Nhập vật phẩm vào shop thất bại!');
              }
            }
          });
        },
        getBuildEquipInfo(index, itemId, type) {
          const _this = this;
          let position = "";
          let i = -1;
          _.forEach(_this.buildEquips[_this.activeBuildName], function(buildEquip, pos) {
            _.forEach(buildEquip, (b, index) => {
              if (itemId === b['item_id']) {
                position = pos;
                i = index;
                return false;
              }
            });
          });
          if (i === -1)
            return;

          switch(type) {
            case 'position':
              return position;
            case 'icon':
              return `https://whisper.hahiu.com/ro/item/${itemId}.png`;
            case 'name':
              if (typeof _this.buildEquips[_this.activeBuildName][position][i] !== "undefined")
                return (_this.buildEquips[_this.activeBuildName][position][i]['refine'] ? '+' + _this.buildEquips[_this.activeBuildName][position][i]['refine'] : '') + ' ' + _this.buildEquips[_this.activeBuildName][position][i]['item_name'];
              return '';
          }
        },
        getBuildEquipDetail(index, position, refine = 0) {
          const item_id = this.buildEquipExamples[this.activeBuildName][index][position];
          const i = _.findIndex(this.buildEquips[this.activeBuildName][position], function(o) {
            const c1 = o['item_id'] === item_id;
            const c2 = refine !== 0 ? (o['refine'] === refine) : true;
            return (c1 && c2);
          });
          this.buildDetail_itemId = item_id;
          this.buildDetail_position = position;
          this.buildDetail_itemName = this.buildEquips[this.activeBuildName][position][i]['item_name'];
          this.buildDetail_refine = this.buildEquips[this.activeBuildName][position][i]['refine'];
          this.buildDetail_cards = this.buildEquips[this.activeBuildName][position][i]['cards'].length > 0 ? [...this.buildEquips[this.activeBuildName][position][i]['cards']] : [];
          this.buildDetail_options = this.buildEquips[this.activeBuildName][position][i]['options'].length > 0 ? [...this.buildEquips[this.activeBuildName][position][i]['options']] : [];
        },
        alternativeEQs(position) {
          const _this = this;
          let eqs = [..._this.buildEquips[this.activeBuildName][position]];
          if (position === 'weapon_two_hand') {
            if (typeof _this.buildEquips[this.activeBuildName]['weapon'] !== "undefined")
              eqs.push(..._this.buildEquips[this.activeBuildName]['weapon']);
            if (typeof _this.buildEquips[this.activeBuildName]['shield'] !== "undefined")
              eqs.push(..._this.buildEquips[this.activeBuildName]['shield']);
          } else if (position === 'weapon') {
            if (typeof _this.buildEquips[this.activeBuildName]['weapon_two_hand'] !== "undefined")
              eqs.push(..._this.buildEquips[this.activeBuildName]['weapon_two_hand']);
          } else if (position === 'shield') {
            if (typeof _this.buildEquips[this.activeBuildName]['weapon_two_hand'] !== "undefined")
              eqs.push(..._this.buildEquips[this.activeBuildName]['weapon_two_hand']);
          }
          return eqs;
        },
        changeEquipExample(index, equip) {
          const position = this.getBuildEquipInfo(index, equip['item_id'], 'position');

          if (position === 'weapon' || position === 'shield' || position === 'weapon_two_hand') {
            this.buildEquipExamples[this.activeBuildName][index]['weapon'] = 0;
            this.buildEquipExamples[this.activeBuildName][index]['shield'] = 0;
            this.buildEquipExamples[this.activeBuildName][index]['weapon_two_hand'] = 0;
          }

          this.buildEquipExamples[this.activeBuildName][index][position] = equip['item_id'];
          this.getBuildEquipDetail(index, position, equip['refine']);

          if (this.buildEquipExamples[this.activeBuildName][index]['shield'] === 0 && typeof this.buildEquips[this.activeBuildName]['shield'] !== "undefined")
            this.buildEquipExamples[this.activeBuildName][index]['shield'] = this.buildEquips[this.activeBuildName]['shield'][0]['item_id'];

          if (this.buildEquipExamples[this.activeBuildName][index]['weapon'] === 0 && typeof this.buildEquips[this.activeBuildName]['weapon'] !== "undefined")
            this.buildEquipExamples[this.activeBuildName][index]['weapon'] = this.buildEquips[this.activeBuildName]['weapon'][0]['item_id'];

          this.$refs.dropdown.hide();
        },
        get_random_option_str(option) {
          const option_str = convert_random_option(option['name']);
          const option_val = option['value'];
          let output = "";
          if (option_str.includes("%")) {
            output = option_str.replace("%", (Number(option_val) === 1 ? option_val : "1~" + option_val) + "%");
          } else {
            output = option_str + ' ' + (Number(option_val) === 1 ? option_val : "1~" + option_val);
          }
          return output;
        }
      },
      computed: {
        searchNPCList() {
          let keyword = change_alias(this.searchContent.toLowerCase());
          return this.npcList.filter(n => {
            const name = change_alias(n.name.toLowerCase());
            return name.includes(keyword);
          });
        }
      },
      mounted() {
        const _this = this;

        // Focus Search Bar
        if (_this.$refs.search !== undefined) {
          _this.$refs.search.focus();
        }

        // Init Stack Table
        if ($('.stack-table').length > 0) {
          initStackTable(_this);
        }

        // Click to close sidebar
        initClickOutsideToCloseSidebar(_this);

        // Init Sticky
        if ($('.sticky-col').length > 0) {
          initStickyKit();
        }

        // Init Carousel
        initCarousel();

        // Get Build Equips
        this.$root.$on('bv::collapse::state', (collapseId, isJustShown) => {
          if (isJustShown) {
            const ele = $('.build-list').find(`[aria-controls='${collapseId}']`)[0];
            if (typeof ele !== "undefined") {
              const build_name = ele.dataset['build'];
              _this.activeBuildName = build_name.toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'');
              if (typeof _this.buildEquipExamples[_this.activeBuildName] !== "undefined")
                return;
              $.ajax({
                url: _this.ajaxurl,
                dataType: 'json',
                type: 'post',
                data: {
                  action: 'tee_ajax_function',
                  method: 'get_build_equip',
                  build_name
                },
                success(data) {
                  _this.buildEquips[_this.activeBuildName] = data['equips'];
                  _this.buildEquipExamples[_this.activeBuildName] = data['equip_examples'];
                  _this.$forceUpdate();
                }
              });
            }
          }
        });

        // Pets
        if ($('.table-pets').length > 0) {
          const url = $('#petAPI').val();
          $.ajax({
            url,
            type: 'get',
            dataType: 'json',
            beforeSend() {
              $('#collapse-4').LoadingOverlay('show');
            },
            success(data) {
              $('#collapse-4').LoadingOverlay('hide');
              _this.pets = data;
            }
          });
        }

        // Skill Tree
        const $jobName = $('#jobName');
        if ($jobName.length > 0) {
          const $skillTreeElement = $('#content-skill-tree');

          $.ajax({
            url: $("#apiURL").val(),
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function',
              method: 'get_skill_tree',
              job: $jobName.val()
            },
            beforeSend() {
              $skillTreeElement.LoadingOverlay('show');
            },
            success(response) {
              $skillTreeElement.LoadingOverlay('hide');
              const { status, skillTree } = response;

              if (typeof status !== "undefined") {
                _this.skillTree = skillTree;

                // Read data from URL
                const url = new URL(window.location.href);
                const c = url.searchParams.get("data");

                const arrayData = JSON.parse(c);

                // Update skill tree value
                _.each(_this.skillTree, (skillData, jobName) => {
                  _.each(skillData['skills'], (skill, i) => {
                    if (skill['skillID'] !== undefined) {
                      _.each(arrayData, data => {
                        if (data[0] === skill['skillID']) {
                          _this.skillChangeValue(jobName, skill, data[1]);
                        }
                      });
                    }
                  });
                });

                // Generate Skill Tree URL
                _this.skillUpdateURL();
              } else {
                // Failed
                toastr["warning"]("Trang này đang được cập nhật...");
              }
            },
            error(e) {
              // Failed
              $skillTreeElement.LoadingOverlay('hide');
              toastr["error"]("Có lỗi xảy ra, vui lòng báo lỗi cho GM Tee.");
            }
          });
        }

        // Bootstrap-Vue: collapse events
        this.$root.$on('bv::collapse::state', (collapseId, isJustShown) => {
          // Collapse Skill
          if (collapseId.includes("collapse-skill")) {
            const id = Number(collapseId.replace("collapse-skill-", ""));
            if (!isJustShown) {
              _.remove(_this.collapseSKills, skillID => {
                return skillID === id;
              });
            }
          }
          // Collapse Item
          else if (collapseId.includes("collapse-item")) {
            const id = Number(collapseId.replace("collapse-item-", ""));
            if (!isJustShown) {
              // Will Collapse Again?
              _.remove(_this.collapseItems, itemID => {
                return itemID === id;
              });
            }
          }
        });

        setTimeout(() => {
          const particlesCount = $('.particles').length;
          for (let i = 0; i < particlesCount; i++) {
            initParticles('particles-js-'+i);
          }
        },639);
      },
      watch: {
        loadingSidebar(loading) {
          if (loading) {
            $('#sidebar-app').LoadingOverlay('show');
          } else {
            $('#sidebar-app').LoadingOverlay('hide');
          }
        }
      }
    });
  }

  function initParticles(el) {
    particlesJS(el,
      {
        "particles": {
          "number": {
            "value": 10,
            "density": {
              "enable": true,
              "value_area": 25
            }
          },
          "color": {
            "value": "#ffffff"
          },
          "shape": {
            "type": "triangle",
            "stroke": {
              "width": 0.3,
              "color": "#ffffff"
            },
            "polygon": {
              "nb_sides": 5
            },
            "image": {
              "src": "img/github.svg",
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 1,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 100,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 45,
            "color": "#e0f2f1",
            "opacity": 0.7,
            "width": 2
          },
          "move": {
            "enable": true,
            "speed": 3,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": false,
              "mode": "repulse"
            },
            "onclick": {
              "enable": true,
              "mode": "push"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 400,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 40,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      }
    );
  }

  function initStackTable(vm) {
    $('.stack-table').cardtable();
    $('.small-only .item-name').on('click', function(e) {
      e.preventDefault();
      const type = $(this).data('type');
      if (type === "item-info")
        vm.getItemInfo($(this).data('id'), $(this).data('name'));
      else if (type === "mob-info")
        vm.getMobInfo($(this).data('id'), $(this).data('name'));
      else if (type === "skill-info")
        vm.getSkillInfo($(this).data('id'))
    });
  }

  function initClickOutsideToCloseSidebar(_this) {
    $(document).bind('mouseup touchend', function(e) {
      const ignoreEl = $(".npc-list");
      const ignoreEl2 = $(".btn-toggle-npn-list");
      const ignoreEl3 = $(".where-npc");
      const ignoreEl4 = $(".item-name");
      const ignoreEl5 = $("#modal-mob");
      const ignoreEl6 = $(".skill-name");
      if (
          !ignoreEl.is(e.target) && ignoreEl.has(e.target).length === 0 &&
          !ignoreEl2.is(e.target) && ignoreEl2.has(e.target).length === 0 &&
          !ignoreEl3.is(e.target) && ignoreEl3.has(e.target).length === 0 &&
          !ignoreEl4.is(e.target) && ignoreEl4.has(e.target).length === 0 &&
          !ignoreEl5.is(e.target) && ignoreEl5.has(e.target).length === 0 &&
          !ignoreEl6.is(e.target) && ignoreEl6.has(e.target).length === 0
      ) {
        _this.showSidebar = false;
      }
    });
  }
  
  function initStickyKit() {
    if (!mobileScreen) {
      $(".sticky-col").stick_in_parent({
        parent: '.row-parent'
      });
    }
  }

  function initCarousel() {
    const $carousel = $('.base-jobs-carousel');
    const startPosition = $('#start-position').val();
    if ($carousel.length) {
      let $owl = $carousel.owlCarousel({
        center: true,
        items: 2,
        loop: false,
        margin: 0,
        touchDrag: true,
        mouseDrag: true,
        pullDrag: false,
        smartSpeed: 600,
        nav: false,
        dot: true
      });

      setTimeout(() => {
        $owl.trigger("to.owl.carousel", startPosition);
      }, 369);
    }
  }

  function initCube() {
    $('#post-gallery').cubeportfolio({
      layoutMode: 'mosaic',
      sortToPreventGaps: true,
      defaultFilter: '*',
      animationType: 'quicksand',
      gapHorizontal: 5,
      gapVertical: 5,
      gridAdjustment: 'responsive',
      mediaQueries: [{
        width: 1500,
        cols: 4,
      }, {
        width: 1100,
        cols: 3,
      }, {
        width: 800,
        cols: 3,
      }, {
        width: 480,
        cols: 2,
      }],
      caption: 'zoom',
      displayType: 'sequentially',
      displayTypeSpeed: 100,

      // lightbox
      lightboxDelegate: '.cbp-lightbox',
      lightboxGallery: true,
      lightboxTitleSrc: 'data-title',
      lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} của {{total}}</div>'
    });
  }

  function change_alias(alias) {
    let str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    str = str.trim();
    return str;
  }

  function monsterRaceNumToText(num) {
    switch (num) {
      case 0:
        return "Vô dạng (Formless)";
      case 1:
        return "Thây ma (Undead)";
      case 2:
        return "Quái thú (Brute)";
      case 3:
        return "Cây (Plant)";
      case 4:
        return "Côn trùng (Insect)";
      case 5:
        return "Cá (Fish)";
      case 6:
        return "Quỷ (Demon)";
      case 7:
        return "Á thần (Demi-human)";
      case 8:
        return "Thiên thần (Angel)";
      case 9:
        return "Rồng (Dragon)";
      default:
        return "Không xác định (Unknown)";
    }
  }

  function monsterElementToText(num, level = 0) {
    let element = "";
    switch (num) {
      case 0:
        element = "Trung tính (Neutral)";
        break;
      case 1:
        element = "Nước (Water)";
        break;
      case 2:
        element = "Đất (Earth)";
        break;
      case 3:
        element = "Lửa (Fire)";
        break;
      case 4:
        element = "Gió (Wind)";
        break;
      case 5:
        element = "Độc (Poison)";
        break;
      case 6:
        element = "Thánh (Holy)";
        break;
      case 7:
        element = "Bóng tối (Dark)";
        break;
      case 8:
        element = "Hồn ma (Ghost)";
        break;
      case 9:
        element = "Xác sống (Undead)";
        break;
      default:
        element = "Không xác định (Unknown)";
        break;
    }

    if (level > 0)
      element = `${element} ${level}`;

    return element;
  }

  function monsterSizeToText(size) {
    switch (size) {
      case 0:
        return "Nhỏ (Small)";
      case 1:
        return "Vừa (Medium)";
      case 2:
        return "Lớn (Large)";
      default:
        return "Không xác định (Unknown)";
    }
  }

  function convert_random_option($name) {
    switch ($name){
      default:
        return $name;
      case 'VAR_MAXHPAMOUNT':
        return 'MaxHP';
      case 'VAR_MAXSPAMOUNT':
        return 'MaxSP';
      case 'VAR_STRAMOUNT':
        return 'STR';
      case 'VAR_AGIAMOUNT':
        return 'AGI';
      case 'VAR_VITAMOUNT':
        return 'VIT';
      case 'VAR_INTAMOUNT':
        return 'INT';
      case 'VAR_DEXAMOUNT':
        return 'DEX';
      case 'VAR_LUKAMOUNT':
        return 'LUK';
      case 'VAR_MAXHPPERCENT':
        return 'MaxHP %';
      case 'VAR_MAXSPPERCENT':
        return 'MaxSP %';
      case 'VAR_HPACCELERATION':
        return 'Tốc độ hồi phục HP %';
      case 'VAR_SPACCELERATION':
        return 'Tốc độ hồi phục SP %';
      case 'VAR_ATKPERCENT':
        return 'ATK %';
      case 'VAR_MAGICATKPERCENT':
        return 'MATK %';
      case 'VAR_PLUSASPD':
        return 'ASPD';
      case 'VAR_PLUSASPDPERCENT':
        return 'ASPD %';
      case 'VAR_ATTPOWER':
        return 'ATK';
      case 'VAR_HITSUCCESSVALUE':
        return 'HIT';
      case 'VAR_ATTMPOWER':
        return 'MATK';
      case 'VAR_ITEMDEFPOWER':
        return 'DEF';
      case 'VAR_MDEFPOWER':
        return 'MDEF';
      case 'VAR_AVOIDSUCCESSVALUE':
        return 'FLEE';
      case 'VAR_PLUSAVOIDSUCCESSVALUE':
        return 'Perfect Dodge';
      case 'VAR_CRITICALSUCCESSVALUE':
        return 'CRIT';
      case 'ATTR_TOLERACE_NOTHING':
        return 'Kháng % thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'ATTR_TOLERACE_WATER':
        return 'Kháng % thuộc tính <span style="color: #0000BB;">Nước</span>';
      case 'ATTR_TOLERACE_GROUND':
        return 'Kháng % thuộc tính <span style="color: #CC5500;">Đất</span>';
      case 'ATTR_TOLERACE_FIRE':
        return 'Kháng % thuộc tính <span style="color: #FF0000;">Lửa</span>';
      case 'ATTR_TOLERACE_WIND':
        return 'Kháng % thuộc tính <span style="color: #33CC00;">Gió</span>';
      case 'ATTR_TOLERACE_POISON':
        return 'Kháng % thuộc tính <span style="color: #663399;">Độc</span>';
      case 'ATTR_TOLERACE_SAINT':
        return 'Kháng % thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'ATTR_TOLERACE_DARKNESS':
        return 'Kháng % thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'ATTR_TOLERACE_TELEKINESIS':
        return 'Kháng % thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'ATTR_TOLERACE_UNDEAD':
        return 'Kháng % thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'ATTR_TOLERACE_ALLBUTNOTHING':
        return 'Kháng % tất cả thuộc tính, trừ <span style="color: #777777;">Trung tính</span>';
      case 'DAMAGE_PROPERTY_NOTHING_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'DAMAGE_PROPERTY_NOTHING_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'DAMAGE_PROPERTY_WATER_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #0000BB;">Nước</span>';
      case 'DAMAGE_PROPERTY_WATER_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>';
      case 'DAMAGE_PROPERTY_GROUND_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #CC5500;">Đất</span>';
      case 'DAMAGE_PROPERTY_GROUND_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>';
      case 'DAMAGE_PROPERTY_FIRE_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>';
      case 'DAMAGE_PROPERTY_FIRE_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>';
      case 'DAMAGE_PROPERTY_WIND_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #33CC00;">Gió</span>';
      case 'DAMAGE_PROPERTY_WIND_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>';
      case 'DAMAGE_PROPERTY_POISON_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #663399;">Độc</span>';
      case 'DAMAGE_PROPERTY_POISON_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #663399;">Độc</span>';
      case 'DAMAGE_PROPERTY_SAINT_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'DAMAGE_PROPERTY_SAINT_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'DAMAGE_PROPERTY_DARKNESS_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'DAMAGE_PROPERTY_DARKNESS_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'DAMAGE_PROPERTY_TELEKINESIS_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'DAMAGE_PROPERTY_TELEKINESIS_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'DAMAGE_PROPERTY_UNDEAD_USER':
        return 'Giảm % ATK nhận từ quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'DAMAGE_PROPERTY_UNDEAD_TARGET':
        return 'Tăng % ATK lên quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'MDAMAGE_PROPERTY_NOTHING_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'MDAMAGE_PROPERTY_NOTHING_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'MDAMAGE_PROPERTY_WATER_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #0000BB">Nước</span>';
      case 'MDAMAGE_PROPERTY_WATER_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #0000BB">Nước</span>';
      case 'MDAMAGE_PROPERTY_GROUND_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #CC5500">Đất</span>';
      case 'MDAMAGE_PROPERTY_GROUND_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #CC5500">Đất</span>';
      case 'MDAMAGE_PROPERTY_FIRE_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #FF0000">Lửa</span>';
      case 'MDAMAGE_PROPERTY_FIRE_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #FF0000">Lửa</span>';
      case 'MDAMAGE_PROPERTY_WIND_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #33CC00">Gió</span>';
      case 'MDAMAGE_PROPERTY_WIND_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #33CC00">Gió</span>';
      case 'MDAMAGE_PROPERTY_POISON_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #663399">Độc</span>';
      case 'MDAMAGE_PROPERTY_POISON_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #663399">Độc</span>';
      case 'MDAMAGE_PROPERTY_SAINT_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'MDAMAGE_PROPERTY_SAINT_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'MDAMAGE_PROPERTY_DARKNESS_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'MDAMAGE_PROPERTY_DARKNESS_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'MDAMAGE_PROPERTY_TELEKINESIS_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'MDAMAGE_PROPERTY_TELEKINESIS_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'MDAMAGE_PROPERTY_UNDEAD_USER':
        return 'Giảm % thiệt hại MATK thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'MDAMAGE_PROPERTY_UNDEAD_TARGET':
        return 'Tăng % MATK lên quái vật thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'BODY_ATTR_NOTHING':
        return 'Thuộc tính áo giáp: <span style="color: #777777;">Trung tính</span>';
      case 'BODY_ATTR_WATER':
        return 'Thuộc tính áo giáp: <span style="color: #0000BB">Nước</span>';
      case 'BODY_ATTR_GROUND':
        return 'Thuộc tính áo giáp: <span style="color: #CC5500">Đất</span>';
      case 'BODY_ATTR_FIRE':
        return 'Thuộc tính áo giáp: <span style="color: #FF0000">Lửa</span>';
      case 'BODY_ATTR_WIND':
        return 'Thuộc tính áo giáp: <span style="color: #33CC00">Gió</span>';
      case 'BODY_ATTR_POISON':
        return 'Thuộc tính áo giáp: <span style="color: #663399">Độc</span>';
      case 'BODY_ATTR_SAINT':
        return 'Thuộc tính áo giáp: <span style="color: #777777;">Thánh</span>';
      case 'BODY_ATTR_DARKNESS':
        return 'Thuộc tính áo giáp: <span style="color: #777777;">Bóng tối</span>';
      case 'BODY_ATTR_TELEKINESIS':
        return 'Thuộc tính áo giáp: <span style="color: #777777;">Hồn ma</span>';
      case 'BODY_ATTR_UNDEAD':
        return 'Thuộc tính áo giáp: <span style="color: #777777;">Thây ma</span>';
      case 'RACE_TOLERACE_NOTHING':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_TOLERACE_UNDEAD':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_TOLERACE_ANIMAL':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_TOLERACE_PLANT':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_TOLERACE_INSECT':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_TOLERACE_FISHS':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_TOLERACE_DEVIL':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_TOLERACE_HUMAN':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
      case 'RACE_TOLERACE_ANGEL':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_TOLERACE_DRAGON':
        return 'Giảm % sát thương nhận từ chủng loài <span style="color: #FF0000;">Rồng</span>';
      case 'RACE_DAMAGE_NOTHING':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_DAMAGE_UNDEAD':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_DAMAGE_ANIMAL':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_DAMAGE_PLANT':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_DAMAGE_INSECT':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_DAMAGE_FISHS':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_DAMAGE_DEVIL':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_DAMAGE_HUMAN':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
      case 'RACE_DAMAGE_ANGEL':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_DAMAGE_DRAGON':
        return 'Tăng % ATK lên chủng loài <span style="color: #FF0000;">Rồng</span>';
      case 'RACE_MDAMAGE_NOTHING':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_MDAMAGE_UNDEAD':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_MDAMAGE_ANIMAL':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_MDAMAGE_PLANT':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_MDAMAGE_INSECT':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_MDAMAGE_FISHS':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_MDAMAGE_DEVIL':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_MDAMAGE_HUMAN':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
      case 'RACE_MDAMAGE_ANGEL':
        return 'Tăng % MATK lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_MDAMAGE_DRAGON':
        return 'Tăng % MATK lên chủng  <span style="color: #FF0000;">Rồng</span>';
      case 'RACE_CRI_PERCENT_NOTHING':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_CRI_PERCENT_UNDEAD':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_CRI_PERCENT_ANIMAL':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_CRI_PERCENT_PLANT':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_CRI_PERCENT_INSECT':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_CRI_PERCENT_FISHS':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_CRI_PERCENT_DEVIL':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_CRI_PERCENT_HUMAN':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Á thần</span>';
      case 'RACE_CRI_PERCENT_ANGEL':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_CRI_PERCENT_DRAGON':
        return 'Tăng % CRIT lên chủng loài <span style="color: #FF0000;">Rồng</span>';
      case 'RACE_IGNORE_DEF_PERCENT_NOTHING':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_IGNORE_DEF_PERCENT_UNDEAD':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_IGNORE_DEF_PERCENT_ANIMAL':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_IGNORE_DEF_PERCENT_PLANT':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_IGNORE_DEF_PERCENT_INSECT':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_IGNORE_DEF_PERCENT_FISHS':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_IGNORE_DEF_PERCENT_DEVIL':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_IGNORE_DEF_PERCENT_HUMAN':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
      case 'RACE_IGNORE_DEF_PERCENT_ANGEL':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_IGNORE_DEF_PERCENT_DRAGON':
        return 'Xuyên % DEF của chủng loài <span style="color: #FF0000;">Rồng</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_NOTHING':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Vô dạng</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_UNDEAD':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thây ma</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_ANIMAL':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Quái thú</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_PLANT':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thực vật</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_INSECT':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Côn trùng</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_FISHS':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Cá</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_DEVIL':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Quỷ</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_HUMAN':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Á thần (trừ Người chơi)</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_ANGEL':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Thiên thần</span>';
      case 'RACE_IGNORE_MDEF_PERCENT_DRAGON':
        return 'Xuyên % MDEF của chủng loài <span style="color: #FF0000;">Rồng</span>';
      case 'CLASS_DAMAGE_NORMAL_TARGET':
        return 'Tăng % ATK lên quái vật Thường';
      case 'CLASS_DAMAGE_BOSS_TARGET':
        return 'Tăng % ATK lên quái vật Trùm';
      case 'CLASS_DAMAGE_NORMAL_USER':
        return 'Giảm %d sát thương nhận từ quái vật Thường';
      case 'CLASS_DAMAGE_BOSS_USER':
        return 'Giảm % sát thương nhận từ quái vật Trùm';
      case 'CLASS_MDAMAGE_NORMAL':
        return 'Tăng % MATK lên quái vật Thường';
      case 'CLASS_MDAMAGE_BOSS':
        return 'Tăng % MATK lên quái vật Trùm';
      case 'CLASS_IGNORE_DEF_PERCENT_NORMAL':
        return 'Xuyên % DEF quái vật Thường';
      case 'CLASS_IGNORE_DEF_PERCENT_BOSS':
        return 'Xuyên % DEF quái vật Trùm';
      case 'CLASS_IGNORE_MDEF_PERCENT_NORMAL':
        return 'Xuyên % MDEF quái vật Thường';
      case 'CLASS_IGNORE_MDEF_PERCENT_BOSS':
        return 'Xuyên % MDEF quái vật Trùm';
      case 'DAMAGE_SIZE_SMALL_TARGET':
        return 'Tăng % ATK lên quái vật cỡ nhỏ';
      case 'DAMAGE_SIZE_MIDIUM_TARGET':
        return 'Tăng % ATK lên quái vật cỡ vừa';
      case 'DAMAGE_SIZE_LARGE_TARGET':
        return 'Tăng % ATK lên quái vật cỡ lớn';
      case 'DAMAGE_SIZE_SMALL_USER':
        return 'Giảm % ATK nhận từ quái vật cỡ nhỏ';
      case 'DAMAGE_SIZE_MIDIUM_USER':
        return 'Giảm % ATK nhận từ quái vật cỡ vừa';
      case 'DAMAGE_SIZE_LARGE_USER':
        return 'Giảm % ATK nhận từ quái vật cỡ lớn';
      case 'DAMAGE_SIZE_PERFECT':
        return 'Kích cỡ quái vật không giảm sát thương từ vũ khí';
      case 'DAMAGE_CRI_TARGET':
        return 'Tăng % sát thương chí mạng';
      case 'DAMAGE_CRI_USER':
        return 'Giảm % sát thương chí mạng nhận từ đối phương';
      case 'RANGE_ATTACK_DAMAGE_TARGET':
        return 'Tăng % sát thương vật lý tầm xa';
      case 'RANGE_ATTACK_DAMAGE_USER':
        return 'Giảm % sát thương vật lý tầm xa nhận từ đối phương';
      case 'HEAL_VALUE':
        return 'Tăng % lượng hồi phục các kỹ năng HEAL';
      case 'HEAL_MODIFY_PERCENT':
        return 'Tăng % lượng hồi phục kỹ năng HEAL nhân từ người chơi khác';
      case 'DEC_SPELL_CAST_TIME':
        return 'Giảm % thời gian thi triển kỹ năng';
      case 'DEC_SPELL_DELAY_TIME':
        return 'Giảm % thời gian delay kỹ năng';
      case 'DEC_SP_CONSUMPTION':
        return 'Giảm % độ tiêu hao SP của các kỹ năng';
      case 'WEAPON_ATTR_NOTHING':
        return 'Thuộc tính vũ khí: <span style="color: #777777;">Trung tính</span>';
      case 'WEAPON_ATTR_WATER':
        return 'Thuộc tính vũ khí: <span style="color: #0000BB;">Nước</span>';
      case 'WEAPON_ATTR_GROUND':
        return 'Thuộc tính vũ khí: <span style="color: #CC5500;">Đất</span>';
      case 'WEAPON_ATTR_FIRE':
        return 'Thuộc tính vũ khí: <span style="color: #FF0000;">Lửa</span>';
      case 'WEAPON_ATTR_WIND':
        return 'Thuộc tính vũ khí: <span style="color: #33CC00;">Gió</span>';
      case 'WEAPON_ATTR_POISON':
        return 'Thuộc tính vũ khí: <span style="color: #663399;">Độc</span>';
      case 'WEAPON_ATTR_SAINT':
        return 'Thuộc tính vũ khí: <span style="color: #777777;">Thánh</span>';
      case 'WEAPON_ATTR_DARKNESS':
        return 'Thuộc tính vũ khí: <span style="color: #777777;">Bóng tối</span>';
      case 'WEAPON_ATTR_TELEKINESIS':
        return 'Thuộc tính vũ khí: <span style="color: #777777;">Hồn ma</span>';
      case 'WEAPON_ATTR_UNDEAD':
        return 'Thuộc tính vũ khí: <span style="color: #777777;">Thây ma</span>';
      case 'WEAPON_INDESTRUCTIBLE':
        return 'Vũ khí không bị hư trong giao tranh';
      case 'BODY_INDESTRUCTIBLE':
        return 'Giáp không bị hư trong giao tranh';
      case 'MDAMAGE_SIZE_SMALL_TARGET':
        return 'Tăng % MATK lên quái vật cỡ nhỏ';
      case 'MDAMAGE_SIZE_MIDIUM_TARGET':
        return 'Tăng % MATK lên quái vật cỡ vừa';
      case 'MDAMAGE_SIZE_LARGE_TARGET':
        return 'Tăng % MATK lên quái vật cỡ lớn';
      case 'MDAMAGE_SIZE_SMALL_USER':
        return 'Giảm % MATK nhận từ quái vật cỡ nhỏ';
      case 'MDAMAGE_SIZE_MIDIUM_USER':
        return 'Giảm % MATK nhận từ quái vật cỡ vừa';
      case 'MDAMAGE_SIZE_LARGE_USER':
        return 'Giảm % MATK nhận từ quái vật cỡ lớn';
      case 'MELEE_ATTACK_DAMAGE_TARGET':
        return 'Tăng % sát thương vật lý cận chiến';
      case 'MELEE_ATTACK_DAMAGE_USER':
        return 'Kháng % sát thương vật lý cận chiến';
      case 'ADDSKILLMDAMAGE_NOTHING':
        return 'Tăng % MATK thuộc tính <span style="color: #777777;">Trung tính</span>';
      case 'ADDSKILLMDAMAGE_WATER':
        return 'Tăng % MATK thuộc tính <span style="color: #0000BB;">Nước</span>';
      case 'ADDSKILLMDAMAGE_GROUND':
        return 'Tăng % MATK thuộc tính <span style="color: #CC5500;">Đất</span>';
      case 'ADDSKILLMDAMAGE_FIRE':
        return 'Tăng % MATK thuộc tính <span style="color: #FF0000;">Lửa</span>';
      case 'ADDSKILLMDAMAGE_WIND':
        return 'Tăng % MATK thuộc tính <span style="color: #33CC00;">Gió</span>';
      case 'ADDSKILLMDAMAGE_POISON':
        return 'Tăng % MATK thuộc tính <span style="color: #663399;">Độc</span>';
      case 'ADDSKILLMDAMAGE_SAINT':
        return 'Tăng % MATK thuộc tính <span style="color: #777777;">Thánh</span>';
      case 'ADDSKILLMDAMAGE_DARKNESS':
        return 'Tăng % MATK thuộc tính <span style="color: #777777;">Bóng tối</span>';
      case 'ADDSKILLMDAMAGE_TELEKINESIS':
        return 'Tăng % MATK thuộc tính <span style="color: #777777;">Hồn ma</span>';
      case 'ADDSKILLMDAMAGE_UNDEAD':
        return 'Tăng % MATK thuộc tính <span style="color: #777777;">Thây ma</span>';
      case 'ATTR_TOLERACE_ALL':
        return 'Kháng % tất cả thuộc tính';
    }
  }
})(jQuery);
<?php
/**
 * Template Name: Build
 */
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/builds/'); ?>">Builds</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="display-4 mb-5"><?php the_title(); ?></h1>

            <div class="bg-light p-3 rounded border">
                <section class="build-section" id="overview">
                    <div class="row">
                        <div class="col-lg-8">
                            <h2 class="color-600">Tổng quan</h2>
                            <?php the_content(); ?>

                            <?php
                            global $post;
                            $post_slug = $post->post_name;
                            $skill_tree_link = home_url('/skill-tree/' . $post_slug . "/");
                            ?>
                            <a target="_blank" href="<?php echo $skill_tree_link ?>" class="btn btn-primary mb-3 mb-lg-0">Bảng kỹ năng của <?php the_title(); ?></a>
                        </div>
                        <div class="col-lg-4">
                            <div class="sidebar-job-info text-center">
                                <div class="header-job-info">
                                    <h4 class="title-sidebar">Job Bases</h4>
                                </div>
                                <div class="body-job-info">
                                    <?php $job_bases = get_field('job_bases'); ?>

                                    <input type="hidden" id="start-position" hidden value="<?php echo count($job_bases); ?>">

                                    <div class="owl-carousel owl-theme base-jobs-carousel">
                                        <?php if ($job_bases): ?>
                                            <?php foreach($job_bases as $job): ?>
                                                <div class="item job-item">
                                                    <?php $img = $job['image']; ?>
                                                    <?php if ($img): ?>
                                                        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt']; ?>">
                                                    <?php endif; ?>
                                                    <span><i class="ion ion-ios-arrow-forward"></i></span>
                                                    <h5><?php echo $job['job_name'] ?></h5>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <div class="item job-item">
                                            <?php if (has_post_thumbnail()): ?>
                                                <?php the_post_thumbnail('full', array('class' => 'img-fluid')) ?>
                                            <?php endif; ?>
                                            <h5><?php the_title(); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer-job-info">
                                    <h4 class="title-sidebar">Job Bonuses</h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STR</th>
                                            <th>AGI</th>
                                            <th>VIT</th>
                                            <th>INT</th>
                                            <th>DEX</th>
                                            <th>LUK</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>+<?php the_field('str') ?></td>
                                            <td>+<?php the_field('agi') ?></td>
                                            <td>+<?php the_field('vit') ?></td>
                                            <td>+<?php the_field('int') ?></td>
                                            <td>+<?php the_field('dex') ?></td>
                                            <td>+<?php the_field('luk') ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <?php include locate_template('/parts/build/main-block.php') ?>

            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

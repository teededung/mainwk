<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<div class="bg-colored">
    <div class="container content-sm">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>
        <h1 class="mb-5">Cây kỹ năng</h1>

        <div class="mb-3">
            <div id="branch-swordsman-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Kiếm sĩ</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/swordsman/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Kiếm sĩ</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Swordman.png' ?>" alt="Swordsman">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/knight/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Hiệp sĩ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Knight.png' ?>" alt="Knight">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/lord-knight/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Đại hiệp sĩ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Lord_Knight.png' ?>" alt="Lord Knight">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/rune-knight/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Hiệp sĩ cổ ngữ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rune_Knight.png' ?>" alt="Rune Knight">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/crusader/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Chiến binh thập tự</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Crusader.png' ?>" alt="Crusader">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/paladin/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Thánh kỵ binh</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Paladin.png' ?>" alt="Paladin">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/royal-guard/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Hộ vệ Hoàng gia</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Royal_Guard.png' ?>" alt="Royal Guard">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-archer-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary color-primary">Nhánh nghề Cung thủ</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/archer/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Cung thủ</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Archer.png' ?>" alt="Archer">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/hunter/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Thợ săn</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Hunter.png' ?>" alt="Hunter">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/sniper/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Xạ thủ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sniper.png' ?>" alt="Sniper">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/ranger/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Siêu xạ thủ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Ranger.png' ?>" alt="Ranger">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <div class="block-job has-caret text-center">
                                    <div class="label">Nghề 2</div>
                                    <a href="<?php echo home_url('/skill-tree/bard/'); ?>">
                                        <div class="job-name">Nhạc công</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Bard.png' ?>" alt="Bard">
                                    </a>
                                    <a href="<?php echo home_url('/skill-tree/dancer/'); ?>">
                                        <div class="job-name">Vũ công</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Dancer.png' ?>" alt="Dancer">
                                    </a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="block-job has-caret text-center">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <a href="<?php echo home_url('/skill-tree/minstrel/'); ?>">
                                        <div class="job-name">Nhạc sĩ</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Minstrel.png' ?>" alt="Minstrel">
                                    </a>
                                    <a href="<?php echo home_url('/skill-tree/gypsy/'); ?>">
                                        <div class="job-name">Vũ thần</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Gypsy.png' ?>" alt="Gypsy">
                                    </a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="block-job text-center">
                                    <div class="label">Nghề 3</div>
                                    <a href="<?php echo home_url('/skill-tree/minstrel/'); ?>">
                                        <div class="job-name">Nhạc trưởng</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Maestro.png' ?>" alt="Minstrel">
                                    </a>
                                    <a href="<?php echo home_url('/skill-tree/wanderer/'); ?>">
                                        <div class="job-name">Vũ thần lang thang</div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Wanderer.png' ?>" alt="Wanderer">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-mage-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Pháp sư</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/magician/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Pháp sư</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Mage.png' ?>" alt="Magician">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/wizard/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Phù thủy</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Wizard.png' ?>" alt="Wizard">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/high-wizard/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Đại phù thủy</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/High_Wizard.png' ?>" alt="High Wizard">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/warlock/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Phù thủy dị giáo</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Warlock.png' ?>" alt="Warlock">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/sage/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Hiền triết</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sage.png' ?>" alt="Sage">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/professor/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Học giả</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Scholar.png' ?>" alt="Scholar">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/sorcerer/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Mị thuật sư</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sorcerer.png' ?>" alt="Sorcerer">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-merchant-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Thương gia</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/merchant/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Thương gia</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Merchant.png' ?>" alt="Merchant">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/blacksmith/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Thợ rèn</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Blacksmith.png' ?>" alt="Blacksmith">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/whitesmith/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Thợ thiếc</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Mastersmith.png' ?>" alt="Whitesmith">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/mechanic/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Thợ máy</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Mechanic.png' ?>" alt="Mechanic">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/alchemist/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Nhà giả kim</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Alchemist.png' ?>" alt="Alchemist">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/creator/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Nhà hóa sinh học</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Biochemist.png' ?>" alt="Biochemist">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/genetic/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Nhà di truyền học</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Geneticist.png' ?>" alt="Geneticist">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-thief-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Đạo chích</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/thief/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Đạo chích</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Thief.png' ?>" alt="Thief">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/assassin/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Sát thủ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Assassin.png' ?>" alt="Assassin">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/assassin-cross/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Đại sát thủ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Assassin_Cross.png' ?>" alt="Assassin Cross">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/guillotine-cross/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Đao phủ thập tự</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Guillotine_Cross.png' ?>" alt="Guillotine Cross">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/rogue/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Đạo tặc</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rogue.png' ?>" alt="Rogue">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/stalker/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Truy tặc</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Stalker.png' ?>" alt="Stalker">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/shadow-chaser/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Hắc tặc</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Shadow_Chaser.png' ?>" alt="Shadow Chaser">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-acolyte-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Tu sĩ</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/acolyte/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Tu sĩ</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Acolyte.png' ?>" alt="Acolyte">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/priest/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Linh mục</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Priest.png' ?>" alt="Priest">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/high-priest/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Đại linh mục</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/High_Priest.png' ?>" alt="High Priest">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/arch-bishop/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Giám mục</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Arch_Bishop.png' ?>" alt="Arch Bishop">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/monk/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Thầy tu</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Monk.png' ?>" alt="Monk">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job has-caret text-center" href="<?php echo home_url('/skill-tree/champion/'); ?>">
                                    <div class="label">Nghề 2 (Tái sinh)</div>
                                    <div class="job-name">Quyền vương</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Champion.png' ?>" alt="Champion">
                                </a>
                            </div>
                            <div class="col-4">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/sura/'); ?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Quyền sư</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Sura.png' ?>" alt="Sura">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-ninja-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Nhẫn giả</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/ninja/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Nhẫn giả</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Ninja.png' ?>" alt="Ninja">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/kagerou/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Ảnh sư</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Kagerou.png' ?>" alt="Kagerou">
                                </a>
                            </div>
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/oboro/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">U nữ</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Oboro.png' ?>" alt="Oboro">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-gunslinger-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Xạ thủ</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/gunslinger/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Thiện xạ</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/Gunslinger.png' ?>" alt="Gunslinger">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center">
                            <div class="col">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/rebellion/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Phiến quân</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Rebel.png' ?>" alt="Rebellion">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="branch-taekwon-jobs" class="mb-3 p-3 border-s block-jobs">
                <h5 class="mb-3 color-primary">Nhánh nghề Võ sĩ</h5>

                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a class="block-job has-caret text-center mb-3 mb-lg-0" href="<?php echo home_url('/skill-tree/taekwon-boy/'); ?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">TaeKwon Boy</div>
                            <img src="<?php echo get_template_directory_uri() . '/images/jobs/TaeKwon_Kid.png' ?>" alt="TaeKwon Boy">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="row row-job-options justify-content-center mb-3">
                            <div class="col">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/star-gladiator/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Star Gladiator</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/TaeKwon_Master.png' ?>" alt="Star Gladiator">
                                </a>
                            </div>
                            <!--<div class="col-6">
                                <a class="block-job text-center" href="<?php /*echo home_url('/skill-tree/star-emperor/'); */?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Star Emperor</div>
                                    <img class="img-fluid" src="<?php /*echo get_template_directory_uri() . '/images/jobs/Star_Emperor.png' */?>" alt="Star Emperor">
                                </a>
                            </div>-->
                        </div>
                        <div class="row row-job-options justify-content-center">
                            <div class="col">
                                <a class="block-job text-center" href="<?php echo home_url('/skill-tree/soul-linker/'); ?>">
                                    <div class="label">Nghề 2</div>
                                    <div class="job-name">Kết giới sư</div>
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/jobs/Soul_Linker.png' ?>" alt="Soul Linker">
                                </a>
                            </div>
                            <!--<div class="col">
                                <a class="block-job text-center" href="<?php /*echo home_url('/skill-tree/soul-reaper/'); */?>">
                                    <div class="label">Nghề 3</div>
                                    <div class="job-name">Soul Reaper</div>
                                    <img class="img-fluid" src="<?php /*echo get_template_directory_uri() . '/images/jobs/Soul_Reaper.png' */?>" alt="Soul Reaper">
                                </a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>

            <!--<div id="branch-doram-jobs" class="p-3 border-s block-jobs">
                <h5 class="mb-3">Nhánh nghề Mèo</h5>

                <div class="row align-items-center">
                    <div class="col">
                        <a class="block-job text-center" href="<?php /*echo home_url('/skill-tree/summoner/'); */?>">
                            <div class="label">Nghề 1</div>
                            <div class="job-name">Doram (Summoner)</div>
                            <img src="<?php /*echo get_template_directory_uri() . '/images/jobs/Summoner.png' */?>" alt="Summoner">
                        </a>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>

<?php
/**
 * Template Name: Nhiệm vụ
 * Template Post Type: post
 */
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <nav aria-label="breadcrumb">
            <?php
            $categories = get_the_category();
            $category_link = '';
            $category_display = '';

            if ( class_exists('WPSEO_Primary_Term') ) {
                // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
                $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                $term = get_term( $wpseo_primary_term );
                if (is_wp_error($term)) {
                    // Default to first category (not Yoast) if an error is returned
                    $category_display = $categories[0]->name;
                    $category_link = get_category_link( $categories[0]->term_id );
                } else {
                    // Yoast Primary category
                    $category_display = $term->name;
                    $category_link = get_category_link( $term->term_id );
                }
            } else {
                // Default, display the first category in WP's list of assigned categories
                $category_display = $categories[0]->name;
                $category_link = get_category_link( $categories[0]->term_id );
            }
            ?>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/huong-dan/') ?>">Hướng dẫn</a></li>
                <li class="breadcrumb-item"><a href="<?php echo $category_link; ?>"><?php echo $category_display; ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>
        </nav>

        <?php
        $sticky = get_field('sticky');
        $sticky_box_content = get_field('sticky_box_content');
        ?>

        <div class="row <?php echo ($sticky || $sticky_box_content) ? 'row-parent': ''; ?> row-info-quest">
            <div class="col-lg-4 order-lg-2 mb-4 mb-lg-0">
                <?php $use_box_content = get_field('use_box_content'); ?>
                <?php if ($use_box_content): ?>
                    <div class="card mb-4 <?php echo $sticky_box_content ? 'sticky-col' : ''; ?>">
                        <div class="card-body">
                            <h5 class="card-title">
                                Mục lục
                            </h5>
                            <?php if (have_rows('contents')): ?>
                                <?php $n1 = 1; ?>
                                <?php while (have_rows('contents')): the_row(); ?>
                                    <div class="each-item mb-2">
                                        <?php $title_slug = sanitize_title(get_sub_field('menu')); ?>
                                        <span><?php echo $n1; ?>.</span> <?php echo apply_filters('the_content', get_sub_field('menu')); ?>
                                        <?php if (have_rows('sub_menus')): ?>
                                            <?php $n2 = 1; ?>
                                            <ul class="mb-0 pl-3 list-unstyled">
                                                <?php while (have_rows('sub_menus')): the_row(); ?>
                                                    <li>
                                                        <?php echo $n1 . "." . $n2 . ". " . apply_filters('the_content', get_sub_field('sub_menu')); ?>
                                                    </li>
                                                    <?php $n2++; ?>
                                                <?php endwhile; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                    <?php $n1++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php $use_box_info = get_field('use_box_info'); ?>
                <?php if ($use_box_info): ?>
                    <div class="col-quest-info <?php echo $sticky ? 'sticky-col': ''; ?>">
                        <h3>Thông tin nhiệm vụ</h3>
                        <div class="w-table-require">
                            <table class="table table-bordered">
                                <tbody>
                                <?php if (have_rows('requirement')): ?>
                                    <?php $require_note = get_field('require_note'); ?>
                                    <?php if ($require_note): ?>
                                        <tr>
                                            <td colspan="2"><small class="text-muted text-center d-block"><?php echo $require_note; ?></small></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php while (have_rows('requirement')): the_row(); ?>
                                        <tr>
                                            <td class="table-require-col-title"><?php the_sub_field('title'); ?></td>
                                            <td><?php echo apply_filters('the_content', get_sub_field('content')); ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="2" class="text-center">Không có yêu cầu nào</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="rewards-info">
                            <h3>Phần thưởng</h3>
                            <table class="table table-bordered">
                                <tbody>
                                <?php if (have_rows('rewards')): ?>
                                    <?php $reward_note = get_field('reward_note'); ?>
                                    <?php if ($reward_note): ?>
                                        <tr>
                                            <td colspan="2"><small class="text-muted text-center d-block"><?php echo $reward_note; ?></small></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php while (have_rows('rewards')): the_row(); ?>
                                        <tr>
                                            <td class="table-require-col-title"><?php the_sub_field('title') ?></td>
                                            <td><?php echo apply_filters('the_content', get_sub_field('content')); ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="2" class="text-center">Không có phần thưởng nào</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-12">
                <h1 class="quest-title mt-4 mb-4"><?php the_title(); ?></h1>
            </div>

            <div class="col-lg-8 order-lg-1">
                <div class="mb-5">
                    <div class="post-the_content p-3 rounded">
                        <?php the_content(); ?>
                    </div>

                    <?php $author = get_field('author'); ?>
                    <?php if ($author): ?>
                        <div class="mt-1 p-2 author-tag rounded d-flex align-items-center">
                            <p class="mb-0"><img class="mr-2" src="<?php echo get_template_directory_uri() . '/images/tag-name.png'; ?>" alt=""><span class="mr-1">Cảm ơn bạn</span><b-badge variant="dark"><?php echo $author ?></b-badge> ở server <strong>Laniakea RO</strong> đã đóng góp bài viết này.</span></p>
                        </div>
                    <?php endif; ?>
                </div>

                <?php $type = get_field('type'); ?>
                <?php
                switch($type):
                    case "steps":
                        include locate_template('/parts/quest/quest-steps.php');
                        break;
                    case "list":
                        include locate_template('/parts/quest/quest-list.php');
                        break;
                    case "flexible":
                        include locate_template('/parts/quest/flexible-content.php');
                        break;
                endswitch;
                ?>
            </div>
        </div>

        <?php
        switch($type):
        case 'html_template':
            global $post;
            $post_slug = $post->post_name;
            if ($post_slug == 'he-thong-thu-cung-pet-system'):
                include locate_template('/parts/he-thong-thu-cung.php');
            endif;
            break;
        case 'steps_with_title':
            include locate_template('/parts/quest/quest-steps-with-title.php');
            break;
        endswitch;
        ?>
    </div>

    <?php $gallery_items = get_field('gallery'); ?>
    <?php if ($gallery_items): ?>
        <div class="wrap-gallery pb-5">
            <div class="container">
                <div class="alert alert-info mb-0" :class="{'d-none': showGallery}">
                    Bài viết này có bộ sưu tập ảnh. Bạn có muốn xem không? <a href="#" @click.prevent="getGallery">Xem ngay</a>.
                </div>
                <div class="d-none" :class="{'d-block': showGallery}">
                    <h2 class="mb-3">Một số hình ảnh</h2>
                    <div class="cbp" id="post-gallery">
                        <?php foreach($gallery_items as $item): ?>
                            <div class="cbp-item">
                                <a class="cbp-lightbox" data-title="<?php echo $item['title'] ?>" href="<?php echo $item['url']; ?>">
                                    <img src="<?php echo $item['sizes']['medium'] ?>" alt="<?php echo $item['alt'] ?>" width="100%">
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (have_rows('related_links')): ?>
    <div class="container">
        <div class="related-links">
            <?php while (have_rows('related_links')): the_row(); ?>
                <div class="each-related-link">
                    <?php
                    $heading_related_text = get_sub_field('heading');
                    if ($heading_related_text == "")
                        $heading_related_text = "Bài viết liên quan";
                    ?>
                    <h1 class="quest-title"><?php echo $heading_related_text ?></h1>
                    <hr>
                    <?php if (have_rows('links')): ?>
                        <?php while (have_rows('links')): the_row(); ?>
                            <div class="mb-1">
                                <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
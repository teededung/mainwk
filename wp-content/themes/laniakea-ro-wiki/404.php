<?php get_header(); ?>
<style>
    .error-template { padding: 40px 15px; text-align: center; }
    .error-actions { margin-top: 15px; margin-bottom: 15px; }
    .error-actions .btn { margin-right: 10px; }
</style>

<div class="bg-colored">
    <div class="container content">
        <div class="error-template">
            <h1>Oops!</h1>
            <h2>404 Not Found</h2>
            <div class="error-details mb-3">
                Trang này không tồn tại!
            </div>
            <div class="error-actions">
                <a href="<?php echo home_url(); ?>" class="btn btn-primary btn-lg">
                    Quay lại trang chủ
                </a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

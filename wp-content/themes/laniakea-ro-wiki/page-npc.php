<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

    <input type="hidden" id="apiURL" value="<?php echo admin_url( 'admin-ajax.php' ); ?>">

    <div class="npc-live bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5">NPC</h1>

            <div class="w-list-npc">
                <h2 class="mb-3">NPC cửa hàng</h2>
                <?php
                $args = array(
                    'post_type' => 'npc',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                    'meta_query'	=> array(
                        array(
                            'key' => 'sell_item',
                            'value' => '1',
                            'compare' => '='
                        )
                    )
                );

                $npc_objects = new WP_Query($args);
                ?>

                <?php if ($npc_objects->have_posts()): ?>
                    <div class="row row-live-npc">
                    <?php while ($npc_objects->have_posts()): $npc_objects->the_post(); ?>
                        <div class="col-md-3">
                            <div class="inner-col-live-npc p-3 rounded border">
                                <img class="img-fluid mb-2" onerror="this.src='<?php echo get_template_directory_uri() . '/images/NPC/npc-no-img.png' ?>'" src="<?php echo get_template_directory_uri() . '/images/NPC/' . get_field('sprite_constant') . '.gif'; ?>" alt="<?php the_title(); ?>">

                                <div class="meta">
                                    <h4 class="npc-name color-500 mb-1"><a href="<?php the_permalink() ?>"><?php echo the_title(); ?></a></h4>
                                    <?php $location = get_field('map') . ' ' . get_field('x') . '/' . get_field('y') ?>
                                    <code class="clipboard" data-clipboard-text="<?php echo '/navi ' . $location ?>"><?php echo $location; ?></code>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
		<div class="container content">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
                <li class="breadcrumb-item active" aria-current="page">Trang bị Verus</li>
            </ol>

			<h1 class="mb-4">Trang bị Verus</h1>

            <div class="bg-light p-3 border rounded">
				<p class="lead mb-3">
					Nhà máy Charleston đã dừng hoạt động nhưng công nghệ và sản phẩm của nhà máy thì vẫn còn.
					Các trang bị dưới đây khá tốt, các bạn xem hướng dẫn xong rồi làm cho mình 1 bộ nhé ^.^
				</p>

				<hr>

				<h3 class="quest-sub-title mb-4">Trang bị dành cho Mechanic</h3>

				<div class="mb-5">
					<div class="mb-4">
						<h5>1. Nâng cấp Pile Bunker</h5>
						<p>
							Để nâng cấp vũ khí Pile Bunker bạn hãy tìm đến Mass Charleston ở tọa độ <code class="clipboard" data-clipboard-text="/navi verus04 65/112">verus04 65/112</code>.
						</p>
						<p>
							Bạn cần có:
						</p>
						<ul>
							<li><?php echo do_shortcode('[item item_id="1549"]Pile Bunker[/item]') ?> x1</li>
							<li><?php echo do_shortcode('[item item_id="6751"]Dented Iron Plate[/item]') ?> x300</li>
							<li><?php echo do_shortcode('[item item_id="6750"]Broken Engine[/item]') ?> x15</li>
							<li>10.000.000 Zeny</li>
						</ul>

						<p>Bạn chọn 1 trong những các vũ khí nâng cấp sau:</p>

						<ul>
							<li>
								<?php echo do_shortcode('[item item_id="16031"]Pile Bunker P[/item]') ?>
							</li>
							<li>
								<?php echo do_shortcode('[item item_id="16030"]Pile Bunker S [1][/item]') ?>
							</li>
							<li>
								<?php echo do_shortcode('[item item_id="16032"]Pile Bunker T [1][/item]') ?>
							</li>
						</ul>

						<div class="alert alert-info">
							<?php echo do_shortcode('[item item_id="16031"]Pile Bunker P[/item]') ?> cũng rơi ra từ quái vật Charleston 3 ở Instance <a href="<?php echo home_url('/nhiem-vu/charleston-crisis/') ?>" target="_blank">Charleston Crisis</a>.
						</div>
					</div>

					<div class="small-line mb-3"></div>

					<div class="mb-3">
						<h5>2. Bộ trang bị Upgrade Part</h5>
						<p>Phần áo giáp <?php echo do_shortcode('[item item_id="15111"]Upgrade Part - Plate[/item]') ?> được bán với giá 7 <?php echo do_shortcode('[item item_id="6752"]Charleston Component[/item]') ?> và <strong>3.999.999</strong> zeny từ Mass Charleston tại <code class="clipboard" data-clipboard-text="/navi verus04 65/112">verus04 65/112</code>.</p>

						<p>Các phần còn lại trong set Upgrade như</p>
						<ul>
							<li><?php echo do_shortcode('[item item_id="20733"]Upgrade Part - Engine [1][/item]') ?></li>
							<li><?php echo do_shortcode('[item item_id="22044"]Upgrade Part - Booster [1][/item]') ?></li>
							<li><?php echo do_shortcode('[item item_id="2996"]Upgrade Part - Gun Barrel [1][/item]') ?></li>
						</ul>
						<p>sẽ đổi từ Mass Charleston tại <code class="clipboard" data-clipboard-text="/navi verus04 65/112">verus04 69/109</code> với 10 <?php echo do_shortcode('[item item_id="6752"]Charleston Component[/item]') ?>.</p>
					</div>

					<div class="small-line mb-3"></div>

					<div class="mb-3">
						<h5>3. Bộ trang bị Supplement Part</h5>
						<p>Phần áo giáp <?php echo do_shortcode('[item item_id="15110"]Supplement Part Str[/item]') ?> được bán với giá 7 <?php echo do_shortcode('[item item_id="6752"]Charleston Component[/item]') ?> và <strong>3.999.999</strong> zeny từ Mass Charleston tại <code class="clipboard" data-clipboard-text="/navi verus04 65/112">verus04 65/112</code>.</p>

						<p>Các phần còn lại trong set Supplement như</p>
						<ul>
							<li><?php echo do_shortcode('[item item_id="20732"]Supplement Part Con [1][/item]') ?></li>
							<li><?php echo do_shortcode('[item item_id="22043"]Supplement Part Agi [1][/item]') ?></li>
							<li><?php echo do_shortcode('[item item_id="2995"]Supplement Part Dex [1][/item]') ?></li>
						</ul>
						<p>sẽ đổi từ Mass Charleston tại <code class="clipboard" data-clipboard-text="/navi verus04 65/112">verus04 69/109</code> với 10 <?php echo do_shortcode('[item item_id="6752"]Charleston Component[/item]') ?>.</p>
					</div>

					<div class="small-line mb-3"></div>

					<div class="mb-3">
						<h5>4. Cường hóa bộ trang bị Upgrade Part và Supplement Part</h5>

						NPC đang được cập nhật...
					</div>
				</div>

				<hr>

				<h3 class="quest-sub-title mb-4">Trang bị Excelion</h3>

				<div class="mb-5">
					<p>
						Bộ trang bị Excelion cũng được chế tạo từ công nghệ bí ẩn, không giống như bộ trang bị cho Mechanic, mọi nghề đều sử dụng được bộ trang bị Excelion.<br>
						Ngoài ra bộ Excelion còn hỗ trợ cường hóa (enchant). Hãy bắt đầu nhiệm vụ để đổi trang bị Excelion và cường hóa tại <code class="clipboard" data-clipboard-text="/navi verus04 164/218">verus04 164/218</code>.
					</p>

					<div class="small-line mb-3"></div>

					<div class="mb-4">
						<h5>Bắt đầu nhiệm vụ:</h5>
						<ul class="list-group">
							<li class="list-group-item">
								<strong>MARS_01</strong> và <strong>PLUTO_09</strong> đều đã cạn kiệt năng lượng, riêng MARS_01 thì còn một ít năng lượng, MARS_01 báo rằng năng lượng sắp hết. <br>
								Bạn hãy đi tìm về 1 <?php echo do_shortcode('[item item_id="6962"]Old Fuel Tank[/item]'); ?> và cung cấp năng lượng cho MARS_01 và PLUTO_09 nhé.
							</li>
							<li class="list-group-item">
								Sau khi đã cung cấp xong năng lượng cho <strong>MARS_01</strong> và <strong>PLUTO_09</strong>. Cả 2 đã hoạt động bình thường.<br>
								<strong>MARS_01</strong> có khả năng cường hóa trang bị Excelion.<br>
								<strong>PLUTO_09</strong> có khả năng đổi 5 <?php echo do_shortcode('[item item_id="6962"]Old Fuel Tank[/item]'); ?> thành <strong>Blueprint</strong> với tỉ lệ thành công 70%.
								hoặc đổi 10 <?php echo do_shortcode('[item item_id="6961"]Huge Metal Scrap[/item]'); ?> và 5 <?php echo do_shortcode('[item item_id="6962"]Old Fuel Tank[/item]'); ?> ra ngẫu nhiên trang bị <?php echo do_shortcode('[item item_id="15128"]Excelion Suit[/item]'); ?> hoặc <?php echo do_shortcode('[item item_id="20773"]Excelion Wing[/item]'); ?>.
							</li>
						</ul>
					</div>

					<div class="small-line mb-3"></div>

					<div class="mb-3">
						<h5>Cường hóa</h5>
						<p>
							Để cường hóa các bạn phải có vật phẩm <strong>Blueprint</strong>. <br>
							Nếu bạn vẫn chưa có <strong>Blueprint</strong>, hãy đổi từ PLUTO_09 với 5 <?php echo do_shortcode('[item item_id="6962"]Old Fuel Tank[/item]'); ?> (Tỉ lệ thành công 70%). PLUTO_09 sẽ gửi bạn ngẫu nhiên 1 trong các vật phẩm <strong>Blueprint</strong>.
						</p>
						<p>
							Sau khi đã có <strong>Blueprint</strong>. Để tiến hành cường hóa bạn hãy nói chuyện với MARS_01. Ở Laniakea RO, cường hóa trang bị Excelion sẽ luôn luôn thành công. Trang bị sẽ được cường hóa 3 lần tùy vào <strong>Blueprint</strong> hiện có của bạn.
							Dưới đây là dánh sách hiệu ứng của <strong>Blueprint</strong> và hạn chế của chúng:
						</p>

						<div class="table-responsive">
							<table class="table table-bordered table-re-color">
								<thead>
								<tr>
									<th>Blueprint</th>
									<th>Hiệu ứng</th>
									<th>Loại</th>
									<th>Giới hạn</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6965"]Fire Property Blueprint[/item]'); ?></td>
									<td>Cường hóa áo giáp thành hệ lửa.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6966"]Water Property Blueprint[/item]'); ?></td>
									<td>Cường hóa áo giáp thành hệ nước.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6967"]Earth Property Blueprint[/item]'); ?></td>
									<td>Cường hóa áo giáp thành hệ đất.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6968"]Wind Property Blueprint[/item]'); ?></td>
									<td>Cường hóa áo giáp thành hệ gió.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6969"]Fire Resistance Blueprint[/item]'); ?></td>
									<td>Giảm 25% sát thương nhận từ hệ lửa nhưng nhận thêm 25% sát thương từ hệ nước.</td>
									<td>Wing</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6970"]Water Resistance Blueprint[/item]'); ?></td>
									<td>Giảm 25% sát thương nhận từ hệ nước nhưng nhận thêm 25% sát thương từ hệ gió.</td>
									<td>Wing</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6971"]Earth Resistance Blueprint[/item]'); ?></td>
									<td>Giảm 25% sát thương nhận từ hệ đất nhưng nhận thêm 25% sát thương từ hệ lửa.</td>
									<td>Wing</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6972"]Wind Resistance Blueprint[/item]'); ?></td>
									<td>Giảm 25% sát thương nhận từ hệ gió nhưng nhận thêm 25% sát thương từ hệ đất.</td>
									<td>Wing</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6973"]Recovery 101 Blueprint[/item]'); ?></td>
									<td>Hồi 50 HP mỗi 5 giây. Nếu nâng cấp +7 hoặc cao hơn, hồi thêm 50 HP mỗi 5 giây.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6974"]Recovery 102 Blueprint[/item]'); ?></td>
									<td>Hồi 3 SP mỗi 5 giây. Nếu nâng cấp +7 hoặc cao hơn, hồi thêm 2 SP mỗi 5 giây.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6976"]Recovery 201 Blueprint[/item]'); ?></td>
									<td>Tăng 50% tốc độ hồi phục HP. Nếu nâng cấp +7 hoặc cao hơn, thêm 50% tốc độ hồi HP.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6976"]Recovery 202 Blueprint[/item]'); ?></td>
									<td>Tăng 50% tốc độ hồi phục SP. Nếu nâng cấp +7 hoặc cao hơn, thêm 50% tốc độ hồi SP.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6977"]STR Supplement Blueprint[/item]'); ?></td>
									<td>ATK +5 mỗi 10 STR gốc. Khi nâng cấp +7 hoặc cao hơn, ATK +10.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6978"]INT Supplement Blueprint[/item]'); ?></td>
									<td>MATK +5 mỗi 10 INT gốc. Khi nâng cấp +7 hoặc cao hơn, MATK +10.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6979"]DEF Supplement Blueprint[/item]'); ?></td>
									<td>DEF +100.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6980"]PD Supplement Blueprint[/item]'); ?></td>
									<td>Perfect Dodge +3 (né tránh hoàn hảo).</td>
									<td>Cả hai</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6981"]Attack Supplement Blueprint[/item]'); ?></td>
									<td>ATK +20.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6982"]Magic Supplement Blueprint[/item]'); ?></td>
									<td>MATK +20.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6983"]HP Supplement Blueprint[/item]'); ?></td>
									<td>Max HP +5%.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6984"]SP Supplement Blueprint[/item]'); ?></td>
									<td>Max SP +3%.</td>
									<td>Cả hai</td>
									<td>3</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6985"]Frozen Supplement Blueprint[/item]'); ?></td>
									<td>Chống đóng băng.</td>
									<td>Suit</td>
									<td>1</td>
								</tr>
								<tr>
									<td><?php echo do_shortcode('[item item_id="6986"]ASPD Supplement Blueprint[/item]'); ?></td>
									<td>ASPD +1.</td>
									<td>Cả hai</td>
									<td>1</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>

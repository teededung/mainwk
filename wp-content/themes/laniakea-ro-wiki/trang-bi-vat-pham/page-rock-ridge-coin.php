<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">

        <?php $item = do_shortcode('[item id="25250"]Rock Ridge Coin[/item]'); ?>

        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
                <li class="breadcrumb-item active" aria-current="page">Rock Ridge Coin</li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="mb-5">

                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Rock Ridge Coin là gì?</h2>
                            <div class="card-text">
                                <?php echo $item ?> là một loại tiền tệ để trao đổi các vật phẩm hoặc trang bị ở <strong>Rock Ridge</strong>.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Sử dụng Rock Ridge Coin ở đâu?</h2>
                            <div class="card-text">
                                <p>Để sử dụng Rock Ridge Coin bạn hãy gặp các NPC dưới đây:</p>

                                <h3 class="mt-5 color-300">Howard</h3>
                                <p>
                                    Howard ở tọa độ <code class="clipboard" data-clipboard-text="/navi har_in01 34/81">har_in01 34/81</code>, anh ta sẽ đổi cho bạn các loại áo giáp hoặc vũ khí.<br>
                                </p>

                                <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/vat-pham-trang-bi/howard-rock-ridge.jpg' ?>" alt="howard-rock-ridge">

                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="250">Tên vật phẩm</th>
                                        <th>Coin</th>
                                        <th class="text-center">Công dụng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15176"]Vigilante Suit[/item]') ?></td>
                                        <td class="align-middle">24</td>
                                        <td class="align-middle text-center">
                                            <p>
                                                DEX +5. Mỗi 2 nâng cấp tăng +3% sát thương tầm xa. Nếu nâng cấp +9 hoặc hơn. Tăng 20% sát thương kỹ năng Triangle Shot.
                                            </p>
                                            <p>
                                                Nếu trang bị cùng với Vigilante Badge và Vigilante Bow, tăng 50% sát thương kỹ năng Double Strafe, giảm 10 SP kỹ năng Triangle Shot.
                                            </p>
                                            <p>Chỉ dành cho Shadow Chaser.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15177"]Elemental Robe[/item]') ?></td>
                                        <td class="align-middle">24</td>
                                        <td class="align-middle text-center">
                                            <p>
                                                Mỗi 2 nâng cấp tăng MATK +5. ASPD +10%. Nếu nâng cấp +9 hoặc hơn, có 5% tỉ lệ tự động dùng kỹ năng Thunder Storm lên đối phương khi đang đánh cự ly gần.
                                            </p>
                                            <p>
                                                Nếu trang bị cùng với Elemental cape và Elemental Origin, có 5% tỉ lệ tự động dùng kỹ năng Fire Ball cấp độ 5 (hoặc cấp độ cao nhất mà bạn học) vào đối phương khi sử dụng đòn đánh phép thuật.
                                                Có tỉ lệ 6% MATK +50 trong 1 phút khi sử dụng kỹ năng Double Bolt.
                                            </p>
                                            <p>Chỉ dành cho Sorcerer.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15178"]Golden Ninja Suit[/item]') ?></td>
                                        <td class="align-middle">24</td>
                                        <td class="align-middle text-center">
                                            <p>Mỗi 2 nâng cấp, Flee +5. Nếu nâng cấp +9 hoặc hơn, tăng 20% sát thương kỹ năng Kunai Splash.</p>
                                            <p>Nếu trang bị cùng với Golden Scarf và Monokage, tăng 20% sát thương kỹ năng Kunai Explosion, tăng 50% sát thương kỹ năng Kunai Splash.</p>
                                            <p>Nếu trang bị cùng với Hundred Leaf Huuma Shuriken và Golden Scarf, tăng 15% sát thương vật lý tầm xa, giảm 1 giây cooldown kỹ năng Swirling Petal</p>
                                            <p>Chỉ dành cho Oboro hoặc Kagerou.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15180"]Hippie Clothes[/item]') ?></td>
                                        <td class="align-middle">24</td>
                                        <td class="align-middle text-center">
                                            <p>ATK +20. Nếu nâng cấp +9 hoặc hơn, có tỉ lệ tự động sử dụng kỹ năng Double Strafing cấp độ 3 (hoặc cao nhất mà bạn học) khi đang đánh cự ly gần.</p>
                                            <p>Nếu trang bị cùng với Hippie Rope hoặc Hippie Guitar và Hippie Feather, ATK +30, ASPD +1. Tăng 20% sát thương chí mạng.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="20834"]Drifter\'s Cape[/item]') ?></td>
                                        <td class="align-middle">150</td>
                                        <td class="align-middle text-center">
                                            <p>Kháng 20% thuộc tính Neutral.</p>
                                            <p>Nếu nâng cấp +7. Kháng thêm 5% thuộc tính Neutral.</p>
                                            <p>Nếu nâng cấp +9. Kháng thêm 5% thuộc tính Neutral.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="22131"]Spurred Boots [1][/item]') ?></td>
                                        <td class="align-middle">150</td>
                                        <td class="align-middle text-center">
                                            <p>MaxSP +5%.</p>
                                            <p>Nếu nâng cấp +7. MaxSP +5%.</p>
                                            <p>Nếu nâng cấp +9. MaxSP +5%. Tăng tốc độ di chuyển.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="16060"]Flask [3][/item]') ?></td>
                                        <td class="align-middle">180</td>
                                        <td class="align-middle text-center">
                                            <p>Có tỉ lệ làm choáng đối phương. Mỗi cấp độ nâng cấp tăng 3% tỉ lệ làm choáng.</p>
                                            <p>Không bị hư trong giao tranh.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="26107"]Elder Staff [3][/item]') ?></td>
                                        <td class="align-middle">300</td>
                                        <td class="align-middle text-center">
                                            <p>MATK +150. Tăng 10% độ hồi phục của các kỹ năng hồi phục.</p>
                                            <p>Nếu nâng cấp +7, Tăng 5% độ hồi phục của các kỹ năng hồi phục.</p>
                                            <p>Nếu nâng cấp +9, Tăng 10% độ hồi phục của các kỹ năng hồi phục.</p>
                                            <p>Chỉ dành cho Archbishop và Sura.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="31140"]Costume Black Cowboy Hat[/item]') ?></td>
                                        <td class="align-middle">100</td>
                                        <td class="align-middle text-center">
                                            <p>Đồ thời trang.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="31141"]Costume Cactus Flower Corsage[/item]') ?></td>
                                        <td class="align-middle">100</td>
                                        <td class="align-middle text-center">
                                            <p>Đồ thời trang.</p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="mb-3">
                                    <h3 class="mt-5 color-300">Contraband Processor</h3>
                                    <p>Contraband Processor ở tọa độ <code class="clipboard" data-clipboard-text="/navi har_in01 17/74">har_in01 17/74</code> sẽ cường hóa trang bị cho bạn.</p>
                                    <p><img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/vat-pham-trang-bi/contraband-processor-rock-ridge.jpg' ?>" alt=""></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Làm sao để có Rock Ridge Coin</h2>
                            Bạn có thể thu thập <?php echo $item ?> từ các nhiệm vụ sau:

                            <ul>
                                <li><a href="<?php echo home_url('/nhiem-vu/fistful-of-zeny/') ?>">Fistful of Zeny</a></li>
                                <li><a href="<?php echo home_url('/nhiem-vu-hang-ngay/pipe-cleaning/') ?>">Pipe Cleaning</a></li>
                                <li><a href="<?php echo home_url('/nhiem-vu-hang-ngay/gas-gas/') ?>">Gas! Gas!</a></li>
                                <li><a href="<?php echo home_url('/nhiem-vu-vat-phamy/gas-gas/') ?>">Spotty and Her Ring (Spotty và chiếc nhẫn của cô chủ)</a></li>
                                <li><a href="<?php echo home_url('/nhiem-vu-hang-ngay/collecting-ore-fragments/') ?>">Collecting Ore Fragments</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

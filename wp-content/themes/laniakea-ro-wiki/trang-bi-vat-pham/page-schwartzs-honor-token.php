<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="bg-colored">
        <?php $item_honor_token = do_shortcode('[item id="25155"]Schwartz\'s Honor Token[/item]'); ?>

        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
                <li class="breadcrumb-item active" aria-current="page">Schwartz's Honor Token</li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="mb-5">
                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Schwartz's Honor Token là gì?</h2>
                            <div class="card-text">
                                <?php echo $item_honor_token ?> là tiền tệ lưu hành ở Cộng hòa Schwartzvald. Bạn có thể thu thập đồng tiền này bằng cách làm nhiệm vụ chính hoặc loạt nhiệm vụ hàng ngày trong phiên bản Terra Gloria.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Schwartz's Honor Token dùng để làm gì?</h2>
                            <div class="card-text">
                                <p>
                                    <?php echo $item_honor_token ?> là một loại tiền tệ để trao đổi các vật phẩm hoặc trang bị của phiên bản 16.2 Terra Gloria.
                                </p>

                                <p>
                                    <strong>Strasse</strong> đứng ở <code class="clipboard" data-clipboard-text="/navi >rebel_in 74/67">rebel_in 74/67</code> là người sẽ cho bạn lựa chọn trang bị trao đổi. Nhớ rằng đừng để hành trang quá nặng khi mua hàng nhé.
                                </p>

                                <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/strasse-trao-doi-trang-bi-16-2.jpg'; ?>" alt="strasse-trao-doi-trang-bi-16-2">

                                <div class="mb-3">
                                    <h3 class="mt-5 color-300">Trao đổi trang bị</h3>

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="250">Tên vật phẩm</th>
                                                <th width="250" class="text-center"><?php echo $item_honor_token ?></th>
                                                <th width="100" class="d-none d-md-table-cell">Loại</th>
                                                <th class="d-none d-md-table-cell">Mô tả</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="15163"]Agenda Robe [1][/item]') ?></td>
                                                <td class="align-middle text-center">1,000</td>
                                                <td class="align-middle d-none d-md-table-cell">Áo giáp</td>
                                                <td class="align-middle d-none d-md-table-cell">
                                                    MATK +5%<br>
                                                    Cho phép sử dụng kỹ năng Spell Breaker cấp 1.<br><br>
                                                    Nếu Base Level đạt ít nhất 120:<br>
                                                    MATK +4%<br><br>
                                                    Nếu Base Level đạt ít nhất 140:<br>
                                                    MATK +5%<br>
                                                    <br>
                                                    Khi kết hợp với Cape of Ancient Lord:<br>
                                                    Giảm 2% thời gian thi triển kỹ năng (VCT).<br>
                                                    Thi triển kỹ năng không bị gián đoạn.<br>
                                                    <br>
                                                    Khi kết hợp với Manteau of Survival:<br>
                                                    Có tỉ lệ tự động dùng kỹ năng Heal cấp 1 lên người đeo khi đang gây sát thương phép thuật.<br>
                                                    Mỗi 1 độ tinh luyện:<br>
                                                    Tăng tỉ lệ tự động dùng kỹ năng Heal.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="15164"]Consultative Robe [1][/item]') ?></td>
                                                <td class="align-middle text-center">1,000</td>
                                                <td class="align-middle d-none d-md-table-cell">Áp giáp</td>
                                                <td class="align-middle d-none d-md-table-cell">
                                                    VIT +5<br><br>
                                                    Cho phép sử dụng kỹ năng Sacrifice cấp 1.<br><br>
                                                    Mỗi 1 độ tinh luyện:<br>
                                                    Giảm 3% sát thương thuộc tính Lửa và Bóng tối nhận từ đối phương.<br><br>
                                                    Khi kết hợp với Morrigan's Manteau:<br>
                                                    Consultation Robe trở thành thuộc tính Hồn ma.<br><br>
                                                    Mỗi độ tinh luyện của Consultation Robe:<br>
                                                    AGI +1<br>
                                                    Flee +2<br><br>
                                                    Khi kết hợp với Valkyrie Manteau:<br>
                                                    VIT +5<br>
                                                    Giảm 10% sát thương thuộc tính Lửa và Bóng tối nhận từ đối phương.<br>
                                                    Mỗi độ tinh luyện của Consultation Robe:<br>
                                                    VIT +1<br>
                                                    LUK +1<br>
                                                    Nếu độ tinh luyện của Consultation Robe đạt ít nhất +10:<br>
                                                    Giảm 10% sát thương thuộc tính Trung tính nhận từ đối phương.<br><br>
                                                    Khi kết hợp với Devilring Card:<br>
                                                    Hủy hiệu ứng Devilring Card.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="20456"]Costume Combat Vestige[/item]') ?></td>
                                                <td class="align-middle text-center">1,500</td>
                                                <td class="align-middle d-none d-md-table-cell">Đồ thời trang</td>
                                                <td class="align-middle d-none d-md-table-cell"></td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="19115"]Republic Hat [1][/item]') ?></td>
                                                <td class="align-middle text-center">2,500</td>
                                                <td class="align-middle d-none d-md-table-cell">Mũ</td>
                                                <td class="align-middle d-none d-md-table-cell">
                                                    Tăng 6% sát thương vật lý tầm xa.<br><br>
                                                    Tinh luyện +8:<br>
                                                    Tăng 2% sát thương vật lý tầm xa.<br><br>
                                                    Tinh luyện +10:<br>
                                                    Tăng 4% sát thương vật lý tầm xa.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="28425"]Mercenary Ring Type A[/item]') ?></td>
                                                <td class="align-middle text-center">1,000</td>
                                                <td class="align-middle d-none d-md-table-cell">Trang sức</td>
                                                <td class="align-middle d-none d-md-table-cell">
                                                    VIT +3<br><br>
                                                    Khi được đeo bởi Novice, Taekwon boy, Ninja và Gunslinger:<br>
                                                    MaxHP +1000<br>
                                                    MaxSP +200
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle"><?php echo do_shortcode('[item id="28426"]Mercenary Ring Type B[/item]') ?></td>
                                                <td class="align-middle text-center">1,000</td>
                                                <td class="align-middle d-none d-md-table-cell">Trang sức</td>
                                                <td class="align-middle d-none d-md-table-cell">
                                                    INT +3<br><br>
                                                    Khi được đeo bởi Novice, Taekwon boy, Ninja và Gunslinger:<br>
                                                    Giảm 30% thời gian thi triển kỹ năng (VCT).
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <h3 class="mt-5 color-300">Cường hóa</h3>
                                    <p>
                                        NPC <strong>Hogar</strong> đứng ở <code class="clipboard" data-clipboard-text="/navi >rebel_in 90/40">rebel_in 90/40</code> là người sẽ nâng cấp trang bị mạnh hơn hoặc làm chúng quay về ban đầu. Anh ta chỉ có thể cường hóa hoặc thay đổi 2 loại áo và 2 loại trang sức của phiên bản Terra Gloria.
                                    </p>

                                    <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/hogar-cuong-hoa-trang-bi-16-2.jpg'; ?>" alt="hogar-cuong-hoa-trang-bi-16-2">

                                    <div class="mb-3">
                                        <h5 class="color-200"><u>Cường hóa Áo giáp</u></h5>

                                        <ul class="mb-3">
                                            <li>Đối với áo choàng, phí nâng cấp là 20 <?php echo $item_honor_token ?> cho mỗi ô của áo.</li>
                                            <li>Bạn tốn 10 đồng để tẩy cường hóa, <strong>luôn luôn thành công</strong> với điều kiện <strong>phải nâng cấp áo choàng tối đa</strong>.</li>
                                            <li>Cường hóa là ngẫu nhiên, danh sách ngọc cường hóa các bạn xem ở bảng dưới.</li>
                                        </ul>

                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Loại</th>
                                                <th>Cường hóa</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="align-middle">STR</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="4994"]Rune of Strength Lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="4995"]Rune of Strength Lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="4996"]Rune of Strength Lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle">AGI</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="4997"]Rune of Agility Lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="4998"]Rune of Agility Lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="4999"]Rune of Agility Lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle">INT</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="29000"]Rune of Intellect lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29001"]Rune of Intellect lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29002"]Rune of Intellect lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle">DEX</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="29003"]Rune of Dexterity Lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29004"]Rune of Dexterity Lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29005"]Rune of Dexterity Lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle">LUK</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="29006"]Rune of Luck Lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29007"]Rune of Luck Lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29008"]Rune of Luck Lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle">VIT</td>
                                                <td class="align-middle">
                                                    <?php echo do_shortcode('[item id="29009"]Rune of Vitality Lv 1[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29010"]Rune of Vitality Lv 2[/item]') ?><br>
                                                    <?php echo do_shortcode('[item id="29011"]Rune of Vitality Lv 3[/item]') ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="mb-3">
                                        <h5 class="color-200"><u>Cường hóa Trang sức</u></h5>

                                        <ul class="mb-3">
                                            <li>Đối với trang sức, phí nâng cấp là 5 <?php echo $item_honor_token ?> cho mỗi ô của trang sức.</li>
                                            <li>Phí tẩy cường hóa vẫn là 10 đồng nhưng sẽ có 20% chiếc nhẫn của bạn sẽ tan thành mây khói.</li>
                                        </ul>

                                        <p>
                                            Trang sức được tăng ngẫu nhiên +1 hoặc +2 điểm chỉ số mỗi lần nâng cấp gồm: STR, AGI, VIT, INT, DEX hoặc LUK.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title color-600">Làm sao để có Schwartz's Honor Token?</h2>
                            <p>
                                Thu thập <?php echo $item_honor_token ?> ở các nhiệm vụ sau:
                            </p>
                            <ul>
                                <li><a target="_blank" href="https://hahiu.com/episode-16-2/terra-gloria-nhiem-vu-chinh/">Terra Gloria - Nhiệm vụ chính</a></li>
                                <li><a target="_blank" href="https://hahiu.com/episode-16-2/terra-gloria-nhiem-vu-phu/">Terra Gloria - Nhiệm vụ phụ</a></li>
                                <li><a target="_blank" href="https://hahiu.com/nhiem-vu-hang-ngay/terra-gloria-nhiem-vu-hang-ngay/">Terra Gloria - Nhiệm vụ hằng ngày</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="related-links">
                    <div class="each-related-link">
                        <h1 class="quest-title">Các bài viết liên quan</h1>
                        <hr>
                        <div class="mb-1">
                            <a href="<?php echo home_url('/') . 'episode-16-2/instance-heart-hunter-war-base/' ?>">Instance: Heart Hunter War Base</a>
                        </div>
                        <div class="mb-1">
                            <a href="<?php echo home_url('/') . 'episode-16-2/instance-werner-laboratory-central-room/' ?>">Instance: Werner Laboratory Central Room</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>

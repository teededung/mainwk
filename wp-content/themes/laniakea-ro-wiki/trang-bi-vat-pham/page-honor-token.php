<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <?php $item_honor_token = do_shortcode('[item id="6919"]Honor Token[/item]'); ?>

    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
            <li class="breadcrumb-item active" aria-current="page">Honor Token</li>
        </ol>

        <h1 class="mb-5"><?php the_title(); ?></h1>

        <div class="mb-5">

            <div class="mb-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title color-600">Honor Token là gì?</h2>
                        <div class="card-text">
                            <?php echo $item_honor_token ?> là một loại tiền tệ để trao đổi các vật phẩm hoặc trang bị của phiên bản 16.1 Banquet for Heroes.
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title color-600">Honor Token dùng để làm gì?</h2>
                        <div class="card-text">
                            <p>
                                <?php echo $item_honor_token ?> là một loại tiền tệ để trao đổi các vật phẩm hoặc trang bị của phiên bản 16.1 Banquet for Heroes.
                            </p>

                            <p>
                                Để trao đổi Honor Token bạn hãy gặp NPC <strong>Commissary Arner</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi prt_cas 166/255">prt_cas 166/255</code> trong lâu đài Prontera.
                            </p>

                            <h3 class="mt-5 color-300">Trao đổi vật phẩm tiêu dùng</h3>

                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="200">Tên vật phẩm</th>
                                        <th width="150" class="text-center"><?php echo $item_honor_token ?></th>
                                        <th>Công dụng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="align-middle"><?php echo do_shortcode('[item id="22899"]City Center Map[/item]') ?></td>
                                    <td class="align-middle text-center">1</td>
                                    <td class="align-middle">Mất 15 HP và cho phép sử dụng cấp độ 1 kỹ năng Increase AGI.</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><?php echo do_shortcode('[item id="11600"]Shining Holy Water[/item]') ?></td>
                                    <td class="align-middle text-center">3</td>
                                    <td class="align-middle">
                                        Hồi phục 1000 HP.<br>
                                        Giảm 10% sát thương nhận từ tất cả quái vật ở Room of Consciousness và Prontera Invasion.
                                        Giải độc, câm lặng, nguyền rủa.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><?php echo do_shortcode('[item id="22848"]Prison Key[/item]') ?></td>
                                    <td class="align-middle text-center">3</td>
                                    <td class="align-middle">Khi sử dụng bạn sẽ được dịch chuyển tới nhà tù Prontera.</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><?php echo do_shortcode('[item id="22847"]Prontera Badge[/item]') ?></td>
                                    <td class="align-middle text-center">3</td>
                                    <td class="align-middle">Dịch chuyển bạn đến Prontera.</td>
                                </tr>
                                </tbody>
                            </table>

                            <h3 class="mt-5 color-300">Trang bị</h3>

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="270">Tên vật phẩm</th>
                                        <th width="150" class="text-center"><?php echo do_shortcode('[item id="6919" no-icon="true"]Honor Token[/item]') ?></th>
                                        <th width="100" class="d-none d-md-table-cell">Loại</th>
                                        <th class="d-none d-md-table-cell">Mô tả</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="28354"]City Map [1][/item]') ?></td>
                                        <td class="align-middle text-center">150</td>
                                        <td class="align-middle d-none d-md-table-cell">Trang sức</td>
                                        <td class="align-middle d-none d-md-table-cell">Cho phép sử dụng cấp độ 3 kỹ năng Increase AGI.</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="28356"]Prontera Badge[/item]') ?></td>
                                        <td class="align-middle text-center">150</td>
                                        <td class="align-middle d-none d-md-table-cell">Trang sức</td>
                                        <td class="align-middle d-none d-md-table-cell">Cho phép dùng kỹ dịch chuyển về Prontera. Thời gian sử dụng lại là 15 phút.</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="28355"]Shining Holy Water [1][/item]') ?></td>
                                        <td class="align-middle text-center">400</td>
                                        <td class="align-middle d-none d-md-table-cell">Trang sức</td>
                                        <td class="align-middle d-none d-md-table-cell">
                                            Giảm 20% sát thương nhận từ tất cả quái vật ở Room of Consciousness và Prontera Invasion.<br>
                                            Hồi phục 100 HP và 30 SP mỗi 10 giây.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="28900"]Royal Guard Shield [1][/item]') ?></td>
                                        <td class="align-middle text-center">400</td>
                                        <td class="align-middle d-none d-md-table-cell">Khiên</td>
                                        <td class="align-middle d-none d-md-table-cell">
                                            DEF: 30, Nặng: 300<br>
                                            Mỗi cấp độ nâng cấp DEF +10, MDEF +1<br>
                                            Cho phép sử dụng cấp độ 1 kỹ năng Shield Spell.<br>
                                            Khi nhận sát thương tầm gần, có tỉ lệ 0.5% tự động sử dụng cấp độ 3 kỹ năng Assumptio vào bản thân, tỉ lệ này tăng 0.3% mỗi độ nâng cấp.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15146"]Flattery Robe [1][/item]') ?></td>
                                        <td class="align-middle text-center">600</td>
                                        <td class="align-middle d-none d-md-table-cell">Áo giáp</td>
                                        <td class="align-middle d-none d-md-table-cell">
                                            MATK +50<br>
                                            Nếu cấp độ của bạn trên 119, MATK +50<br>
                                            Nếu cấp độ của bạn trên 139, MATK +50<br>
                                            Cho phép sử dụng cấp độ 1 kỹ năng Endure.<br>
                                            Nếu mặc cùng với Ancient Cape, MaxSP +10%, FLEE +1.<br>
                                            Nếu mặc cùng với Survivor's Manteau, MaxHP +10%, MATK 2%.<br>
                                            Mỗi độ nâng cấp, MaxHP +1%, MATK +1%.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="15147"]Abusive Robe [1][/item]') ?></td>
                                        <td class="align-middle text-center">600</td>
                                        <td class="align-middle d-none d-md-table-cell">Áo giáp</td>
                                        <td class="align-middle d-none d-md-table-cell">
                                            Cho phép sử dụng cấp độ 1 kỹ năng Improve Concentration.<br>
                                            Mỗi độ nâng cấp, bỏ qua 4% DEF đối với Demi-human, Demon và Undead.<br>
                                            Nếu mặc cùng với Morrigane's Manteau, LUK +3, CRIT +10.<br>
                                            Nếu mặc cùng với Valkyrian Manteau, MaxHP +10%, ATK +2%.<br>
                                            Mỗi độ nâng cấp, MaxHP +1%, ATK +1%.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="20246"]Costume Time Decor[/item]') ?></td>
                                        <td class="align-middle text-center">1100</td>
                                        <td class="align-middle d-none d-md-table-cell">Costume Middle</td>
                                        <td class="align-middle d-none d-md-table-cell">Đồ thời trang</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle"><?php echo do_shortcode('[item id="20247"]Costume Black Hand of Fate[/item]') ?></td>
                                        <td class="align-middle text-center">1100</td>
                                        <td class="align-middle d-none d-md-table-cell">Costume Upper</td>
                                        <td class="align-middle d-none d-md-table-cell">Đồ thời trang</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <h3 class="mt-5 color-300">Cường hóa</h3>

                            <p>NPC <strong>Dylan</strong> sẽ giúp bạn cường hóa trang bị. Bạn có thể tìm anh ta ở nhà tù, tọa độ <code class="clipboard" data-clipboard-text="/navi prt_pri00 60/136">prt_pri00 60/136</code>.</p>

                            <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/dylan-banquet-for-heroes.png'; ?>" alt="enchant-dylan-banquet-for-heroes">

                            <div class="mb-3">
                                <h5 class="color-200"><u>Cường hóa Prontera Badge</u></h5>
                                <p>
                                    <?php echo do_shortcode('[item id="28356"]Prontera Badge[/item]') ?> có thể cường hóa được 2 lần. Mỗi cường hóa sẽ tăng 1 hoặc 2 điểm Stat.
                                </p>
                                <ul>
                                    <li>Cường hóa <?php echo do_shortcode('[item id="28356"]Prontera Badge[/item]') ?> sẽ cần x5 <?php echo $item_honor_token ?> và không có tỉ lệ thất bại.</li>
                                    <li>Để tẩy cường hóa cần x10 <?php echo $item_honor_token ?> và có tỉ lệ 20% sẽ phá hủy trang sức của bạn.</li>
                                </ul>
                            </div>

                            <div class="mb-3">
                                <h5 class="color-200"><u>Cường hóa Áo giáp</u></h5>

                                <p>
                                    Ta có thể cường hóa <?php echo do_shortcode('[item id="15146"]Flattery Robe [1][/item]') ?> và <?php echo do_shortcode('[item id="15147"]Abusive Robe [1][/item]') ?>, cả 2 áo này đều có thể cường hóa 2 lần.
                                </p>
                                <ul class="mb-3">
                                    <li>Cường hóa 2 loại áo này cần x20 <?php echo $item_honor_token ?> và luôn luôn thành công.</li>
                                    <li>Để tẩy cường hóa cần x10 <?php echo $item_honor_token ?> và luôn luôn thành công.</li>
                                    <li>Cường hóa là ngẫu nhiên, danh sách ngọc cường hóa các bạn xem ở bảng dưới.</li>
                                </ul>

                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Loại</th>
                                        <th>Cường hóa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="align-middle">STR</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="4994"]Rune of Strength Lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="4995"]Rune of Strength Lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="4996"]Rune of Strength Lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle">AGI</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="4997"]Rune of Agility Lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="4998"]Rune of Agility Lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="4999"]Rune of Agility Lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle">INT</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="29000"]Rune of Intellect lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29001"]Rune of Intellect lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29002"]Rune of Intellect lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle">DEX</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="29003"]Rune of Dexterity Lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29004"]Rune of Dexterity Lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29005"]Rune of Dexterity Lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle">LUK</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="29006"]Rune of Luck Lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29007"]Rune of Luck Lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29008"]Rune of Luck Lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle">VIT</td>
                                        <td class="align-middle">
                                            <?php echo do_shortcode('[item id="29009"]Rune of Vitality Lv 1[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29010"]Rune of Vitality Lv 2[/item]') ?><br>
                                            <?php echo do_shortcode('[item id="29011"]Rune of Vitality Lv 3[/item]') ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="mb-3">
                                <h5 class="color-200"><u>Đổi hộp bí ẩn</u></h5>
                                <p>Bạn có thể đổi x30 <?php echo $item_honor_token; ?> với NPC <strong>Logistics Manager</strong> để nhận 1 <?php echo do_shortcode('[item id="22901"]Mysterious Blue Box[/item]') ?>.<br> Anh ta ở gần khu vực nhà tù ở tọa độ <code class="clipboard" data-clipboard-text="/navi prt_cas 180/275">prt_cas 180/275</code>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title color-600">Làm sao để có Honor Token?</h2>
                        Bạn có thể thu thập <?php echo $item_honor_token ?> từ nhiệm vụ <a target="_blank" href="<?php echo home_url('/nhiem-vu/banquet-for-heroes/') ?>">Banquet for Heroes</a> hoặc <a target="_blank" href="<?php echo home_url('/nhiem-vu/nhiem-vu-hang-ngay-banquet-for-heroes/') ?>">nhiệm vụ hằng ngày Banquet for Heroes</a>.
                    </div>
                </div>
            </div>

            <div class="related-links">
                <div class="each-related-link">
                    <h1 class="quest-title">Các bài viết liên quan</h1>
                    <hr>
                    <div class="mb-1">
                        <a href="<?php echo home_url('/') . 'nhiem-vu/banquet-for-heroes/' ?>">Banquet for Heroes</a>
                    </div>
                    <div class="mb-1">
                        <a href="<?php echo home_url('/') . 'nhiem-vu/nhiem-vu-hang-ngay-banquet-for-heroes/' ?>">Nhiệm vụ hằng ngày Banquet for Heroes</a>
                    </div>
                    <div class="mb-1">
                        <a href="<?php echo home_url('/') . 'nhiem-vu/sky-fortress-instance/' ?>">Sky Fortress Instance</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>

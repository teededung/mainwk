<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
            <li class="breadcrumb-item active" aria-current="page">Vũ khí Crimson</li>
        </ol>

        <h1 class="mb-4">Vũ khí Crimson</h1>

        <div class="bg-light p-3 mb-3 border rounded">
            <div class="pb-3">
                <h3 class="color-600">Giới thiệu</h3>
                <p class="lead">
                    Vũ khí Crimson là loại vũ khí đặc biệt rơi ra từ quái vật, các vũ khí này khá phù hợp cho các bạn đang ở giai đoạn luyện cấp từ cấp độ 100 đến 160.
                </p>
                <p>Đặc điểm vũ khí Crimson:</p>
                <ul>
                    <li>Vũ khí cấp độ 3</li>
                    <li>Có 2 ô để gắn thẻ bài</li>
                    <li>Có thuộc tính nào đó (được liệt kê bên dưới)</li>
                    <li>Cấp độ nhân vật trên 70 là có thể đeo được</li>
                    <li>Càng tinh luyện lên cao, ATK hoặc MATK càng cao</li>
                </ul>
            </div>

            <div class="mb-3">
                <p>Có 8 thuộc tính của vũ khí Crimson:</p>
                <ul class="mb-3">
                    <li><span style="color: Gray;">Trung tính (Neutral)</span></li>
                    <li><span style="color: Red;">Lửa (Fire)</span></li>
                    <li><span style="color: Blue;">Nước (Water)</span></li>
                    <li><span style="color: SaddleBrown;">Đất (Earth)</span></li>
                    <li><span style="color: Green;">Gió (Wind)</span></li>
                    <li><span style="color: Purple;">Độc (Poison)</span></li>
                    <li><span style="color: DarkGray;">Thánh (Holy)</span></li>
                    <li>Bóng tối (Shadow)</li>
                </ul>

                <p>Sau khi bạn nhặt được vũ khí Crimson từ quái vật, hãy dùng <?php echo_item(611, 'Kính lúp') ?> để giám định vũ khí. Sau khi giám định xong vũ khí Crimson của bạn sẽ có 1 thuộc tính trong 8 thuộc tính trên.</p>
            </div>

            <figure class="figure mb-3">
                <img src="<?php echo get_template_directory_uri() . '/images/vat-pham-trang-bi/example-crimson.jpg' ?>" class="figure-img img-fluid" alt="Vũ khí Crimson mẫu">
                <figcaption class="figure-caption">1 mẫu Crimson Katar</figcaption>
            </figure>

            <h2 class="color-600">Danh sách vũ khí Crimson</h2>

            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <td>Tên</td>
                    <td>Rơi từ</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="align-middle"><?php echo_item(28705, 'Crimson Dagger [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1154"]Pasana[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2719"]Swift Pasana[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1386"]Sleeper[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2655"]Elusive Sleeper[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2656"]Swift Sleeper[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1619"]Porcellio[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2700"]Solid Porcellio[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2199"]Siorava[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1106"]Desert Wolf[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1204"]Ogretooth[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1203"]Mysteltainn[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1653"]Wickebine Tres[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1290"]Skeleton General[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2658"]Skeleton General Ringleader[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2659"]Furious Skeleton General[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(13454, 'Crimson Saber [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1154"]Pasana[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2719"]Swift Pasana[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1205"]Executioner[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1204"]Ogretooth[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1203"]Mysteltainn[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1654"]Armeyer Dinze[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1652"]Egnigem Cenia[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(21015, 'Crimson Two-handed Sword [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1584"]Tamruan[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2639"]Furious Tamruan[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1302"]Dark Illusion[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1702"]Baroness of Retribution[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2684"]Furious Baroness of Retribution[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2685"]Elusive Baroness of Retribution[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2686"]Swift Baroness of Retribution[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1219"]Abysmal Knight[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1203"]Mysteltainn[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1829"]Sword Master[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1652"]Egnigem Cenia[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1658"]Egnigem Cenia (MVP)[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(28106, 'Crimson Two-Handed Axe [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1135"]Kobold[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1310"]Majoruros[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2767"]Furious Majoruros[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2353"]Nightmare Minorous[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1654"]Armeyer Dinze[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(16040, 'Crimson Mace [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li> <?php echo do_shortcode('[mob id="1517"]Jing Guai[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="2778"]Elusive Jing Guai[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1155"]Petite[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="2714"]Swift Petite[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="2715"]Solid Petite[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1143"]Marionette[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="2761"]Marionette Ringleader[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1316"]Solider[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1296"]Kobold Leader[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1655"]Errende Ebecee[/mob]'); ?></li>
                            <li> <?php echo do_shortcode('[mob id="1301"]Am Mut[/mob]'); ?></li>
                        </ul>

                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1443, 'Crimson Spear [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1215"]Stem Worm[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2641"]Swift Stem Worm[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1993"]Naga[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1498, 'Crimson Lance [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1320"]Owl Duke[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2721"]Owl Duke Ringleader[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1030"]Anacondaq[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2904"]Furious Anacondaq[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1259"]Gryphon[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(18130, 'Crimson Bow [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1295"]Owl Baron[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1830"]Bow Master[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1656"]Kavach Icarus[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(28007, 'Crimson Katar [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="2071"]Headless Mule[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1257"]Injustice[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2792"]Furious Injustice[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1839, 'Crimson Knuckles [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="2072"]Jaguar[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1631"]Green Maiden[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1408"]Bloody Butterfly[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2883"]Swift Bloody Butterfly[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1736"]Aliot[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1655"]Errende Ebecee[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(28604, 'Crimson Bible [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1117"]Evil Druid[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2838"]Elusive Evil Druid[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2362"]Nightmare Amon Ra[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1680, 'Crimson Rod [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1102"]Bathory[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2885"]Furious Bathory[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1657"]Laurell Weinder[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2692"]Laurell Weinder Ringleader[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(2025, 'Crimson Staff [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1320"]Owl Duke[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2721"]Owl Duke Ringleader[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1303"]Giant Hornet[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2821"]Giant Hornet Ringleader[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1939, 'Crimson Violin [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1404"]Miyabi Doll[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2746"]Miyabi Doll Ringleader[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1416"]Evil Nymph[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2617"]Elusive Evil Nymph[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1390"]Violy[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2621"]Elusive Violy[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2622"]Swift Violy[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2623"]Solid Violy[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1703"]Lady Solace[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1829"]Sword Master[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1656"]Kavach Icarus[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(1995, 'Crimson Wire [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1413"]Hermit Plant[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1416"]Evil Nymph[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2617"]Elusive Evil Nymph[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(13327, 'Crimson Huuma Shuriken [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1405"]Tengu[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle"><?php echo_item(13127, 'Crimson Revolver [2]'); ?></td>
                    <td>
                        <ul class="mb-0">
                            <li><?php echo do_shortcode('[mob id="1613"]Metaling[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2755"]Solid Metaling[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2756"]Metaling Ringleader[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="1628"]Holden[/mob]'); ?></li>
                            <li><?php echo do_shortcode('[mob id="2745"]Solid Holden[/mob]'); ?></li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="related-links">
            <div class="each-related-link">
                <h1 class="quest-title">Các bài viết liên quan</h1>
                <hr>
                <div class="mb-1">
                    <a href="<?php echo home_url('/') . 'he-thong-tinh-nang/tinh-luyen-trang-bi-refinement/' ?>">Tinh luyện trang bị (Refinement)</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

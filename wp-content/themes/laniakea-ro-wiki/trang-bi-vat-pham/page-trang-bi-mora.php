<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/trang-bi-va-vat-pham/') ?>">Trang bị và vật phẩm</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
        </ol>

        <h1 class="mb-4">Trang bị Mora</h1>

        <div class="bg-light p-3 mb-3 border rounded">
            <div class="pb-3">
                <h3 class="color-600">Giới thiệu</h3>
                <p class="lead">
                    Làng Mora, nơi các anh hùng ở lại để tìm kiếm những vật phẩm có giá trị từ các nhà thám hiểm bị mất tích từ khu rừng
                    <a href="<?php echo home_url('/nhiem-vu/nhiem-vu-wandering-guardian-hazy-forest/') ?>">Hazy Forest</a>. Người ta nói rằng người dân đang chờ các nhà thám hiểm tìm thấy các vật phẩm đó.
                </p>
                <p class="lead">
                    Các nghề hiện tại có thể mặc trang bị Mora bao gồm <strong>Rune Knight</strong>, <strong>Guillotine Cross</strong>, <strong>Ranger</strong>, <strong>Arch Bishop</strong> và <strong>Warlock</strong>.<br>
                    Để mặc trang bị Mora bạn phải ở cấp độ 100 hoặc hơn và bạn sẽ nhận hiệu ứng tốt nếu bạn mặc đủ combo, thường là 4 món.<br>
                </p>
            </div>

            <div class="alert alert-info mb-3">
                Bạn có thể đến cửa hàng Mora tại tọa độ <code class="clipboard" data-clipboard-text="/navi mora 145/98">mora 145/98</code> để mua các trang bị Mora. Tất cả đều có giá 10 <?php echo_item(6380, 'Mora Coin') ?>. Hoặc là mua từ các NPC dưới đây.
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h3 class="color-600">
                    <b-link class="tag-collapse" v-b-toggle.collapse-1>
                        <span>Arch Bishop</span> <i></i>
                    </b-link>
                </h3>
                <b-collapse id="collapse-1">
                    <div class="pb-3">
                        NPC <strong>Keeper of Secrets</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 88/89">mora 88/89</code> cho phép bạn đổi các trang bị Mora của nghề <strong>Arch Bishop</strong>.
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Trang bị</th>
                            <th>Ghi chú</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo_item(2864, 'Light of Cure') ?></td>
                            <td>
                                VIT +2.<br>
                                Tăng 2% độ hồi phục các kỹ năng hồi phục.<br><br>
                                Loại: Trang sức<br>
                                Cấp độ yêu cầu: 110.
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2865, 'Seal of Cathedral') ?></td>
                            <td>
                                INT +2.<br>
                                Tăng 2% độ hồi phục các kỹ năng hồi phục.<br><br>
                                Loại: Trang sức<br>
                                Cấp độ yêu cầu: 110.
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2866, 'Ring of Archbishop') ?></td>
                            <td>
                                DEX +2.<br>
                                Tăng 2% độ hồi phục các kỹ năng hồi phục.<br><br>
                                Loại: Trang sức<br>
                                Cấp độ yêu cầu: 110.
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2471, 'Shoes of Affection') ?></td>
                            <td>
                                MaxHP +500.<br>
                                Kháng 10% Á thần (Demi-humnan).<br>
                                Giảm 10% kháng các chủng tộc khác.<br><br>
                                Loại: Giày<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2472, 'Shoes of Judgement') ?></td>
                            <td>
                                MaxSP +150.<br>
                                Tăng 30% sát thương kỹ năng Judex. Kỹ năng Judex sẽ hao tốn hơn 40 SP.<br><br>
                                Loại: Giày<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2569, 'Shawl of Affection') ?></td>
                            <td>
                                Perfect Dodge +5. <br>
                                Có tỉ lệ tự động sử dụng kỹ năng Renovatio khi nhận sát thương thương vật lý hoặc sát thương phép thuật.<br><br>
                                Loại: Áo choàng<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2570, 'Shawl of Judgement') ?></td>
                            <td>
                                Flee +5. <br>
                                Có tỉ lệ tự động sử dụng kỹ năng Oratio khi nhận sát thương thương vật lý hoặc sát thương phép thuật.<br><br>
                                Loại: Áo choàng<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(15029, 'Robe of Affection') ?></td>
                            <td>
                                INT +1, MDEF +10.<br>
                                Giảm 50 SP kỹ năng Clementia.<br><br>
                                Loại: Áo choàng<br>
                                Thuộc tính: Thánh.<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(15030, 'Robe of Judgement') ?></td>
                            <td>
                                STR +1, INT +1, MDEF +10.<br>
                                Tăng 10% kháng quái vật chủng tộc Demon và Undead.<br>
                                Giảm 10% kháng các chủng tộc khác.<br><br>
                                Loại: Áo choàng<br>
                                Thuộc tính: Shadow<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(1657, 'Wand of Affection [2]') ?></td>
                            <td>
                                INT +2, MATK +160.<br>Tăng 10% độ hồi phục của các kỹ năng hồi phục.<br><br>
                                Nếu mặc cùng với Wand of Affection, Shawl of Affection, Shoes of Affection:<br>
                                Tăng 25% độ hồi phục của các kỹ năng hồi phục.<br>Kỹ năng Heal hao tốn hơn 20 SP.<br>
                                Giảm 2 giây thời gian sử dụng lại kỹ năng Sacrement, Lauda Agnus và Lauda Ramus.<br><br>
                                Loại: Gậy<br>
                                Thuộc tính: Thánh<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(16013, 'Mace of Judgement [2]') ?></td>
                            <td>
                                STR +1, INT +1, MATK +180.<br>
                                Có tỉ lệ tăng 20% sát thương vật lý và sát thương phép thuật lên quái vật chủng tộc Demon trong 7 giây.<br><br>
                                Nếu mặc cùng với Shoes of Judgement, Shawl of Judgement và Robe of Judgement:<br>
                                Tăng 15% sát thương lên quái vật hệ Undead.<br>
                                Tăng 200% sát thương kỹ năng Adoramus, kỹ năng Adoramus hao tốn hơn 30 SP.<br><br>
                                Loại: Chùy.<br>
                                Cấp độ yêu cầu: 100
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo_item(2156, 'Bible of Promise(1st Vol.) [1]') ?></td>
                            <td>Cho phép sử dụng kỹ năng Odin's Power cấp độ 1<br>Loại: Khiên<br>Cấp độ yêu cầu: 110</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </b-collapse>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h3 class="color-600">
                    <b-link class="tag-collapse" v-b-toggle.collapse-2>
                        <span>Warlock</span> <i></i>
                    </b-link>
                </h3>
                <b-collapse id="collapse-2">
                    <div class="pb-3">
                        NPC <strong>Guardian of Artifacts</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 104/76">mora 104/76</code> cho phép bạn đổi các trang bị Mora của nghề <strong>Warlock</strong>.
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Trang bị</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo_item(2467, 'Golden Rod Shoes') ?></td>
                                <td>
                                    MDEF +2, MaxHP +500.<br>
                                    Kháng 15% đòn đánh thuộc tính gió (Gió).<br><br>
                                    Loại: Giày.<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2468, 'Aqua Shoes') ?></td>
                                <td>
                                    MDEF +2, MaxHP +500.<br>
                                    Kháng 15% đòn đánh thuộc tính nước (Water).<br><br>
                                    Loại: Giày.<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2469, 'Crimson Shoes') ?></td>
                                <td>
                                    MDEF +2, MaxHP +500.<br>
                                    Kháng 15% đòn đánh thuộc tính lửa (Lửa).<br><br>
                                    Loại: Giày.<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2470, 'Forest Shoes') ?></td>
                                <td>
                                    MDEF +2, MaxHP +500.<br>
                                    Kháng 15% đòn đánh thuộc tính đất (Earth).<br><br>
                                    Loại: Giày.<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2859, 'Golden Rod Orb') ?></td>
                                <td>
                                    INT +1, MDEF +2.<br>
                                    Kháng 15% đòn đánh thuộc tính gió (Wind).<br>
                                    Có tỉ lệ tự động sử dụng kỹ năng Jupitel Thunder cấp độ 3 khi nhận sát thương từ kẻ thù.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2860, 'Aqua Orb') ?></td>
                                <td>
                                    INT +1, MDEF +2.<br>
                                    Kháng 15% đòn đánh thuộc tính nước (Water).<br>
                                    Có tỉ lệ tự động sử dụng kỹ năng Frost Nova cấp độ 3 khi nhận sát thương từ kẻ thù.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2861, 'Crimson Orb') ?></td>
                                <td>
                                    INT +1, MDEF +2.<br>
                                    Kháng 15% đòn đánh thuộc tính lửa (Fire).<br>
                                    Có tỉ lệ tự động sử dụng kỹ năng Sightrasher cấp độ 3 khi nhận sát thương từ kẻ thù.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2862, 'Forest Orb') ?></td>
                                <td>
                                    INT +1, MDEF +2.<br>
                                    Kháng 15% đòn đánh thuộc tính đất (Earth).<br>
                                    Có tỉ lệ tự động sử dụng kỹ năng Quagmire cấp độ 3 khi nhận sát thương từ kẻ thù.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15025, 'Golden Rod Robe') ?></td>
                                <td>
                                    INT +1, MDEF +10.<br>
                                    Giảm 3 giây thời gian thi triển kỹ năng Lord of Vermilion.<br>
                                    Nếu 120 INT hoặc hơn: INT +1<br><br>
                                    Loại: Áo giáp<br>
                                    Thuộc tính: Gió<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15026, 'Aqua Robe') ?></td>
                                <td>
                                    INT +1, MDEF +10.<br>
                                    Giảm 3 giây thời gian thi triển kỹ năng Storm Gust.<br>
                                    Nếu 120 INT hoặc hơn: INT +1<br><br>
                                    Loại: Áo giáp<br>
                                    Thuộc tính: Nước<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15027, 'Crimson Robe') ?></td>
                                <td>
                                    INT +1, MDEF +10.<br>
                                    Giảm 3 giây thời gian thi triển kỹ năng Meteor Storm.<br>
                                    Nếu 120 INT hoặc hơn: INT +1<br><br>
                                    Loại: Áo giáp<br>
                                    Thuộc tính: Lửa<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15028, 'Forest Robe') ?></td>
                                <td>
                                    INT +1, MDEF +10.<br>
                                    Giảm 3 giây thời gian thi triển kỹ năng Heaven's Drive.<br>
                                    Nếu 120 INT hoặc hơn: INT +1<br><br>
                                    Loại: Áo giáp<br>
                                    Thuộc tính: Đất<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2007, 'Golden Rod Staff [2]') ?></td>
                                <td>
                                    INT +3, MATK +230.<br>
                                    Tăng 12% sát thương kỹ năng Jupitel Thunder.<br><br>
                                    Nếu mặc cùng với Golden Rod Robe, Golden Rod Shoes, Golden Rod Orb:<br>
                                    Tăng 40% sát thương đòn đánh thuộc tính gió (Wind).<br>
                                    Giảm 30% sát thương đòn đánh thuộc tính đất (Earth).<br>
                                    Nhận thêm 50% sát thương từ đòn đánh thuộc tính đất (Earth).<br><br>
                                    Loại: Trượng hai tay<br>
                                    Thuộc tính: Gió<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2008, 'Aqua Staff [2]') ?></td>
                                <td>
                                    INT +3, MATK +230.<br>
                                    Tăng 10% sát thương kỹ năng Cold Bolt và Frost Dive.<br><br>
                                    Nếu mặc cùng với Aqua Robe, Aqua Shoes, Aqua Orb:<br>
                                    Tăng 40% sát thương đòn đánh thuộc tính nước (Water).<br>
                                    Giảm 30% sát thương đòn đánh thuộc tính gió (Wind).<br>
                                    Nhận thêm 50% sát thương từ đòn đánh thuộc tính gió (Wind).<br><br>
                                    Loại: Trượng hai tay<br>
                                    Thuộc tính: Nước<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2009, 'Crimson Staff [2]') ?></td>
                                <td>
                                    INT +3, MATK +230.<br>
                                    Tăng 10% sát thương kỹ năng Fire Bolt và Fire Ball.<br><br>
                                    Nếu mặc cùng với Crimson Robe, Crimson Shoes, Crimson Orb:<br>
                                    Tăng 40% sát thương đòn đánh thuộc tính lửa (Fire).<br>
                                    Giảm 30% sát thương đòn đánh thuộc tính nước (Water).<br>
                                    Nhận thêm 50% sát thương từ đòn đánh thuộc tính nước (Water).<br><br>
                                    Loại: Trượng hai tay<br>
                                    Thuộc tính: Lửa<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2010, 'Forest Staff [2]') ?></td>
                                <td>
                                    INT +3, MATK +230.<br>
                                    Tăng 10% sát thương kỹ năng Earth Spike và Heaven's Drive.<br><br>
                                    Nếu mặc cùng với Forest Robe, Forest Shoes, Forest Orb:<br>
                                    Tăng 40% sát thương đòn đánh thuộc tính đất (Earth).<br>
                                    Giảm 30% sát thương đòn đánh thuộc tính lửa (Fire).<br>
                                    Nhận thêm 50% sát thương từ đòn đánh thuộc tính lửa (Fire).<br><br>
                                    Loại: Trượng hai tay<br>
                                    Thuộc tính: Đất<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </b-collapse>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h3 class="color-600">
                    <b-link class="tag-collapse" v-b-toggle.collapse-3>
                        <span>Rune Knight</span> <i></i>
                    </b-link>
                </h3>
                <b-collapse id="collapse-3">
                    <div class="pb-3">
                        NPC <strong>Guardian of Power</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 152/97">mora 152/97</code> cho phép bạn đổi các trang bị Mora của nghề <strong>Rune Knight</strong>.
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Trang bị</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo_item(2475, 'Ur\'s Greaves [1]') ?></td>
                                <td>
                                    MaxSP +40.<br>
                                    Giảm 7% MaxHP. Mỗi cấp độ tinh luyện tăng 1% MaxHP. Tối đa +8.<br><br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2476, 'Peuz\'s Greaves [1]') ?></td>
                                <td>
                                    MaxSP +40.<br>
                                    Giảm 7 AGI. Mỗi cấp độ tinh luyện tăng 1 AGI. Tối đa +8.<br><br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2574, 'Ur\'s Manteau') ?></td>
                                <td>
                                    MaxHP +2%.<br>
                                    Kháng 10% đòn đánh thuộc tính trung tính (Neutral).<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2575, 'Peuz\'s Manteau') ?></td>
                                <td>
                                    Flee +10, CRIT +10.<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15036, 'Ur\'s Plate') ?></td>
                                <td>
                                    MDEF +10.<br>
                                    Mỗi độ tinh luyện:<br>
                                    Tăng 1% MaxHP.<br>
                                    Tăng 1% kháng thuộc tính trung tính (Neutral).<br>
                                    Tăng 1% kháng Á thần (Demi-human).<br><br>
                                    Nếu mặc cùng với Ur's Greaves, Ur's Manteau và Ur's Seal:<br>
                                    MaxHP +14%.<br>
                                    Các kỹ năng hao tốn hơn 10% SP.<br>
                                    Kháng 10% thuộc tính trung tính (Neutral).<br>
                                    Tăng 50% sát thương kỹ năng Hundred Spear.<br>
                                    Cho phép sử dụng kỹ năng Guard cấp độ 1.<br><br>
                                    Loại: Áo giáp<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Khalitzburg (15%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15037, 'Peuz\'s Plate') ?></td>
                                <td>
                                    MDEF +10.<br>
                                    ATK +20, Flee +17.<br>
                                    Nếu mặc cùng với Peuz's Greaves, Peuz's Manteau, Peuz's Seal:<br>
                                    Tăng 10% lên mọi kích cỡ quái vật.<br>
                                    Các kỹ năng hao tốn hơn 10% SP.<br>
                                    Tăng 100% sát thương kỹ năng Wind Cutter và Sonic Wave.<br>
                                    Có tỉ lệ tự động kích hoạt Storm Blast khi đang tấn công.<br>
                                    ASPD +2 sau khi bạn sử dụng kỹ năng Concentration.<br><br>
                                    Loại: Áo giáp<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Raydric (1.5%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2883, 'Ur\'s Seal [1]') ?></td>
                                <td>
                                    MaxHP +2%.<br>
                                    Giảm 5 SP kỹ năng Spiral Pierce và Hundred Spear.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Isilla và Vanberk (1.5%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2884, 'Peuz\'s Seal [1]') ?></td>
                                <td>
                                    ATK +20, MaxSP +20.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Frus và Skogul (0.3%).
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </b-collapse>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h3 class="color-600">
                    <b-link class="tag-collapse" v-b-toggle.collapse-4>
                        <span>Guillotine Cross</span> <i></i>
                    </b-link>
                </h3>

                <b-collapse id="collapse-4">
                    <div class="pb-3">
                        NPC <strong>Guardian of Power</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 152/97">mora 152/97</code> cho phép bạn đổi các trang bị Mora của nghề <strong>Guillotine Cross</strong>.
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Trang bị</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo_item(2477, 'Sapha Shoes [1]') ?></td>
                                <td>
                                    LUK +3, MaxSP +30.<br><br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Loli Ruri (3%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2478, 'Nab Shoes [1]') ?></td>
                                <td>
                                    Flee +3, INT +2.<br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Dullahan (3%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2577, 'Sapha Hood') ?></td>
                                <td>
                                    Flee +12.<br>
                                    Mỗi độ tinh luyện tăng 1% sát thương đòn đánh chí mạng.<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2578, 'Nab Hood') ?></td>
                                <td>
                                    Flee +1, ATK +1 mỗi 2 độ tinh luyện.<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15038, 'Sapha\'s Cloth [1]') ?></td>
                                <td>
                                    LUK +3.<br>
                                    Có tỉ lệ tự động dùng kỹ năng Meteor Assault cấp độ 1 (hoặc cấp độ cao nhất mà bạn học được) khi đang tấn công vật lý.<br>
                                    Nếu mặc cùng với Sapha Shoes, Sapha Hood và Sapha Ring:<br>
                                    Flee +10, CRIT +15, +40% sát thương đòn đánh chí mạng.<br>
                                    Tăng 20% sát thương kỹ năng Cross Impact.<br>
                                    Các kỹ năng hao tốn hơn 10% SP.<br><br>
                                    Loại: Áo giáp<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15039, 'Nab\'s Cloth [1]') ?></td>
                                <td>
                                    STR +2, INT +2.<br>
                                    Nếu mặc cùng với Nab Shoes, Nab Hood và Nab Ring:<br>
                                    Có tỉ lệ tự động dùng kỹ năng Soul Destroyer cấp độ 1 (hoặc cấp độ cao nhất mà bạn học được) khi đang tấn công vật lý.<br>
                                    Nếu 120 STR hoặc hơn:<br>
                                    ATK +30.<br>
                                    Tăng 10% sát thương lên mọi kích cỡ quái vật.<br><br>
                                    Loại: Áo giáp<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2886, 'Sapha Ring') ?></td>
                                <td>
                                    CRIT +3.<br>
                                    Kỹ năng Dark Illusion hao tốn hơn 5 SP.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Zombie Slaughter (1.5%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2887, 'Nab Ring') ?></td>
                                <td>
                                    ATK +10, MATK +20.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Ragged Zombie (15%).
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </b-collapse>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h3 class="color-600">
                    <b-link class="tag-collapse" v-b-toggle.collapse-5>
                        <span>Ranger</span> <i></i>
                    </b-link>
                </h3>
                <b-collapse id="collapse-5">
                    <div class="pb-3">
                        NPC <strong>Guardian of Power</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 152/97">mora 152/97</code> cho phép bạn đổi các trang bị Mora của nghề <strong>Ranger</strong>.
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="250">Trang bị</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo_item(2479, 'White Wing Boots [1]') ?></td>
                                <td>
                                    AGI +2.<br>
                                    Giảm 10 SP kỹ năng Aimed Bolt.<br><br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2480, 'Black Wing Boots [1]') ?></td>
                                <td>
                                    INT +2, MaxSP +5%.<br><br>
                                    Loại: Giày<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2580, 'White Wing Manteau') ?></td>
                                <td>
                                    AGI +2, Flee +10.<br>
                                    Có tỉ lệ tự động kích hoạt Flee +10 trong 7 giây khi đang tấn công vật lý.<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2581, 'Black Wing Manteau') ?></td>
                                <td>
                                    INT +2.<br>
                                    Perfect Dodge +1 mỗi cấp độ tinh luyện trên +6.<br><br>
                                    Loại: Áo choàng<br>
                                    Cấp độ yêu cầu: 100
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15042, 'White Wing Suits [1]') ?></td>
                                <td>
                                    +2% sát thương vật lý tầm xa mỗi độ tinh luyện.<br>
                                    Flee +1 mỗi độ tinh luyện.<br>
                                    Nếu mặc cùng với White Wing Boots, White Wing Manteau và White Wing Brooch:<br>
                                    ASPD +2.<br>
                                    Tăng 30% sát thương vật lý tầm xa.<br>
                                    Tăng 50% sát thương kỹ năng Arrow Storm.<br>
                                    Có tỉ lệ tự động dùng kỹ năng Double Strafe cấp độ 3 (hoặc cấp độ cao nhất mà bạn học được) khi đang tấn công tầm xa.<br><br>
                                    Loại: Áp giáp<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Anubis (3%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(15043, 'Black Wing Suits [1]') ?></td>
                                <td>
                                    INT +2.<br>
                                    ATK +3 mỗi độ tinh luyện.<br>
                                    Nếu mặc cùng với Black Wing Boots, Black Wing Manteau và Black Wing Brooch:<br>
                                    MaxHP +15%, Perfect Dodge +20.<br>
                                    Tăng 20% sát thương kỹ năng Cluster Bomb.<br>
                                    Giảm 30% sát thương vật lý tầm xa<br>
                                    ASPD -7.<br><br>
                                    Loại: Áo giáp<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Cũng rơi ra từ quái vật Luciola Vespa (1,5%).
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2890, 'White Wing Brooch') ?></td>
                                <td>
                                    DEX +2.<br>
                                    Tăng 3% sát thương vật lý tầm xa.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Rơi ra từ quái vật Gold Acidus (1,5%), Blue Acidus (1,5%)
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo_item(2891, 'Black Wing Brooch') ?></td>
                                <td>
                                    INT +2, Perfect Dodge +3.<br><br>
                                    Loại: Trang sức<br>
                                    Cấp độ yêu cầu: 100<br><br>

                                    Rơi ra từ quái vật Red Ferus (1.5%), Green Ferus (1.5%)
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </b-collapse>
            </div>
        </div>

        <div class="bg-light p-3 mb-3 border rounded">
            <h3 class="color-600">Cường hóa trang bị Mora</h3>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h5 class="color-500">Arch Bishop</h5>

                <p>NPC <strong>Master of Relics</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 96/74">mora 96/74</code> sẽ giúp bạn cường hóa trang bị Mora của Arch Bishop.</p>
                <p>Lưu ý khi cường hóa từ <strong>Artifact Researcher</strong>:</p>
                <ul>
                    <li>Mỗi cường hóa cần 2 <?php echo_item(6380, 'Mora Coin') ?>.</li>
                    <li>Cường hóa có tỉ lệ thất bại. Nếu thất bại trang bị của bạn sẽ biến mất.</li>
                    <li>Cường hóa thành công không làm mất độ tinh luyện, lá bài.</li>
                </ul>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h5 class="color-500">Warlock</h5>

                <p>NPC <strong>Artifact Crafter</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 99/93">mora 99/93</code> sẽ giúp bạn cường hóa trang bị Mora của Warlock.</p>
                <p>Lưu ý khi cường hóa từ <strong>Artifact Crafter</strong>:</p>
                <ul>
                    <li>Mỗi cường hóa cần 2 <?php echo_item(6380, 'Mora Coin') ?>.</li>
                    <li>Cường hóa có tỉ lệ thất bại. Nếu thất bại trang bị của bạn sẽ biến mất.</li>
                    <li>Cường hóa thành công không làm mất độ tinh luyện, lá bài.</li>
                </ul>
            </div>

            <div class="pt-1 pb-3 mb-3 border-bottom">
                <h5 class="color-500">Rune Knight, Guillotine Cross và Ranger</h5>

                <p>NPC <strong>Artifact Researcher</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 148/98">mora 148/98</code> sẽ giúp bạn cường hóa trang bị Mora của Rune Knight, Guillotine Cross và Ranger.</p>

                <p>Lưu ý khi cường hóa từ <strong>Artifact Researcher</strong>:</p>
                <ul>
                    <li>Mỗi cường hóa cần 1 <?php echo_item(6380, 'Mora Coin') ?> và 100,000 Zeny.</li>
                    <li>Tẩy cường hóa cần 1 <?php echo_item(6380, 'Mora Coin') ?> và 100,000 Zeny.</li>
                    <li>Cường hóa lần thứ 2 có tỉ lệ thất bại. Nếu thất bại sẽ mất hết các cường hóa.</li>
                    <li>Cường hóa lần thứ 3 có tỉ lệ thất bại. Nếu thất bại có 2 trường hợp sẽ xảy ra, 1 là mất hết các cường hóa, 2 là mất trang bị.</li>
                    <li>Tỉ lệ thành công cho lần cường hóa thứ 2 rơi vào khoảng 40%~60%.</li>
                    <li>Tỉ lệ thành công cho lần cường hóa thứ 3 rơi vào khoảng 30%~50%.</li>
                    <li>Cường hóa thành công không làm mất độ tinh luyện, lá bài.</li>
                    <li>Nếu độ tinh luyện của trang bị là +9 hoặc hơn. Bạn sẽ được lựa chọn kiểu cường hóa.</li>
                </ul>
            </div>

            <div class="pt-1">
                <h5 class="color-500">Các cường hóa khác</h5>

                <ul class="mb-3">
                    <li><strong>Master Tailor</strong> ở <code class="clipboard" data-clipboard-text="/navi mora 105/176">mora 105/176</code> cho phép cường hóa <?php echo_item(15024, 'Army Padding') ?>.</li>
                    <li><strong>Pendant Crafter</strong> ở <code class="clipboard" data-clipboard-text="/navi mora 123/177">mora 123/177</code> cho phép cường hóa <?php echo_item(2858, 'Pendant of Guardian') ?>.</li>
                    <li><strong>Pendant Crafter</strong> ở <code class="clipboard" data-clipboard-text="/navi mora 134/166">mora 134/166</code> cho phép cường hóa <?php echo_item(2568, 'Loki\'s Muffler') ?>.</li>
                </ul>

                <p>
                    Để cường hóa 3 trang bị trên bạn cần có 5 <?php echo_item(6380, 'Mora Coin') ?>.<br>
                    Nếu cường hóa thất bại thì sẽ mất hết các cường hóa trước đó, riêng trang bị <?php echo_item(15024, 'Army Padding') ?> sẽ biến mất nếu cường hóa thất bại.
                </p>
            </div>
        </div>

        <div class="bg-light p-3 mb-3 border rounded">
            <h3 class="color-600">Empower vũ khí Mora của Arch Bishop và Warlock</h3>

            <p>NPC <strong>Artifact Collector</strong> ở tọa độ <code class="clipboard" data-clipboard-text="/navi mora 124/82">mora 124/82</code> sẽ giúp bạn nâng cấp vũ khí Mora của Arch Bishop và Warlock.</p>
            <p>Vũ khí Mora của Arch Bishop và Warlock có thể nâng cấp lên phiên bản "Empower". Khi vũ khí đã được "Empower" thì số slot gắn card bị giảm từ 2 xuống 1 nhưng bù lại có MATK hoặc ATK cao hơn.</p>

            <p>Lưu ý khi nâng cấp vũ khí từ <strong>Artifact Collector</strong>:</p>
            <ul>
                <li>Độ tinh luyện của vũ khí phải +7 thì mới có thể nâng cấp.</li>
                <li>Không thất bại khi nâng cấp "Empower".</li>
                <li>Sau khi nâng cấp, độ tinh luyện sẽ mất, tất cả card, cường hóa trước đó cũng sẽ biến mất.</li>
            </ul>
        </div>

        <div class="bg-light p-3 mb-3 border rounded">
            <h3 class="color-600">Bảo vệ trang bị khi cường hóa?</h3>

            <p>Để bảo vệ trang bị khi cường hóa bạn có thể mang theo vật phẩm <?php echo_item(55003, 'HAB') ?>. Nếu cường hóa thất bại NPC sẽ hỏi bạn có muốn dùng HAB để phục hồi trang bị đã mất hay không, chọn "có" nếu bạn muốn phục hồi.</p>
        </div>

        <div class="bg-light p-3 border rounded">
            <h3 class="color-600">Cách kiếm Mora Coin</h3>

            <p>Bạn có thể kiếm Mora Coin bằng các nhiệm vụ sau đây:</p>
            <ul>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu/nhiem-vu-wandering-guardian-hazy-forest/') ?>">Wandering Guardian (Hazy Forest Instance)</a></li>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu/nhiem-vu-hang-ngay-o-mora/') ?>">Nhiệm vụ hằng ngày ở Mora</a></li>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu-vat-pham/helping-lope-and-euridi/') ?>">Helping Lope and Euridi</a></li>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu/find-the-research-tools/') ?>">Find the Research Tools</a></li>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu/bao-ve-moi-truong-cung-jojo/') ?>">Bảo vệ môi trường cùng Jojo</a></li>
                <li><a target="_blank" href="<?php echo home_url('/nhiem-vu/part-time-cung-liu/') ?>">Part time cùng Liu</a></li>
            </ul>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>

    <div class="scroll-top-wrapper">
        <span class="scroll-top-inner">
            <i class="ion-md-arrow-up"></i>
        </span>
    </div>

    <footer class="footer-page text-center">
        <div class="container">
            All ragnarok-related graphics and materials are copyright © 2002-2024 Gravity Co., Ltd. & Lee Myoungjin.
        </div>
	</footer>

    <b-button href="https://discord.gg/GTFn39vP3u" target="_blank" class="join-discord" v-b-tooltip.hover title="Vào kênh thảo luận của tụi mình nhé!">
        <ion-icon name="logo-discord"></ion-icon>
    </b-button>

    <?php get_template_part('/parts/modal-mob-info'); ?>

</div><!-- #app -->

<?php wp_footer(); ?>
</body>
</html>

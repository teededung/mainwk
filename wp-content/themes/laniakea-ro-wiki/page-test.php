<?php
fix_item_info();
function fix_item_info() {
    $args = array(
        'post_type' => 'items',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'	 	=> 'item_id',
                'compare' 	=> 'NOT EXISTS',
            ),
        )
    );

    $p = new WP_Query($args);
    $i = 0;
    if ($p->have_posts()):
        while ($p->have_posts()):
            $p->the_post();
            $title = get_the_title();

            // Nếu có chứa từ -
            if (strpos($title, ' ') !== false):
                $pieces = explode(" ", $title);
                $item_id = $pieces[0];
            else:
                $item_id = $title;
                $item_data = get_item_data($item_id, false, false);

                if (!empty($item_data->itemInfo->name)):
                    $item_name = $item_data->itemInfo->name;
                    $post_title = $item_id;
                    $post_title .= ' - ' . $item_name;

                    $my_post = array(
                        'ID'           => get_the_ID(),
                        'post_title'   => $post_title,
                    );
                    // Update the post into the database
                    wp_update_post( $my_post );
                endif;
            endif;

            update_field('item_id', $item_id);
            echo "Đã update cho item " . $item_id . '<br>';
            $i++;
        endwhile;

        if ($i):
            echo "-------------<br>";
            echo "Tổng cộng: " . $i . ' - đã được cập nhật item_id';
        endif;
    else:
        echo 'Not found!';
    endif;
}

function insert_skill_database() {
    $array_upload_dir = wp_upload_dir();
    $base_dir = $array_upload_dir['basedir'];
    $icon_url = $array_upload_dir['baseurl'] . '/skills/';

    //for ($i = 2001; $i <= 2010; $i++) {
    //    $url = 'https://static.divine-pride.net/images/skill/' .$i. '.png';
    //    $img = $array_upload_dir['path'] . '/' . $i . '.png';
    //    file_put_contents($img, file_get_contents($url));
    //}

    $parser_des = new LUAParser();
    $parser_id = new LUAParser();

    try {
        $parser_id->parseFile($base_dir . '/b.lua');
        $ids = $parser_id->data;

        $parser_des->parseFile($base_dir . '/a.lua');
        $descriptions = $parser_des->data['SKILL_DESCRIPT'];

        foreach($descriptions as $key => $description) {
            $skill_name = $description[0];
            $is_quest_skill = false;

            if ($skill_name != "") {
                $skill_label = str_replace("SKID.", "", $key);
                $skill_id = $ids["SKID"][$skill_label];

                if ($description[1] == "" || strpos($description[1], "Cấp độ tối đa: ") === false) {
                    $skill_max = 1;

                    if (strpos($description[1], "Finish Quest")) {
                        $is_quest_skill = true;
                    }
                } else {
                    $skill_max = str_replace("Cấp độ tối đa: ", "", $description[1]);
                }

                $args = array(
                    'numberposts' => 1,
                    'post_type' => 'skills',
                    'meta_key' => 'id',
                    'meta_value' => $skill_id
                );

                $the_query = new WP_Query($args);

                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()): $the_query->the_post();
                        $post_id = get_the_ID();

                        $args = array(
                            'ID' => $post_id,
                            'post_title' => $skill_id . " - " . $skill_name,
                            'post_name' => '',
                            'post_status' => 'publish',
                            'post_type' => 'skill'
                        );

                        wp_insert_post($args);

                        update_field('skill_label', str_replace("SKID.", "", $skill_label));
                        update_field('id', $skill_id);
                        update_field('max', $skill_max);
                        update_field('skill_name', $skill_name);
                        if (!has_post_thumbnail()) {
                            add_skill_icon($icon_url . $skill_id . '.png', $post_id, $skill_id . " - " . $skill_name);
                        }
                        if ($is_quest_skill) {
                            update_field('is_platinum_skill', 1);
                        }
                    endwhile;
                } else {
                    $args = array(
                        'post_title' => $skill_id . " - " . $skill_name,
                        'post_status' => 'publish',
                        'post_type' => 'skills'
                    );

                    $post_id = wp_insert_post($args);
                    if (!has_post_thumbnail($post_id)) {
                        add_skill_icon($icon_url . $skill_id . '.png', $post_id, $skill_id . " - " . $skill_name);
                    }
                    update_field('skill_label', $skill_label, $post_id);
                    update_field('id', $skill_id, $post_id);
                    update_field('max', $skill_max, $post_id);
                    update_field('skill_name', $skill_name, $post_id);

                    if ($is_quest_skill) {
                        update_field('is_platinum_skill', 1, $post_id);
                    }
                }
            }
        }
    } catch(Exception $e) {
        echo 'Exception: ',  $e->getMessage(), PHP_EOL;
    }
}
function remove_all_skill_icon() {
    // Remove all skill icons
    $args = array(
        'post_type'         => 'attachment',
        'post_status'       => 'inherit',
        'posts_per_page'    => - 1
    );

    $media = new WP_Query($args);

    if ($media->have_posts()):
        while ($media->have_posts()): $media->the_post();
            if (get_the_excerpt() == 'Skill Icon') {
                wp_delete_post(get_the_ID(), true);
            }
        endwhile;
    endif;

    // Remove all skill post
    $args = array(
        'post_type'         => 'skills',
        'post_status'       => 'publish',
        'posts_per_page'    => - 1
    );

    $skill = new WP_Query($args);

    if ($skill->have_posts()):
        while ($skill->have_posts()): $skill->the_post();
            wp_delete_post(get_the_ID(), true);
        endwhile;
    endif;
}
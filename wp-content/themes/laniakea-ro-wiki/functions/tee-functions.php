<?php

/**
 * Excerpt limit
 * @param  int 		$limit 	[Number characters]
 * @return string        	[short description]
 */
if (!function_exists('excerpt')):
	function excerpt($limit) {
	     $excerpt = explode(' ', get_the_excerpt(), $limit);
	     if (count($excerpt)>=$limit) {
	          array_pop($excerpt);
	          $excerpt = implode(" ",$excerpt).'...';
	     } else {
	          $excerpt = implode(" ",$excerpt);
	     }
	     $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	     return $excerpt;
	}
endif;


/**
 * Get related posts
 * @param  array 	$categories [Array categories of post]
 * @param  int 		$number     [Number post you want to get]
 * @return object           	[object posts]

 */
if (!function_exists('get_related_posts')):
	function get_related_posts($categories, $number = 10) {
	    $category_ids = array();
	    foreach($categories as $cat)
	        $category_ids[] = $cat->term_id;

		global $post;

		$args = array(
	        'category__in'          => $category_ids,
	        'post__not_in'          => array($post->ID),
	        'posts_per_page'     	=> $number,
	        'ignore_sticky_posts '  => 1
	    );

	    $posts = new WP_query($args);

	    return $posts;
	}
endif;


function exclude_term_by( $taxonomy = 'category', $args = [], $exclude = [] ) {
    /*
     * If there are no term slugs to exclude or if $exclude is not a valid array, return get_terms
     */
    if ( empty( $exclude ) || !is_array( $exclude ) )
        return get_terms( $taxonomy, $args );

    /*
     * If we reach this point, then we have terms to exclude by slug
     * Simply continue the process.
     */
    foreach ( $exclude as $value ) {

        /*
         * Use get_term_by to get the term ID and add ID's to an array
         */
        $term_objects = get_term_by( 'slug', $value, $taxonomy );
        $term_ids[] = (int) $term_objects->term_id;

    }

    /*
     * Set up the exclude parameter with an array of ids from $term_ids
     */
    $excluded_ids = [
        'exclude' => $term_ids
    ];

    /*
     * Merge the user passed arguments $args with the excluded terms $excluded_ids
     * If any value is passed to $args['exclude'], it will be ignored
     */
    $merged_arguments = array_merge( $args, $excluded_ids );

    /*
     * Lets pass everything to get_terms
     */
    $terms = get_terms( $taxonomy, $merged_arguments );

    /*
     * Return the results from get_terms
     */
    return $terms;
}
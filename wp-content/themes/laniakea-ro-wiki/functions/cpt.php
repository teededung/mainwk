<?php
// Register Custom Post Type
function cpt_item_quests() {

	$labels = array(
		'name'                  => _x( 'Item Quests', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Item Quest', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Item Quests', 'text_domain' ),
		'name_admin_bar'        => __( 'Item Quests', 'text_domain' ),
		'archives'              => __( 'Item Quest Archives', 'text_domain' ),
		'attributes'            => __( 'Item Quest Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item Quest:', 'text_domain' ),
		'all_items'             => __( 'All Item Quests', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item Quest', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Item Quests', 'text_domain' ),
		'description'           => __( 'Item Quests', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'item_quests', $args );

}
add_action( 'init', 'cpt_item_quests', 0 );


function cpt_skills() {
    $labels = array(
        'name'                  => _x( 'Skills', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Skill', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Skills', 'text_domain' ),
        'name_admin_bar'        => __( 'Skills', 'text_domain' ),
        'archives'              => __( 'Skill Archives', 'text_domain' ),
        'attributes'            => __( 'Skill Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Skill:', 'text_domain' ),
        'all_items'             => __( 'All Skills', 'text_domain' ),
        'add_new_item'          => __( 'Add New Skill', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Skill', 'text_domain' ),
        'edit_item'             => __( 'Edit Skill', 'text_domain' ),
        'update_item'           => __( 'Update Skill', 'text_domain' ),
        'view_item'             => __( 'View Skill', 'text_domain' ),
        'view_items'            => __( 'View Skills', 'text_domain' ),
        'search_items'          => __( 'Search Skill', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into skill', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this skill', 'text_domain' ),
        'items_list'            => __( 'Skills list', 'text_domain' ),
        'items_list_navigation' => __( 'Skills list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter skills list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Skills', 'text_domain' ),
        'description'           => __( 'Skills', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail'),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'skills', $args );

}
add_action( 'init', 'cpt_skills', 0 );


function cpt_build() {
    $labels = array(
        'name'                  => _x( 'Builds', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Build', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Builds', 'text_domain' ),
        'name_admin_bar'        => __( 'Builds', 'text_domain' ),
        'archives'              => __( 'Build Archives', 'text_domain' ),
        'attributes'            => __( 'Build Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Build:', 'text_domain' ),
        'all_items'             => __( 'All Builds', 'text_domain' ),
        'add_new_item'          => __( 'Add New Build', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Build', 'text_domain' ),
        'edit_item'             => __( 'Edit Build', 'text_domain' ),
        'update_item'           => __( 'Update Build', 'text_domain' ),
        'view_item'             => __( 'View Build', 'text_domain' ),
        'view_items'            => __( 'View Builds', 'text_domain' ),
        'search_items'          => __( 'Search Build', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into build', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this build', 'text_domain' ),
        'items_list'            => __( 'Builds list', 'text_domain' ),
        'items_list_navigation' => __( 'Builds list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter builds list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Build', 'text_domain' ),
        'description'           => __( 'Build', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor'),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page'
    );
    register_post_type( 'build', $args );

}
add_action( 'init', 'cpt_build', 0 );

function cpt_items() {
    $labels = array(
        'name'                  => _x( 'Items', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Item', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Items', 'text_domain' ),
        'name_admin_bar'        => __( 'Item', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'attributes'            => __( 'Item Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'view_items'            => __( 'View Items', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Item', 'text_domain' ),
        'description'           => __( 'Item', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor'),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page'
    );
    register_post_type( 'items', $args );

}
add_action( 'init', 'cpt_items', 0 );


function cpt_npc() {
    $labels = array(
        'name'                  => _x( 'NPC', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'NPC', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'NPC', 'text_domain' ),
        'name_admin_bar'        => __( 'NPC', 'text_domain' ),
        'archives'              => __( 'NPC Archives', 'text_domain' ),
        'attributes'            => __( 'NPC Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'NPC Item:', 'text_domain' ),
        'all_items'             => __( 'All NPCs', 'text_domain' ),
        'add_new_item'          => __( 'Add New NPC', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New NPC', 'text_domain' ),
        'edit_item'             => __( 'Edit NPC', 'text_domain' ),
        'update_item'           => __( 'Update NPC', 'text_domain' ),
        'view_item'             => __( 'View NPC', 'text_domain' ),
        'view_items'            => __( 'View NPCs', 'text_domain' ),
        'search_items'          => __( 'Search NPC', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into NPC', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this NPC', 'text_domain' ),
        'items_list'            => __( 'NPCs list', 'text_domain' ),
        'items_list_navigation' => __( 'NPCs list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter NPCs list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'NPC', 'text_domain' ),
        'description'           => __( 'NPC', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail'),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page'
    );
    register_post_type( 'NPC', $args );

}
add_action( 'init', 'cpt_npc', 0 );


function cpt_lapine() {
    $labels = array(
        'name'                  => _x( 'Lapine', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Lapine', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Lapine', 'text_domain' ),
        'name_admin_bar'        => __( 'Lapine', 'text_domain' ),
        'archives'              => __( 'Lapine Archives', 'text_domain' ),
        'attributes'            => __( 'Lapine Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Lapine Item:', 'text_domain' ),
        'all_items'             => __( 'All Lapines', 'text_domain' ),
        'add_new_item'          => __( 'Add New Lapine', 'text_domain' ),
        'add_new'               => __( 'Add Lapine', 'text_domain' ),
        'new_item'              => __( 'New Lapine', 'text_domain' ),
        'edit_item'             => __( 'Edit Lapine', 'text_domain' ),
        'update_item'           => __( 'Update Lapine', 'text_domain' ),
        'view_item'             => __( 'View Lapine', 'text_domain' ),
        'view_items'            => __( 'View Lapines', 'text_domain' ),
        'search_items'          => __( 'Search Lapine', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into Lapine', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Lapine', 'text_domain' ),
        'items_list'            => __( 'Lapines list', 'text_domain' ),
        'items_list_navigation' => __( 'Lapines list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter Lapines list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Lapine', 'text_domain' ),
        'description'           => __( 'Lapine', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page'
    );
    register_post_type( 'lapine', $args );

}
add_action( 'init', 'cpt_lapine', 0 );
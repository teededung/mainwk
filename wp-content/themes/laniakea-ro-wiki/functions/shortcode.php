<?php

/**
 * @param $atts
 * @param $content
 * @return false|string
 */
function badge_short_code($atts, $content)
{
    ob_start(); ?>
    <span class="badge bg-<?php echo $atts['bg']; ?>"><?php echo $content ?></span>
<?php
    return ob_get_clean();
}
add_shortcode("badge", "badge_short_code");

/**
 * @param $atts
 * @param $name
 * @return string
 */
function where_npc_short_code($atts, $name)
{
    $index = isset($atts['index']) ? $atts['index'] : 1;
    return "<a href='#' class='where-npc' @click.prevent='whereNPC(\"" . sanitize_title($name) . "\", " . $index . ")'>" . $name . "</a>";
}
add_shortcode("npc", "where_npc_short_code");

/**
 * @param $atts
 * @param $content
 * @return false|string
 */
function alert_short_code($atts, $content)
{
    ob_start(); ?>
    <div class="alert alert-<?php echo $atts['type']; ?>"><?php echo apply_filters('the_content', $content); ?></div>
<?php
    return ob_get_clean();
}
add_shortcode("alert", "alert_short_code");

/**
 * @param $atts
 * @param $name
 * @return false|string
 */
function item_info_short_code($atts, $name)
{
    ob_start();
    $item_id = 512; // Apple
    if (isset($atts['item_id'])) :
        $item_id = $atts['item_id'];
    elseif (isset($atts['id'])) :
        $item_id = $atts['id'];
    endif;
?>

    <?php if (isset($atts['no-click'])) : ?>
        <a class="item-name no-click">
            <?php if (!isset($atts['no-icon'])) : ?>
                <img src="https://whisper.hahiu.com/ro/item/<?php echo $item_id; ?>.png" alt="<?php echo $name; ?>">
            <?php endif; ?>
            <?php if (!isset($atts['no-name'])) : ?>
                <span><?php echo $name; ?></span>
            <?php endif; ?>
        </a>
    <?php else : ?>
        <a class="item-name" href="#" data-type="item-info" data-id="<?php echo $item_id; ?>" data-name="<?php echo $name; ?>" @click.prevent="getItemInfo(<?php echo $item_id ?>, `<?php echo $name ?>`)">
            <?php if (!isset($atts['no-icon'])) : ?>
                <img src="https://whisper.hahiu.com/ro/item/<?php echo $item_id; ?>.png" alt="<?php echo $name; ?>">
            <?php endif; ?>
            <?php if (!isset($atts['no-name'])) : ?>
                <span><?php echo $name; ?></span>
            <?php endif; ?>
        </a>
    <?php endif; ?>
<?php
    return ob_get_clean();
}
add_shortcode("item", "item_info_short_code");

/**
 * @param $atts
 * @param $name
 * @return false|string
 */
function monster_info_short_code($atts, $name)
{
    ob_start();
    $mob_id = 1002; // Poring
    if (isset($atts['mob_id'])) :
        $mob_id = $atts['mob_id'];
    elseif (isset($atts['id'])) :
        $mob_id = $atts['id'];
    endif;
?>
    <a class="item-name" data-type="mob-info" data-id="<?php echo $mob_id; ?>" data-name="<?php echo $name; ?>" href="#" @click.prevent="getMobInfo(<?php echo $mob_id ?>, `<?php echo $name ?>`)">
        <span><?php echo $name; ?></span>
    </a><?php
        return ob_get_clean();
    }
    add_shortcode("mob", "monster_info_short_code");

    /**
     * @param $atts
     * @param $content
     * @return string
     */
    function scroll_short_code($atts, $content)
    {
        return "<a href='#" . sanitize_title($content) . "' @click.prevent='scrollTo(\"" . sanitize_title($content) . "\")'>" . $content . "</a>";
    }
    add_shortcode("scroll", "scroll_short_code");

    /**
     * @param $id
     * @param $name
     * @return string
     */
    function _item($id, $name)
    {
        return do_shortcode('[item id="' . $id . '"]' . $name . '[/item]');
    }

    /**
     * @param $id
     * @param $name
     */
    function echo_item($id, $name)
    {
        echo do_shortcode('[item id="' . $id . '"]' . $name . '[/item]');
    }

    function skill_short_code($atts)
    {
        $skill_id = 1;
        if (isset($atts['skill_id'])) :
            $skill_id = $atts['skill_id'];

        elseif (isset($atts['id'])) :
            $skill_id = $atts['id'];
        endif;

        $skill_data = get_skill_info($skill_id);
        $skill = $skill_data['skillInfo'];
        ob_start();
        ?>
    <a class="skill item-name" data-type="skill-info" data-id="<?php echo $skill_id ?>" href="#" @click.prevent="getSkillInfo(<?php echo $skill_id ?>)">
        <?php if (!isset($atts['no-icon'])) : ?>
            <img src="<?php echo $skill['icon'] ?>" alt="<?php echo $skill['skillName']; ?>">
        <?php endif; ?>
        <span><?php echo $skill['skillName'] ?></span>
    </a>
<?php
        return ob_get_clean();
    }
    add_shortcode("skill", "skill_short_code");

    function lapine_short_code($atts)
    {
        if (isset($atts['item_id'])) :
            $lapine_id = $atts['item_id'];
        endif;

        $hide_require = false;
        if (isset($atts['hide_require'])) :
            $hide_require = $atts['hide_require'];
        endif;

        // [lapine item_id="23720"]
        $args = array(
            'post_type' => 'lapine',
            'posts_per_page' => 1,
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'lapine_item_id',
                    'value' => $lapine_id,
                    'compare' => '='
                )
            )
        );
        $lapine_object = new WP_Query($args);

        ob_start(); ?>
    <?php if ($lapine_object->have_posts()) : ?>
        <?php while ($lapine_object->have_posts()) : $lapine_object->the_post(); ?>
            <?php
                $lapine_item_id = get_field("lapine_item_id");
                $lapine_item_name = get_field('lapine_item_name');
                $need_refine_min = get_field('need_refine_min');
                $need_refine_max = get_field('need_refine_max');
                $type = get_field('type');
            ?>

            <div class="row no-gutters row-lapine" id="lapine-<?php echo sanitize_title($lapine_item_id); ?>">
                <div class="col-12 mb-3 lapine-title">
                    <h4 class="color-500">
                        <?php if ($lapine_item_id < 700000) : ?>
                            <?php echo_item($lapine_item_id, $lapine_item_name); ?>
                        <?php else : ?>
                            <?php echo $lapine_item_name ?>
                        <?php endif; ?>
                    </h4>
                </div>

                <?php if (!$hide_require) : ?>
                    <div class="col-md-5 pr-0 pr-md-3">
                        <ul class="pl-3 lapine-info-list">
                            <?php $description = get_field('description'); ?>
                            <?php if ($description) : ?>
                                <li>
                                    <div class="mb-2"><strong>Giới thiệu:</strong></div>
                                    <?php echo $description; ?>
                                </li>
                            <?php endif; ?>
                            <li><strong>Cách thu thập:</strong>
                                <?php $how_to_obtain = get_field('how_to_obtain'); ?>
                                <?php echo (strlen($how_to_obtain) > 0) ? $how_to_obtain : 'Đang cập nhật...' ?>
                            </li>
                            <li>
                                <div class="mb-2"><strong>Cường hóa các trang bị:</strong></div>

                                <?php if (get_field('use_text_for_target_items')) : ?>
                                    <?php the_field('target_items_text'); ?>
                                <?php else : ?>
                                    <?php $target_items = get_field("target_items"); ?>
                                    <?php if ($target_items) : ?>
                                        <div class="w-lapine-target-items position-relative">
                                            <ul class="lapine-target-items">
                                                <?php foreach ($target_items as $index => $item) : ?>
                                                    <li class="<?php echo ($index > 9) ? "hide-when-not-expand" : ""; ?>"><?php echo_item($item['item_id'], $item['item_name']) ?></li>
                                                <?php endforeach; ?>
                                                <?php $row_count = count($target_items); ?>
                                                <?php if ($row_count > 9) : ?>
                                                    <li class="will-hide-when-expand">...</li>
                                                    <li class="will-hide-when-expand"><a href="#" @click.prevent="readMore(<?php echo $lapine_item_id; ?>)">Click để xem <?php echo ($row_count - 10) ?> trang bị nữa</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </li>
                            <?php if ($need_refine_min > 0 || ($need_refine_max && $need_refine_max < 20)) : ?>
                                <li>
                                    <?php if ($need_refine_min > 0) : ?>
                                        <div><strong>Yêu cầu tinh luyện tối thiểu:</strong>&nbsp;+<?php echo $need_refine_min; ?></div>
                                    <?php endif; ?>
                                    <?php if ($need_refine_max && $need_refine_max < 20) : ?>
                                        <div><strong>Yêu cầu tinh luyện tối đa:</strong>&nbsp;+<?php echo $need_refine_max; ?></div>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <div class="<?php echo (!$hide_require) ? 'col-md-7' : 'col-md-12'; ?>">
                    <?php if ($type == 'upgrade') : ?>
                        <?php $enchant_lists = get_field('enchant_list'); ?>
                        <?php if ($enchant_lists) : ?>
                            <b-tabs>
                                <?php foreach ($enchant_lists as $index => $list) : ?>
                                    <b-tab title="<?php echo 'Dòng ' . ($index + 1) ?>">
                                        <?php if ($list['options']) : ?>
                                            <table class="table table-hover table-sm table-bordered table-striped">
                                                <caption>Nhận được 1 hiệu ứng ngẫu nhiên trong các hiệu ứng trên</caption>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Hiệu ứng</th>
                                                        <th class="text-center">Min ~ Max</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($list['options'] as $option) : ?>
                                                        <tr>
                                                            <td class="text-center align-middle"><small><?php echo convert_random_option($option['name']) ?></small></td>
                                                            <td class="text-center align-middle"><small><?php echo $option['value'] ?></small></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </b-tab>
                                <?php endforeach; ?>
                            </b-tabs>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
<?php
        wp_reset_postdata();
        return ob_get_clean();
    }
    add_shortcode("lapine", "lapine_short_code");

<?php

/**
 * MAIN AJAX FUNCTION
 * $ajax_nonce = wp_create_nonce( "n" );
 * @return string
 * 
 * -----------------------------------------
 */
function tee_ajax_function(){
	//$check_nonce = check_ajax_referer( 'n', 'n', false );

    $method = $_POST['method'];

    $result = array();

    switch ($method):
        case 'load_npc':
            $result = load_npc($_POST['post_id']);
            break;
        case 'get_item_info':
            $result = get_item_data($_POST['itemId']);
            break;
        case 'search_posts':
            $result = search_posts($_POST['keyword']);
            break;
        case 'get_skill_tree':
            $result = get_skill_tree($_POST['job']);
            break;
        case 'get_skill_info':
            $result = get_skill_info($_POST['skill_id'], $_POST['get_required_skills']);
            break;
        case 'get_how_to_get':
            $result = get_how_to_get($_POST['itemId']);
            break;
        case 'insert_how_to_get':
            $result = insert_how_to_get($_POST['itemId'], $_POST['howToGet']);
            break;
        case 'update_shop':
            $result = update_shop($_POST['shop_NPCID'], $_POST['shop_optionName'], $_POST['shop_currency'], $_POST['shop_ItemID_Price']);
            break;
        case 'get_build_equip':
            $result = get_build_equip($_POST['build_name']);
            break;
        default:
            # code...
            break;
    endswitch;

	echo json_encode($result);
	exit;
}
add_action( 'wp_ajax_tee_ajax_function', 'tee_ajax_function' );
add_action( 'wp_ajax_nopriv_tee_ajax_function', 'tee_ajax_function' );
/*---------------------------------------------*/


/**
 * @param $post_id
 *
 * @return array
 */
function load_npc($post_id) {
	$array = array();

	$npc = get_field('npc', $post_id);

	$npc_list = array();
	if ($npc):
		foreach($npc as $n):

            $locs = $n['location'];

            $locations = explode("|", $locs);

			$npc_list[] = array(
				'id'        => sanitize_title($n['name']),
				'name'      => $n['name'],
				'img'       => ($n['image_name'] != '') ? $n['image_name'] : 'npc-no-img.png',
				'locations' => $locations,
				'mark'      => false,
                'index'     => 1
			);
		endforeach;
	endif;

	$array['status'] = 'OK';
	$array['npc'] = $npc_list;
	return $array;
}


function get_item_data($item_id, $get_drops = true, $get_desc = true) {
	$url = 'https://api.hahiu.com/?module=item&action=view&id=' . $item_id;
	if (!$get_drops)
        $url .= '&no_drops';
	if (!$get_desc)
	    $url .= '&no_desc';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result);

	return $result;
}

function search_posts($keyword) {
    $array = array();

    $array['status'] = 'OK';

    // Query Posts
    $args_search_1 = array(
        's' => $keyword,
        'post_status'       => 'publish',
        'posts_per_page'    => 7,
        'post_type'         => 'post'
    );

    // Builds
    $page = get_page_by_path('builds');
    $args_search_2 = array(
        's' => $keyword,
        'post_status'       => 'publish',
        'posts_per_page'    => 5,
        'post_type'         => 'page',
        'post_parent'       => $page->ID
    );
    // == Results

    // Posts
    $posts_object = new WP_Query($args_search_1);
    if ($posts_object->have_posts()):
        while ($posts_object->have_posts()): $posts_object->the_post();
            $array['results'][] = array(
                'id'    => get_the_ID(),
                'type'  => 1,
                'title' => get_the_title(),
                'category'  => get_the_category(),
                'permalink' => get_the_permalink(),
                'select' => false
            );
        endwhile;
    endif;

    // Builds
    $posts_object = new WP_Query($args_search_2);
    if ($posts_object->have_posts()):
        while ($posts_object->have_posts()): $posts_object->the_post();
            $array['results'][] = array(
                'id'    => get_the_ID(),
                'type'  => 2,
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'select' => false
            );
        endwhile;
    endif;

    if (count($array['results']) == 0) {
        $array['status'] = 'Not Found';
    }

    return $array;
}

/**
 * @param $job
 * @return array
 */
function get_skill_tree($job) {
    $array = array();
    $array['status'] = 'OK';

    $post_object = get_page_by_path('skill-tree/' . sanitize_title($job), 'OBJECT');
    $post_id = $post_object->ID;

    // Novice
    $novice_other = get_field('novice_using_from_other_class', $post_id);
    if ($novice_other):
        $class = get_field('novice_class', $post_id);
        $novice_skills = get_skill_tree_sub($class->ID, 'novice_skill_tree');
        $novice_skill_points = get_field('novice_skill_points', $class->ID);
    else:
        $novice_skills = get_skill_tree_sub($post_id, 'novice_skill_tree');
        $novice_skill_points = get_field('novice_skill_points', $post_id);
    endif;
    $novice_skills['skills'] = get_dummy_skills($novice_skills['skills']);


    // Job 1
    $job_1_other = get_field('job_1_using_from_other_class', $post_id);
    if ($job_1_other):
        $class = get_field('job_1_class', $post_id);
        $job_1_skills = get_skill_tree_sub($class->ID, 'job_1_skill_tree');
        $job_1_skill_points = get_field('job_1_skill_points', $class->ID);
    else:
        $job_1_skills = get_skill_tree_sub($post_id, 'job_1_skill_tree');
        $job_1_skill_points = get_field('job_1_skill_points', $post_id);
    endif;
    $job_1_skills['skills'] = get_dummy_skills($job_1_skills['skills']);


    // Job 2
    $job_2_other = get_field('job_2_using_from_other_class', $post_id);
    if ($job_2_other):
        $class = get_field('job_2_class', $post_id);
        $job_2_skills = get_skill_tree_sub($class->ID, 'job_2_skill_tree');
        $job_2_skill_points = get_field('job_2_skill_points', $class->ID);
    else:
        $job_2_skills = get_skill_tree_sub($post_id, 'job_2_skill_tree');
        $job_2_skill_points = get_field('job_2_skill_points', $post_id);
    endif;
    $job_2_skills['skills'] = get_dummy_skills($job_2_skills['skills']);


    // Transcendent
    $job_transcendent_other = get_field('job_transcendent_using_from_other_class', $post_id);
    if ($job_transcendent_other):
        $class = get_field('job_transcendent_class', $post_id);
        $job_transcendent_skills = get_skill_tree_sub($class->ID, 'job_transcendent_skill_tree');
        $job_transcendent_skill_points = get_field('job_transcendent_skill_points', $class->ID);
    else:
        $job_transcendent_skills = get_skill_tree_sub($post_id, 'job_transcendent_skill_tree');
        $job_transcendent_skill_points = get_field('job_transcendent_skill_points', $post_id);
    endif;
    $job_transcendent_skills['skills'] = get_dummy_skills($job_transcendent_skills['skills']);


    // Job 3
    $job_3_other = get_field('job_3_using_from_other_class', $post_id);
    if ($job_3_other):
        $class = get_field('job_3_class', $post_id);
        $job_3_skills = get_skill_tree_sub($class->ID, 'job_3_skill_tree');
        $job_3_skill_points = get_field('job_3_skill_points', $class->ID);
    else:
        $job_3_skills = get_skill_tree_sub($post_id, 'job_3_skill_tree');
        $job_3_skill_points = get_field('job_3_skill_points', $post_id);
    endif;
    $job_3_skills['skills'] = get_dummy_skills($job_3_skills['skills']);


    if (count($novice_skills['skills'])):
        $array['skillTree'][$novice_skills['job']]['skills'] = $novice_skills['skills'];
        $array['skillTree'][$novice_skills['job']]['skillPoints'] = intval($novice_skill_points);
        $array['skillTree'][$novice_skills['job']]['skillPointsUsed'] = intval($novice_skill_points);
    endif;

    if (count($job_1_skills['skills'])):
        $array['skillTree'][$job_1_skills['job']]['skills'] = $job_1_skills['skills'];
        $array['skillTree'][$job_1_skills['job']]['skillPoints'] = intval($job_1_skill_points);
        $array['skillTree'][$job_1_skills['job']]['skillPointsUsed'] = 0;
    endif;

    $count_1 = count($job_2_skills['skills']);
    if ($count_1):
        $array['skillTree'][$job_2_skills['job']]['skills'] = $job_2_skills['skills'];
        $array['skillTree'][$job_2_skills['job']]['skillPoints'] = intval($job_2_skill_points);
        $array['skillTree'][$job_2_skills['job']]['skillPointsUsed'] = 0;
    endif;

    $count_2 = count($job_transcendent_skills['skills']);
    if ($count_2):
        $count = $count_1 > $count_2 ? $count_1 : $count_2;

        for($i = 0; $i < $count; $i++):
            if (empty($job_transcendent_skills['skills'][$i])):
                if (!empty($job_2_skills['skills'][$i])):
                    $job_transcendent_skills['skills'][$i] = $job_2_skills['skills'][$i];
                endif;
            endif;
        endfor;

        $array['skillTree'][$job_transcendent_skills['job']]['skills'] = $job_transcendent_skills['skills'];
        $array['skillTree'][$job_transcendent_skills['job']]['skillPoints'] = intval($job_transcendent_skill_points);
        $array['skillTree'][$job_transcendent_skills['job']]['skillPointsUsed'] = 0;

        unset($array['skillTree'][$job_2_skills['job']]);
    endif;

    if (count($job_3_skills['skills'])):
        $array['skillTree'][$job_3_skills['job']]['skills'] = $job_3_skills['skills'];
        $array['skillTree'][$job_3_skills['job']]['skillPoints'] = intval($job_3_skill_points);
        $array['skillTree'][$job_3_skills['job']]['skillPointsUsed'] = 0;
    endif;

    return $array;
}

/**
 * @param $post_id
 * @param $field
 * @return array
 */
function get_skill_tree_sub($post_id, $field) {
    $skills_data = get_field($field, $post_id);

    if ($skills_data):
        $skills = array();
        foreach($skills_data as $skill_data):
            $skill = $skill_data['skill'];
            $p_id = $skill->ID;
            $skill_id = intval(get_field('id', $p_id));
            $position = $skill_data['position'];

            $skill_info = array(
                'postID' => $p_id,
                'permalink' => get_permalink($p_id),
                'skillID' => $skill_id,
                'skillName' => get_field('skill_name', $p_id),
                'icon' => get_the_post_thumbnail_url($p_id),
                'position' => intval($position),
                'max' => intval(get_field('max', $p_id)),
                'isQuestSkill' => get_field('is_platinum_skill', $p_id) == 1 ? 1 : 0,
                'isSpiritSkill' => get_field('is_spirit_skill', $p_id) == 1 ? 1 : 0,
                'currentLv' => ($skill_id == 1) ? 9 : 0,
                'required' => false,
                'requiredLv' => 0
            );

            $has_required_skills = $skill_data['has_required_skills'];

            if ($has_required_skills):
                $require_skills = $skill_data['required_skills'];

                $temp = array();
                foreach($require_skills as $sk):
                    $skill_id = get_field("id", $sk['skill']);
                    $temp[] = array(
                        'level' => intval($sk['level']),
                        'skillID' => intval($skill_id)
                    );
                endforeach;

                $skill_info['requiredSkills'] = $temp;
            endif;

            $skills[] = $skill_info;
        endforeach;

        $trans_text = "";
        if ($field == "job_transcendent_skill_tree") {
            $trans_text = " (Transcendent)";
        }

        return array(
            'job' => get_the_title($post_id) . $trans_text,
            'skills' => $skills
        );
    endif;

    return array();
}

/**
 * @param $skills
 * @return array
 */
function get_dummy_skills($skills) {
    $position = 0;
    $temp = array();
    $array_position = array();

    if ($skills):
        foreach ($skills as $skill):
            $array_position[] = intval($skill['position']);
            if ($skill['position'] > $position):
                $position = $skill['position'];
            endif;
        endforeach;

        $cells = ceil($position / 7) * 7;
        $count = 0;

        for ($i = 1; $i <= $cells; $i++):
            if (in_array($i, $array_position)):
                $temp[] = $skills[$count];
                $count++;
            else:
                $temp[] = array();
            endif;
        endfor;
    endif;

    return $temp;
}

/**
 * @param $skill_id
 * @param bool $get_required_skills
 * @return array
 */
function get_skill_info($skill_id, $get_required_skills = false) {
    $array = array();

    $args = array(
        'post_type' => 'skills',
        'post_status' => 'publish',
        'posts_per_page' => 1,
        'meta_key' => 'id',
        'meta_value' => $skill_id
    );

    $skill_object = new WP_Query($args);

    if ($skill_object->have_posts()):
        while ($skill_object->have_posts()): $skill_object->the_post();
            $array['status'] = "OK";

            $array['skillInfo'] = array(
                'skillID' => intval($skill_id),
                'skillName' => get_field('skill_name'),
                'max' => get_field('max'),
                'otherName' => get_field('other_name') ? get_field('other_name') : "",
                'isQuestSkill' => get_field('is_platinum_skill') ? true : false,
                'isSpiritSkill' => get_field('is_spirit_skill') ? true : false,
                'icon' => get_the_post_thumbnail_url(),
                'description' => get_the_content(),
                'mark' => false
            );

            // Has Required Skills?
            if ($get_required_skills):
                $pid_this_skill = get_the_ID();
                $required_skills_info = array_merge(
                    get_required_skills_sub('job_1_skill_tree', $pid_this_skill),
                    get_required_skills_sub('job_2_skill_tree', $pid_this_skill),
                    get_required_skills_sub('job_transcendent_skill_tree', $pid_this_skill),
                    get_required_skills_sub('job_3_skill_tree', $pid_this_skill)
                );

                $array['skillInfo']['requiredSkills'] = $required_skills_info;
            endif;
        endwhile;
        wp_reset_postdata();
    else:
        $array['status'] = "Failed";
        $array['test'] = $skill_object;
        $array['message'] = "Không tìm thấy thông tin kỹ năng này.";
    endif;

    return $array;
}

/**
 * @param $key
 * @param $skill_post_id
 * @return array
 */
function get_required_skills_sub($key, $skill_post_id) {
    $args = array(
        'posts_per_page'   => -1,
        'post_type'     => 'page',
        'meta_query'    => array(
            array(
                'key'       => $key . '_$_skill',
                'compare'   => '=',
                'value'     => $skill_post_id
            ),
        )
    );

    $required_skills = new WP_Query($args);
    $required_skills_info = array();

    // Những bài viết skill tree có chứa $skill_post_id
    if ($required_skills->have_posts()):
        while($required_skills->have_posts()): $required_skills->the_post();
            $temp = array();
            $temp2 = array();

            $skill_tree = get_field($key);
            foreach($skill_tree as $index => $skill_data):
                if ($skill_data['skill']->ID == $skill_post_id && $skill_data['has_required_skills']):
                    $temp['jobName'] = get_base_class(get_the_title());
                    foreach($skill_tree[$index]['required_skills'] as $skill_data_sub):
                        $skill_id = convert_post_id_to_skill_id($skill_data_sub['skill']);

                        $temp2[] = array(
                            'info' => get_skill_info($skill_id)['skillInfo'],
                            'level' => intval($skill_data_sub['level'])
                        );
                    endforeach;

                    $temp['skills'] = $temp2;
                endif;
            endforeach;

            if ($temp):
                if (!array_search($temp['jobName'], $required_skills_info)):
                    $required_skills_info[] = $temp;
                endif;
            endif;
        endwhile;
    endif;

    return $required_skills_info;
}

/**
 * @param $item_id
 * @return array
 */
function get_how_to_get($item_id) {
    $array = array();

    $args = array(
        'post_type'     => 'items',
        'post_status'   => 'publish',
        'meta_query'	=> array(
            'relation' => 'AND',
            array(
                'key' => 'item_id',
                'value' => $item_id,
                'compare' => '='
            )
        )
    );

    $p = new WP_Query($args);

    if ($p->have_posts()):
        $p->the_post();
        $array['howToGet'][] = get_field("how_to_get",  get_the_ID());
    endif;

    $args = array(
        'post_type'     => 'npc',
        'post_status'   => 'publish',
        'meta_query'    => array(
            'relation' => 'AND',
            array(
                'key'   => 'shops_uuu_items_uuu_item_id',
                'value' => $item_id,
                'compare' => '='
            )
        )
    );

    $find_posts = new WP_Query($args);

    if ($find_posts->have_posts()):
        $array['status'] = 'OK';
        while ($find_posts->have_posts()):
            $find_posts->the_post();
            $array['howToGet'][] .= "<a target='_blank' href='". get_the_permalink() ."'>" . get_the_title() . "</a>.";
        endwhile;
    endif;

    if (empty($array['howToGet'])):
        $array['status'] = 'Not Found';
    else:
        $array['status'] = 'OK';
    endif;

    return $array;
}

/**
 * @param $item_id
 * @param $how_to_get
 * @return array
 */
function insert_how_to_get($item_id, $how_to_get) {
    $array = array();

    $item_data = get_item_data($item_id, false, false);
    $item_name = $item_data->itemInfo->name;

    $post_title = $item_id;
    $post_title .= $item_name ? ' - ' . $item_name : '';

    $args = array(
        'post_title' => $post_title,
        'post_status' => 'publish',
        'post_type' => 'items'
    );

    $post_id = wp_insert_post($args);
    if ($post_id) {
        $array['status'] = 'OK';
        $array['howToGet'] = $how_to_get;
        update_field('item_id', $item_id, $post_id);
        update_field('how_to_get', $how_to_get, $post_id);
    } else {
        $array['status'] = 'Failed';
    }
    return $array;
}

/**
 * @param $npc_id
 * @param $option_name
 * @param $currency
 * @param $value
 * @return bool|int
 */
function update_shop($npc_id, $option_name, $currency, $value) {
    if (get_the_title($npc_id) == "")
        return 0;

    if (get_post_type($npc_id) != "npc")
        return 0;

    $array = explode(",", $value);
    $results = array();

    foreach($array as $item_data):
        $item = explode(":", $item_data);
        if (!empty($item)):
            $item_id = $item[0];
            $price = (int)$item[1];

            $item_data = get_item_data($item_id, false, false);
            $item_info = $item_data->itemInfo;
            $item_sell = (int)str_replace(",", "", $item_info->npc_sell);

            $results[] = array(
                'item_id' => $item_id,
                'price' => ($price == -1) ? $item_sell : $price,
                'item_name' => $item_info->name,
                'slots' => (int)$item_info->slots
            );
        endif;
    endforeach;
    $currency_name = "";
    if ($currency) {
        $currency_data = get_item_data($currency, false, false);
        $currency_name = $currency_data->itemInfo->name;
    }

    $shop_row = array(
        'option_name' => $option_name,
        'currency_item_id' => $currency,
        'currency_item_name' => $currency_name,
    );

    update_field('sell_item', 1, $npc_id);
    $add_shop_row_success = add_row('shops', $shop_row, $npc_id);

    if ($add_shop_row_success):
        foreach($results as $result):
            $slots = $result['slots'];
            $item_name = $result['item_name'];
            if ($slots > 0)
                $item_name .= " [" . $slots . "]";

            $item_row = array(
                'item_id' => $result['item_id'],
                'item_name' => $item_name,
                'price' => $result['price']
            );

            $count_row = count(get_field('shops', $npc_id));
            add_sub_row( array('shops', $count_row, 'items'), $item_row, $npc_id);
        endforeach;
    endif;

    return $add_shop_row_success;
}

function get_build_equip($build_name) {
    $array = array();

    $args = array(
        'post_type' => 'build',
        'posts_per_page' => 1,
        'post_status' => 'publish',
        'meta_query'	=> array(
            'relation'		=> 'AND',
            array(
                'key'	 	=> 'build_name',
                'value'	  	=> $build_name,
                'compare' 	=> '=',
            ),
        )
    );

    $build_object = new WP_Query($args);

    if($build_object->have_posts()):
        while ($build_object->have_posts()): $build_object->the_post();
            $array['status'] = 'OK';
            $equips = get_field('equips');
            foreach($equips as $equip):
                $array['equips'][$equip['position']][] = array(
                    'item_id'   => (int)$equip['item_id'],
                    'item_name' => $equip['item_name'],
                    'refine'    => (int)$equip['refine'],
                    'add_cards' => $equip['add_cards'],
                    'cards'     => $equip['cards'] ? $equip['cards'] : [],
                    'add_options' => $equip['add_options'],
                    'options'   => $equip['options']
                );
            endforeach;

            $equip_examples = get_field('equip_examples');
            foreach($equip_examples as $equip_example):
                $array['equip_examples'][] = array(
                    'upper' => (int)$equip_example['upper'],
                    'middle' => (int)$equip_example['middle'],
                    'lower' => (int)$equip_example['lower'],
                    'armor' => (int)$equip_example['armor'],
                    'weapon' => (int)$equip_example['weapon'],
                    'weapon_two_hand' => (int)$equip_example['weapon_two_hand'],
                    'shield' => (int)$equip_example['shield'],
                    'garment' => (int)$equip_example['garment'],
                    'footgear' => (int)$equip_example['footgear'],
                    'accessory_left' => (int)$equip_example['acc_left'],
                    'accessory_right' => (int)$equip_example['acc_right'],
                    'accessory' => (int)$equip_example['accessory'],
                    'shadow_armor' => (int)$equip_example['shadow_armor'],
                    'shadow_weapon' => (int)$equip_example['shadow_weapon'],
                    'shadow_shield' => (int)$equip_example['shadow_shield'],
                    'shadow_shoes' => (int)$equip_example['shadow_shoes'],
                    'shadow_accessory_left' => (int)$equip_example['shadow_acc_left'],
                    'shadow_accessory_right' => (int)$equip_example['shadow_acc_right'],
                );
            endforeach;
        endwhile;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Not Found';
    endif;

    return $array;
}
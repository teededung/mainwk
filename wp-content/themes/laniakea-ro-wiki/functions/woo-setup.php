<?php 
/**
 * ONLY LOAD CSS AND STYLES IN WOOCOMMERCE PAGE
 * -------------------------------
 */
function child_manage_woocommerce_styles(){
    if (!is_woocommerce() && ! is_cart() && ! is_checkout()):
        wp_dequeue_style( 'woocommerce-general' );
        wp_dequeue_style( 'woocommerce-layout' );
        wp_dequeue_style( 'woocommerce-smallscreen' );

        wp_dequeue_script( 'wc-add-to-cart' );
        wp_dequeue_script( 'wc-cart-fragments' );
        wp_dequeue_script( 'jquery-blockui' );
        wp_dequeue_script( 'woocommerce' );
    endif;
}
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

<?php
// filter
function my_posts_where( $where ) {
    $where = str_replace("meta_key = 'job_1_skill_tree_$", "meta_key LIKE 'job_1_skill_tree_%", $where);
    $where = str_replace("meta_key = 'job_2_skill_tree_$", "meta_key LIKE 'job_2_skill_tree_%", $where);
    $where = str_replace("meta_key = 'job_transcendent_skill_tree_$", "meta_key LIKE 'job_transcendent_skill_tree_%", $where);
    $where = str_replace("meta_key = 'job_3_skill_tree_$", "meta_key LIKE 'job_3_skill_tree_%", $where);

    $where = str_replace("meta_key = 'shops_uuu_items_uuu_item_id", "meta_key LIKE 'shops_%_items_%_item_id", $where);
    return $where;
}

add_filter('posts_where', 'my_posts_where');


function add_type_attribute($tag, $handle, $src) {
    // if not your script, do nothing and return original $tag
    if ( 'ionicons-module' !== $handle ) {
        return $tag;
    }
    // change the script tag by adding type="module" and return it.
    $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    return $tag;
}
add_filter('script_loader_tag', 'add_type_attribute' , 10, 3);
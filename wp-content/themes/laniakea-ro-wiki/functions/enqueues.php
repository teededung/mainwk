<?php

/**
 * CHECK SYSTEM IS LOCAL OR NOT
 * @return bool
 */
function is_local(){
	return ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '::1');
}

/**
 * Enqueue css, js in front-end
 */
function front_end_enqueue() {
	$version = 260;

	if (is_local()):
		$version = rand(0, 9999);
	endif;
	
	$root_css 			= get_template_directory_uri() . '/css/';
	$root_js 			= get_template_directory_uri() . '/js/';
	$root_lib_js 		= get_template_directory_uri() . '/js/libs/';

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', $root_lib_js . 'jquery-3.2.1.min.js', false, '3.2.1', true );
	wp_enqueue_script( 'jquery' );

	if (is_local()):
        wp_enqueue_script( 'vue', $root_lib_js . 'vue.js', array(), '2.6.10', true );
	else:
        wp_enqueue_script( 'vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js', array(), '2.6.10', true );
        //wp_enqueue_script( 'vue', $root_lib_js . 'vue.min.js', array( ), '2.6.10', true );
	endif;

	wp_enqueue_script( 'popper', $root_lib_js . 'popper.min.js', array('jquery'), '1.16.1', true );

	wp_enqueue_style( 'bootstrap', $root_css . 'bootstrap.min.css', array(), '4.4.1');

	wp_enqueue_style( 'bootstrap-vue', $root_css . 'bootstrap-vue.min.css', array('bootstrap'), $version);
    wp_enqueue_script( 'bootstrap-vue', $root_lib_js . 'bootstrap-vue.min.js', array('vue'), $version, true );

    wp_enqueue_style( 'base', $root_css . 'base.css', array(), $version);

    wp_deregister_script( 'lodash' );
	wp_enqueue_script( 'lodash', $root_lib_js . 'lodash.min.js', array(), '4.17.11', true );

	wp_enqueue_style( 'toastr', $root_css . 'toastr.min.css', array(), '2.1.3');
	wp_enqueue_script( 'toastr', $root_lib_js . 'toastr.min.js', array('jquery'), '2.1.3', true );

	wp_enqueue_style( 'lity', $root_css . 'lity.min.css', array(), '3.0.0');
	wp_enqueue_script( 'lity', $root_lib_js . 'lity.min.js', array('jquery'), '3.0.0', true );

	wp_enqueue_script( 'clipboard', $root_lib_js . 'clipboard.min.js', array(), '2.0.0', true );
	wp_enqueue_script( 'loadingOverlay', $root_lib_js . 'loadingOverlay.min.js', array( ), '1.5.3', true );
	wp_enqueue_script( 'nprogress', $root_lib_js . 'nprogress.js', array(), '1.0.0', true );

    wp_enqueue_script( 'stacktable', $root_lib_js . 'stacktable.js', array(), '1.0.0', true );
    wp_enqueue_script( 'particles', $root_lib_js . 'particles.min.js', array(), $version, true );

    wp_enqueue_script( 'ScrollTo', $root_lib_js . 'jquery.scrollTo.min.js', array(), '2.1.2', true );

    wp_enqueue_script( 'ionicons-module', 'https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js', array(), '5.5.2', true );
    wp_enqueue_script( 'ionicons', 'https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js', array(), '5.5.2', true );

	if (is_home() || is_front_page()):
		wp_enqueue_style( 'front-page', $root_css . 'front-page.css', array(), $version);

	elseif (is_page('faq')):
		wp_enqueue_style( 'faq', $root_css . 'page-faq.css', array(), $version);
		wp_enqueue_script( 'faq', $root_js . 'page-faq.js', array( ), $version, true );

	elseif (is_page('huong-dan')
	        || is_page('may-chuyen-hoa')
	        || is_page_template('template-nhiem-vu.php')
            || is_category()) :

        wp_enqueue_style( 'cubeportfolio', $root_css . 'cubeportfolio.min.css', array(), $version);
        wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', array(), '4.4.0', true );
        wp_enqueue_script( 'sticky-kit', $root_lib_js . 'jquery.sticky-kit.min.js', array(), '1.1.3', true );

        wp_enqueue_style( 'he-thong-tinh-nang', $root_css . 'page-he-thong-tinh-nang.css', array(), $version);

    elseif (is_page('xay-dung-nhan-vat') || is_page_template('template-build.php')):
        wp_enqueue_style( 'owl.carousel', $root_css . 'owl.carousel.min.css', array(), '2.3.4');
        wp_enqueue_style( 'owl.carousel-theme', $root_css . 'owl.theme.default.min.css', array(), '2.3.4');
        wp_enqueue_script( 'owl.carousel', $root_lib_js . 'owl.carousel.min.js', array('jquery'), '2.3.4', true );

        wp_enqueue_style( 'builds', $root_css . 'page-builds.css', array(), $version);
        wp_enqueue_script( 'build', $root_js . 'page-build.js', array('owl.carousel'), $version, true );

    elseif (is_page('npc') || is_singular('npc')):
        wp_enqueue_style( 'npc', $root_css . 'page-npc.css', array(), $version);
        wp_enqueue_script( 'sticky-kit', $root_lib_js . 'jquery.sticky-kit.min.js', array(), '1.1.3', true );
	endif;

	if (is_singular('skills')):
        wp_enqueue_style( 'skills', $root_css . 'page-skills.css', array(), $version);
    endif;

    wp_enqueue_script( 'app', $root_js . 'app.js', array(), $version, true );

    // Main JS
    wp_register_script('main', $root_js . 'main.js', array('jquery', 'lodash'), $version, true);
    wp_enqueue_script('main');
    wp_localize_script('main', 'TEE', array(
        'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
        'n'			=> wp_create_nonce( "n" )
    ));
	
}
add_action( 'wp_enqueue_scripts', 'front_end_enqueue' );


/**
 * Enqueue css, js in admin
 */
function admin_enqueue() {
	$root_css 			= get_template_directory_uri() . '/css/';
	// $root_js 			= get_template_directory_uri() . '/js/';
	// $root_lib_js 		= get_template_directory_uri() . '/js/libs/';
	
	wp_enqueue_style( 'starter-admin-css', $root_css . 'admin.css');
}

add_action( 'admin_enqueue_scripts', 'admin_enqueue' );
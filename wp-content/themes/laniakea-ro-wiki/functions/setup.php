<?php
/**
 * Setup thumbnails size
 */
function tee_setup_thumbnails() {
    add_theme_support('post-thumbnails');

    update_option('thumbnail_size_w', 145);
    update_option('thumbnail_size_h', 120);
    update_option('thumbnail_crop', 1);

    update_option('medium_size_w', 300);
    update_option('medium_size_h', 175);
    update_option('medium_crop', 1);

    update_option('large_size_w', 970);
}
add_action('init', 'tee_setup_thumbnails');


/**
 * Options page admin
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Website Settings',
        'menu_title'    => 'Website Settings',
        'menu_slug'     => 'tee-options-page',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));
	
	acf_add_options_sub_page(array(
		'page_title'    => 'Quests',
		'menu_title'    => 'Quests',
		'parent_slug'   => 'tee-options-page',
	));
    
  acf_add_options_sub_page(array(
      'page_title'    => 'Scripts',
      'menu_title'    => 'Scripts',
      'parent_slug'   => 'tee-options-page',
  ));
}


/**
 * Removing Default Image Link in WordPress
 */
function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
    
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);


/**
 * Readmore excerpt
 */
function new_excerpt_more($more) {
    global $post;
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function wpdocs_theme_name_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }

    global $page, $paged;

    // Add the blog name
    $title .= get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }
    return $title;
}
add_filter( 'wp_title', 'wpdocs_theme_name_wp_title', 10, 2 );


/**
 * AFTER SETUP THEME
 */
add_action('after_switch_theme', 'active_starter_setup');

function active_starter_setup() {
	include_once(ABSPATH . 'wp-admin/includes/plugin.php');
	
	$plugin_path            = ABSPATH . 'wp-content/plugins/';
	$acf_plugin_path        = $plugin_path . 'advanced-custom-fields-pro/acf.php';
	$wp_scss_plugin_path    = $plugin_path . 'wp-scss/wp-scss.php';
	
	// Active ACF
	$active_acf = activate_plugin( $acf_plugin_path );
	if ( is_wp_error( $active_acf ) ):
		echo "Can't active ACF plugin! Please put ACF plugin to plugin folder :D";
	endif;
	
	// Active WP SCSS
	$options = array(
		'scss_dir'              => '/css/scss/',
		'css_dir'               => '/css/',
		'compiling'             => 'scss_formatter',
		'errors'                => 'show',
		'enqueue'               => 0
	);
	update_option('wpscss_options', $options);
	
	$active_wp_scss = activate_plugin( $wp_scss_plugin_path );
	if ( is_wp_error( $active_wp_scss ) ):
		echo "Can't active wp-scss plugin! Please put wp-scss plugin to plugin folder :D";
	endif;
}

/**
 * Assign parent terms
 */
add_action('save_post', 'assign_parent_terms', 10, 2);
function assign_parent_terms($post_id, $post) {
	
	if($post->post_type != 'post')
		return $post_id;
	
	// get all assigned terms
	$terms = wp_get_post_terms($post_id, 'category' );
	foreach($terms as $term){
		while($term->parent != 0 && !has_term( $term->parent, 'category', $post )){
			// move upward until we get to 0 level terms
			wp_set_post_terms($post_id, array($term->parent), 'category', true);
			$term = get_term($term->parent, 'category');
		}
	}
}

/**
 * Clean up pagination
 */
if (function_exists("wp_pagenavi")) {
    function wiaw_pagenavi_to_bootstrap($html) {
        $out = '';
        $out = str_replace('<div','',$html);
        $out = str_replace('class=\'wp-pagenavi\' role=\'navigation\'>','',$out);
        $out = str_replace('<a','<li class="page-item"><a class="page-link"',$out);
        $out = str_replace('</a>','</a></li>',$out);
        $out = str_replace('<span aria-current=\'page\' class=\'current\'','<li aria-current="page" class="page-item active"><span class="page-link current"',$out);
        $out = str_replace('<span class=\'pages\'','<li class="page-item"><span class="page-link pages"',$out);
        $out = str_replace('<span class=\'extend\'','<li class="page-item"><span class="page-link extend"',$out);
        $out = str_replace('</span>','</span></li>',$out);
        $out = str_replace('</div>','',$out);
        return '<ul class="pagination" role="navigation">'.$out.'</ul>';
    }
    add_filter( 'wp_pagenavi', 'wiaw_pagenavi_to_bootstrap', 10, 2 );
}

// Auto add class to img tag
function add_image_class($class){
    $class .= ' img-thumbnail';
    return $class;
}
add_filter('get_image_tag_class','add_image_class');
<?php get_header(); ?>

<div class="bg-colored">
    <div class="container content">
        <?php while(have_posts()): the_post(); ?>
            <h1 class="skill-name d-flex align-items-center">
                <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail('full'); ?>
                <?php endif; ?>
                <span><?php the_field('skill_name'); ?></span>
            </h1>

            <div class="meta mb-3">
                <h5>Cấp độ tối đa: <span><?php the_field('max'); ?></span></h5>
            </div>

            <div class="skill-description bg-white p-3 rounded">
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div><!-- /.container -->
</div>

<?php get_footer(); ?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="bg-colored">
        <div class="container content-sm">
            <h1 class="mb-5"><?php the_title(); ?></h1>

            <h2 class="color-600">Nhập vật phẩm vào cửa hàng</h2>
            <div class="bg-white rounded border p-3">
                <form id="form-shop" @submit.prevent="updateShop">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npc_id">* NPC POST ID:</label>
                                <input id="npc_id" class="form-control" type="text" placeholder="NPC ID" v-model="shop_NPCID">
                            </div>

                            <div class="form-group">
                                <label for="shop_optionName">Option Name:</label>
                                <input id="shop_optionName" class="form-control" type="text" placeholder="" v-model="shop_optionName">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shop_currency">Currency:</label>
                                <input id="shop_currency" class="form-control" type="text" placeholder="ItemID, Zeny, CP, Point" v-model="shop_currency">
                            </div>
                            <div class="form-group">
                                <label for="item_id_and_price">Item ID:Price</label>
                                <input id="item_id_and_price" class="form-control" type="text" v-model="shop_ItemID_Price" placeholder="ITEM ID:PRICE">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" v-cloak>Add items to shop</button>
                    <button class="btn btn-info" type="button" @click="shop_NPCID = 0; shop_currency = ''; shop_ItemID_Price = ''">Reset</button>
                </form>
            </div>

        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content pb-5">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <?php
            $episode_slugs = array(
                'episode-16-2',
                'episode-16-1',
                'episode-15-1-15-2',
                'episode-14-3',
                'episode-14-2',
                'episode-14-1',
                'episode-13-3',
                'episode-13-1-13-2',
            );
            ?>

            <div class="mb-5">
                <h2 class="color-primary mb-3">Nhiệm vụ theo phiên bản - Episode</h2>
                <div class="bg-white rounded border p-3 p-md-4">
                    <div class="row">
                        <?php foreach($episode_slugs as $index => $episode_slug): ?>
                            <?php $category = get_category_by_slug($episode_slug); ?>
                            <?php $image = get_field('image', $category); ?>
                            <div class="col-lg-4 mb-4 col-md-6">
                                <div class="card card-category">
                                    <a href="<?php echo get_category_link($category) ?>" title="<?php echo $category->name ?>">
                                        <img class="img-responsive w-100" src="<?php echo $image['url'] ?>" alt="">
                                        <div class="particles" id="particles-js-<?php echo $index ?>"></div>
                                        <h3>
                                            <?php echo $category->name ?>
                                        </h3>
                                    </a>
                                    <div class="card-body card-body-with-border-top">
                                        <div class="card-text">
                                            <?php echo $category->description ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <?php
            $type_slugs = array(
                'nhiem-vu-chuyen-nghe',
                'instance-pho-ban',
                'ban-do-hang-dong',
                'nhiem-vu-nhan-exp',
                'nhiem-vu-tao-mu',
                'nhiem-vu-hang-ngay',
                'nhiem-vu-vat-pham',
                'su-kien'
            );
            ?>

            <div class="mb-3">
                <h2 class="color-primary mb-3">Nhiệm vụ theo loại</h2>
                <div class="bg-white rounded border p-3 p-md-4">
                    <div class="row">
                        <?php foreach($type_slugs as $index2 => $type_slug): ?>
                            <?php $category = get_category_by_slug($type_slug); ?>
                            <?php $image = get_field('image', $category); ?>
                            <div class="col-lg-4 mb-4 col-md-6">
                                <div class="card card-category">
                                    <a href="<?php echo get_category_link($category) ?>" title="<?php echo $category->name ?>">
                                        <img class="img-responsive w-100" src="<?php echo $image['url'] ?>" alt="">
                                        <div class="particles" id="particles-js-<?php echo ($index +$index2 +1) ?>"></div>
                                        <h3>
                                            <?php echo $category->name ?>
                                        </h3>
                                    </a>
                                    <div class="card-body card-body-with-border-top">
                                        <div class="card-text">
                                            <?php echo $category->description ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

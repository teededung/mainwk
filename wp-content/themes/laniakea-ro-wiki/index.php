<?php get_header(); ?>

<div class="container content">
	<?php
    while(have_posts()): the_post();
        the_content();
    endwhile;
	?>
</div><!-- /.container -->

<?php get_footer(); ?>

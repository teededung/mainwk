<?php get_header(); ?>
<div class="bg-colored">
    <div class="container content">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo home_url('/huong-dan/') ?>">Hướng dẫn</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo single_cat_title(); ?></li>
        </ol>

        <h1 class="mb-5"><?php echo single_cat_title(); ?></h1>

        <div class="mb-3">
            <h2 class="color-primary mb-3">Chuyển nghề 1</h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'category_name' => 'chuyen-nghe-1',
                    'posts_per_page' => -1
                );
                $query = new WP_Query( $args );
                ?>
                <?php if ($query->have_posts()): ?>
                    <?php while ($query->have_posts()): $query->the_post(); ?>
                        <div class="col-lg-6">
                            <?php get_template_part('/parts/loop/loop-post-excerpt'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>

        <div class="mb-3">
            <h2 class="color-primary mb-3">Chuyển nghề 2</h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'category_name' => 'chuyen-nghe-2',
                    'posts_per_page' => -1
                );
                $query = new WP_Query( $args );
                ?>
                <?php if ($query->have_posts()): ?>
                    <?php while ($query->have_posts()): $query->the_post(); ?>
                        <div class="col-lg-6">
                            <?php get_template_part('/parts/loop/loop-post-excerpt'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>

        <div>
            <h2 class="color-primary mb-3">Chuyển nghề 3</h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'category_name' => 'chuyen-nghe-3',
                    'posts_per_page' => -1
                );
                $query = new WP_Query( $args );
                ?>
                <?php if ($query->have_posts()): ?>
                    <?php while ($query->have_posts()): $query->the_post(); ?>
                        <div class="col-lg-6">
                            <?php get_template_part('/parts/loop/loop-post-excerpt'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

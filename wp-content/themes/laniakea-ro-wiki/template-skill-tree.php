<?php
/**
 * Template Name: Skill Tree
 * Template Post Type: page
 */
?>
<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<div class="bg-colored">
    <div class="container content-sm">
        <h1 class="mb-5">Skill Tree: <?php the_title(); ?></h1>

        <input type="hidden" id="jobName" value="<?php echo get_the_title(); ?>">
        <input type="hidden" id="apiURL" value="<?php echo admin_url( 'admin-ajax.php' ); ?>">

        <div id="content-skill-tree" v-cloak>
            <b-alert class="mb-5" show dismissible>
                Click vào <u>tên kỹ năng</u> để xem thông tin của kỹ năng.
            </b-alert>

            <div class="skill-section mb-5" v-for="(skillData, jobName) in skillTree">
                <h3>{{ jobName }}</h3>
                <div class="mb-2">
                    <small><span>Điểm kỹ năng:</span> (<span :class="{'text-danger font-weight-bold': skillTree[jobName]['skillPointsUsed'] > skillTree[jobName]['skillPoints'] }">{{ skillTree[jobName]['skillPointsUsed'] }}</span>/<span>{{ skillTree[jobName]['skillPoints'] }}</span>)</small>
                    <template v-if="jobName != 'Novice'">
                        <button class="btn btn-sm btn-reset btn-outline-primary" @click="resetSkill(jobName)">Reset</button>
                    </template>
                </div>

                <div class="wrap-skill-tree rounded box-shadow bg-white mb-2">
                    <div class="row no-gutters skill-row">
                        <div class="col col-slot-skill" v-for="skill in skillData['skills']">
                            <template v-if="!isEmpty(skill)">
                                <a href="#" class="d-block skill-name" @click.prevent="getSkillInfo(skill.skillID)" :title="skill.skillName">{{ skill.skillName }}</a>
                                <div class="will-hover has-skill" :class="{'required' : skill.required}" @mouseenter="skillMouseOver(jobName, skill)" @mouseleave="skillMouseLeave()">
                                    <div class="slot">
                                        <span class="required-level" v-if="skill.requiredLv > 0">{{ skill.requiredLv }}</span>
                                        <img :src="skill.icon" :alt="skill.skillName" @click="skillChangeValue(jobName, skill, 1)">
                                        <template v-if="!skill.isQuestSkill && !skill.isSpiritSkill && !(skill.skillID === 1)">
                                            <a class="control control-up-lv" @click="skillChangeValue(jobName, skill, 1)"><i class="ion-ios-arrow-forward"></i></a>
                                            <a class="control control-down-lv" @click="skillChangeValue(jobName, skill, -1)"><i class="ion-ios-arrow-back"></i></a>
                                        </template>
                                    </div>
                                </div>
                                <template v-if="skill.isQuestSkill || skill.isSpiritSkill">
                                    <span class="d-block skill-max">1</span>
                                </template>
                                <template v-else>
                                    <div class="d-block skill-max">{{ skill.currentLv }}/{{ skill.max }}</div>
                                </template>
                            </template>
                            <template v-else>
                                <template v-if="skill">
                                    <span class="d-block skill-name"></span>
                                    <div class="will-hover">
                                        <div class="slot"></div>
                                    </div>
                                    <span class="d-block skill-max"></span>
                                </template>
                            </template>
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="Object.keys(skillTree).length > 0" class="bg-white p-3 rounded border box-shadow">
                <div class="mb-3" v-if="skillTreeURL1 !== ''">
                    <h3>Chia sẻ:</h3>
                    <div class="form-group">
                        <input readonly type="text" class="form-control" v-model="skillTreeURL1" @focus="copyToClipboard($event)">
                    </div>
                    <template v-if="skillTreeURL1 != skillTreeURL2">
                        <h6>Hoặc:</h6>
                        <div class="form-group">
                            <input readonly type="text" class="form-control" v-model="skillTreeURL2" @focus="copyToClipboard($event)">
                        </div>
                    </template>
                </div>

                <div class="form-group" v-if="skillTreeText != ''">
                    <h3>Skill note:</h3>
                    <textarea readonly class="form-control" v-model="skillTreeText" rows="10" @focus="copyToClipboard($event)"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer() ?>

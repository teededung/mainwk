<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css" rel="stylesheet">

    <?php wp_head(); ?>
	
    <?php $scripts_in_header = get_field('scripts_in_header', 'option'); ?>
    <?php if ($scripts_in_header): ?>
        <script>
	        <?php echo $scripts_in_header; ?>
        </script>
    <?php endif; ?>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114523878-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114523878-2');
    </script>
</head>

<body <?php body_class(); ?>>
    <!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="app">
        <header class="header-page">
            <b-navbar toggleable="lg" type="light" variant="light" v-cloak>
                <div class="container">
                    <b-navbar-brand href="<?php echo home_url('/'); ?>">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . "/images/HAHIU-LOGO.png" ?>" alt="">
                    </b-navbar-brand>

                    <b-navbar-toggle target="header-nav"></b-navbar-toggle>

                    <b-collapse id="header-nav" is-nav>
                        <div class="row w-100">
                            <div class="col-xl-4 col-lg-3 my-lg-0 my-2">
                                <div class="w-search w-100">
                                <?php echo do_shortcode("[wpdreams_ajaxsearchpro id=1]"); ?>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-9">
                                <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'header-nav',
                                    'depth'             => 2,
                                    'container'         => 'b-collapse',
                                    'container_class'   => 'collapse navbar-collapse',
                                    'container_id'      => 'header-nav',
                                    'menu_class'        => 'nav navbar-nav justify-content-end w-100',
                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'            => new WP_Bootstrap_Navwalker())
                                );
                                ?>
                            </div>
                        </div>
                    </b-collapse>
                </div>
            </b-navbar>
        </header>

        <?php get_template_part('/parts/sidebar-app'); ?>



<?php $steps = get_field('steps'); ?>

<?php if ($steps): ?>
<ul class="list-group">
	<?php foreach ($steps as $stepArr): ?>
		<li class="list-group-item">
			<?php echo $stepArr['step']; ?>
		</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>
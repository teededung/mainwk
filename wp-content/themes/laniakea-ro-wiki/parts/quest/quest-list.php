<?php if (have_rows('list')): ?>
	<?php $num = 0; ?>
	<?php while ( have_rows('list') ) : the_row(); ?>
		<div class="card mb-5">
			<div class="card-body">
				<h3 class="mb-3"><small>NPC</small> <?php echo do_shortcode('[npc]'. get_sub_field('npc_name') .'[/npc]');  ?></h3>

				<?php $item_posts = get_sub_field('item_quest'); ?>

				<?php if ($item_posts): ?>

					<?php foreach ($item_posts as $post): ?>
						<?php setup_postdata($post); ?>
						<?php
						$npc_name = get_field('npc_name');
						$item_id = get_field('item_id');
						$item_name = get_field('item_name');
						$item_description = get_field('item_description');
						$note = get_field('note');
						$ingredients_1 = get_field('ingredients');
						$ingredients_2 = get_field('ingredients_2');
						$collapse_id = 'item-' . $item_id . '-' . $num;
						?>
						<div class="block-item pb-3 pt-3">
							<div class="mb-3 d-flex align-items-center">
								<img class="icon-headgear" src="https://api.hahiu.com/data/items/icons/<?php echo $item_id; ?>.png" alt="<?php echo get_the_title(); ?>">
								<span class="will-show-item-info" @click="getItemInfo(<?php echo $item_id; ?>, `<?php echo $item_name; ?>`)"><?php echo $item_name; ?> (<?php echo $item_id; ?>)</span>
							</div>

							<?php if ($note): ?>
								<?php echo apply_filters('the_content', $note); ?>
							<?php endif; ?>

							<?php if ($item_description): ?>
								<div class="lead">
									<?php echo apply_filters('the_content', $item_description); ?>
								</div>
							<?php endif; ?>

                            <b-button variant="primary" v-b-toggle="'<?php echo $collapse_id ?>'">
                                Xem nguyên liệu
                            </b-button>

							<b-collapse id="<?php echo $collapse_id; ?>">
								<div class="mt-3"></div>
								<?php if ($ingredients_1 && !$ingredients_2): ?>
									<ul class="list-group">
										<?php foreach ($ingredients_1 as $it): ?>
											<?php if ($it['item_name'] == 'Zeny'): ?>
                                                <li class="list-group-item text-center text-sm-right">
                                                    <?php echo $it['quantity'] ?> <?php echo $it['item_name']; ?>
                                                </li>
											<?php else: ?>
                                                <li class="list-group-item list-group-quest-item">
                                                    <div class="row align-items-center">
                                                        <div class="col-quantity">
                                                            <?php if ($it['quantity']): ?>
                                                            <span class="item-quantity">x<?php echo $it['quantity'] ?></span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-icon">
                                                            <?php if ($it['item_id'] != 'zeny'): ?>
                                                                <img src="https://api.hahiu.com/data/items/icons/<?php echo $it['item_id']; ?>.png" alt="<?php echo $it['item_name']; ?>">
                                                            <?php else: ?>
                                                            <span>và</span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-name">
                                                            <?php if ($it['item_id'] == 'zeny'): ?>
	                                                            <?php echo $it['item_name']; ?>
                                                            <?php else: ?>
                                                                <a class="item-name" href="#" @click.prevent="getItemInfo(<?php echo $it['item_id']; ?>, `<?php echo $it['item_name']; ?>`)"><?php echo $it['item_name']; ?></a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </li>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								<?php else: ?>
									<div class="row">
										<div class="col-12">
											<ul class="list-group">
												<?php foreach ($ingredients_1 as $it): ?>
													<?php if ($it['item_name'] == 'Zeny'): ?>
														<li class="list-group-item text-center text-sm-right">
                                                            <?php echo $it['quantity'] ?> <?php echo $it['item_name']; ?>
                                                        </li>
													<?php else: ?>
                                                        <li class="list-group-item list-group-quest-item">
                                                            <div class="row align-items-center">
                                                                <div class="col-quantity">
                                                                    <span class="item-quantity">x<?php echo $it['quantity'] ?></span>
                                                                </div>
                                                                <div class="col-icon">
                                                                    <img src="https://api.hahiu.com/data/items/icons/<?php echo $it['item_id']; ?>.png" alt="<?php echo $it['item_name']; ?>">
                                                                </div>
                                                                <div class="col-name">
                                                                    <a class="item-name" href="#" @click.prevent="getItemInfo(<?php echo $it['item_id']; ?>, `<?php echo $it['item_name']; ?>`)">
                                                                        <?php echo $it['item_name']; ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </li>
													<?php endif; ?>
												<?php endforeach; ?>
											</ul>
										</div>
										<div class="col-12">
											<div class="separate">
												-- Hoặc --
											</div>
										</div>
										<div class="col-12">
											<ul class="list-group">
												<?php foreach ($ingredients_2 as $it): ?>
													<?php if ($it['item_name'] == 'Zeny'): ?>
														<li class="list-group-item"><?php echo $it['quantity'] ?> <?php echo $it['item_name']; ?></li>
													<?php else: ?>
                                                        <li class="list-group-item list-group-quest-item">
                                                            <div class="row align-items-center">
                                                                <div class="col-quantity">
                                                                    <span class="item-quantity">x<?php echo $it['quantity'] ?></span>
                                                                </div>
                                                                <div class="col-icon">
                                                                    <img src="https://api.hahiu.com/data/items/icons/<?php echo $it['item_id']; ?>.png" alt="<?php echo $it['item_name']; ?>">
                                                                </div>
                                                                <div class="col-name">
                                                                    <a class="item-name" href="#" @click.prevent="getItemInfo(<?php echo $it['item_id']; ?>, `<?php echo $it['item_name']; ?>`)"><?php echo $it['item_name']; ?></a>

                                                                </div>
                                                            </div>
                                                        </li>
													<?php endif; ?>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
								<?php endif; ?>
							</b-collapse>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php wp_reset_postdata(); ?>
			</div>
		</div>
        <?php $num++; ?>
	<?php endwhile; ?>
<?php endif; ?>
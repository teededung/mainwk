<?php if( have_rows('steps_with_title') ): ?>
    <?php while ( have_rows('steps_with_title') ) : the_row(); ?>
        <?php $hide_this_section = get_sub_field('hide_this_section') ?>

        <?php if (!$hide_this_section): ?>
        <?php $stick_other_notes = get_sub_field('sticky_other_notes'); ?>
        <div class="row <?php echo $stick_other_notes ? 'row-parent' : ''; ?> row-info-loop">
            <div class="col-12">
                <h2 class="mb-3 mt-5" id="<?php echo sanitize_title(get_sub_field('title')); ?>"><?php the_sub_field("title"); ?></h2>
            </div>

            <?php
                $hide_sidebar = get_sub_field('hide_sidebar');
                $col_size = $hide_sidebar ? 12 : 8;
            ?>

            <?php if (!$hide_sidebar): ?>
            <div class="col-md-4 order-md-2 mb-4 mb-md-0">
                <div class="wrap-col-quest-info <?php echo $stick_other_notes ? 'sticky-col' : ''; ?>">
                    <div class="col-quest-info">
                        <h3>Yêu cầu của nhiệm vụ</h3>
                        <div class="w-table-require">
                            <table class="table table-bordered">
                                <tbody>
                                <?php if (have_rows('sub_requirement')): ?>
                                    <?php $require_note = get_field('sub_require_note'); ?>
                                    <?php if ($require_note): ?>
                                        <tr>
                                            <td colspan="2"><small class="text-muted text-center d-block"><?php echo $require_note; ?></small></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php while (have_rows('sub_requirement')): the_row(); ?>
                                        <tr>
                                            <td class="table-require-col-title"><?php the_sub_field('title'); ?></td>
                                            <td><?php echo apply_filters('the_content', get_sub_field('content')); ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="2" class="text-center">Không có yêu cầu nào</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="rewards-info">
                            <h3>Phần thưởng</h3>
                            <table class="table table-bordered">
                                <tbody>
                                <?php if (have_rows('sub_rewards')): ?>
                                    <?php $reward_note = get_sub_field('sub_reward_note'); ?>
                                    <?php if ($reward_note): ?>
                                        <tr>
                                            <td colspan="2"><small class="text-muted text-center d-block"><?php echo $reward_note; ?></small></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php while (have_rows('sub_rewards')): the_row(); ?>
                                        <tr>
                                            <td class="table-require-col-title"><?php the_sub_field('title') ?></td>
                                            <td><?php echo apply_filters('the_content', get_sub_field('content')); ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="2" class="text-center">Không có phần thưởng nào</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php $other_notes = get_sub_field('other_notes'); ?>
                    <?php if ($other_notes): ?>
                        <div class="col-quest-info mt-4">
                            <div class="other-notes">
                                <?php echo apply_filters('the_content', $other_notes); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <div class="col-md-<?php echo $col_size; ?> order-md-1">
                <?php $use_flexible = get_sub_field('use_flexible'); ?>

                <?php if (!$use_flexible): ?>
                    <?php $steps = get_sub_field('steps'); ?>
                    <?php if ($steps): ?>
                        <ul class="list-group">
                            <?php foreach ($steps as $stepArr): ?>
                            <li class="list-group-item">
                                <?php echo $stepArr['step']; ?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if (have_rows("step_flexible")): ?>
                        <ul class="list-group">
                            <li class="list-group-item">
                            <?php while (have_rows('step_flexible')): the_row(); ?>
                                <?php
                                    switch (get_row_layout()):
                                        case "text_input": ?>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <?php echo apply_filters('the_content', get_sub_field('text_input')) ?>
                                                </div>
                                            </div><?php
                                            break;
                                        case "row_100": ?>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <?php the_sub_field('row_100') ?>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        case "row_30_70": ?>
                                            <div class="row mb-3">
                                                <div class="col-md-4">
                                                    <?php the_sub_field('row_30') ?>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php the_sub_field('row_70') ?>
                                                </div>
                                            </div><?php
                                            break;
                                        case "row_70_30": ?>
                                            <div class="row mb-3">
                                                <div class="col-md-8">
                                                    <?php the_sub_field('row_70') ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php the_sub_field('row_30') ?>
                                                </div>
                                            </div><?php
                                            break;
                                        case "row_50_50": ?>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <?php the_sub_field('row_50_1') ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php the_sub_field('row_50_2') ?>
                                                </div>
                                            </div><?php
                                            break;
                                        case 'table_with_two_columns': ?>
                                            <table class="table table-bordered mb-3 stack-table table_with_two_columns">
                                                <thead>
                                                <tr>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_1')); ?></th>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_2')); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if (have_rows('rows')): ?>
                                                    <?php while(have_rows('rows')): the_row(); ?>
                                                        <tr>
                                                            <td><?php the_sub_field('content_1'); ?></td>
                                                            <td><?php the_sub_field('content_2'); ?></td>
                                                        </tr>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                            <?php
                                            break;
                                        case 'table_with_two_columns_items_list': ?>
                                            <table class="table table-bordered mb-3 stack-table table_with_two_columns">
                                                <thead>
                                                <tr>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_1')); ?></th>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_2')); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if (have_rows('rows')): ?>
                                                    <?php while(have_rows('rows')): the_row(); ?>
                                                        <tr>
                                                            <td>
                                                                <?php if (have_rows('list_1')): ?>
                                                                    <ul>
                                                                        <?php while (have_rows('list_1')): the_row(); ?>
                                                                        <li><?php echo apply_filters('the_content', get_sub_field('item')); ?></li>
                                                                        <?php endwhile; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if (have_rows('list_2')): ?>
                                                                    <ul>
                                                                        <?php while (have_rows('list_2')): the_row(); ?>
                                                                        <li><?php echo apply_filters('the_content', get_sub_field('item')); ?></li>
                                                                        <?php endwhile; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                            <?php
                                            break;
                                        case 'table_with_three_columns': ?>
                                            <table class="table table-bordered mb-3 stack-table table_with_three_columns">
                                            <thead>
                                            <tr>
                                                <th><?php echo apply_filters('the_content', get_sub_field('heading_1')); ?></th>
                                                <th><?php echo apply_filters('the_content', get_sub_field('heading_2')); ?></th>
                                                <th><?php echo apply_filters('the_content', get_sub_field('heading_3')); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (have_rows('rows')): ?>
                                                <?php while(have_rows('rows')): the_row(); ?>
                                                    <tr>
                                                        <td><?php the_sub_field('content_1'); ?></td>
                                                        <td><?php the_sub_field('content_2'); ?></td>
                                                        <td><?php the_sub_field('content_3'); ?></td>
                                                    </tr>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                            </tbody>
                                            </table>
                                            <?php
                                            break;
                                        case 'table_with_three_columns_items_list': ?>
                                            <table class="table table-bordered mb-3 stack-table table_with_three_columns">
                                                <thead>
                                                <tr>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_1')); ?></th>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_2')); ?></th>
                                                    <th><?php echo apply_filters('the_content', get_sub_field('heading_3')); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if (have_rows('rows')): ?>
                                                    <?php while(have_rows('rows')): the_row(); ?>
                                                        <tr>
                                                            <td>
                                                                <?php if (have_rows('list_1')): ?>
                                                                    <ul>
                                                                        <?php while (have_rows('list_1')): the_row(); ?>
                                                                        <li><?php echo apply_filters('the_content', get_sub_field('item')); ?></li>
                                                                        <?php endwhile; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if (have_rows('list_2')): ?>
                                                                    <ul>
                                                                        <?php while (have_rows('list_2')): the_row(); ?>
                                                                        <li><?php echo apply_filters('the_content', get_sub_field('item')); ?></li>
                                                                        <?php endwhile; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if (have_rows('list_3')): ?>
                                                                    <ul>
                                                                        <?php while (have_rows('list_3')): the_row(); ?>
                                                                        <li><?php echo apply_filters('the_content', get_sub_field('item')); ?></li>
                                                                        <?php endwhile; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                     <?php endwhile; ?>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                            <?php
                                            break;
                                    endswitch;
                                ?>
                            <?php endwhile; ?>
                            </li>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php endif ?>
    <?php endwhile; ?>
<?php endif; ?>


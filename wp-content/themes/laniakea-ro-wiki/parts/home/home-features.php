<?php
$page = get_page_by_path('he-thong-tinh-nang',OBJECT);

$args = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent' => $page->ID
);

$parent = new WP_Query($args);
?>

<?php if ($parent->have_posts()): ?>
    <div class="section-home section-home-5">
        <div class="container content">
            <h2 class="mb-5 text-center">
                <a href="<?php echo home_url('/he-thong-tinh-nang/'); ?>">Hệ thống và tính năng</a>
            </h2>

            <div class="owl-carousel carousel-features mb-3">
                <?php while ($parent->have_posts()): $parent->the_post(); ?>
                    <div class="item">
                        <a href="<?php the_permalink(); ?>" class="d-inline-block mb-1">
                            <img class="owl-lazy" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?php the_title(); ?>">
                        </a>
                        <div class="text-center">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="text-center">
                <a class="btn btn-primary" href="<?php echo home_url('/he-thong-tinh-nang/') ?>">Xem tất cả hệ thống và tính năng</a>
            </div>
        </div>
    </div>
<?php endif; ?>
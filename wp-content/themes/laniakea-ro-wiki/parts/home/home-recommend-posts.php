<section class="section-home section-home-3">
    <div class="container content">
        <h2 class="mb-5 text-center">Các nhiệm vụ nên làm</h2>

        <?php $recommended_quests = get_field('recommended_quests'); ?>

        <?php if ($recommended_quests): ?>
            <div class="row mb-4">
                <?php foreach($recommended_quests as $post): ?>
                    <?php setup_postdata($post); ?>
                    <div class="col-md-4">
                        <?php get_template_part('parts/loop/loop-recent-post'); ?>
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>

        <div class="text-center">
            <a class="btn btn-primary" href="<?php echo home_url('/huong-dan-nhiem-vu/'); ?>">Xem tất cả danh mục nhiệm vụ</a>
        </div>

    </div>
</section>
<section id="nhiem-vu" v-cloak>
    <div class="container content">
        <b-card no-body>
            <div class="card-body border-bottom">
                <h2 class="card-title">
                    Nhiệm vụ theo phiên bản - Episode
                </h2>
                <p class="card-text">Thực hiện các nhiệm vụ để nhận điểm thưởng kinh nghiệm và các trang bị ngon lành cành đào.</p>
            </div>
            <b-tabs pills card vertical class="g-0">
                <?php $tabs = get_field('tabs', 'option'); ?>
                <?php foreach($tabs as $index => $tab): ?>
                    <b-tab title="<?php echo $tab['tab_name'] ?>" @click="scrollTo('__BVID__5__BV_tab_container_', true)" <?php echo ($index === 0) ? 'active' : ''?>>
                        <b-card-text>
                            <div class="row">
                                <?php foreach($tab['posts'] as $quest): ?>
                                    <?php if ($quest['from_post']): ?>
                                        <?php $post = $quest['post']; ?>
                                        <?php setup_postdata($post); ?>
                                        <div class="col-lg-6">
                                            <?php get_template_part('parts/loop/loop-post-excerpt'); ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-lg-6">
                                            <?php include locate_template('/parts/loop/loop-post-muted.php'); ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </b-card-text>
                    </b-tab>
                <?php endforeach; ?>
            </b-tabs>
        </b-card>
    </div>
</section>
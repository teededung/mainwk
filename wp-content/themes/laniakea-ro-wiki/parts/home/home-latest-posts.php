<section class="section-home section-home-2">
    <div class="container content">
        <h2 class="mb-5 text-center">Các bài viết mới nhất</h2>

        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 9,
            'post_status' => 'publish'
        );

        $latest_posts = new WP_Query($args);
        ?>

        <?php if ($latest_posts->have_posts()): ?>
            <div class="row">
                <?php while ($latest_posts->have_posts()): $latest_posts->the_post(); ?>
                    <div class="col-md-4">
                        <?php get_template_part('parts/loop/loop-recent-post'); ?>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>
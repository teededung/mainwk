<section class="section-home section-home-1">
    <div class="container content">
        <div class="top-title text-center">
            <h1>Hướng dẫn chơi Ragnarok Online Việt Nam</h1>
        </div>

        <div id="search">
            <div class="input-group">
                <form @submit.prevent="search" class="input-group">
                    <input type="text" ref="search" class="form-control" v-model="keyword" @input="search" @keydown="searchKeydown" placeholder="Gõ nội dung tìm kiếm ở đây" aria-label="Gõ nội dung tìm kiếm ở đây" aria-describedby="button-search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-search" :disabled="searching ? true : false" v-cloak>{{searching ? 'Đợi chút nhé' : 'Tìm'}}</button>
                    </div>
                </form>
            </div>
            <div class="search-results" :class="{'show': results.length > 0}" v-if="results.length > 0">
                <div class="w-section-result" v-if="filterByType(1).length > 0">
                    <h2>Hướng dẫn nhiệm vụ</h2>
                    <div class="single-result" v-for="result in filterByType(1)" :class="{'select':result.select}" :id="'post-'+result.id">
                        <h3><i class="icon ion-md-arrow-forward"></i><a :href="result.permalink" v-html="result.title"></a></h3>
                    </div>
                </div>
                <div class="w-section-result" v-if="filterByType(2).length > 0">
                    <h2>Xây dựng nhân vật</h2>
                    <div class="single-result" v-for="result in filterByType(2)" :class="{'select':result.select}" :id="'post-'+result.id">
                        <h3><i class="icon ion-md-arrow-forward"></i><a :href="result.permalink" v-html="result.title"></a></h3>
                    </div>
                </div>
                <button class="btn btn-primary btn-hide-results rounded-0" @click="hideResults">Đóng</button>
            </div>
            <div class="search-results" :class="{'show': results.length === 0 && notFound}" v-if="notFound">
                <div class="w-section-result">
                    <h2>Không tìm thấy, hãy thử từ khóa khác.</h2>
                </div>
            </div>
        </div>
    </div>
</section>
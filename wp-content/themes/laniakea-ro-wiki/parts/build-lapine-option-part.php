<div class="mt-2">
    <small><u>Lapine Enchant:</u></small>
    <ul class="lapine-enchant-list list-unstyled">
        <?php foreach($eq['options'] as $option): ?>
            <li class="lapine-option">
                <?php
                $option_name = convert_random_option($option['name']);
                $option_value = $option['value'];

                if (strpos($option_name, "%")):
                    $output = str_replace("%", $option_value . "%", $option_name);
                else:
                    $output = $option_name . " +" . $option_value;
                endif;
                ?>
                <span class="option-name"><?php echo $output; ?></span>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

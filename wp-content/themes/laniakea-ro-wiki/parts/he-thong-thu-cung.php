<div class="bg-light p-3 border rounded" id="pet-content-wrap">
    <div class="pt-1 pb-3 mb-3 border-bottom">
        <h3 class="color-600">
            <b-link class="tag-collapse" v-b-toggle.collapse-1>
                <span>Cách thuần hóa thú</span> <i></i>
            </b-link>
        </h3>
        <b-collapse id="collapse-1">
            <p>
                <strong>Bước 1</strong>: Để thuần hóa bạn phải có <strong>đồ thuần hóa</strong> (Taming Item). Ví dụ như {!! _item(619, 'Unripe Apple') !!}, {!! _item(634, 'Tropical Banana') !!}, ...
            </p>

            <figure class="figure">
                <img class="figure-img img-fluid rounded" src="<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/cach-thuan-hoa-thu-buoc-1.jpg' ?>" alt="Bước 1 - Cách thuần hóa thú cưng">
                <figcaption class="figure-caption">
                    Nhấp đôi vào đô thuần hóa...
                </figcaption>
            </figure>

            <p>
                <strong>Bước 2</strong>: Nhấp đôi vào đồ thuần hóa và rê vòng tròn lên đầu quái vật mà bạn muốn thuần hóa.
            </p>

            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/cach-thuan-hoa-thu-buoc-2.jpg' ?> alt="Bước 2 - Cách thuần hóa thú cưng">
                <figcaption class="figure-caption">
                    Đưa lên đầu quái vật và click...
                </figcaption>
            </figure>

            <p>
                <strong>Bước 3:</strong>
                Một chiếc máy <strong>Slot Machine</strong> sẽ xuất hiện lên màn hình. Click vào chiếc máy để bắt quái vật.<br>
                Khi bắt quái vật thành công, Slot Machine sẽ hiện mặt cười và bạn sẽ nhận được <strong>trứng thú cưng</strong>, nếu thất bại Slot Machine sẽ hiện mặt buồn.
            </p>

            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/thuan-hoa-slot-machine.jpg' ?> alt="thuan-hoa-slot-machine">
                <figcaption class="figure-caption">
                    Slot machine xuất hiện khi bạn thuần hóa quái vật
                </figcaption>
            </figure>
        </b-collapse>
    </div>

    <div class="pt-1 pb-3 mb-3 border-bottom">
        <h3 class="color-600">
            <b-link class="tag-collapse" v-b-toggle.collapse-2>
                Cách ấp trứng <i></i>
            </b-link>
        </h3>
        <b-collapse id="collapse-2">
            <p>
                Để ấp trứng bạn phải có lò ấp trứng {!! _item(643, 'Pet Incubator') !!}, bạn có thể mua nó từ NPC <strong>Pet Groomer</strong> ở các thành phố.<br>
                Hình dưới đây là NPC Pet Groomer ở <code class="clipboard" data-clipboard-text="/navi prontera 217/210">prontera 217/210</code>.
            </p>

            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/mua-lo-ap-trung-pet-groomer.jpg' ?> alt="mua-lo-ap-trung-pet-groomer">
                <figcaption class="figure-caption">Mua lò ấp trứng ở Prontera</figcaption>
            </figure>
        </b-collapse>
    </div>

    <div class="pt-1 pb-3 mb-3 border-bottom">
        <h3 class="color-600">
            <b-link class="tag-collapse" v-b-toggle.collapse-5>
                Cho thú cưng ăn <i></i>
            </b-link>
        </h3>
        <b-collapse id="collapse-5">
            <p>Bạn sẽ thấy dưới chân của thú cưng có 1 thanh màu trắng, đó không phải là thanh máu của thú cưng, đó là thanh cho biết độ no của thú cưng. Khi thanh này dưới 1 nửa thì bạn nên cho thú cưng ăn nhé.</p>

            <p><strong>Bước 1:</strong> Để cho thú cưng ăn bạn phải cố thức ăn dành riêng cho thú. Bạn có thể thức ăn cho thú từ NPC Pet Groomer ở các thành phố. 1 lần mua các bạn nên mua tầm 30-50 cái.</p>
            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/mua-lo-ap-trung-pet-groomer.jpg' ?> alt="mua-lo-ap-trung-pet-groomer">
                <figcaption class="figure-caption">Mua đồ ăn cho thú cưng</figcaption>
            </figure>

            <p><strong>Bước 2:</strong> Chuột phải vào thú cưng và chọn <strong>Cho ăn</strong>.</p>

            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/cho-thu-cung-an.jpg' ?> alt="cho-thu-cung-an">
                <figcaption class="figure-caption">Menu hiện ra khi chuột phải vào thú cưng.</figcaption>
            </figure>

            <div class="alert alert-info"><strong>Mẹo:</strong> Nếu bạn không biết thú cưng thích ăn món gì, bạn có thể thử cho thú cưng ăn, 1 đoạn thông báo sẽ hiện ở khung chat và nói bạn đang thiếu đồ ăn gì.</div>

            <h5 class="color-500">Tự dộng cho thú cưng ăn:</h5>
            <p>
                <strong>Bước 1:</strong> Chuột phải vào thú cưng và chọn <strong>Kiểm tra trạng thái</strong> hoặc bấm tổ hợp phím <kbd>ALT + J</kbd>, cửa sổ thông tin thú cưng sẽ hiện ra.
            </p>
            <figure class="figure">
                <img class="figure-img img-fluid rounded" src=<?php echo get_template_directory_uri() . '/images/he-thong-tinh-nang/he-thong-thu-cung/cua-so-thong-tin-thu-cung.jpg' ?> alt="Cửa sổ thông tin thú cưng">
                <figcaption class="figure-caption">Cửa sổ thông tin thú cưng</figcaption>
            </figure>
            <p>
                <strong>Bước 2:</strong> Click vào <strong>< Command ></strong> và chọn <strong>Tự động cho ăn</strong>. Bây giờ bạn không lo lắng về chuyện lỡ bỏ đói thú cưng nữa. Nhưng bạn hãy chắc chắn là có nhiều thức ăn ở trong hành trang nhé.
            </p>
        </b-collapse>
    </div>

    <div class="pt-1 pb-3 mb-3 border-bottom">
        <h3 class="color-600">
            <b-link class="tag-collapse" v-b-toggle.collapse-3>
                <span>Chỉ số thân mật</span> <i></i>
            </b-link>
        </h3>

        <b-collapse id="collapse-3">
            <p>
                Thú cưng sẽ cho bạn một số hiệu ứng tốt khi đạt độ thân mật nhất định.<br>
                Bạn có thể kiểm tra độ thân mật bằng lệnh <code>@petinfo</code>, độ thân mật phụ thuộc vào sự chăm sóc của bạn dành cho thú cưng.<br>
            </p>

            <p>Dưới đây là các cấp độ thân mật của thú cưng:</p>

            <table class="table table-bordered table-intimate w-auto">
                <tr>
                    <td><code>1 -> 99</code></td>
                    <td>Lúng túng (Awkward)</td>
                </tr>
                <tr>
                    <td><code>100 ->  249</code></td>
                    <td>Nhát (Shy)</td>
                </tr>
                <tr>
                    <td><code>250 -> 749</code></td>
                    <td>Bình thường (Neutral)</td>
                </tr>
                <tr>
                    <td><code>750 -> 909</code></td>
                    <td>Thân mật (Cordial)</td>
                </tr>
                <tr>
                    <td><code>910 -> 1000</code></td>
                    <td>Trung thành (Loyal)</td>
                </tr>
            </table>

            <p>Nếu độ thân mật của thú cưng bị giảm xuống còn 0, thú cưng sẽ rời bỏ bạn vĩnh viễn.</p>

            <div class="alert alert-info">
                <strong>Mẹo:</strong> Hãy cho thú cưng ăn đúng lúc, không nên cho ăn khi còn no. Nếu thú cưng của bạn quá đói mà trong hành trang của bạn đã hết thức ăn, bạn có thể thu hồi thú cưng trở thành trứng để tránh tình trạng thú cưng bỏ đi mất.
            </div>
        </b-collapse>
    </div>

    <div class="pt-1 pb-3 mb-3 border-bottom">
        <input type="hidden" id="petAPI" value=<?php echo home_url('/api/?type=pets')?>>

        <h3 class="color-600">
            <a class="tag-collapse">
                <span>Danh sách thú cưng</span> <i></i>
            </a>
        </h3>

        <b-collapse class="pt-2" id="collapse-4" visible>
            <div class="mb-3">
                <input type="text" class="form-control" v-model="searchPet" @input="filterPets" placeholder="Tìm tên thú cưng, đồ ăn, đồ thuần hóa...">
            </div>

            <div class="table-responsive-md">
                <table class="table table-bordered table-sm table-pets">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Tên</th>
                        <th>Đồ ăn</th>
                        <th>Thuần hóa</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <template v-for="pet in pets">
                        <tr :class="{'tr-hightlight' : pet.expand, 'pet-mvp' : pet.isMvP }" :id="'pet-' + pet.mobID" v-show="pet.show">
                            <td class="align-middle"><img class="pet-img img-fluid" :src="pet.img" :alt="pet.name"></td>
                            <td class="align-middle">{{ pet.name }}</td>
                            <td class="align-middle">
                                <a class="item-name" href="#" @click.prevent="getItemInfo(pet.food[0],pet.food[1])">
                                    <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/'+pet.food[0]+'.png'" :alt="pet.food[1]">
                                    <span>{{ pet.food[1] }}</span>
                                </a>
                            </td>
                            <td class="align-middle">
                                <template v-if="Array.isArray(pet.taming)">
                                    <a class="item-name" href="#" @click.prevent="getItemInfo(pet.taming[0],pet.taming[1])">
                                        <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/'+pet.taming[0]+'.png'" :alt="pet.taming[1]">
                                        <span>{{ pet.taming[1] }}</span>
                                    </a>
                                </template>
                                <template v-else>
                                    <span>{{ pet.taming }}</span>
                                </template>
                            </td>
                            <td class="align-middle"><a href="#" @click.prevent="showModalPet(pet)" rel="nofollow">Xem thêm</a></td>
                        </tr>
                        <tr class="tr-pet-detail" v-if="!isEmpty(modalPet) && pet.expand" v-show="pet.show">
                            <td colspan="5">
                                <div class="pet-general-info">
                                    <div class="row align-items-center pb-1">
                                        <div class="col-9">
                                            <div class="mb-1">
                                                <strong>Thú cưng:</strong> <span>{{ modalPet.name }}</span>
                                            </div>
                                            <div class="mb-1" v-if="modalPet.eggID">
                                                <strong>Trứng:</strong>
                                                <a class="item-name" href="#" v-if="modalPet.eggID" @click.prevent="getItemInfo(modalPet.eggID[0],modalPet.eggID[1])">
                                                    <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/' + modalPet.eggID[0] + '.png'" :alt="modalPet.eggID[1]">
                                                    <span>{{ modalPet.eggID[1] }}</span>
                                                </a>
                                            </div>
                                            <div class="mb-1">
                                                <strong>Thuần hóa:</strong>
                                                <template v-if="Array.isArray(modalPet.taming)">
                                                    <a class="item-name" href="#" v-if="modalPet.taming" @click.prevent="getItemInfo(modalPet.taming[0],modalPet.taming[1])">
                                                        <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/' + modalPet.taming[0] + '.png'" :alt="modalPet.taming[1]">
                                                        <span>{{ modalPet.taming[1] }}</span>
                                                    </a>
                                                </template>
                                                <template v-else>
                                                    <span>{{ modalPet.taming }}</span>
                                                </template>
                                            </div>
                                            <div class="mb-1">
                                                <strong>Đồ ăn:</strong>
                                                <a class="item-name" href="#" v-if="modalPet.food" @click.prevent="getItemInfo(modalPet.food[0],modalPet.food[1])">
                                                    <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/' + modalPet.food[0] + '.png'" :alt="modalPet.food[1]">
                                                    <span>{{ modalPet.food[1] }}</span>
                                                </a>
                                            </div>
                                            <div class="mb-1" v-if="modalPet.equip">
                                                <strong>Trang sức:</strong>
                                                <a class="item-name" href="#" v-if="modalPet.equip" @click.prevent="getItemInfo(modalPet.equip[0],modalPet.equip[1])">
                                                    <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/' + modalPet.equip[0] + '.png'" :alt="modalPet.equip[1]">
                                                    <span>{{ modalPet.equip[1] }}</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-3 text-end">
                                            <img class="img-thumbnail d-inline-block" :src="modalPet.img" :alt="modalPet.name">
                                        </div>
                                    </div>
                                </div>

                                <ul class="list-group list-group-flush list-group-pet-bonus">
                                    <li class="list-group-item" v-if="modalPet.support">
                                        <strong>Hỗ trợ:</strong><br>
                                        <span v-html="modalPet.support"></span>
                                    </li>
                                    <li class="list-group-item" v-if="modalPet.shy || modalPet.neutral || modalPet.cordial || modalPet.loyal">
                                        <div class="row">
                                            <div class="col-6 mb-1" v-if="modalPet.shy">
                                                <strong>Lúng túng/Nhát (Awkward/Shy):</strong><br>
                                                <span v-html="modalPet.shy"></span>
                                            </div>
                                            <div class="col-6 mb-1" v-if="modalPet.neutral">
                                                <strong>Bình thường (Neutral):</strong><br>
                                                <span v-html="modalPet.neutral"></span>
                                            </div>
                                            <div class="col-6 mb-1" v-if="modalPet.cordial">
                                                <strong>Thân mật (Cordial):</strong><br>
                                                <span v-html="modalPet.cordial"></span>
                                            </div>
                                            <div class="col-6 mb-1" v-if="modalPet.loyal">
                                                <strong>Trung thành (Loyal):</strong><br>
                                                <span v-html="modalPet.loyal"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <div class="pets-evolution" v-if="modalPet.evolution">
                                    <div class="row mb-3 align-items-center" v-for="petEvo in modalPet.evolution">
                                        <div class="col-12 mb-2">
                                            <i class="icon ion-ios-arrow-forward"></i> Tiến hóa lên <strong>{{ getPetDetail(petEvo.targetID, 'name') }}</strong>:
                                        </div>
                                        <div class="col-9">
                                            <ul class="mb-0">
                                                <li v-for="item in petEvo.items">
                                                    <a class="item-name" href="#" v-if="item" @click.prevent="getItemInfo(item.itemID,item.itemName)">
                                                        <img :src="'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/' + item.itemID + '.png'" :alt="item.itemName">
                                                        <span>{{ item.itemName }}</span>
                                                    </a>
                                                    x {{ item.amount }}
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-3 text-end">
                                            <img class="img-thumbnail d-inline-block" :src="getPetDetail(petEvo.targetID, 'img')" :alt="getPetDetail(petEvo.targetID, 'name')">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr >
                    </template>
                    </tbody>
                </table>
            </div>
        </b-collapse>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-4">
            <section id="huong-dan-co-ban">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title"><a href="<?php echo home_url('/newbie/'); ?>">Hướng dẫn cơ bản</a></h2>
                        <p>Sự khởi đầu bao giờ cũng gian nan và vất vả, những hướng dẫn cơ bản sẽ giúp bạn có sự khởi đầu thuận lợi hơn.</p>
                        <div class="list-group list-group-flush">
                            <a href="<?php echo home_url('/newbie/tao-nhan-vat-gameplay'); ?>" class="list-group-item list-group-item-action">
                                1. Tạo nhân vật & Gameplay
                            </a>
                            <a href="<?php echo home_url('/newbie/gia-nhap-hoc-vien-criatura/') ?>" class="list-group-item list-group-item-action">
                                2. Gia nhập học viện Criatura
                            </a>
                            <a href="<?php echo home_url('/newbie/chuyen-nghe/') ?>" class="list-group-item list-group-item-action">
                                3. Chuyển nghề
                            </a>
                            <a href="<?php echo home_url('/newbie/eden-group/') ?>" class="list-group-item list-group-item-action">
                                4. Gia nhập hội Eden
                            </a>
                            <a href="<?php echo home_url('/newbie/luyen-cap/') ?>" class="list-group-item list-group-item-action">
                                5. Luyện cấp
                            </a>
                            <a href="<?php echo home_url('/newbie/su-dung-dich-chuyen/') ?>" class="list-group-item list-group-item-action">
                                6. Sử dụng dịch chuyển
                            </a>
                            <a href="<?php echo home_url('/newbie/tim-quai-vat-va-vat-pham/') ?>" class="list-group-item list-group-item-action">
                                7. Tìm quái vật và vật phẩm
                            </a>
                            <a href="<?php echo home_url('/newbie/cac-lenh-huu-ich/') ?>" class="list-group-item list-group-item-action">
                                8. Các lệnh hữu ích
                            </a>
                            <a href="<?php echo home_url('/newbie/tong-hop-cac-dong-tien/') ?>" class="list-group-item list-group-item-action">
                                9. Tổng hợp các đồng tiền
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /.col-md-4 -->
    </div>
</div>
<?php if ($eq_builds[$key][$tier]): ?>
    <?php foreach($eq_builds[$key][$tier] as $eq): ?>
        <?php $refine = $eq['refine'] > 0 ? "+" . $eq['refine'] . " " : ""; ?>
        <?php $recommend = $eq['recommend']; ?>

        <li class="list-group-item <?php echo $recommend ? 'recommended' : ''; ?>">
            <?php if ($recommend): ?>
                <span class="recommended-box">
                    <i class="ion ion-ios-star"></i>&nbsp;Nên dùng
                </span>
            <?php endif; ?>
            <?php echo _item($eq['item_id'], $refine . $eq['item_name']) ?>
            <?php if ($eq['add_cards']): ?>
                <ul class="combo-with">
                    <?php foreach($eq['cards'] as $card): ?>
                        <li>
                            <div class="group-line">
                                <span class="vli"></span>
                                <?php if ($card['or']): ?>
                                    <span class="combo-or">Hoặc</span>
                                <?php endif; ?>
                            </div>
                            <?php echo _item($card['card_id'], $card['card_name']) ?> <?php echo ($card['how_many_card'] > 1) ? " x<span>" . $card['how_many_card'] . "</span>": "" ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <?php if ($eq['add_options']): ?>
                <div class="mt-2">
                    <small><u>Lapine Enchant:</u></small>
                    <ul class="lapine-enchant-list list-unstyled">
                        <?php foreach($eq['options'] as $option): ?>
                            <li class="lapine-option">
                                <?php
                                $option_name = convert_random_option($option['name']);
                                $option_value = $option['value'];

                                if (strpos($option_name, "%")):
                                    $output = str_replace("%", $option_value . "%", $option_name);
                                else:
                                    $output = $option_name . " +" . $option_value;
                                endif;
                                ?>
                                <span class="option-name"><?php echo $output; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
<?php endif; ?>
<div class="equip-show">
    <b-tabs>
        <b-tab title="Trang bị" active>
            <div class="row no-gutters align-items-center">
                <div class="col-5">
                    <div class="col-equip-position-left">
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].upper }">
                            <div @click="getBuildEquipDetail(index, 'upper')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].upper"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].upper, 'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].upper, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'upper')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].upper ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].upper ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].upper, 'name') : 'Upper' }}</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].lower }">
                            <div @click="getBuildEquipDetail(index, 'lower')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].lower"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].lower,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].lower, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'lower')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].lower ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].lower ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].lower, 'name') : 'Lower' }}</span>
                            </div>
                        </div>

                        <template v-if="buildEquipExamples[activeBuildName][index].weapon_two_hand">
                            <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].weapon_two_hand }">
                                <div @click="getBuildEquipDetail(index, 'weapon_two_hand')" class="col-2 col-equip-block-icon">
                                    <span v-if="buildEquipExamples[activeBuildName][index].weapon_two_hand"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'name')"></span>
                                    <span v-else></span>
                                </div>
                                <div @click="getBuildEquipDetail(index, 'weapon_two_hand')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].weapon_two_hand ? 'item-equipped' : '')">
                                    <span>{{ buildEquipExamples[activeBuildName][index].weapon_two_hand ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'name') : 'Right Hand' }}</span>
                                </div>
                            </div>
                        </template>
                        <template v-else>
                            <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].weapon }">
                                <div @click="getBuildEquipDetail(index, 'weapon')" class="col-2 col-equip-block-icon">
                                    <span v-if="buildEquipExamples[activeBuildName][index].weapon"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon, 'icon', 'weapon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon, 'name')"></span>
                                    <span v-else></span>
                                </div>
                                <div @click="getBuildEquipDetail(index, 'weapon')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].weapon ? 'item-equipped' : '')">
                                    <span>{{ buildEquipExamples[activeBuildName][index].weapon ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon, 'name') : 'Right Hand' }}</span>
                                </div>
                            </div>
                        </template>

                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].garment }">
                            <div @click="getBuildEquipDetail(index, 'garment')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].garment"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].garment,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].garment, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'garment')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].garment ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].garment ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].garment, 'name') : 'Garment' }}</span>
                            </div>
                        </div>
                        <div class="row no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].accessory_right }">
                            <div @click="getBuildEquipDetail(index, 'accessory_right')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].accessory_right"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_right,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_right, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'accessory_right')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].accessory_right ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].accessory_right ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_right,'name') : 'Accessory 1' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2 text-center">
                    <?php $title = sanitize_title(get_the_title()); ?>
                    <img src="<?php echo get_template_directory_uri() . '/images/jobs/'. $title.'-1.png'; ?>" alt="<?php echo $title ?>-job">
                </div>
                <div class="col-5">
                    <div class="col-equip-position-right">
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].middle }">
                            <div @click="getBuildEquipDetail(index, 'middle')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].middle"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].middle,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].middle, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'middle')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].middle ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].middle ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].middle, 'name') : 'Middle' }}</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].armor }">
                            <div @click="getBuildEquipDetail(index, 'armor')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].armor"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].armor,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].armor, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'armor')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].armor ? 'item-equipped' : '')">
                                <span>{{buildEquipExamples[activeBuildName][index].armor ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].armor, 'name') : 'Armor' }}</span>
                            </div>
                        </div>

                        <template v-if="buildEquipExamples[activeBuildName][index].weapon_two_hand">
                            <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].weapon_two_hand }">
                                <div @click="getBuildEquipDetail(index, 'weapon_two_hand')" class="col-2 col-equip-block-icon">
                                    <span v-if="buildEquipExamples[activeBuildName][index].weapon_two_hand"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'name')"></span>
                                    <span v-else></span>
                                </div>
                                <div @click="getBuildEquipDetail(index, 'weapon_two_hand')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].weapon_two_hand ? 'item-equipped' : '')">
                                    <span>{{ buildEquipExamples[activeBuildName][index].weapon_two_hand ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].weapon_two_hand, 'name') : 'Left Hand' }}</span>
                                </div>
                            </div>
                        </template>
                        <template v-else>
                            <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shield }">
                                <div @click="getBuildEquipDetail(index, 'shield')" class="col-2 col-equip-block-icon">
                                    <span v-if="buildEquipExamples[activeBuildName][index].shield"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shield, 'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shield, 'name')"></span>
                                    <span v-else></span>
                                </div>
                                <div @click="getBuildEquipDetail(index, 'shield')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shield ? 'item-equipped' : '')">
                                    <span>{{ buildEquipExamples[activeBuildName][index].shield ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shield, 'name') : 'Left Hand' }}</span>
                                </div>
                            </div>
                        </template>

                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].footgear }">
                            <div @click="getBuildEquipDetail(index, 'footgear')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].footgear"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].footgear, 'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].footgear, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'footgear')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].footgear ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].footgear ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].footgear, 'name') : 'Footgear' }}</span>
                            </div>
                        </div>
                        <div class="row no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].accessory_left }">
                            <div @click="getBuildEquipDetail(index, 'accessory_left')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].accessory_left"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_left,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_left, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'accessory_left')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].accessory_left ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].accessory_left ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].accessory_left, 'name') : 'Accessory 2' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </b-tab>
        <b-tab title="Shadow">
            <div class="row no-gutters align-items-center">
                <div class="col-5">
                    <div class="col-equip-position-left">
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip">
                            <div class="col-2 col-equip-block-icon">
                                <span></span>
                            </div>
                            <div class="col-10 col-equip-block-name">
                                <span>Costume Upper</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip">
                            <div class="col-2 col-equip-block-icon">
                                <span></span>
                            </div>
                            <div class="col-10 col-equip-block-name">
                                <span>Costume Lower</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_weapon }">
                            <div @click="getBuildEquipDetail(index, 'shadow_weapon')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_weapon"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_weapon,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_weapon, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_weapon')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_weapon ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_weapon ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_weapon, 'name') : 'Shadow Weapon' }}</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters align-items-center d-flex row-equip">
                            <div class="col-2 col-equip-block-icon">
                                <span></span>
                            </div>
                            <div class="col-10 col-equip-block-name">
                                <span>Costume Garment</span>
                            </div>
                        </div>
                        <div class="row no-gutters align-items-center d-flex row-equip" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_accessory_right }">
                            <div @click="getBuildEquipDetail(index, 'shadow_accessory_right')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_accessory_right"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_right,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_right, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_accessory_right')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_accessory_right ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_accessory_right ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_right, 'name') : 'Shadow Acc Right' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2 text-center">
                    <?php $title = sanitize_title(get_the_title()); ?>
                    <img src="<?php echo get_template_directory_uri() . '/images/jobs/'. $title.'-1.png'; ?>" alt="<?php echo $title ?>-job">
                </div>
                <div class="col-5">
                    <div class="col-equip-position-right">
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left">
                            <div class="col-2 col-equip-block-icon">
                                <span></span>
                            </div>
                            <div class="col-10 col-equip-block-name">
                                <span>Costume Middle</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_armor }">
                            <div @click="getBuildEquipDetail(index, 'shadow_armor')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_armor"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_armor,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_armor, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_armor')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_armor ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_armor ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_armor, 'name') : 'Shadow Armor' }}</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_shield }">
                            <div @click="getBuildEquipDetail(index, 'shadow_shield')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_shield"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shield,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shield, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_shield')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_shield ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_shield ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shield, 'name') : 'Shadow Shield' }}</span>
                            </div>
                        </div>
                        <div class="row mb-1 no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_shoes }">
                            <div @click="getBuildEquipDetail(index, 'shadow_shoes')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_shoes"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shoes,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shoes, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_shoes')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_shoes ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_shoes ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_shoes, 'name') : 'Shadow Shoes' }}</span>
                            </div>
                        </div>
                        <div class="row no-gutters d-flex align-items-center justify-content-end row-equip-left" :class="{'equip-example-picked' : buildDetail_itemId === buildEquipExamples[activeBuildName][index].shadow_accessory_left }">
                            <div @click="getBuildEquipDetail(index, 'shadow_accessory_left')" class="col-2 col-equip-block-icon">
                                <span v-if="buildEquipExamples[activeBuildName][index].shadow_accessory_left"><img :src="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_left,'icon')" :alt="getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_left, 'name')"></span>
                                <span v-else></span>
                            </div>
                            <div @click="getBuildEquipDetail(index, 'shadow_accessory_left')" :class="'col-10 col-equip-block-name ' + (buildEquipExamples[activeBuildName][index].shadow_accessory_left ? 'item-equipped' : '')">
                                <span>{{ buildEquipExamples[activeBuildName][index].shadow_accessory_left ? getBuildEquipInfo(index, buildEquipExamples[activeBuildName][index].shadow_accessory_left, 'name') : 'Shadow Acc Left' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </b-tab>
    </b-tabs>
</div>
<section class="build-section" id="builds" v-cloak>
    <h2 class="color-600 mb-3">Các kiểu xây dựng</h2>
    <?php $builds = get_field('builds'); ?>
    <?php if ($builds): ?>
        <div class="build-list">
            <div class="row">
                <?php foreach($builds as $build_index => $build): ?>
                    <?php $author = get_field('author', $build) ? get_field('author', $build) : 'Anonymous'; ?>
                    <?php $from_server = get_field('from_server', $build) ? get_field('from_server', $build) : 'Anonymous'; ?>
                    <?php $build_name = get_field('build_name', $build); ?>
                    <div class="col-sm-6 col-md-4">
                        <b-link href="#equip-build-<?php echo sanitize_title($build_name); ?>" data-build="<?php echo $build_name ?>" class="tag-collapse" v-b-toggle.<?php echo 'equip-build-' . sanitize_title($build_name); ?>>
                            <div class="e-build">
                                <h3><?php echo $build_name; ?></h3>
                                <hr>
                                <div class="build-meta">
                                    <div class="build-author">
                                        <span>Tác giả:</span> <?php echo $author; ?>
                                    </div>
                                    <div class="build-from-server">
                                        <span>Từ Server:</span> <?php echo $from_server; ?>
                                    </div>
                                </div>
                            </div>
                        </b-link>
                    </div>
                <?php endforeach; ?>
            </div>

            <?php foreach($builds as $build_index => $build): ?>
                <?php $build_name = get_field('build_name', $build); ?>
                <b-collapse id="equip-build-<?php echo sanitize_title($build_name); ?>" accordion="build-accordion">
                    <ul class="list-group">
                        <li class="list-group-item list-group-block">
                            <?php include locate_template('/parts/build/present-block.php'); ?>
                        </li>
                        <li class="list-group-item list-group-block">
                            <?php include locate_template('/parts/build/stats-block.php'); ?>
                        </li>
                        <?php $equip_examples = get_field('equip_examples', $build); ?>
                        <?php if ($equip_examples): ?>
                            <li class="list-group-item list-group-block equip-builds-with-example">
                                <?php include locate_template('/parts/build/equips-block-new.php'); ?>
                            </li>
                        <?php else: ?>
                            <li class="list-group-item list-group-block equip-builds">
                                <?php include locate_template('/parts/build/equips-block-old.php'); ?>
                            </li>
                        <?php endif; ?>
                        <li class="list-group-item list-group-block">
                            <?php include locate_template('/parts/build/skills-block.php'); ?>
                        </li>
                        <?php $conclusion = get_field('conclusion', $build); ?>
                        <?php if ($conclusion): ?>
                            <li class="list-group-item list-group-block">
                                <?php include locate_template('/parts/build/conclusion-block.php'); ?>
                            </li>
                        <?php endif; ?>
                    </ul>
                </b-collapse>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</section><?php

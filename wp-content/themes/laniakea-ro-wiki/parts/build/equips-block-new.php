<div class="detail-build-block equipment-block">
    <div class="mb-3">
        <h5 class="color-600 mb-3"><i class="ion ion-ios-star"></i> Trang bị (Equipment):</h5>
    </div>

    <div class="equip-example" v-if="!isEmpty(buildEquipExamples)">
        <div class="row">
            <div class="col-lg-7">
                <b-card no-body>
                    <b-tabs pills fill card>
                        <b-tab v-for="(eq, index) in buildEquipExamples[activeBuildName]" :title="'Mẫu ' + (index+1)" :active="(index === 0)" @click="buildEquipExample_index = index">
                            <b-card-text>
                                <?php include locate_template('/parts/build/eq-example.php'); ?>
                            </b-card-text>
                        </b-tab>
                    </b-tabs>
                </b-card>
            </div>
            <div class="col-lg-5 pl-lg-0 col-build-detail">
                <template v-if="buildDetail_itemId > 0">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <h5 class="mb-0 text-uppercase">{{ `${buildDetail_position.split("_").join(" ")}` }}</h5>
                        </div>
                        <div class="col-6 text-right">
                            <b-dropdown size="sm" text="Lựa chọn thay thế" variant="primary" ref="dropdown" right>
                                <b-dropdown-text style="width: 300px;" v-for="eq in alternativeEQs(buildDetail_position)">
                                    <a href="#" @click.prevent="changeEquipExample(buildEquipExample_index, eq)" data-type="item-info" class="item-name">
                                        <img :src="`https://api.hahiu.com/data/items/icons/${convert_item_image(eq.item_id)}.png`"> <span>{{ (eq.refine > 0 ? '+' + eq.refine : '') + ' ' + eq.item_name }}</span>
                                    </a>
                                </b-dropdown-text>
                            </b-dropdown>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <a @click.prevent="getItemInfo(buildDetail_itemId, buildDetail_itemName)" href="#" data-type="item-info" :dataId="buildDetail_itemId" :dataName="`+${buildDetail_refine} ${buildDetail_itemName}`" class="item-name">
                            <img :src="`https://api.hahiu.com/data/items/icons/${convert_item_image(buildDetail_itemId)}.png`" :alt="`+${buildDetail_refine} ${buildDetail_itemName}`"> <span>{{ buildDetail_refine > 0 ? `+${buildDetail_refine} ${buildDetail_itemName}` : buildDetail_itemName }}</span>
                        </a> <small>({{ buildDetail_itemId }})</small>
                    </div>
                    <template v-if="buildDetail_cards.length">
                        <ul class="combo-with">
                            <li v-for="card in buildDetail_cards">
                                <div class="group-line">
                                    <span class="vli"></span>
                                    <span v-if="card.or" class="combo-or">Hoặc</span>
                                </div>
                                <a v-if="card.card_id > 0" @click.prevent="getItemInfo(card.card_id, card.card_name)" href="#" data-type="item-info" class="item-name">
                                    <img :src="`https://api.hahiu.com/data/items/icons/${convert_item_image(card.card_id)}.png`" :alt="card.card_name"> <span>{{ card.card_name }}</span>
                                </a>
                                <span class="small" v-if="card.how_many_card > 1">{{ `x${card.how_many_card}` }}</span>
                            </li>
                        </ul>
                    </template>
                    <div class="mt-2" v-if="buildDetail_options.length > 0">
                        <small><u>Lapine Enchant:</u></small>
                        <ul class="lapine-enchant-list list-unstyled">
                            <li v-for="option in buildDetail_options" class="lapine-option">
                                <span class="option-name">{{ get_random_option_str(option) }}</span>
                            </li>
                        </ul>
                    </div>
                </template>
                <template v-else>
                    <div class="equip-example-mini-help color-400 text-center">
                        <div class="alert alert-info mb-0">
                            Nhấp vào tên trang bị để xem chi tiết
                        </div>
                    </div>
                </template>
            </div>
        </div>
    </div>
</div>
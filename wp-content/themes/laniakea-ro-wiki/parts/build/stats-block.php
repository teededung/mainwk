<div class="detail-build-block stats">
    <h5 class="color-600 mb-3"><i class="ion ion-ios-star"></i> Kiểu nâng chỉ số (Stats):</h5>
    <table class="table table-sm table-bordered bg-light w-auto">
        <thead>
        <tr>
            <th>STR</th>
            <th>AGI</th>
            <th>VIT</th>
            <th>INT</th>
            <th>DEX</th>
            <th>LUK</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php the_field('str', $build); ?></td>
            <td><?php the_field('agi', $build); ?></td>
            <td><?php the_field('vit', $build); ?></td>
            <td><?php the_field('int', $build); ?></td>
            <td><?php the_field('dex', $build); ?></td>
            <td><?php the_field('luk', $build); ?></td>
        </tr>
        </tbody>
    </table>
    <?php the_field('stat_description', $build); ?>
</div>
<?php
$equips = get_field('equips', $build);
$eq_builds = array();
foreach($equips as $equip):
    $eq_builds[$equip['position']][$equip['tier']][] = array(
        'item_id' => $equip['item_id'],
        'item_name' => $equip['item_name'],
        'refine' => $equip['refine'],
        'add_cards' => $equip['add_cards'],
        'cards' => $equip['cards'],
        'add_options' => $equip['add_options'],
        'options' => $equip['options'],
        'recommend' => $equip['recommend']
    );
endforeach;
?>
<?php if ($eq_builds): ?>
    <h5 class="color-600 mb-3"><i class="ion ion-ios-star"></i> Trang bị (Gears):</h5>
    <div class="list-eq-build">
        <?php foreach($eq_builds as $key => $eq_build): ?>
            <div class="eq-build">
                <div class="eq-head">
                    <?php echo $key; ?>
                </div>
                <div class="eq-body">
                    <div class="row no-gutters">
                        <div class="col-sm-4 col-high-tier">
                            <ul class="list-group">
                                <li class="list-group-item active">Cao cấp</li>
                                <?php $tier = 'high' ?>
                                <?php include locate_template('/parts/build-tier.php'); ?>
                            </ul>
                        </div>

                        <div class="col-sm-4 col-mid-tier">
                            <ul class="list-group">
                                <li class="list-group-item active">Trung bình</li>
                                <?php $tier = 'middle' ?>
                                <?php include locate_template('/parts/build-tier.php'); ?>
                            </ul>
                        </div>

                        <div class="col-sm-4 col-low-tier">
                            <ul class="list-group">
                                <li class="list-group-item active">Khởi đầu</li>
                                <?php $tier = 'beginner' ?>
                                <?php include locate_template('/parts/build-tier.php'); ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<div class="detail-build-block present-block">
    <div class="mb-3">
        <h5 class="color-600 mb-3"><i class="ion ion-ios-star"></i> Giới thiệu:</h5>
        <?php echo get_the_content(null, false, $build); ?>
    </div>
    <?php $pros = get_field('pros', $build); ?>
    <?php $cons = get_field('cons', $build); ?>
    <?php if ($pros || $cons): ?>
    <div class="row">
        <?php if ($pros): ?>
        <div class="col-lg-6">
            <div class="pros mb-3">
                <h6 class="color-600 mb-3">Ưu điểm (Pros):</h6>
                <div class="pl-3">
                    <?php the_field('pros', $build); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($cons): ?>
        <div class="col-lg-6">
            <div class="cons">
                <h6 class="color-600 mb-3">Hạn chế (Cons):</h6>
                <div class="pl-3">
                    <?php the_field('cons', $build); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
</div>
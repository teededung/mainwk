<div class="detail-build-block skills-block">
    <h5 class="color-600 mb-3"><i class="ion ion-ios-star"></i> Kiểu nâng kỹ năng (Skill):</h5>
    <?php $skill_tree_link = get_field('skill_tree_link', $build); ?>
    <?php $skill_tree_description = get_field('skill_tree_description', $build); ?>

    <?php if ($skill_tree_link): ?>
    <div class="card-text mb-3"><?php echo $skill_tree_description; ?></div>
    <a target="_blank" href="<?php echo $skill_tree_link ?>" class="btn btn-primary">Xem bảng kỹ năng mẫu</a>
    <?php else: ?>
    <div class="alert alert-warning">
        <p>Chưa có hướng dẫn tăng kỹ năng.</p>
    </div>
    <?php endif; ?>
</div>

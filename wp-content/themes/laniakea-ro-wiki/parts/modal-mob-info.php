<b-modal v-cloak id="modal-mob" ref="modalMob" modal-class="modal-monster-info" :title="modalMob.name + ' #' + modalMob.monsterId" body-class="p-0" centered hide-footer>
    <div class="table-monster-drops-band">
        Thông tin quái vật
    </div>
    <div class="row row-mob-info g-0">
        <div class="col-md-3 d-flex flex-column justify-content-center align-items-center">
            <div class="mob-img mb-3">
                <img :src="'https://whisper.hahiu.com/ro/npc/' + modalMob.sprite + '.GIF'" :alt="modalMob.name">
            </div>

            <div class="row">
                <div class="col">
                    <span class="label">Cấp độ:</span> <code>{{ modalMob.level }}</code>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <span class="label">HP:</span> <code>{{ numberWithCommas(modalMob.hp) }}</code>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <span class="label">Tốc độ đi:</span> <code>{{ modalMob.speed }}</code>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            <span class="label">ATK:</span> <code>{{ modalMob.atk1 }}</code>
                        </div>
                        <div class="col">
                            <span class="label">MATK:</span> <code>{{ modalMob.atk2 }}</code>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            <span class="label">DEF:</span> <code>{{ modalMob.def }}</code>
                        </div>
                        <div class="col">
                            <span class="label">MDEF:</span> <code>{{ modalMob.mdef }}</code>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            <span class="label">Base EXP:</span> <code>{{ numberWithCommas(modalMob.baseExp) }}</code>
                        </div>
                        <div class="col">
                            <span class="label">Job EXP:</span> <code>{{ numberWithCommas(modalMob.jobExp) }}</code>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <span class="label">Kích cỡ (Size):</span> <code>{{ modalMob.size }}</code>
                </li>
                <li class="list-group-item">
                    <span class="label">Chủng loài (Race):</span> <code>{{ modalMob.race }}</code>
                </li>
                <li class="list-group-item">
                    <span class="label">Thuộc tính (Element):</span> <code>{{ modalMob.element }}</code>
                </li>
                <li class="list-group-item p-0 m-0">
                    <div class="row g-0 row-stats">
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mStr }}</code></span>
                            <span>STR</span>
                        </div>
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mAgi }}</code></span>
                            <span>AGI</span>
                        </div>
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mVit }}</code></span>
                            <span>VIT</span>
                        </div>
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mInt }}</code></span>
                            <span>INT</span>
                        </div>
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mDex }}</code></span>
                            <span>DEX</span>
                        </div>
                        <div class="col col-stat">
                            <span><code>{{ modalMob.mLuk }}</code></span>
                            <span>LUK</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="w-table-monster-drops">
        <div class="table-monster-drops-band">
            Danh sách rơi đồ
        </div>

        <template v-if="!isEmpty(modalMob.items)">
            <table class="table mb-0">
                <tr>
                    <th style="width: 54px">ID</th>
                    <th>Tên vật phẩm</th>
                    <th style="width: 62px;">Tỉ lệ rơi</th>
                </tr>

                <tr class="will-show-mob-info" v-for="itemObject in modalMob.items" v-if="!itemObject.isMvP">
                    <td class="align-middle">{{ itemObject.item.itemId }}</td>
                    <td class="align-middle">
                        <a class="item-name" href="#" @click.prevent="getItemInfo(Number(itemObject.item.itemId))">
                            <img :src="'https://whisper.hahiu.com/ro/item/'+itemObject.item.itemId+'.png'" :alt="itemObject.item.identifiedDisplayName">
                            <span>{{ itemObject.item.slots > 0 ? itemObject.item.identifiedDisplayName + ' [' + itemObject.item.slots + ']' : itemObject.item.identifiedDisplayName }}</span>
                        </a>
                    </td>
                    <td class="align-middle">{{ (itemObject.chance/100) + '%' }}</td>
                </tr>

                <tr class="will-show-mob-info" v-for="itemObject in modalMob.items" v-if="itemObject.isMvP">
                    <td class="align-middle">{{ itemObject.item.itemId }}</td>
                    <td class="align-middle">
                        <a class="item-name" href="#" @click.prevent="getItemInfo(Number(itemObject.item.itemId))">
                            <b-badge variant="danger">MVP!</b-badge>
                            <img :src="'https://whisper.hahiu.com/ro/item/'+itemObject.item.itemId+'.png'" :alt="itemObject.item.identifiedDisplayName">
                            <span>{{ itemObject.item.slots > 0 ? itemObject.item.identifiedDisplayName + ' [' + itemObject.item.slots + ']' : itemObject.item.identifiedDisplayName }}</span>
                        </a>
                    </td>
                    <td class="align-middle">{{ (itemObject.chance/100) + '%' }}</td>
                </tr>
            </table>
        </template>
        <div v-else class="alert alert-warning mb-0">Quái vật này không rơi đồ.</div>
    </div>
</b-modal>
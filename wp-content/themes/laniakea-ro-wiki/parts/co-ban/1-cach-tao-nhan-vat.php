<div class="section" id="cach-tao-nhan-vat">
	<h2>Cách tạo nhân vật</h2>

	<p>
		Sau khi bạn đã đăng nhập thành công bằng ID và mật khẩu bạn đã tạo trước đó.

		Nhấp vào đấu (+) để tạo một nhân vật mới.
	</p>

	<a data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-tao-nhan-vat-edited.jpg'; ?>">
		<img class="img-fluid img-huong-dan" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-tao-nhan-vat-edited.jpg'; ?>" alt="laniakea-ro-tao-nhan-vat">
	</a>

	<a data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-tao-nhan-vat-2-edited.jpg'; ?>">
		<img class="img-fluid img-huong-dan" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-tao-nhan-vat-2-edited.jpg'; ?>" alt="laniakea-ro-tao-nhan-vat">
	</a>

	<div class="clearfix margin-bottom-30"></div>

	<div class="row margin-bottom-30">
		<div class="col-sm-4">
			<div class="card card-class">
				<div class="img-card text-center">
					<img class="card-img-top align-self-center" src="<?php echo get_template_directory_uri() . '/images/classes/NOVICE.gif' ?>" alt="Novice">
				</div>

				<div class="card-body text-center">
					<h5 class="card-title">Novice</h5>
					<p class="card-text">Đây là nghề đầu tiên của bạn nếu bạn chọn tộc người</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="card card-class">
				<div class="img-card text-center">
					<img class="card-img-top align-self-center" src="<?php echo get_template_directory_uri() . '/images/classes/DO_SUMMONER.gif' ?>" alt="Novice">
				</div>
				<div class="card-body text-center">
					<h5 class="card-title">Summoner</h5>
					<p class="card-text">Đây là tộc Mèo mới được cập nhật vào Ragnarok</p>
				</div>
			</div>
		</div>
	</div>

    <p>Ragnarok có tổng cộng 18 nhánh nghề với 63 nghề khác nhau. Mỗi nghề đều có những kỹ năng, trang bị và cách xây dựng nhân vật khác nhau.</p>

    <p>Ở Laniakea RO, bạn có thể đổi nghề từ Novice sang các nghề khác bằng cách nói chuyện với Sự phụ ở làng Payon.</p>

	<p>Sau khi bạn tạo nhân vật xong, nhấp đôi vào nhân vật để vào game.</p>

	<a data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-chon-nhan-vat-edited.jpg'; ?>">
		<img class="img-fluid img-huong-dan" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-chon-nhan-vat-edited.jpg'; ?>" alt="laniakea-ro-chon-nhan-vat">
	</a>
</div>
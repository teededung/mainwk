<div class="section" id="phim-tat">
	<h2 class="margin-bottom-20">Phím tắt</h2>

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="width: 120px;">Phím tắt</th>
					<th>Công dụng</th>
				</tr>
			</thead>
			<tbody>
			<tr>
				<td><kbd>ALT + A</kbd></td>
				<td>Mở cửa sổ chỉ số nhân vật</td>
			</tr>
			<tr>
				<td><kbd>ALT + B</kbd></td>
				<td>Mở cửa sổ nhiệm vụ Instance.</td>
			</tr>
			<tr>
				<td><kbd>ALT + V</kbd></td>
				<td>
					Mở cửa sổ thông tin cơ bản, thông tin cơ bản bao gồm tên nhân vật, nghề, cấp độ & cấp độ nghề, máu (HP) và năng lượng (SP) hiện tại, trọng lượng hành trang (Weight) và Zeny.
				</td>
			</tr>
			<tr>
				<td><kbd>ALT + E</kbd></td>
				<td>Mở cửa sổ hành trang</td>
			</tr>
			<tr>
				<td><kbd>ALT + Q</kbd></td>
				<td>Mở cửa sổ trang bị</td>
			</tr>
			<tr>
				<td><kbd>ALT + W</kbd></td>
				<td>Mở cửa sổ xe đẩy. Cửa số này chỉ dành cho nghề Merchant hoặc Super Novice đã trang bị xe đẩy.</td>
			</tr>
			<tr>
				<td><kbd>ALT + U</kbd></td>
				<td>Mở cửa sổ nhiệm vụ. Cửa sổ này hiển thị các nhiệm vụ mà bạn đã nhận.</td>
			</tr>
            <tr>
                <td><kbd>ALT + R</kbd></td>
                <td>Mở cửa sổ Homunculus. Cửa sổ này chỉ có nghề Alchemist mới có thể mở.</td>
            </tr>
            <tr>
                <td><kbd>ALT + J</kbd></td>
                <td>Mở cửa sổ thông tin thú cưng. Chỉ mở được khi bạn đang nuôi thú cưng.s</td>
            </tr>
            <tr>
                <td><kbd>ALT + S</kbd></td>
                <td>Mở cửa sổ bảng kỹ năng nhánh nghề hiện tại của bạn.</td>
            </tr>
            <tr>
                <td><kbd>F12</kbd></td>
                <td>Hiện thanh phím tắt. Bấm nhiều lần để bung ra thêm nhiều hàng hoặc đóng thanh phím tắt.</td>
            </tr>
            <tr>
                <td><kbd>ALT + F10</kbd></td>
                <td>Hiện/Đóng cửa sổ tán gẫu.</td>
            </tr>
            <tr>
                <td><kbd>F10</kbd></td>
                <td>Điều chỉnh độ cao của cửa sổ tán gẫu.</td>
            </tr>
            <tr>
                <td><kbd>F11</kbd></td>
                <td>Đóng hết tất cả cửa sổ ngoại trừ cửa sổ thông tin cơ bản và cửa sổ tấn gẫu.</td>
            </tr>
            <tr>
                <td><kbd>F12</kbd></td>
                <td>Ẩn/Hiện/Điều chỉnh thanh phím tắt.</td>
            </tr>
            <tr>
                <td><kbd>ALT + Z</kbd></td>
                <td>Mở cửa sổ tổ đội.</td>
            </tr>
            <tr>
                <td><kbd>ALT + H</kbd></td>
                <td>Mở cửa sổ danh sách bạn bè.</td>
            </tr>
            <tr>
                <td><kbd>CTRL + TAB</kbd></td>
                <td>Ấn nhiều lần phím tắt này để thay đổi bản đồ nhỏ (Mini Map).</td>
            </tr>
            <tr>
                <td><kbd>ALT + O</kbd></td>
                <td>Mở cửa sổ tùy chỉnh âm thanh, nhạc nền, hiệu ứng, skin.</td>
            </tr>
            <tr>
                <td><kbd>ALT + C</kbd></td>
                <td>Tạo phòng tán gẫu.</td>
            </tr>
            <tr>
                <td><kbd>ALT + L</kbd></td>
                <td>Hiện bảng danh sách emoticon.</td>
            </tr>
            <tr>
                <td><kbd>ALT + M</kbd></td>
                <td>Mở cửa sổ Marco, cửa sổ này có tác dụng giúp bạn cài đặt phím tắt cho những dòng lệnh hoặc là những emoticon.</td>
            </tr>
            <tr>
                <td><kbd>ALT + Y</kbd></td>
                <td>Hiện danh sách các câu lệnh có sẵn.</td>
            </tr>
            <tr>
                <td><kbd>ALT + G</kbd></td>
                <td>Mở cửa sổ bang hội.</td>
            </tr>
            <tr>
                <td><kbd>CTRL + R</kbd></td>
                <td>Mở cửa sổ thông tin của lính đánh thuê (Mercenary).</td>
            </tr>
            <tr>
                <td><kbd>ALT + T</kbd></td>
                <td>Chuyển đổi chế độ bị động hay chủ động của Homunculus.</td>
            </tr>
            <tr>
                <td><kbd>CTRL + T</kbd></td>
                <td>Chuyển đổi chế độ bị động hay chủ động của lính đánh thuê (Mercenary).</td>
            </tr>
            <tr>
                <td><kbd>INSERT</kbd></td>
                <td>Nhân vật của bạn ngồi xuống hoặc đứng lên.</td>
            </tr>
            <tr>
                <td><kbd>CTRL + ~</kbd></td>
                <td>Mở bản đồ thế giới.</td>
            </tr>
            <tr>
                <td><kbd>ALT + END</kbd></td>
                <td>Hiện/Đóng thanh máu và năng lượng của người chơi.</td>
            </tr>
			</tbody>
		</table>
	</div>
</div>

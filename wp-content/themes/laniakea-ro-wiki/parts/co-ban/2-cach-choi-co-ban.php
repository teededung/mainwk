<div class="section" id="cach-choi-co-ban">
	<h2>Thông tin cơ bản</h2>

    <div class="margin-bottom-30">
        <h3 id="giao-dien">Giao diện</h3>
        <p>
            Dưới đây là giao diện của game (click vào hình ảnh để phóng to):
        </p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-giao-dien-ban-dau-edited.jpg'; ?>">
            <img class="img-fluid img-huong-dan-big" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-giao-dien-ban-dau-edited.jpg'; ?>" alt="laniakea-ro-giao-dien-ban-dau">
        </a>

        <ul class="list-group margin-bottom-20">
            <li class="list-group-item"><strong>(1) Cấp độ / Nghề / % kinh nghiệm hiện tại</strong> - Hãy chú ý trong việc luyện cấp, bạn sẽ thường xuyên kiểm tra phần này.</li>
            <li class="list-group-item"><strong>(2) Máu | Năng lượng</strong> - Máu và năng lượng hiện tại của bạn.</li>
            <li class="list-group-item"><strong>
                (3) Thanh phím tắt</strong> - Nếu bạn không thấy thanh phím tắt thì hãy bấm phím <kbd>F12</kbd> để hiện lại thanh phím tắt này. Nếu bạn muốn mở rộng thêm thì bấm phím <kbd>F12</kbd> thêm lần nữa. Mở rộng tối đa 4 dòng phím tắt.<br>
                Bạn có thể kéo vật phẩm hồi phục, kỹ năng hoặc trang bị vào các ô phím tắt này. Mặc định thanh phím tắt dòng 1 sẽ trải dài từ <kbd>F1</kbd> -> <kbd>F9</kbd>. Dòng 2 từ số <kbd>1</kbd> -> <kbd>9</kbd>.
            </li>
            <li class="list-group-item"><strong>(4) Bản đồ nhỏ (mini map)</strong> - Bạn hãy chú ý nhìn bản đồ thường xuyên.</li>
            <li class="list-group-item"><strong>(5) Tọa độ hiện tại (x, y)</strong> - Bạn cũng có thể xem tọa độ hiện tại của bạn bằng câu lệnh <strong>/where</strong></li>
            <li class="list-group-item"><strong>(6) Thu phóng bản đồ</strong> - Bạn nên thu nhỏ hết cỡ để bạn biết mình đang ở đâu trên bản đồ. Khi nào bạn cần xem chi tiết khu vực mình đang đứng thì bạn mới nên phóng to bản đồ.</li>
            <li class="list-group-item"><strong>(7) Bảng chat</strong> - Bấm phím <kbd>Enter</kbd> để bạn có thể gõ nội dung chat, <kbd>Enter</kbd> thêm lần nữa để gửi nội dung chat cho mọi người.</li>
        </ul>

        <p>Để xem thông tin cơ bản chi tiết bạn dùng phím tắt <kbd>ALT + V</kbd>, bảng thông tin cơ bản sẽ sổ ra như hình dưới đây:</p>

        <a class="img-lightbox img-has-border" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-thong-tin-co-ban-edited.png'; ?>">
            <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-thong-tin-co-ban-edited.png'; ?>" alt="laniakea-ro-thong-tin-co-ban">
        </a>

        <ul class="list-group">
            <li class="list-group-item"><strong>(1) Trọng lượng mang vác</strong> - Mỗi vật phẩm hoặc trang bị đều có trọng lượng, và nếu trọng lượng vượt quá 50% thì nhân vật của bạn sẽ không tự động phục hồi Máu và Năng lượng nữa. Nếu trọng lượng vượt quá 90% bạn sẽ không thể dùng kỹ năng. Hãy luôn để hành trang của bạn dưới 50%. Nếu bạn vượt quá 50% bạn nên cất đồ vào Rương chứa đồ ở nhân viên Kafra.</li>
            <li class="list-group-item"><strong>(2) Số tiền bạn đang có</strong> - Zeny là đơn vị tiền tệ của Ragnarok, Zeny có được bằng cách thu thập vật phẩm từ quái vật và bán cho NPC ở các thành phố.</li>
        </ul>
    </div>

    <div class="margin-bottom-30">
        <h3 id="hop-cong-cu">Hộp công cụ</h3>
        <p>
            Dưới đây là cách để bạn mở hộp công cụ:
        </p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-hop-cong-cu-edited.png'; ?>">
            <img class="img-fluid img-huong-dan" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-hop-cong-cu-edited.png'; ?>" alt="laniakea-ro-hop-cong-cu">
        </a>
    </div>

    <h3 id="trang-thai">Trạng thái</h3>
    <div class="margin-bottom-30">
        <p>
            Hệ thống điểm trạng thái của Ragnarok rất đa dạng, được chia thành 6 loại. Khi mới bắt đầu tất cả đều là 1 và bạn có thể tăng lên đến 99. Nếu là nghề thứ 3 bạn có thể tăng lên đến 130.
        </p>

        <p>Để mở cửa sổ trạng thái, bạn bấm phím tắt <kbd>ALT + A</kbd> hoặc nhấp vào biểu tượng khuôn mặt trong hộp công cụ.</p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-diem-trang-thai-edited.jpg'; ?>">
            <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-diem-trang-thai-edited.jpg'; ?>" alt="laniakea-ro-diem-trang-thai">
        </a>

        <ul class="list-group">
            <li class="list-group-item">
                <strong>Str</strong> (Strength) - tăng sát thương vật lý (ATK) và khả năng vác đồ (Weight).
            </li>
            <li class="list-group-item"><strong>Agi</strong> (Agility) - tăng khả năng nhé tránh (FLEE) và tốc độ tấn công (ASPD).</li>
            <li class="list-group-item">
                <strong>Vit</strong> (Vitality) - tăng Máu (HP) và phòng thủ vật lý (DEF).
                Càng nhiều Vit thì nhân vật của bạn càng "trâu bò".
            </li>
            <li class="list-group-item"><strong>Int</strong> (Intelligence) - tăng Năng lượng (SP), phòng thủ phép thuật (MDEF), giảm thời gian thi triển kỹ năng (Cast Time).</li>
            <li class="list-group-item">
                <strong>Dex</strong> (Dexterity) - tăng độ chuẩn xác (HIT), tăng sát thương vật lý tầm xa, giảm thời gian thi triển kỹ năng (Cast Time).
                Chỉ số Dex rất quan trọng, bạn nên tăng Dex ít nhất 60 ở cấp độ 60, và ít nhất 80 ở cấp độ 120.
            </li>
            <li class="list-group-item">
                <strong>Luk</strong> (Luck) - tăng tỉ lệ đánh chí mạng (CRIT), một chút sát thương vật lý (ATK), sát thương phép thuật (MATK) và độ chính xác (HIT).
                Càng nhiều Luk thì tỉ lệ chí mạng càng cao, đòn đánh chí mạng sẽ gây hơn 40% sát thương bình thường và không thể đánh trượt.
            </li>
        </ul>
    </div>

    <div class="margin-bottom-30">
        <h3 id="ky-nang">Kỹ năng</h3>
        <p>Để mở cửa sổ kỹ năng, bạn bấm phím tắt <kbd>ALT + S</kbd> hoặc nhấp vào biểu tượng hình quyển sách trong hộp công cụ.</p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-cay-ky-nang-edited.jpg'; ?>">
            <img class="img-fluid img-huong-dan-big" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-cay-ky-nang-edited.jpg'; ?>" alt="laniakea-ro-cay-ky-nang">
        </a>

        <ul class="list-group">
            <li class="list-group-item">
                <strong>(1) Danh sách kỹ năng hiện có.</strong> Mỗi nghề đều có những kỹ năng riêng biệt, ở trên hình là kỹ năng của nghề Novice (Tập sự).
            </li>
            <li class="list-group-item">
                <strong>(2) Điểm kỹ năng hiện có.</strong> Mỗi lần bạn lên cấp độ nghề (Lv JOB), bạn sẽ được 1 điểm kỹ năng, điểm kỹ năng này dùng để nâng cấp độ của kỹ năng. Ngoài ra sẽ có những kỹ năng yêu cầu những kỹ năng khác mới được phép tăng.
            </li>
            <li class="list-group-item">
                <strong>(3) Apply, Reset.</strong> Sau khi bạn đã nâng điểm kỹ năng, ấn vào nút "Apply" để xác nhận tăng cấp độ kỹ năng. Hoặc ấn nút "Reset" để tẩy lại. Bạn nên chú ý rằng, một khi đã ấn nút "Apply" là bạn không thể phục hồi lại được nữa, nên bạn hãy cân nhắc việc tăng kỹ năng cho chính xác.
            </li>
            <li class="list-group-item">
                <strong>(4) Xem thông tin kỹ năng.</strong> Mặc định khi bạn rê chuột vào kỹ năng nào đó thì sẽ có 1 bảng hiển thị ra cho bạn xem thông tin về kỹ năng đó. Nhưng nếu bạn không muốn rê chuột mà hiện lên thông tin của kỹ năng thì bạn hãy bỏ đánh dấu ô này.
            </li>
        </ul>
    </div>

    <h3 id="hanh-trang">Hành trang</h3>

    <div class="margin-bottom-30">
        <p>Để mở cửa sổ hành trang, bạn bấm phím tắt <kbd>ALT + E</kbd> hoặc nhấp vào biểu tượng như hình dưới đây.</p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-hanh-trang-edited.png'; ?>">
            <img class="img-fluid img-has-border" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-hanh-trang-edited.png'; ?>" alt="laniakea-ro-hanh-trang">
        </a>

        <ul class="list-group">
            <li class="list-group-item">
                <strong>(1) Vật phẩm sử dụng.</strong> Tất cả vật phẩm sử dụng được đều ở tab này như đồ hồi phục HP, SP. Thuốc bổ trợ, đồ ăn...
            </li>
            <li class="list-group-item">
                <strong>(2) Trang bị.</strong> Tất cả trang bị sẽ đều ở tab này như Mũ, Áo giáp, Vũ khí...
            </li>
            <li class="list-group-item">
                <strong>(3) Vật phẩm khác.</strong> Những vật phẩm nhặt được từ quái vật, nguyên liệu, vật phẩm nhiệm vụ đều ở tab này.
            </li>
            <li class="list-group-item">
                <strong>(4) Vật phẩm yêu thích.</strong> Nếu bạn hay sử dụng các vật phẩm nào đó, thì bạn hãy cho nó vào tab này để bạn dễ dàng tìm kiếm.
            </li>
        </ul>
    </div>

    <div class="margin-bottom-30">
        <h3 id="trang-bi">Trang bị</h3>
        <p>Để mở cửa sổ trang bị, bạn bấm phím tắt <kbd>ALT + Q</kbd> hoặc nhấp vào biểu tượng như hình dưới đây.</p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-trang-bi.png'; ?>">
            <img class="img-fluid img-has-border" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-trang-bi.png'; ?>" alt="laniakea-ro-trang-bi">
        </a>

        <ul class="list-group margin-bottom-20">
            <li class="list-group-item">
                Mặc định đối về tộc người, trang bị khởi đầu của bạn sẽ là <strong>Knife</strong> và <strong>Cotton Shirt</strong>.
            </li>
            <li class="list-group-item">
                Để tháo trang bị bạn nhấp đôi vào trang bị cần tháo hoặc kéo thả trang bị vào ô chứa hành trang.
            </li>
            <li class="list-group-item">
                Để mặc lại trang bị bạn nhấp đôi vào trang bị cần mặc ở hành trang.
            </li>
        </ul>

        <p>
            Ngoài ra, Ragnarok còn có hệ thống thời trang được gọi là Costume. Bạn sẽ thấy tab "Costume" ở cửa sổ Trang bị. Nhấp vào tab đó để xem đồ thời trang của bạn.
        </p>

        <p>
            Ở Laniakea RO, khi bạn tạo nhân vật bạn sẽ được tặng 1 <strong>Costume Happy Balloon</strong>. Hãy nhấp đôi vào vật phẩm là bạn đã trang bị được món đồ thời trang đầu tiên.
        </p>

        <a class="img-lightbox" data-lity href="<?php echo get_template_directory_uri() . '/images/co-ban/t-costume.jpg'; ?>">
            <img class="img-fluid img-huong-dan-big" src="<?php echo get_template_directory_uri() . '/images/co-ban/t-costume.jpg'; ?>" alt="laniakea-ro-costume">
        </a>
    </div>
</div>
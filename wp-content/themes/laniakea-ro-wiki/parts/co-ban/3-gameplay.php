<div class="section" id="gameplay">
	<h2 class="margin-bottom-20">Gameplay</h2>

    <div class="margin-bottom-30">
        <h3 id="chien-dau">Chiến đấu</h3>
        <p>
            Cách đánh cũng rất đơn giản, bạn chỉ cần rê chuột vào quái vật và nhấp vào nó. <br>
            Mặc định bạn sẽ tấn công liên tục, nhưng nếu bạn muốn đánh từng cú một cách thủ công thì bạn hãy gõ vào khung chat với nội dụng là <strong>/nc</strong><br>
            Đây là lệnh giúp bạn tấn công liên tục hoặc hủy tấn công liên tục.
        </p>

        <figure class="figure">
            <img class="figure-img img-has-border" src="<?php echo get_template_directory_uri() . '/images/gp-1.jpg' ?>" alt="chien-dau">
            <figcaption class="figure-caption">Cách tấn công quái vật</figcaption>
        </figure>
    </div>

    <div class="margin-bottom-30">
        <h3 id="vat-pham-tu-quai-vat">Vật phẩm từ quái vật</h3>
        <p>
            Sau khi bạn đã hạ gục quái vật, quái vật sẽ rơi ra những vật phẩm.<br>
            Sẽ có 3 loại vật phẩm: <strong>vật phẩm tiêu dùng</strong>, <strong>vật phẩm trang bị</strong> và <strong>vật phẩm khác</strong>.
        </p>

        <figure class="figure">
            <img class="figure-img img-has-border" src="<?php echo get_template_directory_uri() . '/images/gp-2.jpg' ?>" alt="vat-pham-tu-quai-vat">
            <figcaption class="figure-caption">Vật phẩm rơi ra từ quái vật</figcaption>
        </figure>

        <p>
            <strong>Vật phẩm tiêu dùng</strong> bao gồm những vật phẩm như giúp bạn hồi máu (HP), hồi năng lượng (SP), tăng tốc độ tấn công, giải độc, ...
            Bạn có thể kéo <strong>vật phẩm tiêu dùng</strong> lên thành phím tắt để bạn sử dụng chúng một cách nhanh chóng.
        </p>

        <figure class="figure">
            <img class="figure-img img-has-border" src="<?php echo get_template_directory_uri() . '/images/gp-3.jpg' ?>" alt="vat-pham-tieu-dung">
            <figcaption class="figure-caption">Vật phẩm tiêu dùng trên thanh phím tắt</figcaption>
        </figure>

        <div class="alert alert-success">
            Bạn có thể kéo <strong>vật phẩm trang trị</strong>, <strong>tên, đạn, ...</strong> vào thanh phím tắt để thay đổi một cách nhanh chóng.
        </div>

        <p>
            <strong>Vật phẩm trang bị</strong> mà bạn nhặt được từ quái vật sẽ có trạng thái là <strong>chưa giám định</strong>. Ở trạng thái này, bạn không thể xem thông tin hay đeo vật phẩm vào người được.
        </p>

        <div class="align-middle">
            <span>Bạn cần phải có vật phẩm</span> <img src="<?php echo get_template_directory_uri() . '/images/game-icon/magnifier.gif'; ?>" alt=""><strong>Magnifier (kính lúp)</strong> để giám định vật phẩm. Chỉ cần nhấp đôi vào kính lúp và chọn vật phẩm cần giám định là bạn đã giám định thành công vật phẩm.
        </div>
    </div>

    <div class="margin-bottom-30">
        <h3 id="mua-ban-voi-npc">Mua bán với NPC</h3>
        <p>
            Sau một cuộc hành trình đi luyện cấp, hành trang của bạn sẽ bị đầy, rất có thể hành trang của bạn đã bị quá tải <strong>hơn 50%</strong> khiến bạn không thể hồi phục máu và năng lượng một cách tự nhiên.
        </p>
        <p>
            Tệ hơn nữa là bạn sẽ bị quá tải <strong>90%</strong> khiến bạn không thể tấn công hoặc sử dụng kỹ năng.
        </p>
        <p>
            Nếu bạn rơi vào trường hợp này, bạn hãy quay về những thành phố và tìm các thương nhân để bán vật phẩm.
            Các NPC thương nhân là những NPC như <strong>cửa hàng dụng cụ (Tool Shop)</strong>, <strong>cửa hàng vũ khí</strong> và <strong>cửa hàng áo giáp</strong>.<br>
            Nếu bạn vẫn loay hoay tìm cách để quay về thành phố, bạn có thể dùng <strong>Cánh bướm (Butterfly Wing)</strong> hoặc <strong>Cánh bướm Tập sự (Novice Butterfly Wing)</strong> có trong hành trang để quay về thành phố mà bạn đã lưu trước đó.
        </p>
        <ul class="list-group margin-bottom-20">
            <li class="list-group-item">
                <strong>Cửa hàng dụng cụ</strong> có icon là hình bình thuốc trên bản đồ nhỏ.
            </li>
            <li class="list-group-item">
                <strong>Cửa hàng vũ khí</strong> có icon là hình cây kiếm.
            </li>
            <li class="list-group-item">
                <strong>Cửa hàng áo giáp</strong> có icon là hình cái khiên.
            </li>
        </ul>
        <p>
            Bạn có thể bán vật phẩm cho bất cứ NPC thương nhân nào. Để thuận tiện nhất, bạn nên tìm và mua bán với <strong>Cửa hàng dụng cụ (Tool Shop)</strong>.
            Vì sau khi bạn bán vật phẩm cho NPC xong, bạn vừa có thể mua những vật phẩm hồi máu, cánh ruồi (Fly Wing), cánh bướm (Butterfly Wing).
        </p>
    </div>
</div>
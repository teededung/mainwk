<div class="quest-post d-flex media text-muted rounded mb-3">
    <a href="<?php the_permalink() ?>">
        <?php the_post_thumbnail('medium'); ?>
    </a>
    <div class="media-body px-2 pt-2 pb-2 mb-0 small">
        <a href="<?php the_permalink() ?>" class="d-inline-block"><?php the_title(); ?></a>
        <p>
            <?php echo excerpt(30); ?>
        </p>
        <div class="text-end">
            <a href="<?php the_permalink() ?>" class="btn btn-sm btn-primary"><ion-icon name="arrow-forward-outline"></ion-icon></a>
        </div>
    </div>
</div>
<div class="eq-build">
    <div class="eq-head">
        Headgear Upper
    </div>

    <div class="eq-body">
        <div class="row no-gutters">
            <div class="col-sm-4">
                <ul class="list-group high-tier">
                    <li class="list-group-item active">Cao cấp</li>
                    <li class="list-group-item">
                        <?php echo _item(18856, "White Drooping Edgga Hat") ?>
                        <?php echo _item(4910, "Darklord Essence Force 3") ?>
                    </li>
                    <li class="list-group-item">
                        <?php echo _item(4910, "Darklord Essence Force 3") ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul class="list-group mid-tier">
                    <li class="list-group-item active">Trung bình</li>
                    <li class="list-group-item">
                        <?php echo _item(18856, "White Drooping Edgga Hat") ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul class="list-group low-tier">
                    <li class="list-group-item active">Khởi đầu</li>
                    <li class="list-group-item">
                        <?php echo _item(18856, "White Drooping Edgga Hat") ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php $quests = get_field($category_slug, 'option'); ?>
<?php if ($quests): ?>
    <div class="row">
        <?php foreach($quests as $quest): ?>
            <div class="col-lg-6">
                <?php if ($quest['from_post']): ?>
                    <?php $post = $quest['post']; ?>
                    <?php setup_postdata($post); ?>
                    <?php get_template_part('parts/loop/loop-recent-post'); ?>
                <?php else: ?>
                    <?php include locate_template('/parts/loop/loop-post-muted.php'); ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php else: ?>
    <div class="alert alert-warning">
        <p>Đang cập nhật...</p>
    </div>
<?php endif; ?>
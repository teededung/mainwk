<div class="col-xl-6">
    <div class="each-quest">
        <div class="row no-gutters">
            <div class="col-md-2">
                <div class="w-main-img d-none d-md-block">
					<?php if (has_post_thumbnail()): ?>
						<?php the_post_thumbnail('full', array('class' => 'card-img-left d-none d-md-block')); ?>
					<?php else: ?>
                        <img class="card-img-left flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1630b2fe664%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1630b2fe664%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.203125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
					<?php endif; ?>
                </div>
            </div>
            <div class="col-md-10">
                <div class="w-body-q">
                    <h3 class="mb-2">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                    <div class="badges mb-1">
                        <?php $tags = get_the_tags(); ?>
                        <span class="mb-1 small text-calendar text-muted">Thẻ:</span>
                        <?php if ($tags): ?>
                        <?php foreach ($tags as $tag): ?>
                            <span class="badge <?php echo get_badge_type($tag->name) ?>"><?php echo $tag->name ?></span>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <?php $rewards_tags = get_field('rewards_tags'); ?>
                    <?php if ($rewards_tags): ?>
                    <div class="rewards">
                        <span class="mb-1 small text-calendar text-muted">Phần thưởng:</span>
                        <?php foreach ($rewards_tags as $rt): ?>
                            <?php if ($rt['item_id'] == 'zeny'): ?>
                                <span class="badge badge-warning">Zeny</span>
                            <?php else: ?>
                                <img @click="getItemInfo(<?php echo $rt['item_id']; ?>, '')" src="<?php echo 'https://hahiu.com/wp-content/themes/laniakea-ro-wiki/images/items/icons/'. $rt['item_id'] .'.png' ?>" alt="<?php echo $rt['item_id']; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

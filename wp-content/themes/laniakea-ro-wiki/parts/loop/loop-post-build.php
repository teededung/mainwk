<div class="col-md-3">
    <div class="card bg-light build">
        <div class="w-build-img text-center">
            <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
            <?php endif; ?>
        </div>
        <div class="card-body">
            <a class="card-text text-center" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </div>
    </div>
</div>
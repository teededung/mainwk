<?php $link = get_the_permalink(); ?>
<div class="col-lg-4 mb-4 col-md-6">
    <div class="card">
        <a href="<?php echo $link ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('full',array('class' => 'card-img-top img-fluid')) ?>
        </a>
        <div class="card-body card-body-with-border-top">
            <h5 class="card-title">
                <a href="<?php echo $link; ?>">
                    <?php the_title(); ?>
                </a>
            </h5>
            <div class="card-text">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</div>
<?php $npcSource = get_template_directory_uri() . '/images/NPC/'; ?>
<input type="hidden" id="npcSource" value="<?php echo $npcSource; ?>">
<input type="hidden" id="post_id" value="<?php echo get_the_ID(); ?>">

<div id="sidebar-app" class="npc-list" :class="{'show':showSidebar}">
    <b-button variant="light" class="text-center rounded-0 w-100" @click="showSidebar = false">
        <i class="ion-md-close"></i>
    </b-button>

    <div class="tabs-sidebar">
        <div class="row no-gutters">
            <div v-if="!isEmpty(npcList)" class="col" :class="{'col-active' : tabActive === 'npc'}" @click="tabActive = 'npc'">
                NPC
            </div>
            <div v-if="!isEmpty(skills)" class="col" :class="{'col-active' : tabActive === 'skills'}" @click="tabActive = 'skills'">
                Kỹ năng
            </div>
            <div v-if="!isEmpty(items)" class="col" :class="{'col-active' : tabActive === 'items'}" @click="tabActive = 'items'">
                Vật phẩm
            </div>
        </div>
    </div>

    <template v-if="tabActive === 'npc'">
        <div class="no-gutters row-title-npc-list align-items-center">
            <div class="input-group">
                <input type="text" class="form-control" v-model="searchContent" placeholder="Gõ từ khóa để tìm..." aria-label="Gõ từ khóa để tìm" aria-describedby="button-search">
                <div class="input-group-append">
                    <button class="btn btn-primary">
                        <i class="ion-ios-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="content-npc-list">
            <template v-if="searchNPCList.length > 0">
                <div class="npc" v-for="(npc, index) in searchNPCList" :class="{'mark' : npc.mark}" :id="'npc-' + npc.id">
                    <div class="npc-img mb-3">
                        <img :src="npcSource + npc.img" @error="errorNPCImage(npc,index)" :alt="npc.name">
                    </div>
                    <h5 class="card-title">{{ npc.name }}</h5>
                    <p v-for="(location, index) in npc.locations" class="card-text" :class="{'highlight-location': ((index+1) === npc.index)}">
                        <code><a href="#" @click.prevent="" :data-clipboard-text="'/navi ' + location" class="clipboard">{{ location }}</a></code>
                    </p>
                </div>
            </template>
            <template v-else>
                <div class="npc" v-if="!loadingSidebar">
                    <template v-if="loadedNPC">Không tìm thấy NPC</template>
                    <template v-else>Chưa có NPC</template>
                </div>
            </template>
        </div>
    </template>

    <template v-if="tabActive === 'skills'">
        <div class="w-skill-info" v-for="skill in skills" :class="{'highlight' : skill.mark}" :id="'skill-' + skill.skillID" :key="'skill-' + skill.skillID" :class="{'mark' : skill.mark}">

            <b-link class="head-skill-info" v-b-toggle="'collapse-skill-' + skill.skillID">
                <img :src="skill.icon" alt="skill.skillName" class="mr-1"> <strong>{{ skill.skillName }}</strong>
            </b-link>

            <b-collapse :id="'collapse-skill-' + skill.skillID">
                <div class="body-skill-info">
                    <div class="meta-skill-info mb-3">
                        <div class="meta-max">
                            <span class="color-600">* Cấp độ tối đa:</span> <span>{{ skill.max }}</span>
                        </div>
                        <div class="meta-quest-skill" v-if="skill.isQuestSkill">
                            <span class="color-600">* Kỹ năng học từ nhiệm vụ (Quest Skill)</span>
                        </div>
                        <div class="meta-quest-skill" v-if="skill.isSpiritSkill">
                            <span class="color-600">* Kỹ năng từ linh hồn (Spirit Skill)</span>
                        </div>
                        <div class="skill-info-required-skills" v-if="!isEmpty(skill.requiredSkills)">
                            <span class="color-600 mb-1 d-block">* Kỹ năng yêu cầu:</span>
                            <div class="each-job-skill" v-for="skillObject in skill.requiredSkills">
                                <div class="mb-2" v-if="skill.requiredSkills.length > 1">
                                    <small><i class="ion-ios-remove mr-2"></i><span>{{ skillObject.jobName }}</span></small>
                                </div>
                                <ul class="job-required-skills pl-2 mb-0">
                                    <li class="each-skill d-flex align-items-center" v-for="sk in skillObject.skills">
                                        <i class="ion-ios-add mr-2"></i>
                                        <a class="skill mr-1" href="#" @click.prevent="getSkillInfo(sk.info.skillID)">
                                            <img :src="sk.info.icon" :alt="sk.info.skillName">
                                            <span>{{ sk.info.skillName }}</span>
                                        </a>
                                        <span class="skill-require-level">Lv. {{ sk.level }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="skill-content">
                        <template v-if="skill.description">
                            <div class="skill-info-description">
                                <p v-html="skill.description"></p>
                            </div>
                        </template>
                        <template v-else>
                            <div class="skill-info-description">
                                <div class="alert alert-warning">Nội dung đang được cập nhật...</div>
                            </div>
                        </template>
                    </div>
                </div>
                <div class="footer-skill-info">
                    <a target="_blank" rel="nofollow" :href="'https://www.divine-pride.net/database/skill/' + skill.skillID" title="Mở tab mới để xem kỹ năng này trên divine-pride.net">Xem trên divine-pride.net</a>
                </div>
            </b-collapse>
        </div>
    </template>

    <template v-if="tabActive === 'items'">
        <div class="an-item" v-for="item in items" :class="{'highlight' : item.mark}" :id="'item-' + item.itemInfo.itemId" :key="'item-' + item.itemInfo.itemId" :class="{'mark' : item.mark}">
            <b-link class="click-item" v-b-toggle="'collapse-item-' + item.itemInfo.itemId">
                <img :src="'https://whisper.hahiu.com/ro/item/' + item.itemInfo.itemId + '.png'" :alt="item.itemInfo.identifiedDisplayName"> <span>{{ item.itemInfo.identifiedDisplayName }}</span> <small>({{ item.itemInfo.itemId }})</small>
            </b-link>

            <b-collapse :id="'collapse-item-' + item.itemInfo.itemId">
                <div class="pt-2"></div>
                <div class="d-flex flex-row">
                    <div class="item-image-75">
                        <img :src="'https://whisper.hahiu.com/ro/collection/' + item.itemInfo.itemId + '.png'" :alt="item.itemInfo.identifiedDisplayName">
                        <!-- <img v-else src="<?php echo get_template_directory_uri() . '/images/noimg.jpg' ?>" alt="laniakea-ragnarok-online-no-img"> -->
                        <!--<b-button class="btn-sm btn-get-monster-drops" v-b-toggle="'collapse-drops-' + item.itemInfo.itemId" variant="primary">
                            Quái vật rơi
                        </b-button>-->
                    </div>
                    <div class="item-description" :class="{'expanded': item.itemInfo.expandedDescription}">
                        <p v-if="item.itemInfo.identifiedDescriptionName" v-html="convertHTML(item.itemInfo.identifiedDescriptionName)"></p>
                        <p v-else>Vật phẩm này chưa được cập nhật thông tin.</p>
                        <a href="#" class="btn-expand-description" v-if="item.itemInfo.identifiedDescriptionName && !item.itemInfo.expandedDescription" @click.prevent="item.itemInfo.expandedDescription = true">Xem đầy đủ...</a>
                        <div v-if="item.itemInfo.itemdesc && !item.itemInfo.expandedDescription" class="t-fade"></div>
                    </div>
                </div>

                <!--<template v-if="item.itemDrops.length > 0">
                    <b-collapse :id="'collapse-drops-' + item.itemInfo.itemId">
                        <div class="pt-2"></div>
                        <div class="alert alert-light mb-1 border">
                            <a target="_blank" rel="nofollow" :href="'https://www.divine-pride.net/database/item/' + item.itemInfo.itemId + '.png'">Xem trên divine-pride.net</a>
                        </div>
                        <table class="table table-sm mb-0">
                            <tr>
                                <th style="width: 54px">ID</th>
                                <th>Tên quái vật</th>
                                <th>Tỉ lệ rơi</th>
                            </tr>
                            <tr class="will-show-mob-info" v-for="(monster, i) in item.itemDrops" :key="'drop-' + i + '-' + monster.monster_id">
                                <td>{{ monster.monster_id }}</td>
                                <td>
                                    <a class="item-name" href="#" @click.prevent="getMobInfo(Number(monster.monster_id), monster.monster_name)">
                                        {{ monster.monster_name }}
                                    </a>
                                </td>
                                <td>{{ monster.drop_chance + '%' }}</td>
                            </tr>
                        </table>
                    </b-collapse>
                </template>
                <template v-else>
                    <div class="pt-2"></div>
                    <b-collapse :id="'collapse-drops-' + item.itemInfo.itemId">
                        <div class="alert alert-warning mb-0">Không có quái vật nào rơi ra vật phẩm này. <a class="d-inline-block" rel="nofollow" target="_blank" :href="'https://www.divine-pride.net/database/item/' + (item.itemInfo.itemId)">Xem trên divine-pride.net</a></div>
                    </b-collapse>
                </template>-->

                <b-collapse :id="'collapse-howToGet-' + item.itemInfo.itemId">
                    <template v-if="item.itemInfo.howToGet != ''">
                        <div class="alert alert-info alert-info-white mt-2 mb-0">
                            <strong>Cách tìm: </strong>
                            <template v-if="item.itemInfo.howToGet.length > 1">
                                <ul class="pl-4 mb-0">
                                    <li v-for="(text, index) in item.itemInfo.howToGet" :key="index" v-html="text"></li>
                                </ul>
                            </template>
                            <template v-else>
                                <p v-html="item.itemInfo.howToGet[0]"></p>
                            </template>
                        </div>
                    </template>
                    <?php if (current_user_can('editor') || current_user_can('administrator')): ?>
                    <template v-else>
                        <form class="mt-2" @submit.prevent="onSubmitHowToGet(item.itemInfo.itemId)">
                            <div class="mb-2">
                                <textarea class="form-control" name="howToGet" v-model="item.itemInfo.updateHowToGet" placeholder="How to get?" rows="3"></textarea>
                            </div>
                            <button class="btn btn-primary btn-sm">Submit</button>
                        </form>
                    </template>
                    <?php endif; ?>
                </b-collapse>
            </b-collapse>
        </div>
    </template>
</div>
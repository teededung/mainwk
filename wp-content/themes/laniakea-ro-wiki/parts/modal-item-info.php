<div v-if="!isEmpty(modalItem)" class="modal fade modal-item-info" id="modal-item" tabindex="-1" role="dialog" aria-labelledby="modal-item" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Thông tin vật phẩm</h5>
				<button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body modal-item-padding">
				<div class="ro-item-container mb-3">
					<div class="inner">
						<div class="ro-item-left">
							<img v-if="modalItem.itemInfo.image" :src="'https://api.hahiu.com' + modalItem.itemInfo.image" :alt="modalItem.itemInfo.name">
							<img v-else src="<?php echo get_template_directory_uri() . '/images/noimg.jpg' ?>" alt="laniakea-ragnarok-online-no-img">
						</div>
						<div class="ro-item-right">
							<em>{{ modalItem.itemInfo.name }}</em>
							<p v-if="modalItem.itemInfo.itemdesc" v-html="convertHTML(modalItem.itemInfo.itemdesc)"></p>
							<p v-else>Vật phẩm này chưa được cập nhật thông tin.</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="w-table-monster-drops">
					<template v-if="modalItem.itemDrops.length > 0">
						<button class="btn btn-primary w-100 text-center mb-2" type="button" data-toggle="collapse" data-target="#collapseMonsters" aria-expanded="false" aria-controls="collapseMonsters">
							Danh sách quái vật rơi
						</button>
						<div class="collapse" id="collapseMonsters">
							<table class="table mb-0">
								<tr>
									<th style="width: 54px">ID</th>
									<th>Tên quái vật</th>
									<th>Tỉ lệ rơi</th>
								</tr>
								<tr class="will-show-mob-info" v-for="monster in modalItem.itemDrops">
									<td>{{ monster.monster_id }}</td>
									<td>
                                        <template v-if="!noItemLink">
                                            <a class="item-name" href="#" @click.prevent="getMobInfo(Number(monster.monster_id), monster.monster_name)">
                                                {{ monster.monster_name }}
                                            </a>
                                        </template>
                                        <template v-else>
                                            {{ monster.monster_name }}
                                        </template>
                                    </td>
									<td>{{ monster.drop_chance + '%' }}</td>
								</tr>
							</table>
						</div>
					</template>
					<div v-else class="alert alert-warning mb-0">Vật phẩm này không có quái vật nào rơi ra.</div>

                    <div v-if="modalItem.itemInfo.item_id < 36000" class="other-item-links mt-2 small">
                        <strong class="d-block">Tham khảo:</strong>
                        <a target="_blank" rel="nofollow" :href="'https://divine-pride.net/database/item/' + modalItem.itemInfo.item_id">Xem trên divine-pride.net</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
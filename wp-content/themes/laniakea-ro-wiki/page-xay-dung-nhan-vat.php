<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>

            <h1 class="mb-5"><?php the_title(); ?></h1>

            <div class="alert alert-info">
                Trang đang được cập nhật... Vui lòng quay lại sau.
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

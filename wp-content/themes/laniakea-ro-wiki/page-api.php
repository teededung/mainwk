<?php
$type = isset($_GET['type']) ? $_GET['type'] : '';

switch ($type) {
    case 'pets':
        echo json_encode(array (
            array (
                'mobID' => 2398,
                'name' => 'Little Poring',
                'food' => array(531, 'Apple Juice'),
                'taming' => array(12846, 'Little Unripe Apple'),
                'img' => get_template_directory_uri() . '/images/pet/pet_little_poring.jpg',
                'eggID' => array(9062, 'Baby Poring Egg'),
                'cordial' => 'Tăng 50% lượng hồi phục HP tự nhiên.',
                'loyal' => 'Tăng 75% lượng hồi phục HP tự nhiên.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1002,
                'name' => 'Poring',
                'food' => array(531, 'Apple Juice',),
                'taming' => array (619, 'Unripe Apple'),
                'equip' => array(10013, 'Backpack'),
                'evolution' => array(
                    array(
                        'targetID' => 1090,
                        'items' => array(
                            array('itemID' => 610, 'itemName' => 'Yggdrasil Leaf', 'amount' => 10),
                            array('itemID' => 619, 'itemName' => 'Unripe Apple', 'amount' => 3)
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_poring.jpg',
                'eggID' => array(9001, 'Poring Egg'),
                'support' => 'Có khả năng nhặt 10 vật phẩm.',
                'loyal' => 'LUK +3<br>CRIT +1',
                'cordial' => 'LUK +2<br>CRIT +1',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1090,
                'name' => 'Mastering',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Poring',
                'evolution' => array(
                    array(
                        'targetID' => 1096, // Angeling
                        'items' => array(
                            array('itemID' => 503, 'itemName' => 'Yellow Potion', 'amount' => 20),
                            array('itemID' => 2282, 'itemName' => 'Halo', 'amount' => 1),
                            array('itemID' => 509, 'itemName' => 'White Herb', 'amount' => 50),
                            array('itemID' => 909, 'itemName' => 'Jellopy', 'amount' => 200),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_mastering.jpg',
                'eggID' => array(9069, 'Mastering Egg'),
                'shy' => 'LUK +2<br>CRIT +1',
                'neutral' => 'LUK +3<br>CRIT +1',
                'cordial' => 'LUK +3<br>CRIT +2',
                'loyal' => 'LUK +3<br>CRIT +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1096,
                'name' => 'Angeling',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Mastering',
                'img' => get_template_directory_uri() . '/images/pet/pet_angeling.jpg',
                'eggID' => array(9088, 'Angelring Egg'),
                'shy' => 'MaxHP +1%<br>Kỹ năng hồi máu +2%.',
                'neutral' => 'MaxHP +1%<br>Kỹ năng hồi máu +4%.',
                'cordial' => 'MaxHP +2%<br>Kỹ năng hồi máu +6%.',
                'loyal' => 'MaxHP +2%<br>Kỹ năng hồi máu +8%.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1113,
                'name' => 'Drops',
                'food' => array(508, 'Yellow Herb'),
                'taming' => array(620, 'Orange Juice'),
                'equip' => array(10013, 'Backpack'),
                'evolution' => array(
                    array(
                        'targetID' => 3495, // Eggring
                        'items' => array(
                            array('itemID' => 7032, 'itemName' => 'Piece of Egg Shell', 'amount' => 20),
                            array('itemID' => 7031, 'itemName' => 'Old Frying Pan', 'amount' => 10),
                            array('itemID' => 531, 'itemName' => 'Apple Juice', 'amount' => 3),
                            array('itemID' => 4659, 'itemName' => 'Eggring Card', 'amount' => 1),
                        )
                    ),
                    array(
                        'targetID' => 3790, // Sweets Drops
                        'items' => array(
                            array('itemID' => 25290, 'itemName' => 'Sweets Festival Coin', 'amount' => 500),
                            array('itemID' => 529, 'itemName' => 'Candy', 'amount' => 50),
                            array('itemID' => 530, 'itemName' => 'Candy Cane', 'amount' => 50),
                            array('itemID' => 4004, 'itemName' => 'Drops Card', 'amount' => 1),
                        )
                    ),
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_drops.jpg',
                'eggID' => array(9002, 'Drops Egg'),
                'support' => 'Có khả năng nhặt 10 vật phẩm.',
                'loyal' => 'HIT +5<br>ATK +5',
                'cordial' => 'HIT +3<br>ATK +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3495,
                'name' => 'Eggring',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Drops',
                'img' => get_template_directory_uri() . '/images/pet/pet_eggring.jpg',
                'eggID' => array(9092, 'Eggring Egg'),
                'shy'   => 'HIT +3<br>ATK +3',
                'neutral' => 'HIT +5<br>ATK +5',
                'cordial' => 'HIT +7<br>ATK +7',
                'loyal' => 'HIT +9<br>ATK +9',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3790,
                'name' => 'Sweet Drops',
                'food' => array(529, 'Candy'),
                'taming' => 'Tiến hóa từ Drops',
                'img' => get_template_directory_uri() . '/images/pet/pet_sweets_drops.jpg',
                'eggID' => array(9109, 'Sweet Drops Egg'),
                'show' => true,
                'expand' => false,
                'cordial' => 'Tăng 1% điểm kinh nghiệm khi tiêu diệt quái vật.'
            ),
            array (
                'mobID' => 1031,
                'name' => 'Poporing',
                'food' => array(511, 'Green Herb'),
                'taming' => array(621, 'Bitter Herb'),
                'equip' => array(10013, 'Backpack'),
                'img' => get_template_directory_uri() . '/images/pet/pet_poporing.jpg',
                'eggID' => array(9003, 'Poporing Egg'),
                'support' => 'Có khả năng nhặt 15 vật phẩm.',
                'loyal' => 'LUK +3<br>Kháng 15% sát thương thuộc tính <span style="color: #663399">Độc</span>.',
                'cordial' => 'LUK +2<br>Kháng 10% sát thương thuộc tính <span style="color: #663399">Độc</span>.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1242,
                'name' => 'Marin',
                'food' => array(6534, 'Fruit Sundae'),
                'taming' => array(12789, 'Juicy Fruit'),
                'equip' => array(10013, 'Backpack'),
                'img' => get_template_directory_uri() . '/images/pet/pet_marin.jpg',
                'eggID' => array(9061, 'Marin Egg'),
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 1894,
                'name' => 'Pouring',
                'food' => array(512, 'Apple'),
                'taming' => '(Đang cập nhật...)',
                'equip' => array(10013, 'Backpack'),
                'img' => get_template_directory_uri() . '/images/pet/pet_pouring.jpg',
                'eggID' => array(9114, 'Pouring Egg'),
                'neutral' => 'MaxHP +80<br>MaxSP +20',
                'cordial' => 'MaxHP +160<br>MaxSP +30',
                'loyal' => 'MaxHP +240<br>MaxSP +40',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 20695,
                'name' => 'Garling',
                'food' => array(54038, 'Tép tỏi'),
                'taming' => 'Nhiệm vụ "Trừ ma quỷ bằng tỏi? Giọt nước tỏi!',
                'img' => get_template_directory_uri() . '/images/pet/pet_toitoi.jpg',
                'eggID' => array(9150, 'Garling Egg'),
                'shy' => 'INT +1',
                'neutral' => 'INT +2',
                'cordial' => 'INT +3',
                'loyal' => 'INT +4<br>Giảm 4% thời gian thi triển kỹ năng.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1011,
                'name' => 'Chonchon',
                'food' => array(537, 'Pet Food'),
                'taming' => array(624, 'Rotten Fish'),
                'equip' => array(10002, 'Monster Oxygen Mask'),
                'img' => get_template_directory_uri() . '/images/pet/pet_chonchon.jpg',
                'eggID' => array(9006, 'Chonchon Egg'),
                'loyal' => 'AGI +2<br>Flee +3',
                'cordial' => 'AGI +1<br>Flee +2',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1042,
                'name' => 'Steel Chonchon',
                'food' => array(1002, 'Iron Ore'),
                'taming' => array(625, 'Rusty Iron'),
                'equip' => array(10002, 'Monster Oxygen Mask'),
                'img' => get_template_directory_uri() . '/images/pet/pet_steel_chonchon.jpg',
                'eggID' => array(9007, 'Steel Chonchon Egg'),
                'loyal' => 'Flee +9',
                'cordial' => 'Flee +6',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1035,
                'name' => 'Hunter Fly',
                'food' => array (716, 'Red Gemstone'),
                'taming' => array (626, 'Monster Juice'),
                'equip' => array(10002, 'Monster Oxygen Mask'),
                'img' => get_template_directory_uri() . '/images/pet/pet_hunter_fly.jpg',
                'eggID' => array(9008, 'Hunter Fly Egg'),
                'loyal' => 'Perfect Dodge +2<br>HIT +1',
                'cordial' => 'Perfect Dodge +2',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1052,
                'name' => 'Rocker',
                'food' => array(537, 'Pet Food'),
                'taming' => array (629, 'Singing Flower'),
                'equip' => array(10014, 'Rocker Glasses'),
                /*'evolution' => array(
                    array(
                        'targetID' => 1058, // Metaller
                        'items' => array(
                            array('itemID' => 707, 'itemName' => 'Singing Plant', 'amount' => 3),
                            array('itemID' => 940, 'itemName' => 'Grasshopper\'s Leg', 'amount' => 777),
                            array('itemID' => 508, 'itemName' => 'Yellow Herb', 'amount' => 200),
                            array('itemID' => 4057, 'itemName' => 'Metaller Card', 'amount' => 1),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_rocker.jpg',
                'eggID' => array(9011, 'Rocker Egg'),
                'loyal' => 'Tăng 6% lượng hồi phục HP tự nhiên.<br>MaxHP +38',
                'cordial' => 'Tăng 5% lượng hồi phục HP tự nhiên.<br>MaxHP +25',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1058,
                'name' => 'Metaller',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Rocker',
                'img' => get_template_directory_uri() . '/images/pet/pet_metaller.jpg',
                'eggID' => array(9106, 'Metaller Egg'),
                'shy' => 'Tăng 5% lượng hồi phục HP tự nhiên.<br>MaxHP +25',
                'neutral' => 'Tăng 10% lượng hồi phục HP tự nhiên.<br>MaxHP +38',
                'cordial' => 'Tăng 15% lượng hồi phục HP tự nhiên.<br>MaxHP +55<br>Tăng 3% sát thương vật lý và phép thuật lên chủng loài thực vật (Plant).',
                'loyal' => 'Tăng 20% lượng hồi phục HP tự nhiên.<br>MaxHP +70<br>Tăng 6% sát thương vật lý và phép thuật lên chủng loài thực vật (Plant).',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1014,
                'name' => 'Spore',
                'food' => array(537, 'Pet Food',),
                'taming' => array (630, 'Dew Laden Moss'),
                'equip' => array(10017, 'Bark Shorts'),
                'img' => get_template_directory_uri() . '/images/pet/pet_spore.jpg',
                'eggID' => array(9012, 'Spore Egg'),
                'support' => 'Có khả năng giải độc cho chủ nhân.',
                'loyal' => 'HIT +8',
                'cordial' => 'HIT +5',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1077,
                'name' => 'Poison Spore',
                'food' => array(537, 'Pet Food'),
                'taming' => array (631, 'Deadly Noxious Herb'),
                'equip' => array(10017, 'Bark Shorts'),
                'img' => get_template_directory_uri() . '/images/pet/pet_poison_spore.jpg',
                'eggID' => array(9013, 'Poison Spore Egg'),
                'support' => 'Có tỉ lệ 10% gây sát thương thuộc tính độc lên kẻ thù.',
                'loyal' => 'STR +2<br>INT +2',
                'cordial' => 'STR +1<br>INT +1',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1495,
                'name' => 'Stone Shooter',
                'food' => array(6109, 'Plant Neutrient',),
                'taming' => array (12369, 'Oilpalm Coconut'),
                'img' => get_template_directory_uri() . '/images/pet/pet_stone_shooter.jpg',
                'eggID' => array(9051, 'Stone Shooter Egg'),
                'loyal' => 'Kháng 5% sát thương thuộc tính lửa.',
                'cordial' => 'Kháng 3% sát thương thuộc tính lửa.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1063,
                'name' => 'Lunatic',
                'food' => array(534, 'Carrot Juice'),
                'taming' => array (622, 'Rainbow Carrot'),
                'equip' => array(10007, 'Silk Ribbon'),
                /*'evolution' => array(
                    array(
                        'targetID' => 3496, // Leaf Lunatic
                        'items' => array(
                            array('itemID' => 7198, 'itemName' => 'Huge Leaf', 'amount' => 100),
                            array('itemID' => 705, 'itemName' => 'Clover', 'amount' => 250),
                            array('itemID' => 706, 'itemName' => 'Four Leaf Clover', 'amount' => 30),
                            array('itemID' => 4663, 'itemName' => 'Leaf Lunatic Card', 'amount' => 1),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_lunatic.jpg',
                'eggID' => array(9004, 'Lunatic Egg'),
                'loyal' => 'ATK +3<br>CRIT +3',
                'cordial' => 'ATK +2<br>CRIT +2',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 3496,
                'name' => 'Leaf Lunatic',
                'food' => array(534, 'Carrot Juice'),
                'taming' => 'Tiến hóa từ Lunatic',
                'img' => get_template_directory_uri() . '/images/pet/pet_dr_lunatic.jpg',
                'eggID' => array(9094, 'Leaf Lunatic Egg'),
                'shy' => 'ATK +2<br>CRIT +2.',
                'neutral' => 'ATK +3<br>CRIT +3',
                'cordial' => 'ATK +4<br>CRIT +4<br>Tăng 3% sát thương vật lý và phép thuật lên chủng loài Vô dạng.',
                'loyal' => 'ATK +5<br>CRIT +5<br>Tăng 6% sát thương vật lý và phép thuật lên chủng loài Vô dạng.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1879,
                'name' => 'Eclipse',
                'food' => array (7766, 'Bok Choy'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_lunatic.jpg',
                'eggID' => array(9031, 'Spring Rabbit Egg'),
                'support' => 'Có tỉ lệ 5% sử dụng kỹ năng ném đá lên kẻ thù.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1049,
                'name' => 'Picky',
                'food' => array (507, 'Red Herb'),
                'taming' => array (623, 'Earthworm the Dude'),
                'equip' => array(10012, 'Tiny Egg Shell'),
                'img' => get_template_directory_uri() . '/images/pet/pet_picky.jpg',
                'eggID' => array(9005, 'Picky Egg'),
                'support' => 'STR +3 trong 10 giây mỗi 50 giây.',
                'loyal' => 'STR +2<br>ATK +8',
                'cordial' => 'STR +1<br>ATK +5',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1167,
                'name' => 'Savage Babe',
                'food' => array(537, 'Pet Food'),
                'taming' => array(627, 'Sweet Milk'),
                'equip' => array(10015, 'Green Lace'),
                'evolution' => array(
                    array(
                        'targetID' => 1166, // Savage
                        'items' => array(
                            array('itemID' => 537, 'itemName' => 'Pet Food', 'amount' => 10),
                            array('itemID' => 627, 'itemName' => 'Sweet Milk', 'amount' => 3),
                            array('itemID' => 517, 'itemName' => 'Meat', 'amount' => 100),
                            array('itemID' => 949, 'itemName' => 'Feather', 'amount' => 50),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_savage_babe.jpg',
                'eggID' => array(9009, 'Savage Bebe Egg'),
                'loyal' => 'VIT +2<br>MaxHP +75',
                'cordial' => 'VIT +1<br>MaxHP +50',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1166,
                'name' => 'Savage',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Savage Babe',
                'img' => get_template_directory_uri() . '/images/pet/pet_savage.jpg',
                'eggID' => array(9070, 'Savage Egg'),
                'shy' => 'VIT +1<br>MaxHP +50',
                'neutral' => 'VIT +2<br>MaxHP +50',
                'cordial' => 'VIT +2<br>MaxHP +100',
                'loyal' => 'VIT +2<br>MaxHP +200',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1107,
                'name' => 'Baby Desert Wolf',
                'food' => array(537, 'Pet Food'),
                'taming' => array(628, 'Well-Dried Bone'),
                'equip' => array(10003, 'Transparent Head Protector'),
                'img' => get_template_directory_uri() . '/images/pet/pet_desert_wolf_b.jpg',
                'eggID' => array(9010, 'Baby Desert Wolf Egg'),
                'loyal' => 'INT +2<br>MaxSP +75',
                'cordial' => 'INT +1<br>MaxSP +50',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1056,
                'name' => 'Smokie',
                'food' => array(537, 'Pet Food'),
                'taming' => array(633, 'Sweet Potato'),
                'equip' => array(10019, 'Red Scarf'),
                'img' => get_template_directory_uri() . '/images/pet/pet_smokie.jpg',
                'eggID' => array(9015, 'Smokie Egg'),
                'support' => 'Giúp chủ nhân tàng hình.',
                'loyal' => 'AGI +2<br>Né tránh hoàn hảo +1.',
                'cordial' => 'AGI +1<br>Né tránh hoàn hảo +1.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1586,
                'name' => 'Leaf Cat',
                'food' => array(6096, 'Fish with Blue Back'),
                'taming' => array(12359, 'Very Soft Plant'),
                'equip' => array(10023, 'Green Jewel Bag'),
                'img' => get_template_directory_uri() . '/images/pet/pet_leaf_cat.jpg',
                'eggID' => array(9041, 'Leaf Cat Egg'),
                'loyal' => 'Giảm 5% sát thương từ chủng loài Quái thú (Brute).',
                'cordial' => 'Giảm 3% sát thương từ chủng loài Quái thú (Brute).',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1057,
                'name' => 'Yoyo',
                'food' => array(532, 'Banana Juice'),
                'taming' => array (634, 'Tropical Banana'),
                'equip' => array(10018, 'Monkey Circlet'),
                'evolution' => array(
                    array(
                        'targetID' => 1214, // Choco
                        'items' => array(
                            array('itemID' => 634, 'itemName' => 'Tropical Banana', 'amount' => 3),
                            array('itemID' => 753, 'itemName' => 'Yoyo Doll', 'amount' => 2),
                            array('itemID' => 7182, 'itemName' => 'Cacao', 'amount' => 300),
                            array('itemID' => 4051, 'itemName' => 'Yoyo Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_yoyo.jpg',
                'eggID' => array(9016, 'Yoyo Egg'),
                'support' => 'Có khả năng nhặt 20 vật phẩm.',
                'loyal' => 'CRIT +5',
                'cordial' => 'CRIT +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1214,
                'name' => 'Choco',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Yoyo',
                'img' => get_template_directory_uri() . '/images/pet/pet_choco.jpg',
                'eggID' => array(9091, 'Choco Egg'),
                'shy' => 'CRIT +3',
                'neutral' => 'CRIT +5<br>Tăng 1% sát thương vật lý tầm xa.',
                'cordial' => 'CRIT +7<br>Tăng 2% sát thương vật lý tầm xa.',
                'loyal' => 'CRIT +9<br>Tăng 3% sát thương vật lý tầm xa.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1019,
                'name' => 'Peco Peco',
                'food' => array(537, 'Pet Food'),
                'taming' => array(632, 'Fatty Chubby Earthworm'),
                'equip' => array(10010, 'Battered Pot'),
                'evolution' => array(
                    array(
                        'targetID' => 1369, // Grand Peco
                        'items' => array(
                            array('itemID' => 537, 'itemName' => 'Pet Food', 'amount' => 10),
                            array('itemID' => 632, 'itemName' => 'Fatty Chubby Earthworm', 'amount' => 3),
                            array('itemID' => 7101, 'itemName' => 'PecoPeco Feather', 'amount' => 300),
                            array('itemID' => 4031, 'itemName' => 'Pecopeco Card', 'amount' => 1),
                            array('itemID' => 522, 'itemName' => 'Mastela Fruit', 'amount' => 10),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_pecopeco.jpg',
                'eggID' => array(9014, 'PecoPeco Egg'),
                'support' => 'Tăng 25% tốc độ di chuyển trong 20 giây mỗi 20 giây.',
                'loyal' => 'MaxHP +200<br>',
                'cordial' => 'MaxHP +150<br>',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1369,
                'name' => 'Grand Peco',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Peco Peco',
                'img' => get_template_directory_uri() . '/images/pet/pet_grandpeco.jpg',
                'eggID' => array(9071, 'Grand Peco Egg'),
                'shy' => 'MaxHP +150',
                'neutral' => 'MaxHP +200',
                'cordial' => 'MaxHP +300',
                'loyal' => 'MaxHP +400',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1155,
                'name' => 'Petite',
                'food' => array(537, 'Pet Food'),
                'taming' => array(640, 'Shining Stone'),
                'equip' => array(10011, 'Stellar Hairpin'),
                'evolution' => array(
                    array(
                        'targetID' => 3670, // Earth Deleter
                        'items' => array(
                            array('itemID' => 640, 'itemName' => 'Shining Stone', 'amount' => 3),
                            array('itemID' => 6260, 'itemName' => 'Petite\'s Tail', 'amount' => 100),
                            array('itemID' => 606, 'itemName' => 'Aloevera', 'amount' => 150),
                            array('itemID' => 4279, 'itemName' => 'Earth Deleter Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_petit.jpg',
                'eggID' => array(9022, 'Green Petite Egg'),
                'loyal' => 'ASPD +1%<br>AGI +1',
                'cordial' => 'ASPD +1%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3670,
                'name' => 'Earth Deleter',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Petite',
                'img' => get_template_directory_uri() . '/images/pet/pet_deleter_.jpg',
                'eggID' => array(9098, 'Earth Deleter Egg'),
                'shy' => 'ASPD +2%.',
                'neutral' => 'ASPD +2%<br>AGI +1',
                'cordial' => 'ASPD +3%<br>AGI +2',
                'loyal' => 'ASPD +3%<br>AGI +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1026,
                'name' => 'Munak',
                'food' => array(537, 'Pet Food'),
                'taming' => array(636, 'No Recipient'),
                'equip' => array(10008, 'Punisher'),
                'img' => get_template_directory_uri() . '/images/pet/pet_munak.jpg',
                'eggID' => array(9018, 'Munak Egg'),
                'loyal' => 'INT +2<br>DEF +2',
                'cordial' => 'INT +1<br>DEF +1',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1188,
                'name' => 'Bongun',
                'food' => array(537, 'Pet Food'),
                'taming' => array(659, 'Her Heart'),
                'equip' => array(10020, 'Grave Keeper\'s Sword'),
                'evolution' => array(
                    array(
                        'targetID' => 1512, // Yao Jun
                        'items' => array(
                            array('itemID' => 5367, 'itemName' => 'Yao Jun Hat', 'amount' => 1),
                            array('itemID' => 7277, 'itemName' => 'Munak Doll', 'amount' => 100),
                            array('itemID' => 609, 'itemName' => 'Amulet', 'amount' => 100),
                            array('itemID' => 4328, 'itemName' => 'Yao Jun Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_bon_gun.jpg',
                'eggID' => array(9025, 'Bongun Egg'),
                'loyal' => 'VIT +2<br>Kháng 2% trạng thái Choáng (Stun).',
                'cordial' => 'VIT +1<br>Kháng 1% trạng thái Choáng (Stun).',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1512,
                'name' => 'Yao Jun',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Bongun',
                'img' => get_template_directory_uri() . '/images/pet/pet_hyegun.jpg',
                'eggID' => array(9093, 'Hyegun Egg'),
                'shy' => 'VIT +1, Kháng 1% hiệu ứng choáng (Stun).',
                'neutral' => 'VIT +2.<br> Kháng 2% hiệu ứng Choáng (Stun).',
                'cordial' => 'VIT +3.<br>Kháng 3% hiệu ứng Choáng (Stun).<br> Có tỉ lệ chuyển hóa 1% sát thương gây được thành SP.',
                'loyal' => 'VIT +4.<br>Kháng 4% hiệu ứng Choáng (Stun).<br>Có tỉ lệ chuyển hóa 1% sát thương gây được thành SP.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1170,
                'name' => 'Sohee',
                'food' => array(537, 'Pet Food'),
                'taming' => array (638, 'Silver Knife of Chastity'),
                'equip' => array(10016, 'Golden Bell'),
                'img' => get_template_directory_uri() . '/images/pet/pet_sohee.jpg',
                'eggID' => array(9020, 'Sohee Egg'),
                'loyal' => 'STR +2<br>DEX +2',
                'cordial' => 'STR +1<br>DEX +1',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1275,
                'name' => 'Alice',
                'food' => array(504, 'White Potion'),
                'taming' => array(661, 'Soft Apron'),
                /*'evolution' => array(
                    array(
                        'targetID' => 1737, // Aliza
                        'items' => array(
                            array('itemID' => 7047, 'itemName' => 'Alice\'s Apron', 'amount' => 500),
                            array('itemID' => 511, 'itemName' => 'Green Herb', 'amount' => 200),
                            array('itemID' => 985, 'itemName' => 'Elunium', 'amount' => 30),
                            array('itemID' => 4253, 'itemName' => 'Alice Card', 'amount' => 1),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_alice.jpg',
                'eggID' => array(9027, 'Alice Egg'),
                'loyal' => 'MDEF +2<br>Giảm 2% sát thương nhận từ Demi-human.',
                'cordial' => 'MDEF +1<br>Giảm 1% sát thương nhận từ Demi-human.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1737,
                'name' => 'Aliza',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Alice',
                'img' => get_template_directory_uri() . '/images/pet/pet_aliza.jpg',
                'eggID' => array(9120, 'Aliza Egg'),
                'loyal' => 'MDEF +7<br>Tăng 6% hiệu quả các kỹ năng hồi máu',
                'cordial' => 'MDEF +6<br>Tăng 4% hiệu quả các kỹ năng hồi máu',
                'neutral' => 'MDEF +5<br>Tăng 2% hiệu quả các kỹ năng hồi máu',
                'shy' => 'MDEF +4',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1029,
                'name' => 'Isis',
                'food' => array(537, 'Pet Food'),
                'taming' => array(639, 'Armlet of Obedience'),
                'equip' => array(10006, 'Queen\'s Hair Ornament'),
                'evolution' => array(
                    array(
                        'targetID' => 3636, // Little Isis
                        'items' => array(
                            array('itemID' => 639, 'itemName' => 'Armlet of Obedience', 'amount' => 3),
                            array('itemID' => 732, 'itemName' => '3carat Diamond', 'amount' => 6),
                            array('itemID' => 10006, 'itemName' => 'Queen\'s Hair Ornament', 'amount' => 1),
                            array('itemID' => 954, 'itemName' => 'Shining Scale', 'amount' => 300),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_isis.jpg',
                'eggID' => array(9021, 'Isis Egg'),
                'loyal' => 'ATK +2%',
                'cordial' => 'ATK +1%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3636,
                'name' => 'Little Isis',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Isis',
                'img' => get_template_directory_uri() . '/images/pet/pet_little_isis.jpg',
                'eggID' => array(9090, 'Little Isis Egg'),
                'shy' => 'ATK +1%',
                'neutral' => 'ATK +2%',
                'cordial' => 'ATK +3%',
                'loyal' => 'ATK +4%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1505,
                'name' => 'Loli Ruri',
                'food' => array(6097, 'Pumpkin Pie'),
                'taming' => array(12360, 'Very Red Juice'),
                'equip' => array(10024, 'Fashionable Glasses'),
                'img' => get_template_directory_uri() . '/images/pet/pet_loli_ruri.jpg',
                'eggID' => array(9042, 'Loli Ruri Egg'),
                'evolution' => array(
                    array(
                        'targetID' => 20940, // Blue Moon Loli Ruri
                        'items' => array(
                            array('itemID' => 25375, 'itemName' => 'Powerful Soul Essence', 'amount' => 50),
                            array('itemID' => 27102, 'itemName' => 'Matte Drainliar Card', 'amount' => 10),
                            array('itemID' => 4379, 'itemName' => 'Blue Acidus Card', 'amount' => 10),
                            array('itemID' => 4597, 'itemName' => 'Lichtern Blue Card', 'amount' => 10),
                        )
                    )
                ),
                'loyal' => 'MaxHP +5%<br>Có tỉ lệ sử dụng kỹ năng Heal cấp độ 2 khi gây sát thương vật lý.',
                'cordial' => 'MaxHP +3%<br>Có tỉ lệ sử dụng kỹ năng Heal cấp độ 1 khi gây sát thương vật lý.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 20940,
                'name' => 'Blue Moon Loli Ruri',
                'food' => array(6097, 'Pumpkin Pie'),
                'taming' => 'Tiến hóa từ Loli Ruri',
                'img' => get_template_directory_uri() . '/images/pet/pet_bluemoon_loli_ruri.jpg',
                'eggID' => array(9166, 'Bluemoon Loli Ruri Egg'),
                'loyal' => 'MaxHP +5%<br>MaxSP +5%<br>Có tỉ lệ sử dụng kỹ năng Heal cấp độ 5 khi gây sát thương vật lý.',
                'cordial' => 'MaxHP +3%<br>MaxSP +3%<br>Có tỉ lệ sử dụng kỹ năng Heal cấp độ 3 khi gây sát thương vật lý.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1416,
                'name' => 'Evil Nymph',
                'food' => array(6105, 'Morning Dew'),
                'taming' => array(12365, 'Charming Lotus'),
                'equip' => array(10029, 'Jade Trinket'),
                'img' => get_template_directory_uri() . '/images/pet/pet_wicked_nymph.jpg',
                'eggID' => array(9047, 'Wicked Nymph Egg'),
                'evolution' => array(
                    array(
                        'targetID' => 20423, // Bacsojin
                        'items' => array(
                            array('itemID' => 25375, 'itemName' => 'Powerful Soul Essence', 'amount' => 30),
                            array('itemID' => 4202, 'itemName' => 'Mao Guai Card', 'amount' => 10),
                            array('itemID' => 4265, 'itemName' => 'Jing Guai Card', 'amount' => 10),
                            array('itemID' => 4272, 'itemName' => 'Zhu Po Long Card', 'amount' => 10),
                        )
                    )
                ),
                'loyal' => 'MaxSP +45<br>Tăng 8% lượng hồi phục SP tự nhiên.',
                'cordial' => 'MaxSP +30<br>Tăng 5% lượng hồi phục SP tự nhiên.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 20423,
                'name' => 'Bacsojin',
                'food' => array (25377, 'Luxurious Pet Food'),
                'taming' => 'Tiến hóa từ Wicked Nymph',
                'img' => get_template_directory_uri() . '/images/pet/pet_bacsojin.jpg',
                'eggID' => array(9039, 'Bacsojin Egg'),
                'loyal' => 'MaxSP +5%<br> Giảm 3% delay kỹ năng.',
                'cordial' => 'MaxSP +4%<br> Giảm 2% delay kỹ năng.',
                'neutral' => 'MaxSP +3%<br> Giảm 1% delay kỹ năng.',
                'shy' => 'MaxSP +2%',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),
            array (
                'mobID' => 1143,
                'name' => 'Marionette',
                'food' => array(6098, 'Small Snow Flower'),
                'taming' => array(12361, 'Delicious Shaved Ice'),
                'equip' => array(10025, 'Star Hairband'),
                'img' => get_template_directory_uri() . '/images/pet/pet_marionette.jpg',
                'eggID' => array(9043, 'Marionette Egg'),
                'loyal' => 'Có tỉ lệ nhận hiệu ứng giảm 20% sát thương trung tính trong 5 giây và hồi phục 100 HP khi nhận sát thương vật lý.',
                'cordial' => 'Có tỉ lệ nhận hiệu ứng giảm 20% sát thương trung tính trong 3 giây và hồi phục 100 HP khi nhận sát thương vật lý.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1404,
                'name' => 'Miyabi Doll',
                'food' => array(6106, 'Well-ripened Strawberry'),
                'taming' => array(12366, 'Girl\'s Doll'),
                'equip' => array(10030, 'Summer Fan'),
                'img' => get_template_directory_uri() . '/images/pet/pet_miyabi_ningyo.jpg',
                'eggID' => array(9048, 'Miyabi Ningyo Egg'),
                'loyal' => 'INT +2<br>Giảm 5% thời gian thi triển kỹ năng (VTC).',
                'cordial' => 'INT +1<br>Giảm 3% thời gian thi triển kỹ năng (VTC).',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1180,
                'name' => 'Nine Tail',
                'food' => array(25231, 'Suspicious Bottle'),
                'taming' => array(23187, 'Sap Liquid'),
                'evolution' => array(
                    array(
                        'targetID' => 1307, // Cat o' Nine Tails
                        'items' => array(
                            array('itemID' => 23187, 'itemName' => 'Sap Liquid', 'amount' => 3),
                            array('itemID' => 1022, 'itemName' => 'Nine Tails', 'amount' => 999),
                            array('itemID' => 10008, 'itemName' => 'Punisher', 'amount' => 1),
                            array('itemID' => 4159, 'itemName' => 'Nine Tail Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_nine_tail.jpg',
                'eggID' => array(9095, 'Nine Tails Egg'),
                'cordial' => 'HIT +2<br>CRIT +2',
                'loyal' => 'HIT +2<br>CRIT +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1307,
                'name' => 'Cat o\' Nine Tails',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Nine Tail',
                /*'evolution' => array(
                    array(
                        'targetID' => 1150, // Moonlight Flower
                        'items' => array(
                            array('itemID' => 25375, 'itemName' => 'Mighty Soul Essence', 'amount' => 30),
                            array('itemID' => 4159, 'itemName' => 'Nine Tail Card', 'amount' => 10),
                            array('itemID' => 4100, 'itemName' => 'Sohee Card', 'amount' => 10),
                            array('itemID' => 4090, 'itemName' => 'Munak Card', 'amount' => 10),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_cat_o_nine_tail.jpg',
                'eggID' => array(9096, 'Cat O Nine Tails Egg'),
                'shy' => 'HIT +2<br>CRIT +2',
                'neutral' => 'HIT +3<br>CRIT +3',
                'cordial' => 'HIT +4<br>CIRT +4<br>Khi tấn công vật lý có tỉ lệ hồi phục 300 HP trong 5 giây.',
                'loyal' => 'HIT +5<Br>CRIT +5<br>Khi tấn công vật lý có tỉ lệ hồi phục 400 HP trong 5 giây.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1150,
                'name' => 'Moonlight Flower',
                'food' => array(25377, 'Luxurious Pet Food'),
                'taming' => 'Tiến hóa từ Cat o\' Nine Tails',
                'img' => get_template_directory_uri() . '/images/pet/pet_moonlight.jpg',
                'eggID' => array(9112, 'Moonlight Flower Egg'),
                'shy' => 'HIT +3<br>CRIT +3',
                'neutral' => 'HIT +4<br>CRIT +4<br>Khi tấn công vật lý có tỉ lệ hồi phục 300 HP trong 5 giây.',
                'cordial' => 'HIT +5<br>CRIT +5<br>Khi tấn công vật lý có tỉ lệ hồi phục 400 HP và 10 SP trong 5 giây.',
                'loyal' => 'HIT +6<br>CRIT +6<br>Khi tấn công vật lý có tỉ lệ hồi phục 500 HP và 20 SP trong 5 giây.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            /*array (
                'mobID' => 20619,
                'name' => 'Gloom Under Night',
                'food' => array(25377, 'Luxurious Pet Food'),
                'taming' => 'Tiến hóa từ Hodremlin',
                'img' => get_template_directory_uri() . '/images/pet/pet_gloomundernight2.jpg',
                'eggID' => array(9122, 'Gloom Under Night Egg'),
                'shy' => 'MATK +10',
                'neutral' => 'MATK +20',
                'cordial' => 'MATK +30<br>Tăng 5% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>.',
                'loyal' => 'MATK +40<br>Tăng 7% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            /*array (
                'mobID' => 20928,
                'name' => 'Chimera Theone',
                'food' => array(25377, 'Luxurious Pet Food'),
                'taming' => 'Tiến hóa từ Hodremlin',
                'img' => get_template_directory_uri() . '/images/pet/pet_chimera_theone.jpg',
                'eggID' => array(9165, 'Chimera Theone Egg'),
                'shy' => 'MATK +10',
                'neutral' => 'MATK +20',
                'cordial' => 'MATK +30<br>Tăng 3% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>, <span style="color: #33CC00;">Gió</span>, <span style="color: #CC5500;">Đất</span>, <span style="color: #0000BB;">Nước</span>.',
                'loyal' => 'MATK +50<br>Tăng 5% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>, <span style="color: #33CC00;">Gió</span>, <span style="color: #CC5500;">Đất</span>, <span style="color: #0000BB;">Nước</span>.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            /*array (
                'mobID' => 1041,
                'name' => 'Mummy',
                'food' => array(934, 'Memento'),
                'taming' => array(23256, 'Elixir Bandages'),
                'evolution' => array(
                    array(
                        'targetID' => 1297, // Ancient Mummy
                        'items' => array(
                            array('itemID' => 23256, 'itemName' => 'Elixir Bandage', 'amount' => 3),
                            array('itemID' => 7511, 'itemName' => 'Rune of Darkness', 'amount' => 200),
                            array('itemID' => 969, 'itemName' => 'Gold', 'amount' => 30),
                            array('itemID' => 4248, 'itemName' => 'Ancient Mummy Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_mummy.jpg',
                'eggID' => array(9102, 'Mummy Egg'),
                'cordial' => 'HIT +4',
                'loyal' => 'HIT +5',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 1297,
                'name' => 'Ancient Mummy',
                'food' => array(537, 'Pet Food',),
                'taming' => 'Tiến hóa từ Mummy',
                'img' => get_template_directory_uri() . '/images/pet/pet_ancient_mummy.jpg',
                'eggID' => array(9107, 'Ancient Mummy Egg'),
                'shy' => 'HIT +4',
                'neutral' => 'HIT +5',
                'cordial' => 'HIT +6<br>Tăng 3% sát thương vật lý và phép thuật lên quái vật chủng loài Rồng (Dragon).',
                'loyal' => 'HIT +6<br>Tăng 6% sát thương vật lý và phép thuật lên quái vật chủng loài Rồng (Dragon).',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1023,
                'name' => 'Orc Warrior',
                'food' => array(537, 'Pet Food'),
                'taming' => array (635, 'Orc Trophy'),
                'equip' => array(10009, 'Wild Flower'),
                'evolution' => array(
                    array(
                        'targetID' => 1213, // High Orc
                        'items' => array(
                            array('itemID' => 635, 'itemName' => 'Orc Trophy', 'amount' => 3),
                            array('itemID' => 1124, 'itemName' => 'Orcish Sword', 'amount' => 1),
                            array('itemID' => 931, 'itemName' => 'Orcish Voucher', 'amount' => 500),
                            array('itemID' => 2267, 'itemName' => 'Cigarette', 'amount' => 1),
                            array('itemID' => 4066, 'itemName' => 'Orc Warrior Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_ork_warrior.jpg',
                'eggID' => array(9017, 'Orc Warrior Egg'),
                'support' => 'Có tỉ lệ 10% làm tăng sát thương gây được thêm 100.',
                'loyal' => 'ATK +15',
                'cordial' => 'ATK +10',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1213,
                'name' => 'High Orc',
                'food' => array(537, 'Pet Food',),
                'taming' => 'Tiến hóa từ Orc Warrior',
                /*'evolution' => array(
                    array(
                        'targetID' => 20571, // High Orc
                        'items' => array(
                            array('itemID' => 25375, 'itemName' => 'Mighty Soul Essence', 'amount' => 30),
                            array('itemID' => 4066, 'itemName' => 'Orc Warrior Card', 'amount' => 10),
                            array('itemID' => 4375, 'itemName' => 'Orc Baby Card', 'amount' => 10),
                            array('itemID' => 968, 'itemName' => 'Heroic Emblem', 'amount' => 10),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_high_orc.jpg',
                'eggID' => array(9087, 'High Orc Egg'),
                'shy' => 'ATK +10',
                'neutral' => 'ATK +15',
                'cordial' => 'ATK +20',
                'loyal' => 'ATK +25',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 20571,
                'name' => 'Orc Hero',
                'food' => array(25377, 'Luxury Pet Food',),
                'taming' => 'Tiến hóa từ High Orc',
                'img' => get_template_directory_uri() . '/images/pet/pet_ork_hero.jpg',
                'eggID' => array(9121, 'Orc Hero Egg'),
                'shy' => 'ATK +1%',
                'neutral' => 'ATK +2%',
                'cordial' => 'ATK +4%<br>Tăng 1% sát thương chí mạng.',
                'loyal' => 'ATK +7%<br>Tăng 3% sát thương chí mạng.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            array (
                'mobID' => 1040,
                'name' => 'Golem',
                'food' => array(6111, 'Mystic Stone'),
                'taming' => array (12371, 'Magical Lithography'),
                'equip' => array(10035, 'Spring'),
                'img' => get_template_directory_uri() . '/images/pet/pet_golem.jpg',
                'eggID' => array(9053, 'Golem Egg'),
                'loyal' => 'MaxHP +150',
                'cordial' => 'MaxHP +100',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1110,
                'name' => 'Dokebi',
                'food' => array(537, 'Pet Food'),
                'taming' => array(637, 'Old Broom'),
                'equip' => array(10005, 'Wig'),
                'evolution' => array(
                    array(
                        'targetID' => 1301, // Am Mut
                        'items' => array(
                            array('itemID' => 637, 'itemName' => 'Old Broom', 'amount' => 3),
                            array('itemID' => 981, 'itemName' => 'Violet Dyestuffs', 'amount' => 3),
                            array('itemID' => 1021, 'itemName' => 'Dokebi Horn', 'amount' => 300),
                            array('itemID' => 969, 'itemName' => 'Gold', 'amount' => 3),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_dokebi.jpg',
                'eggID' => array(9019, 'Dokebi Egg'),
                'support' => 'Có tỉ lệ 10% sử dụng kỹ năng Hammer Fall cấp độ 1.',
                'loyal' => 'MATK +2%',
                'cordial' => 'MATK +1%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1301,
                'name' => 'Am Mut',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Dokebi',
                'img' => get_template_directory_uri() . '/images/pet/pet_am_mut.jpg',
                'eggID' => array(9089, 'Am Mut Egg'),
                'shy' => 'MATK +1%',
                'neutral' => 'MATK +2%',
                'cordial' => 'MATK +3%',
                'loyal' => 'MATK +4%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1101,
                'name' => 'Bapho Jr.',
                'food' => array(518, 'Honey'),
                'taming' => array(642, 'Book of the Devil'),
                'equip' => array(10001, 'Skull Helm'),
                'img' => get_template_directory_uri() . '/images/pet/pet_baphomet.jpg',
                'eggID' => array(9024, 'Bapho Jr. Egg'),
                'loyal' => 'DEF +2<br>MDEF +2<br>Kháng 2% trạng thái Choáng (Stun).',
                'cordial' => 'DEF +1<br>MDEF +1<br>Kháng 1% trạng thái Choáng (Stun).',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1109,
                'name' => 'Deviruchi',
                'food' => array(711, 'Shoot'),
                'taming' => array(641, 'Contract in Shadow'),
                'equip' => array(10004, 'Pacifier'),
                'evolution' => array(
                    array(
                        'targetID' => 3669, // Diabolic
                        'items' => array(
                            array('itemID' => 641, 'itemName' => 'Contract in Shadow', 'amount' => 3),
                            array('itemID' => 1039, 'itemName' => 'Little Evil Wing', 'amount' => 250),
                            array('itemID' => 1009, 'itemName' => 'Hand of God', 'amount' => 30),
                            array('itemID' => 4122, 'itemName' => 'Deviruchi Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_deviruchi.jpg',
                'eggID' => array(9023, 'Deviruchi Egg'),
                'loyal' => 'MaxHP +1%<br>MaxSP + 1%<br>MATK +1%<br>ATK +1%',
                'cordial' => 'MATK +1%<br>ATK +1%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3669,
                'name' => 'Diabolic',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Deviruchi',
                'img' => get_template_directory_uri() . '/images/pet/pet_diabolic2.jpg',
                'eggID' => array(9097, 'Diabolic Egg'),
                'shy' => 'ATK +1%<br>MATK +1%',
                'neutral' => 'ATK +1%<br>MATK +1%<br>MaxHP +1%<br>MaxSP +1%',
                'cordial' => 'ATK +2%<br>MATK +2%<br>MaxHP +1%<br>MaxSP +1%<br>Có tỉ lệ tự động dùng kỹ năng Fire Bolt cấp độ 3 khi tấn công.',
                'loyal' => 'ATK +2%<br>MATK +2%<br>MaxHP +2%<br>MaxSP +2%<br>Có tỉ lệ tự động dùng kỹ năng Fire Bolt cấp độ 3 khi tấn công.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1401,
                'name' => 'Shinobi',
                'food' => array(6099, 'Grilled Rice Cake'),
                'taming' => array(12362, 'Kuloren'),
                'equip' => array(10026, 'Tassel for Durumagi'),
                'img' => get_template_directory_uri() . '/images/pet/pet_shinobi.jpg',
                'eggID' => array(9044, 'Shinobi Egg'),
                'loyal' => 'AGI +3',
                'cordial' => 'AGI +2',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1299,
                'name' => 'Goblin Leader',
                'food' => array(6104, 'Big Cell'),
                'taming' => array(12364, 'Staff of Leader'),
                'equip' => array(10028, 'Beautiful Badges'),
                'img' => get_template_directory_uri() . '/images/pet/pet_goblin_leader.jpg',
                'eggID' => array(9046, 'Goblin Leader Egg'),
                'loyal' => 'Tăng 5% sát thương vật lý lên Demi-Human.',
                'cordial' => 'Tăng 3% sát thương vật lý lên Demi-Human.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1245,
                'name' => 'Christmas Goblin',
                'food' => array(911, 'Scell'),
                'taming' => array(12225, 'Sweet Candy Cane'),
                'img' => get_template_directory_uri() . '/images/pet/pet_gobline_xmas.jpg',
                'eggID' => array(9029, 'X-mas Goblin Egg'),
                'loyal' => 'MaxHP +45<br>Giảm 2% sát thương nhận từ thuộc tính <span style="color: #0000BB;">Nước</span>.',
                'cordial' => 'MaxHP +30<br>Giảm 1% sát thương nhận từ thuộc tính <span style="color: #0000BB;">Nước</span>.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1122,
                'name' => 'Knife Goblin',
                'food' => array(7821, 'Green Apple'),
                'taming' => array(14569, 'Knife Goblin Ring'),
                'img' => get_template_directory_uri() . '/images/pet/pet_goblin_1.jpg',
                'eggID' => array(9032, 'Knife Goblin Egg'),
                'support' => 'Có tỉ lệ sử dụng kỹ năng Wind Attack lên kẻ thủ.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1123,
                'name' => 'Flail Goblin',
                'food' => array(7821, 'Green Apple'),
                'taming' => array(14570, "Flail Goblin Ring"),
                'img' => get_template_directory_uri() . '/images/pet/pet_goblin_2.jpg',
                'eggID' => array(9033, 'Flail Goblin Egg'),
                'support' => 'Có tỉ lệ sử dụng kỹ năng Fire Attack lên kẻ thủ.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1125,
                'name' => 'Hammer Goblin',
                'food' => array(7821, 'Green Apple'),
                'taming' => array(14571, "Hammer Goblin Ring"),
                'img' => get_template_directory_uri() . '/images/pet/pet_goblin_4.jpg',
                'eggID' => array(9034, 'Hammer Goblin Egg'),
                'support' => 'Có tỉ lệ sử dụng kỹ năng Ground Attack lên kẻ thủ.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1782,
                'name' => 'Roween',
                'food' => array (7564, 'Rotten Meat'),
                'taming' => 'Abracadabra',
                'img' => get_template_directory_uri() . '/images/pet/pet_roween.jpg',
                'eggID' => array(9104, 'Roween Egg'),
                'cordial' => 'Tăng 2% sát thương vật lý lên quái vật hệ <span style="color: #33CC00;">Gió</span>.',
                'loyal' => 'Tăng 3% sát thương vật lý lên quái vật hệ <span style="color: #33CC00;">Gió</span>.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1148,
                'name' => 'Medusa',
                'food' => array (6108, 'Apple Pudding'),
                'taming' => array(12368, 'Splendid Mirror'),
                'equip' => array(10032, 'Queen\'s Coronet'),
                'img' => get_template_directory_uri() . '/images/pet/pet_medusa.jpg',
                'eggID' => array(9050, 'Medusa Egg'),
                'loyal' => 'VIT + 2<br>Kháng 8% trạng thái Hóa đá (Stone Cursed).',
                'cordial' => 'VIT + 1<br>Kháng 5% trạng thái Hóa đá (Stone Cursed).',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1010,
                'name' => 'Willow',
                'food' => array (1066, 'Fine-grained Trunk'),
                'taming' => 'Abracadabra',
                'img' => get_template_directory_uri() . '/images/pet/pet_wilow.jpg',
                'eggID' => array(9103, 'Willow Egg'),
                'loyal' => 'VIT +1<br>Kháng 50% trạng thái Hóa đá (Stone Cursed).',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1208,
                'name' => 'Wanderer',
                'food' => array(7824, 'Spirit Liquor'),
                'taming' => array(14574, "Vagabond's Skull"),
                /*'evolution' => array(
                    array(
                        'targetID' => 20420, // Contaminated Wanderer
                        'items' => array(
                            array('itemID' => 7005, 'itemName' => 'Skull', 'amount' => 500),
                            array('itemID' => 1009, 'itemName' => 'Hand of God', 'amount' => 50),
                            array('itemID' => 1059, 'itemName' => 'Fabric', 'amount' => 100),
                            array('itemID' => 4210, 'itemName' => 'Wanderer Card', 'amount' => 1),
                        )
                    )
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_wander_man.jpg',
                'eggID' => array(9037, 'Wanderer Egg'),
//                'support' => 'Có tỉ lệ sử dụng kỹ năng Under Attack lên kẻ thù.',
                'loyal' => 'AGI +4',
                'cordial' => 'AGI +3',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 20420,
                'name' => 'Contaminated Wanderer',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Wanderer',
                'img' => get_template_directory_uri() . '/images/pet/pet_wander_man_h.jpg',
                'eggID' => array(9117, 'Contaminated Wanderer Egg'),
                'loyal' => 'AGI +4<br>CRIT 3+<br>Tăng 7% sát thương chí mạng.',
                'cordial' => 'AGI +4<br>CRIT +2<br>Tăng 5% sát thương chí mạng.',
                'neutral' => 'AGI +4<br>CRIT +1',
                'shy' => 'AGI +4',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1179,
                'name' => 'Whisper',
                'food' => array(6100, 'Damp Darkness'),
                'taming' => array(12363, 'Ghost Coffin'),
                'equip' => array(10027, 'Pet Soul Ring'),
                'img' => get_template_directory_uri() . '/images/pet/pet_whisper.jpg',
                'eggID' => array(9045, 'Whisper Egg'),
                'loyal' => 'Flee +10<br>Cho phép sử dụng kỹ năng Hiding cấp độ 1.',
                'cordial' => 'Flee +7<br>Cho phép sử dụng kỹ năng Hiding cấp độ 1.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1622,
                'name' => 'Teddy Bear',
                'food' => array(25233, 'Cotton Tufts'),
                'taming' => array(23189, 'Small Needle Kit'),
                /*'evolution' => array(
                    array(
                        'targetID' => 2995, // Abandoned Teddy Bear
                        'items' => array(
                            array('itemID' => 23189, 'itemName' => 'Small Doll Needle', 'amount' => 3),
                            array('itemID' => 7442, 'itemName' => 'Cursed Seal', 'amount' => 300),
                            array('itemID' => 724, 'itemName' => 'Cursed Ruby', 'amount' => 50),
                            array('itemID' => 4340, 'itemName' => 'Teddy Bear Card', 'amount' => 1),
                        )
                    ),
                    array(
                        'targetID' => 1736, // Aliot
                        'items' => array(
                            array('itemID' => 7317, 'itemName' => 'Rusty Screw', 'amount' => 500),
                            array('itemID' => 518, 'itemName' => 'Honey', 'amount' => 100),
                            array('itemID' => 727, 'itemName' => 'Opal', 'amount' => 10),
                            array('itemID' => 4340, 'itemName' => 'Teddy Bear Card', 'amount' => 1),
                        )
                    ),
                    array(
                        'targetID' => 1735, // Alicel
                        'items' => array(
                            array('itemID' => 7317, 'itemName' => 'Rusty Screw', 'amount' => 500),
                            array('itemID' => 518, 'itemName' => 'Honey', 'amount' => 100),
                            array('itemID' => 7449, 'itemName' => 'Bloody Page', 'amount' => 50),
                            array('itemID' => 4340, 'itemName' => 'Teddy Bear Card', 'amount' => 1),
                        )
                    ),
                ),*/
                'img' => get_template_directory_uri() . '/images/pet/pet_teddy_bear.jpg',
                'eggID' => array(9099, 'Teddy Bear Egg'),
                'loyal' => 'MaxSP +150',
                'cordial' => 'MaxSP +100',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 2995,
                'name' => 'Abandoned Teddy Bear',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Teddy Bear',
                'img' => get_template_directory_uri() . '/images/pet/pet_xm_teddy_bear.jpg',
                'eggID' => array(9108, 'Abandoned Teddy Bear Egg'),
                'loyal' => 'MaxSP +150<br>Có tỉ lệ hồi phục 40 SP mỗi giây trong 5 giây khi gây sát thương phép thuật.',
                'cordial' => 'MaxSP +150<br>Có tỉ lệ hồi phục 30 SP mỗi giây trong 5 giây khi gây sát thương phép thuật.',
                'neutral' => 'MaxSP +100',
                'shy' => 'MaxSP +50',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 1736,
                'name' => 'Aliot',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Teddy Bear',
                'img' => get_template_directory_uri() . '/images/pet/pet_aliot.jpg',
                'eggID' => array(9118, 'Aliot Egg'),
                'evolution' => array(
                    array(
                        'targetID' => 21290, // Kiel D-01
                        'items' => array(
                            array('itemID' => 4401, 'itemName' => 'Alicel Card', 'amount' => 10),
                            array('itemID' => 4400, 'itemName' => 'Aliza Card', 'amount' => 10),
                            array('itemID' => 4402, 'itemName' => 'Aliot Card', 'amount' => 10),
                            array('itemID' => 25375, 'itemName' => 'Powerful Soul Essence', 'amount' => 30),
                        )
                    ),
                ),
                'shy' => 'ASPD +2%',
                'neutral' => 'ASPD +3%<br>HIT +6',
                'cordial' => 'ASPD +4%<br>HIT +9',
                'loyal' => 'ASPD +5%<br>HIT +12',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 1735,
                'name' => 'Alicel',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Teddy Bear',
                'img' => get_template_directory_uri() . '/images/pet/pet_alicel.jpg',
                'eggID' => array(9119, 'Alicel Egg'),
                'shy' => 'Giảm 2% thời gian thi triển kỹ năng.',
                'neutral' => 'Giảm 3% thời gian thi triển kỹ năng.',
                'cordial' => 'Giảm 4% thời gian thi triển kỹ năng.',
                'loyal' => 'Giảm 5% thời gian thi triển kỹ năng.<br>Giảm 5% sát thương phép thuật nhận từ mục tiêu có thuộc tính <span style="color: #777777">Trung tính.</span>',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 21290,
                'name' => 'Kiel D-01',
                'food' => array(25377, 'Luxurious Pet Food'),
                'taming' => 'Tiến hóa từ Aliot',
                'img' => get_template_directory_uri() . '/images/pet/pet_kiel.jpg',
                'eggID' => array(9126, 'Kiel D-01 Egg'),
                'shy' => 'ASPD +4%<br>HIT +9',
                'neutral' => 'ASPD +5%<br>HIT +12<br>Tăng 1% sát thương cận chiến.',
                'cordial' => 'ASPD +6%<br>HIT +15<br>Tăng 3% sát thương cận chiến.',
                'loyal' => 'ASPD +7%<br>HIT +18<br>Tăng 5% sát thương cận chiến.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            array (
                'mobID' => 1632,
                'name' => 'Gremlin',
                'food' => array(537, 'Pet Food'),
                'taming' => array(23188, 'Unprocessed Parts'),
                'evolution' => array(
                    array(
                        'targetID' => 1773, // Hodremlin
                        'items' => array(
                            array('itemID' => 23188, 'itemName' => 'Broken Part', 'amount' => 3),
                            array('itemID' => 6100, 'itemName' => 'Damp Darkness', 'amount' => 50),
                            array('itemID' => 7340, 'itemName' => 'Will of the Darkness', 'amount' => 1),
                            array('itemID' => 4413, 'itemName' => 'Hodremlin Card', 'amount' => 1),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_gremlin.jpg',
                'eggID' => array(9100, 'Gremlin Egg'),
                'shy' => 'MaxSP +50',
                'neutral' => 'MaxSP +100',
                'cordial' => 'MaxSP +150<br>Khi tấn công phép thuật có tỉ lệ phục hồi 30 SP mỗi giây trong 5 giây.',
                'loyal' => 'MaxSP +150<br>Khi tấn công phép thuật có tỉ lệ phục hồi 40 SP mỗi giây trong 5 giây.',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1773,
                'name' => 'Hodremlin',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Tiến hóa từ Gremlin',
                'evolution' => array(
                    array(
                        'targetID' => 20619, // Gloom Under Night
                        'items' => array(
                            array('itemID' => 4413, 'itemName' => 'Hodremlin Card', 'amount' => 10),
                            array('itemID' => 4412, 'itemName' => 'Isilla Card', 'amount' => 10),
                            array('itemID' => 4409, 'itemName' => 'Agav Card', 'amount' => 10),
                            array('itemID' => 25375, 'itemName' => 'Powerful Soul Essence', 'amount' => 10),
                        )
                    ),
                    array(
                        'targetID' => 20928, // Chimera Theone
                        'items' => array(
                            array('itemID' => 6965, 'itemName' => 'Fire Property Blueprint', 'amount' => 10),
                            array('itemID' => 6966, 'itemName' => 'Water Property Blueprint', 'amount' => 10),
                            array('itemID' => 6967, 'itemName' => 'Earth Property Blueprint', 'amount' => 10),
                            array('itemID' => 6968, 'itemName' => 'Wind Property Blueprint', 'amount' => 10),
                            array('itemID' => 25375, 'itemName' => 'Powerful Soul Essence', 'amount' => 200),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_hodremlin.jpg',
                'eggID' => array(9105, 'Hodremlin Egg'),
                'shy' => 'DEX +1<br>HIT +1',
                'neutral' => 'DEX +2<br>HIT +1',
                'cordial' => 'DEX +2<br>HIT +2<br>+7% sát thương chí mạng.',
                'loyal' => 'DEX +2<br>HIT +2<br>+9% sát thương chí mạng.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1504,
                'name' => 'Dullahan',
                'food' => array(6107, 'Sunset on the Rock'),
                'taming' => array(12367, 'Luxury Whisky Bottle'),
                'equip' => array(10031, 'Ring Of Death'),
                'img' => get_template_directory_uri() . '/images/pet/pet_dullahan.jpg',
                'eggID' => array(9049, 'Dullahan Egg'),
                'loyal' => '+8% sát thương chí mạng.',
                'cordial' => '+5% sát thương chí mạng.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1379,
                'name' => 'Nightmare Terror',
                'food' => array(6112, 'Fresh Grass'),
                'taming' => array(12372, 'Hell Contract'),
                'equip' => array(10036, 'Horn Of Hell'),
                'img' => get_template_directory_uri() . '/images/pet/pet_nightmare_terror.jpg',
                'eggID' => array(9054, 'Nightmare Terror Egg'),
                'loyal' => 'INT +1<br>Kháng 100% trạng thái Ngủ (Sleep).',
                'cordial' => 'Kháng 100% trạng thái ngủ (Sleep).',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1837,
                'name' => 'Fire Imp',
                'food' => array(6114, 'Flame Gemstone'),
                'taming' => array(12374, 'Ice Fireworks'),
                'equip' => array(10038, 'Horn Barrier'),
                'img' => get_template_directory_uri() . '/images/pet/pet_imp.jpg',
                'eggID' => array(9056, 'Imp Egg'),
                'loyal' => 'Kháng 3% sát thương đòn dánh thuộc tính Lửa.<br>Tăng 2% sát thương vật lý lên quái vật thuộc tính Lửa.',
                'cordial' => 'Kháng 2% sát thương đòn dánh thuộc tính Lửa.<br>Tăng 1% sát thương vật lý lên quái vật thuộc tính Lửa.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 2313,
                'name' => 'Tikbalang',
                'food' => array(528, 'Monster\'s Feed'),
                'taming' => array(12699, 'Tikbalang Harness'),
                'img' => get_template_directory_uri() . '/images/pet/pet_tikbalang.jpg',
                'eggID' => array(9059, 'Tikbalang Egg'),
                'loyal' => 'MDEF +5<br>Tăng 10% sát thương vật lý lên quái vật Bakonawa, Bangungot và Buwaya.',
                'cordial' => 'MDEF +3<br>Tăng 10% sát thương vật lý lên quái vật Bakonawa, Bangungot và Buwaya.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1374,
                'name' => 'Incubus',
                'food' => array(6110, 'Yellow Vital Flower'),
                'taming' => array(12370, 'Girl\'s Naivety'),
                'equip' => array(10034, 'Masked Ball'),
                'img' => get_template_directory_uri() . '/images/pet/pet_incubus.jpg',
                'eggID' => array(9052, 'Incubus Egg'),
                'loyal' => 'MaxSP +5%<br>Có tỉ lệ 3% chuyển hóa 1% sát thương gây được thành SP.',
                'cordial' => 'MaxSP +3%<br>Có tỉ lệ 3% chuyển hóa 1% sát thương gây được thành SP.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1370,
                'name' => 'Succubus',
                'food' => array(6113, 'Blue Vital Flower'),
                'taming' => array(12373, 'Boy\'s Pure Heart'),
                'equip' => array(10037, 'Black Butterfly Mask'),
                'img' => get_template_directory_uri() . '/images/pet/pet_succubus.jpg',
                'eggID' => array(9055, 'Succubus Egg'),
                'loyal' => 'MaxHP +1%<br>Có tỉ lệ 3% chuyển hóa 5% sát thương gây được thành HP.',
                'cordial' => 'Có tỉ lệ 3% chuyển hóa 5% sát thương từ đòn đánh thành HP.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1200,
                'name' => 'Zealotus',
                'food' => array(929, 'Immortal Heart'),
                'taming' => array(660, 'Forbidden Red Candle'),
                'img' => get_template_directory_uri() . '/images/pet/pet_zherlthsh.jpg',
                'eggID' => array(9026, 'Zealotus Egg'),
                'loyal' => 'Tăng 3% sát thương vật lý và phép thuật lên Demi-human.',
                'cordial' => 'Tăng 2% sát thương vật lý và phép thuật lên Demi-human.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1513,
                'name' => 'Mao Guai',
                'food' => array(6095, 'Flavored Alcohol'),
                'taming' => array(12358, 'Fan of Wind'),
                'equip' => array(10022, 'Golden Earring'),
                'img' => get_template_directory_uri() . '/images/pet/pet_civil_servant.jpg',
                'eggID' => array(9040, 'Civil Servant Egg'),
                'loyal' => 'MaxSP +15',
                'cordial' => 'MaxSP +10',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 1519,
                'name' => 'Green Maiden',
                'food' => array(6115, 'Bun'),
                'taming' => array(12395, 'Tantan Noodle'),
                'img' => get_template_directory_uri() . '/images/pet/pet_chung_e.jpg',
                'eggID' => array(9030, 'Green Maiden Egg'),
                'loyal' => 'DEF +2.<br>Giảm 2% sát thương nhận từ Demi-human',
                'cordial' => 'DEF +1.<br>Giảm 1% sát thương nhận từ Demi-human',
                'note' => 'Thuần hóa từ đệ tử của trùm White Lady (Bacsojin).',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 1963,
                'name' => 'New Year Doll',
                'food' => array(554, 'Mochi'),
                'taming' => 'Event',
                'img' => get_template_directory_uri() . '/images/pet/pet_p_chung_e.jpg',
                'eggID' => array(9038, 'New Year Doll Egg'),
                'support' => 'Có tỉ lệ tự dùng kỹ năng Shield Charge lên kẻ thủ.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 1815,
                'name' => 'Rice Cake',
                'food' => array(511, 'Green Herb'),
                'taming' => 'Event',
                'img' => get_template_directory_uri() . '/images/pet/pet_event_ricecake.jpg',
                'eggID' => array(9028, 'Hard Rice Cake Egg'),
                'loyal' => 'Kháng 2% sát thương đòn đánh thuộc tính Trung tính.',
                'cordial' => 'Kháng 1% sát thương đòn đánh thuộc tính Trung tính.',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 2210,
                'name' => 'Christmas Snow Rabbit',
                'food' => array(529, 'Candy'),
                'taming' => 'Event',
                'img' => get_template_directory_uri() . '/images/pet/pet_lunatic.jpg',
                'eggID' => array(9058, 'Christmas Snow Rabbit Egg'),
                'loyal' => 'MATK +2%',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3731,
                'name' => 'Scatleton',
                'food' => array(579, 'Fresh Fish'),
                'taming' => 'Halloween Event',
                'equip' => array(10040, 'Red Drop Necklace'),
                'evolution' => array(
                    array(
                        'targetID' => 3971, // Skelion
                        'items' => array(
                            array('itemID' => 25408, 'itemName' => 'Memories of Gyoll', 'amount' => 2),
                            array('itemID' => 11616, 'itemName' => 'Yummy Meat', 'amount' => 100),
                            array('itemID' => 11605, 'itemName' => 'Cookie Bat', 'amount' => 100),
                        )
                    )
                ),
                'img' => get_template_directory_uri() . '/images/pet/pet_scatleton.jpg',
                'eggID' => array(9101, 'Scatleton Crate'),
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3971,
                'name' => 'Skelion',
                'food' => array(11616, 'Delicious Pork'),
                'taming' => 'Tiến hóa từ Scatleton',
                'equip' => array(10042, 'Dark Mane'),
                'img' => get_template_directory_uri() . '/images/pet/pet_skelion.jpg',
                'eggID' => array(9113, 'Roost of Skelion'),
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 21089,
                'name' => 'Wandering Duck Doll',
                'food' => array(25233, 'Cotton Tufts'),
                'equip' => array(10045, 'Twinkle Star Button'),
                'taming' => array(9897, 'Shiny Doll Button'),
                'img' => get_template_directory_uri() . '/images/pet/pet_duckling.jpg',
                'eggID' => array(9125, 'Wandering Duck Doll Egg'),
                'loyal' => 'Chính xác hoàn hảo +10',
                'cordial' => 'Chính xác hoàn hảo +5',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 3162,
                'name' => 'Elephant',
                'food' => array(6762, 'Banana Can﻿'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_elephant.jpg',
                'eggID' => array(9110, 'Elephant Egg'),
                'shy' => 'DEX +1',
                'loyal' => 'DEX +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3163,
                'name' => 'Gorilla',
                'food' => array(6763, 'Spicy Rice Cake'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_gorilla.jpg',
                'eggID' => array(9065, 'Gorilla Egg'),
                'shy' => 'STR +1',
                'loyal' => 'Base ATK +5',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3164,
                'name' => 'Lion',
                'food' => array(6764, 'Hot Dog'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_lion.jpg',
                'eggID' => array(9066, 'Lion Egg'),
                'shy' => 'MaxSP +10',
                'loyal' => 'INT +3',
                'show' => true,
                'expand' => false
            ),
            array (
                'mobID' => 3165,
                'name' => 'Rhino',
                'food' => array(6765, 'Ferris Wheel Biscuit'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_rhino.jpg',
                'eggID' => array(9067, 'Rhino Egg'),
                'shy' => 'MaxHP +100',
                'loyal' => 'VIT +3',
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 2336,
                'name' => 'Domovoi',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_domovoi.jpg',
                'eggID' => array(9060, 'Domovoi Egg'),
                'loyal' => 'Tăng 1% sát thương vật lý và phép thuật lên Demi-human.<br>Giảm 1% sát thương từ Demi-human.',
                'show' => true,
                'expand' => false
            ),*/
            array (
                'mobID' => 2200,
                'name' => 'Taini',
                'food' => array(512, 'Apple'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_j_taini.jpg',
                'eggID' => array(9057, 'Tiny Egg'),
                'show' => true,
                'expand' => false
            ),
            /*array (
                'mobID' => 3261,
                'name' => 'Blue Unicorn',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_blue_unicorn.jpg',
                'eggID' => array(9068, 'Blue Unicorn Egg'),
                'loyal' => 'Tăng 10% tốc độ di chuyển',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 20425,
                'name' => 'Phreeoni',
                'food' => array(25377, 'Luxurious Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_phreeoni.jpg',
                'eggID' => array(9111, 'Phreeoni Egg'),
                'shy' => 'HIT +6',
                'neutral' => 'HIT +10<br>Có tỉ lệ 5% kích hoạt hiệu ứng Chính xác hoàn hảo +20.',
                'cordial' => 'HIT +14<br>Có tỉ lệ 10% kích hoạt hiệu ứng Chính xác hoàn hảo +20.',
                'loyal' => 'HIT +18<br>Có tỉ lệ 15% kích hoạt hiệu ứng Chính xác hoàn hảo +20.',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            /*array (
                'mobID' => 30105,
                'name' => 'Charleston Mini',
                'food' => array(7317, 'Rusty Screw'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_charleston_sm.jpg',
                'eggID' => array(9154, 'Charleston Mini Egg'),
                'shy' => 'Độ mang vác +500.',
                'neutral' => 'Độ mang vác +1000.',
                'cordial' => 'Độ mang vác +1500.',
                'loyal' => 'Độ mang vác +2000.<br>ATK +5%<br>MATK +5%',
                'show' => true,
                'expand' => false,
                'isMvP' => true
            ),*/
            /*array (
                'mobID' => 30107,
                'name' => 'Blossom Fairy',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_blossomfairy.jpg',
                'eggID' => array(9156, 'Blossom Fairy Egg'),
                'shy' => 'Các chỉ số chính +1.',
                'neutral' => 'Các chỉ số chính +1.',
                'cordial' => 'Các chỉ số chính +1.',
                'loyal' => 'Các chỉ số chính +1.<br>ATK +2%<br>MATK +2%',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 30107,
                'name' => 'Pink Mavka',
                'food' => array(707, 'Singing Plant'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_mavka_pnk.jpg',
                'eggID' => array(9155, 'Pink Mavka Egg'),
                'neutral' => 'Tăng 1% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>.<br>Tăng 1% sát thương phép thuật thuộc tính <span style="color: #CC5500;">Đất</span>.',
                'cordial' => 'Tăng 2% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>.<br>Tăng 2% sát thương phép thuật thuộc tính <span style="color: #CC5500;">Đất</span>.',
                'loyal' => 'Tăng 3% sát thương phép thuật thuộc tính <span style="color: #FF0000;">Lửa</span>.<br>Tăng 3% sát thương phép thuật thuộc tính <span style="color: #CC5500;">Đất</span>.<br>Tăng 3% hiệu quả hồi phục từ các kỹ năng hồi máu.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 1930,
                'name' => 'Piamette',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_piamette.jpg',
                'eggID' => array(9159, 'Piamette Egg'),
                'shy' => 'Độ mang vác +500.<br>ATK +1%<br>MATK +1%',
                'neutral' => 'Độ mang vác +500.<br>ATK +1%<br>MATK +1%',
                'cordial' => 'Độ mang vác +500.<br>ATK +1%<br>MATK +1%',
                'loyal' => 'Độ mang vác +1000.<br>Các chỉ số chính +1.<br>ATK +2%<br>MATK +2%',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3571,
                'name' => 'Armor Squirt',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_armor_squirt.jpg',
                'eggID' => array(9160, 'Armor Squirt Egg'),
                'shy' => 'Tăng 1% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>.',
                'neutral' => 'Tăng 2% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>.',
                'cordial' => 'Tăng 3% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>.',
                'loyal' => 'Tăng 4% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #FF0000;">Lửa</span>.<br>Các chỉ số chính +1.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3573,
                'name' => 'Chipmunk',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_chipmunk.jpg',
                'eggID' => array(9161, 'Chipmunk Egg'),
                'shy' => 'Tăng 1% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>.',
                'neutral' => 'Tăng 2% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>.',
                'cordial' => 'Tăng 3% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>.',
                'loyal' => 'Tăng 4% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #CC5500;">Đất</span>.<br>Các chỉ số chính +1.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3581,
                'name' => 'Torobbie',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_torobbie.jpg',
                'eggID' => array(9162, 'Torobbie Egg'),
                'shy' => 'Tăng 1% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>.',
                'neutral' => 'Tăng 2% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>.',
                'cordial' => 'Tăng 3% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>.',
                'loyal' => 'Tăng 4% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #33CC00;">Gió</span>.<br>Các chỉ số chính +1.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3551,
                'name' => 'Sppo',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_sppo.jpg',
                'eggID' => array(9163, 'Sppo Egg'),
                'shy' => 'Tăng 1% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>.',
                'neutral' => 'Tăng 2% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>.',
                'cordial' => 'Tăng 3% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>.',
                'loyal' => 'Tăng 4% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #777777;">Trung tính</span>.<br>Độ mang vác +500.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3582,
                'name' => 'Tottochi',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_tottochi.jpg',
                'eggID' => array(9164, 'Tottochi Egg'),
                'shy' => 'Tăng 1% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>.',
                'neutral' => 'Tăng 2% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>.',
                'cordial' => 'Tăng 3% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>.',
                'loyal' => 'Tăng 4% sát thương vật lý và phép thuật lên quái vật thuộc tính <span style="color: #0000BB;">Nước</span>.<br>Các chỉ số chính +1.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 30103,
                'name' => 'Ninja Cat',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_ninjacat.jpg',
                'eggID' => array(9152, 'Ninja Cat Egg'),
                'shy' => 'AGI +1',
                'neutral' => 'AGI +2',
                'cordial' => 'AGI +3<br>Flee +10',
                'loyal' => 'AGI +4<br>Flee +20<br>Tăng 5% tốc độ di chuyển (có cộng dồn).',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 30104,
                'name' => 'Sakura Cat',
                'food' => array(537, 'Pet Food'),
                'taming' => 'Mua từ cửa hàng thú cưng Pudo',
                'img' => get_template_directory_uri() . '/images/pet/pet_sakuracat.jpg',
                'eggID' => array(9153, 'Sakura Cat Egg'),
                'shy' => 'LUK +1',
                'neutral' => 'LUK +2',
                'cordial' => 'LUK +3<br>CRIT +3',
                'loyal' => 'LUK +4<br>CRIT +4<br>Tăng 4% sát thương chí mạng.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 2963,
                'name' => 'Woodie',
                'food' => array(6669, 'Emerald Leaf'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_woodie.jpg',
                'eggID' => array(9063, 'Chun Tree Egg'),
                'loyal' => 'LUK +2<br>MaxHP +150',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3306,
                'name' => 'Tama Dora',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_tamadora.jpg',
                'eggID' => array(9080, 'Tama Dora Egg'),
                'cordial' => 'Khi tấn công có tỉ lệ sử dụng kỹ năng Heal cấp độ 1.',
                'loyal' => 'Cho phép sử dụng kỹ năng Heal cấp độ 1.',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3322,
                'name' => 'Metal Dragon',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_metal_dragon.jpg',
                'eggID' => array(9081, 'Metal Dragon Egg'),
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3317,
                'name' => 'Rubylit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_rubylit.jpg',
                'eggID' => array(9074, 'Rubylit Egg'),
                'cordial' => 'Base ATK +10',
                'loyal' => 'Base ATK +20',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3318,
                'name' => 'Sapphilit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_sapphilit.jpg',
                'eggID' => array(9075, 'Sapphilit Egg'),
                'cordial' => 'MaxHP +100',
                'loyal' => 'MaxHP +200',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3319,
                'name' => 'Emelit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_emelit.jpg',
                'eggID' => array(9076, 'Emelit Egg'),
                'cordial' => 'MaxHP +50<br>MaxSP +25',
                'loyal' => 'MaxHP +100<br>MaxSP +50',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3320,
                'name' => 'Topalit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_topalit.jpg',
                'eggID' => array(9077, 'Topalit Egg'),
                'cordial' => 'MaxSP +50',
                'loyal' => 'MaxSP +100',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3321,
                'name' => 'Amelit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_amelit.jpg',
                'eggID' => array(9077, 'Amelit Egg'),
                'cordial' => 'MATK +10',
                'loyal' => 'MATK +20',
                'show' => true,
                'expand' => false
            ),*/
            /*array (
                'mobID' => 3349,
                'name' => 'Mythlit',
                'food' => array(537, 'Pet Food'),
                'taming' => '(Đang cập nhật...)',
                'img' => get_template_directory_uri() . '/images/pet/pet_mythlit.jpg',
                'eggID' => array(9079, 'Mythlit Egg'),
                'cordial' => 'Các chỉ số chính +1.',
                'loyal' => 'Các chỉ số chính +2.',
                'show' => true,
                'expand' => false
            ),*/
        ));
        die;
        break;
}
?>
<?php
/**
 * Template Name: Hệ thống & Tính năng
 */

$post_slug = $post->post_name;

if (file_exists ( locate_template('/he-thong-tinh-nang/page-' . $post_slug. '.php' ))){
	include locate_template('/he-thong-tinh-nang/page-' . $post_slug. '.php');
}
else {
	include locate_template('/page.php');
}
?>
<?php get_header('fluid'); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="container-fluid content-gap">
        <div class="row">
            <nav class="col-md-2 col-nav" id="myScrollspy">
                <ul class="nav nav-pills nav-pills-custom">
                    <li class="nav-item">
                        <a class="nav-link active" href="#cach-tao-nhan-vat">Cách tạo nhân vật</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#cach-choi-co-ban">Cơ bản</a>
                        <nav class="nav nav-pills nav-pills-child flex-column">
                            <a class="nav-link ml-3 my-1" href="#giao-dien">Giao diện</a>
                            <a class="nav-link ml-3 my-1" href="#hop-cong-cu">Hộp công cụ</a>
                            <a class="nav-link ml-3 my-1" href="#trang-thai">Trạng thái</a>
                            <a class="nav-link ml-3 my-1" href="#ky-nang">Kỹ năng</a>
                            <a class="nav-link ml-3 my-1" href="#hanh-trang">Hành trang</a>
                            <a class="nav-link ml-3 my-1" href="#trang-bi">Trang bị</a>
                        </nav>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#gameplay">Gameplay</a>
                        <nav class="nav nav-pills nav-pills-child flex-column">
                            <a class="nav-link ml-3 my-1" href="#chien-dau">Chiến đấu</a>
                            <a class="nav-link ml-3 my-1" href="#vat-pham-tu-quai-vat">Vật phẩm từ quái vật</a>
                            <a class="nav-link ml-3 my-1" href="#mua-ban-voi-npc">Mua bán với NPC</a>
                        </nav>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#phim-tat">Phím tắt</a>
                    </li>
                </ul>
            </nav>
            <div class="col-md-10 col-main">
                <div class="div-scroll">
                    <?php get_template_part('/parts/co-ban/1-cach-tao-nhan-vat'); ?>

	                <?php get_template_part('/parts/co-ban/2-cach-choi-co-ban'); ?>

	                <?php get_template_part('/parts/co-ban/3-gameplay'); ?>

                    <?php get_template_part('/parts/co-ban/4-phim-tat'); ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer('no-copyright'); ?>

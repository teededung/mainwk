<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="bg-colored">
        <div class="container content content-huong-dan-nhiem-vu">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Trang bị và vật phẩm</li>
            </ol>

            <h1 class="mb-5">Trang bị và vật phẩm</h1>

            <?php
            $args = array(
                'post_type' => 'page',
                'posts_per_page' => -1,
                'post_parent' => $post->ID
            );

            $parent = new WP_Query($args);
            ?>

            <?php if ($parent->have_posts()): ?>
                <div class="row">
                    <?php
                    while ($parent->have_posts()): $parent->the_post();
                        get_template_part('/parts/loop/loop-card');
                    endwhile;
                    ?>
                </div>
            <?php else: ?>
                <div class="alert alert-warning">
                    Hiện chưa có bài viết nào. <a href="<?php echo home_url('/'); ?>">Quay về trang chủ WIKI.</a>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

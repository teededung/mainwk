commit 3a5f74bcb89b071c6493ae71ba3de3647fd74ca4
Merge: 4de090f 5e82048
Author: David Anderson <david@updraftplus.com>
Date:   Wed Nov 8 11:37:03 2023 +0000

    Merge branch '1418-live-release-1-23-12' into 'master'
    
    Resolve "Live Release 1.23.12"
    
    Closes #1418
    
    See merge request team-updraft/updraftplus!1547

branch:

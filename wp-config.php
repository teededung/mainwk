<?php






/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', "tee_laniakea_wiki");

/** MySQL database username */
define('DB_USER', "root");

/** MySQL database password */
define('DB_PASSWORD', "");

/** MySQL hostname */
define('DB_HOST', "localhost");

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XS=opzZsVVS|>`K/b9`||[x$|<2ya6X~-@B<]}-QL<2#EjML#izwmN.^]i5|nbDc');
define('SECURE_AUTH_KEY',  'z,l| `8!G!}0Q_KFgWbi(F3L>hq&yCu~)6-HcOR&-Rj2-p1&HFK+)$XLY-hP s|M');
define('LOGGED_IN_KEY',    '{enaG`+P;fMZ0TN.+,<q-t[]v=H3P]1gp =n[5ZlH4c@>1ZN`0y),)c;XGSm;7W[');
define('NONCE_KEY',        '%~fQMC0HCQKc;B$-#R&*Sk+(|3{r7}|qks$lkq<y|deoZ{xJ?BV@2E5K88</&%8n');
define('AUTH_SALT',        'pb2DSW)?tcs ubs]q_-AhD#=(Z#H/- z=m|&OTSPi~[.n[bi/27tzy.&lC9Y?Z@k');
define('SECURE_AUTH_SALT', '5:4p#zZ4[>T5bvR*T,3WhzKSx5`Q-F[dNz0,Gk&lr Y]@k8^t3A5)/}=Uji[0;!,');
define('LOGGED_IN_SALT',   'OG,FdDp,X(()M_UV_l1u@~5h|$aty*7r+O-|jDG+|D/%Tpa:}3U:SO_zH:iO/At*');
define('NONCE_SALT',       'ZY`R.tm%~b>BomuTy3PwjSQAOGD-<1+eBHzt8SMvHCZcs]XRJ!|#,|~gHRS#jM*t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('FS_METHOD', 'direct');

define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );


/**
 * Disable revisions post
 */
define( 'WP_POST_REVISIONS', 0 );


/**
 * Increase PHP Memory
 */
define( 'WP_MEMORY_LIMIT', '128M' );




define('DUPLICATOR_AUTH_KEY', '9264ace853e2fe6b8fecfcb5e1f881da');
define( 'WP_PLUGIN_DIR', 'C:/xampp/htdocs/laniakea-ro-wiki/wp-content/plugins' );
define( 'WPMU_PLUGIN_DIR', 'C:/xampp/htdocs/laniakea-ro-wiki/wp-content/mu-plugins' );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__) . '/');



/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
